// set logger.

单播 unicast pid = 正常 pid 或 0
广播 anycast pid = 正常 pid 或 0
多播 mulcast pid = 0xFFFFFFFF

// 前置包头
struct mm_packet_head
{
	// msg base head.include into head data.
	mm_uint32_t   mid;// msg     id.
	mm_uint32_t   pid;// proxy   id.
	mm_uint64_t   sid;// section id.
	mm_uint64_t   uid;// unique  id.
};

单播 将直接使用
广播 将直接使用
多播 多层特定包
```
mulcast_nt have a complex struct.
// mulcast message struct
// 0xFFFFFFFF is a multicast special mid.we assign at mm_packet_head pid.
message mulcast_nt
{
	enum msg{ id=0xFFFFFFFF;}
	required bytes buffer              = 1               ; // message buffer.
	repeated uint64 uids               = 2               ; // multi uids.
}

unicast_nt just buffer.mm_packet_head can routing correct.
anycast_nt just buffer.mm_packet_head can routing correct.

// package buffer is little endian.
// multicast message struct serialization.
// |--mm_uint32_t--|-------char*------|-mm_uint32_t-|---mm_uint64_t[]---|
// |-buffer_length-|---buffer_array---|-uids_length-|-----uids_array----|
// |---------------buffer-------------|---------------uids--------------|
// buffer_length = buffer_array size not include buffer_length(uint32 size) buffer_array.size()
// uids_length   = uids_array   size not include uids_length  (uint32 size) uids_array.size()

// cback message struct.
// |--mm_packet_head--|--mulcast_nt--|
//     unicast pid != 0xFFFFFFFF
//     anycast pid != 0xFFFFFFFF
//     mulcast pid == 0xFFFFFFFF
```

房间回推
mm_packet_head ph0;
首先回推组件检测ph0.pid是否为0xFFFFFFFF
// 是0xFFFFFFFF
{
	将 mm_packet 包体当成 mulcast_nt 解包
	重新构建
	mm_packet_head ph1;
	
	ph1.mid = ph0.mid;
	ph1.pid = ph0.pid;
	ph1.sid = ph0.sid;
	ph1.uid = ph0.uid;
		
	// 在缓存中使用 sid 反查 l_sid
	hash<uint64,uint32> l_sids;
	
	// 构造网关实例对应 sets
	hash<uint32,set<uint64> > sets;
	// traverse l_sids
	(uid->l_sid at l_sids)
	{
		sets[l_sid].insert(uid);
	}

	hash<uint32,mulcast_nt> pkgs;
	// traverse sets
	(l_sid->pkg at sets)
	{
		// ph1 | pkg 发往 l_sid 对应的房间节点
	}
}
// 否0xFFFFFFFF
{
	// uid 有效值                  
	{
		// sid 有效值                  发给sid对应所在所有房间节点(房间分布在多个房间节点上)
		// sid 是   0                  尝试查询 uid 对应 sid 将该 sid 赋予新值,发往该 sid 对应房间节点
		// sid 是   0xffffffffffffffff 尝试查询 uid 对应 sid 将该 sid 赋予新值,发往该 sid 对应房间节点
	}
	// uid 是   0                  
	{
		// sid 有效值                  不处理
		// sid 是   0                  不处理
		// sid 是   0xffffffffffffffff 不处理
	}
	// uid 是   0xffffffffffffffff 
	{
		// sid 有效值                  发给sid对应所在所有房间节点(房间分布在多个房间节点上)
		// sid 是   0                  不处理
		// sid 是   0xffffffffffffffff 发给所有房间节点
	}

	或

	// sid 有效值                  
	{
		// uid 有效值                  发给uid对应所在所有房间节点(房间分布在多个房间节点上)
		// uid 是   0                  不处理
		// uid 是   0xffffffffffffffff 发给sid对应所在所有房间节点(房间分布在多个房间节点上)
	}
	// sid 是   0                  
	{
		// uid 有效值                  尝试查询 uid 对应 sid 将该 sid 赋予新值,发往该 sid 对应房间节点
		// uid 是   0                  不处理
		// uid 是   0xffffffffffffffff 不处理
	}
	// sid 是   0xffffffffffffffff 
	{
		// uid 有效值                  尝试查询 uid 对应 sid 将该 sid 赋予新值,发往该 sid 对应房间节点
		// uid 是   0                  不处理
		// uid 是   0xffffffffffffffff 发给所有房间节点
	}
}

房间
mm_packet_head ph0;
0 == ph0.mid
{
	当成老式的json来解bbuff类容并触发老逻辑
}
0 != ph0.mid
{
	首先回推组件检测ph0.pid是否为0xFFFFFFFF
	// 是0xFFFFFFFF
	{
		将 mm_packet 包体当成 mulcast_nt 解包
		重新构建
		mm_packet_head ph1;
		
		ph1.mid = ph0.mid;
		ph1.pid = ph0.pid;
		ph1.sid = ph0.sid;
		ph1.uid = ph0.uid;
		
		// traverse uids
		(uid at uids)
		{
			ph1.uid = uid;
			// 据 sid uid 发往对应客户节点
		}
	}
	// 否0xFFFFFFFF
	{
		// sid 有效值                  
		{
			// uid 有效值                  发给发给该房间内uid对应客户节点
			// uid 是   0                  不处理,没有房间号有值而用户号为0的情况
			// uid 是   0xffffffffffffffff 发给该房间内所有用户节点
		}
		// sid 是   0                  
		{
			// uid 有效值                  反查uid对应sid 发往该客户 若不存在该用户则不处理该消息 
			// uid 是   0                  不处理,不存在这种业务
			// uid 是   0xffffffffffffffff 不处理,不存在这种业务
		}
		// sid 是   0xffffffffffffffff 
		{
			// uid 有效值                  发给发给所有房间内存在uid对应客户节点
			// uid 是   0                  不处理,不存在这种业务
			// uid 是   0xffffffffffffffff 发给发给所有房间内所有用户节点
		}
	}
}

网关回推
mm_packet_head ph0;
首先回推组件检测ph0.pid是否为0xFFFFFFFF
// 是0xFFFFFFFF
{
	将 mm_packet 包体当成 mulcast_nt 解包
	重新构建
	mm_packet_head ph1;
	
	ph1.mid = ph0.mid;
	ph1.pid = ph0.pid;
	ph1.sid = ph0.sid;
	ph1.uid = ph0.uid;
		
	// 在缓存中使用 uid 反查 sid
	hash<uint64,uint64> sids;
	
	// 构造网关实例对应 sets
	hash<uint32,set<uint64> > sets;
	// traverse sids
	(uid->sid at sids)
	{
		l_sid = l_32(sid);
		sets[l_sid].insert(uid);
	}

	hash<uint32,mulcast_nt> pkgs;
	// traverse sets
	(l_sid->pkg at sets)
	{
		// ph1 | pkg 发往 l_sid 对应的网关节点
	}
}
// 否0xFFFFFFFF
{
	// sid 是   0                  
	{
		// uid 是   0                  非法值
		// uid 有效值                  使用uid反向映射得到确定sid
		// uid 是   0xffffffffffffffff 非法值
	}
	// sid 是   0                  非法值
	// sid 有效值       
	// 0xFFFFFFFF == sid左32位
	// 是
	{
		发给所有网关节点
	}
	// 否
	{
		发往 sid左32位 网关节点
	}
}

网关
mm_packet_head ph0;
0 == ph0.mid
{
	当成老式的json来解bbuff类容并触发老逻辑
}
0 != ph0.mid
{
	首先回推组件检测ph0.pid是否为0xFFFFFFFF
	// 是0xFFFFFFFF
	{
		将 mm_packet 包体当成 mulcast_nt 解包
		重新构建
		mm_packet_head ph1;
		
		ph1.mid = ph0.mid;
		ph1.pid = ph0.pid;
		ph1.sid = ph0.sid;
		ph1.uid = ph0.uid;
			
		// traverse uids
		(uid at uids)
		{
			ph1.uid = uid;
			// 据 sid 发往对应客户节点
		}
	}
	// 否0xFFFFFFFF
	{
		// sid 是   0               非法值
		// sid 有效值       
		// 0xFFFFFFFF == sid右32位
		// 是
		{
			发给所有客户节点
		}
		// 否
		{
			发往 sid右32位 客户节点
		}
	}
}
