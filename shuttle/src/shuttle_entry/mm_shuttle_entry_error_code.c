﻿#include "mm_shuttle_entry_error_code.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
void mm_error_desc_assign_shuttle_entry(struct mm_error_desc* p)
{
	mm_error_core_assign_code_desc(p,err_shuttle_entry_lobby_entry_failure,"lobby entry failure.");
}
//////////////////////////////////////////////////////////////////////////
