﻿#ifndef __mm_error_code_shuttle_entry_h__
#define __mm_error_code_shuttle_entry_h__

#include "core/mm_core.h"

#include "dish/mm_error_desc.h"

#include "core/mm_prefix.h"
//////////////////////////////////////////////////////////////////////////
enum mm_err_shuttle_entry
{
	err_shuttle_entry_lobby_entry_failure = 400000001,//(400000001)获取大厅入口数据失败
};
//////////////////////////////////////////////////////////////////////////
extern void mm_error_desc_assign_shuttle_entry(struct mm_error_desc* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_error_code_shuttle_entry_h__