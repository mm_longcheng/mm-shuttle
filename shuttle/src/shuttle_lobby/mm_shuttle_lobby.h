#ifndef __mm_shuttle_lobby_h__
#define __mm_shuttle_lobby_h__

#include "core/mm_core.h"
#include "core/mm_timer.h"
#include "core/mm_pool_element.h"

#include "net/mm_mailbox.h"

#include "dish/mm_error_desc.h"

#include "random/mm_xoshiro.h"

#include "jwt/mm_jwt_authority.h"

#include "openssl/mm_openssl_rsa.h"
#include "openssl/mm_openssl_rc4.h"

#include "shuttle_common/mm_sharder_holder.h"
#include "shuttle_common/mm_streambuf_array.h"
#include "shuttle_common/mm_message_publish.h"

#include "mm_shuttle_lobby_launch.h"
#include "mm_shuttle_lobby_cache.h"
#include "mm_shuttle_lobby_runtime.h"

#include "core/mm_prefix.h"

// update milliseconds.default is 60 * 60 * 2 = 7200 second(7200000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_KICKER_US 7200000
// update milliseconds.default is  5 second( 5000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_UPDATE_DT  5000
// launch milliseconds.default is 60 second(60000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_LAUNCH_DB 60000
// commit milliseconds.default is 10 second(10000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_COMMIT_DB 10000
// commit milliseconds.default is 60 second(60000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_COMMIT_ZK 60000
// update milliseconds.default is 60 * 60 second(3600000 ms).
#define MM_SHUTTLE_LOBBY_MSEC_UPDATE_SL 3600000

#define MM_SHUTTLE_LOBBY_JWT_SECRET_LENGTH 16
#define MM_SHUTTLE_LOBBY_RC4_SECRET_LENGTH 16

#define MM_SHUTTLE_LOBBY_RABBITMQ_SECRET_LENGTH 16

#define MM_SHUTTLE_LOBBY_RSA_SECRET_LENGTH 1024

// 60 * 60 = 3600 second(3600000 ms).
#define MM_SHUTTLE_LOBBY_KICKER_TIMEOUT 3600000

#define MM_SHUTTLE_LOBBY_ZK_IMPORT "/mm_proxy"
#define MM_SHUTTLE_LOBBY_ZK_EXPORT "/mm_lobby"

struct mm_shuttle_lobby
{
	struct mm_mailbox external_mailbox;
	struct mm_mailbox internal_mailbox;
	struct mm_timer timer_kicker;
	struct mm_timer timer;
	struct mm_shuttle_lobby_launch launch_info;
	struct mm_shuttle_lobby_runtime runtime_info;
	struct mm_sharder_holder sharder_holder;
	struct mm_shuttle_lobby_cache db_redis_cache_lobby;
	struct mm_mt_contact contact;
	struct mm_streambuf_array streambuf_array;
	struct mm_jwt_authority_array jwt_authority_array;
	struct mm_openssl_rsa_array openssl_rsa_array;
	struct mm_openssl_rc4_array openssl_rc4_array;
	struct mm_pool_element pool_element;
	struct mm_message_publish message_publish;
	struct mm_xoshiro256starstar rabbitmq_password_random;
	struct mm_error_desc error_desc;
	mm_msec_t msec_kicker_us;// launch milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_KICKER_US.
	mm_msec_t msec_update_dt;// launch milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_UPDATE_DT.
	mm_msec_t msec_launch_db;// launch milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_LAUNCH_DB.
	mm_msec_t msec_commit_db;// commit milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_COMMIT_DB.
	mm_msec_t msec_commit_zk;// commit milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_COMMIT_ZK.
	mm_msec_t msec_update_sl;// commit milliseconds.default is MM_SHUTTLE_LOBBY_MSEC_UPDATE_SL.
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_init(struct mm_shuttle_lobby* p);
extern void mm_shuttle_lobby_destroy(struct mm_shuttle_lobby* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_assign_unique_id(struct mm_shuttle_lobby* p,mm_uint32_t unique_id);
extern void mm_shuttle_lobby_assign_internal_mailbox_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_external_mailbox_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_zookeeper_import_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_zookeeper_export_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_rdb_gateway_cache_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_event_queue_rabbitmq_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_node_address_parameters(struct mm_shuttle_lobby* p,const char* parameters);
extern void mm_shuttle_lobby_assign_token_seed(struct mm_shuttle_lobby* p,mm_uint32_t token_seed);
extern void mm_shuttle_lobby_assign_rabbitmq_password_seed(struct mm_shuttle_lobby* p,mm_uint32_t rabbitmq_password_seed);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_start(struct mm_shuttle_lobby* p);
extern void mm_shuttle_lobby_interrupt(struct mm_shuttle_lobby* p);
extern void mm_shuttle_lobby_shutdown(struct mm_shuttle_lobby* p);
extern void mm_shuttle_lobby_join(struct mm_shuttle_lobby* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_lobby_h__