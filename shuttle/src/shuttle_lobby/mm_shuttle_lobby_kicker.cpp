﻿#include "mm_shuttle_lobby_kicker.h"

#include "core/mm_time.h"

#include "openssl/mm_openssl_tcp_context.h"

#include "mm_shuttle_lobby.h"

static void __static_shuttle_lobby_kicker_us_mailbox_traver(void* obj, void* u, mm_uint32_t k, void* v);

void mm_shuttle_lobby_external_mailbox_kicker_us(struct mm_mailbox* external_mailbox)
{
	mm_mailbox_traver(external_mailbox, &__static_shuttle_lobby_kicker_us_mailbox_traver, external_mailbox);
}

static void __static_shuttle_lobby_kicker_us_mailbox_traver(void* obj, void* u, mm_uint32_t k, void* v)
{
	struct mm_tcp* tcp = (struct mm_tcp*) v;
	struct mm_mailbox* external_mailbox = (struct mm_mailbox*)(u);

	mm_uint64_t msec_current = (mm_uint64_t)(mm_time_current_usec() / MM_MSEC_PER_SEC);

	mm_uint32_t sec_current = (mm_uint32_t)(msec_current / MM_MSEC_PER_SEC);

	// timecode for context create.
	mm_uint32_t timecode_create = 0;

	// default is CRYPTO_CONTEXT_INACTIVE.
	mm_uint8_t state = 0;

	struct mm_openssl_tcp_context* tcp_context = NULL;

	mm_tcp_s_lock(tcp);
	tcp_context = (struct mm_openssl_tcp_context*)mm_tcp_get_context(tcp);
	mm_tcp_s_unlock(tcp);

	// some time tcp_context can be NULL.
	if (NULL != tcp_context)
	{
		mm_openssl_tcp_context_lock(tcp_context);

		// copy data.
		timecode_create = tcp_context->timecode_create;
		state = tcp_context->state;

		mm_openssl_tcp_context_unlock(tcp_context);

		// not exchange key, and connect timeout.
		if (CRYPTO_CONTEXT_INACTIVE == state && (MM_SHUTTLE_LOBBY_KICKER_TIMEOUT / MM_MSEC_PER_SEC < sec_current - timecode_create))
		{
			// shutdown tcp from external_mailbox.
			// note:
			// if kicker not need send any notify message.
			mm_mailbox_shutdown_tcp(external_mailbox, tcp);
		}
	}
}