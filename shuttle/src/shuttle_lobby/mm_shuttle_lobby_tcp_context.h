#ifndef __mm_shuttle_lobby_tcp_context_h__
#define __mm_shuttle_lobby_tcp_context_h__

#include "core/mm_core.h"

#include "net/mm_tcp.h"

//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_external_mailbox_event_tcp_alloc(void* p, struct mm_tcp* tcp);
void mm_shuttle_lobby_external_mailbox_event_tcp_relax(void* p, struct mm_tcp* tcp);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_lobby_tcp_context_h__