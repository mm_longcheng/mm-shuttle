﻿#include "mm_shuttle_lobby_error_code.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
void mm_error_desc_assign_shuttle_lobby(struct mm_error_desc* p)
{
	mm_error_core_assign_code_desc(p, err_shuttle_lobby_token_verify_failure, "lobby token verify failure.");
	mm_error_core_assign_code_desc(p, err_shuttle_lobby_server_public_key_invalid, "lobby server public key invalid.");
	mm_error_core_assign_code_desc(p, err_shuttle_lobby_client_public_key_invalid, "lobby client public key invalid.");
}
//////////////////////////////////////////////////////////////////////////
