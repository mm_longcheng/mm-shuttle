#include "mm_shuttle_lobby_external_mailbox_tcp_hd.h"

#include "core/mm_logger.h"
#include "core/mm_byte.h"
#include "core/mm_value_transform.h"
#include "core/mm_time_cache.h"

#include "net/mm_packet.h"
#include "net/mm_streambuf_packet.h"
#include "net/mm_default_handle.h"
#include "redis/mm_redis_common.h"

#include "redis/mm_redis_hash.h"

#include "protobuf/mm_protobuff_cxx.h"
#include "protobuf/mm_protobuff_cxx_net.h"

#include "shuttle_common/mm_error_code_common.h"

#include "cxx/protodef/c_shuttle_lobby.pb.h"
#include "cxx/protodef/s_shuttle_lobby.pb.h"

#include "mm_shuttle_lobby.h"
#include "mm_shuttle_lobby_error_code.h"
#include "mm_shuttle_lobby_cback.h"

//////////////////////////////////////////////////////////////////////////
void hd_shuttle_lobby_c_shuttle_lobby_token_verify_rq(void* obj, void* u, struct mm_packet* rq_pack)
{
	c_shuttle_lobby::token_verify_rq rq_msg;
	c_shuttle_lobby::token_verify_rs rs_msg;
	struct mm_packet rs_pack;
	struct mm_string proto_desc;
	b_error::info* error_info = rs_msg.mutable_error();
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	struct mm_shuttle_lobby* impl = (struct mm_shuttle_lobby*)(u);
	struct mm_error_desc* error_desc = &impl->error_desc;
	////////////////////////////////
	struct mm_string token;
	struct mm_jwt_authority_data authority_data;
	////////////////////////////////
	error_info->set_code(0);
	error_info->set_desc("");
	rs_msg.set_uid(0);
	rs_msg.set_token("");
	////////////////////////////////
	mm_string_init(&proto_desc);
	////////////////////////////////
	mm_string_init(&token);
	mm_jwt_authority_data_init(&authority_data);
	////////////////////////////////
	do
	{
		if (0 != mm_protobuf_cxx_decode_message(rq_pack, &rq_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		// logger rq.
		mm_protobuf_cxx_logger_append_packet_message(&proto_desc, rq_pack, &rq_msg);
		mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		rs_msg.set_uid(rq_msg.uid());
		rs_msg.set_token(rq_msg.token());
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t uid = rq_msg.uid();
		//////////////////////////////////////////////////////////////////////////
		mm_string_assigns(&token, rq_msg.token().c_str());
		// jwt_authority_array is lock free.
		if (MM_TRUE != mm_jwt_authority_array_verify(&impl->jwt_authority_array, &authority_data, &token))
		{
			error_info->set_code(err_shuttle_lobby_token_verify_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		if (0 == authority_data.sub || authority_data.sub != uid)
		{
			error_info->set_code(err_shuttle_lobby_token_verify_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t sid_now = mm_byte_uint64_a(impl->launch_info.unique_id, (mm_uint32_t)(tcp->socket.socket));

		mm_uint64_t sid_old = 0;
		mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hget(&impl->db_redis_cache_lobby, uid, &sid_old);
		//
		mm_shuttle_lobby_cache_rbtree_add(&impl->db_redis_cache_lobby, uid, sid_now);
		//
		{
			struct mm_time_cache* g_time_cache = mm_time_cache_instance();

			struct mm_packet ev_pack;
			s_shuttle_lobby::token_verify_ev ev_msg;

			ev_msg.set_event_time(g_time_cache->msec_current);
			ev_msg.set_sid_now(sid_now);
			ev_msg.set_sid_old(sid_old);
			ev_msg.set_uid(uid);

			// message_publish_publish api is lock free.
			mm_message_publish_publish(&impl->message_publish, s_shuttle_lobby::token_verify_ev_msg_id, sid_now, uid, &ev_msg, &ev_pack);

			// logger ev.
			mm_string_clear(&proto_desc);
			mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &ev_pack, &ev_msg);
			mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		}
		//////////////////////////////////////////////////////////////////////////
		// logger.
		mm_logger_log_T(g_logger, "%s %d" " sid_now: %" PRIu64 " sid_old: %" PRIu64 " uid: %" PRIu64 " token: %s", __FUNCTION__, __LINE__,
			sid_now,
			sid_old,
			uid,
			rq_msg.token().c_str());
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	//////////////////////////////////////////////////////////////////////////
	mm_string_destroy(&token);
	mm_jwt_authority_data_destroy(&authority_data);
	//////////////////////////////////////////////////////////////////////////
	// rs
	mm_tcp_o_lock(tcp);
	mm_protobuf_cxx_n_tcp_append_rs(mailbox, tcp, c_shuttle_lobby::token_verify_rs_msg_id, &rs_msg, rq_pack, &rs_pack);
	mm_protobuf_cxx_n_tcp_flush_send(tcp);
	mm_tcp_o_unlock(tcp);
	// logger rs.
	mm_string_clear(&proto_desc);
	mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &rs_pack, &rs_msg);
	mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
	mm_string_destroy(&proto_desc);
}