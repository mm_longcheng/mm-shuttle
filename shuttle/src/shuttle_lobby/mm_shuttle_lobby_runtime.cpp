﻿#include "mm_shuttle_lobby_runtime.h"
#include "core/mm_logger_manager.h"
#include "core/mm_os_context.h"

#include "shuttle_common/mm_runtime_calculate.h"
#include "shuttle_common/mm_j_runtime_state.h"

#include "mm_shuttle_lobby_launch.h"

#include "openssl/mm_openssl_rand.h"

//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_runtime_init(struct mm_shuttle_lobby_runtime* p)
{
	mm_runtime_stat_init(&p->runtime);
	mm_zkwb_path_init(&p->zkwb_path);
	mm_openssl_rsa_init(&p->openssl_rsa);
	p->rsa_secret_length = 1024;
	p->launch_info = NULL;
	p->internal_mailbox = NULL;
	p->external_mailbox = NULL;
	// first update.
	mm_runtime_stat_update(&p->runtime);
	// srand openssl.
	mm_openssl_rand_srand((mm_uint32_t)time(NULL));
}
void mm_shuttle_lobby_runtime_destroy(struct mm_shuttle_lobby_runtime* p)
{
	mm_runtime_stat_destroy(&p->runtime);
	mm_zkwb_path_destroy(&p->zkwb_path);
	mm_openssl_rsa_destroy(&p->openssl_rsa);
	p->rsa_secret_length = 1024;
	p->launch_info = NULL;
	p->internal_mailbox = NULL;
	p->external_mailbox = NULL;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_runtime_start(struct mm_shuttle_lobby_runtime* p)
{
	mm_zkwb_path_start(&p->zkwb_path);
}
void mm_shuttle_lobby_runtime_interrupt(struct mm_shuttle_lobby_runtime* p)
{
	mm_zkwb_path_interrupt(&p->zkwb_path);
}
void mm_shuttle_lobby_runtime_shutdown(struct mm_shuttle_lobby_runtime* p)
{
	mm_zkwb_path_shutdown(&p->zkwb_path);
}
void mm_shuttle_lobby_runtime_join(struct mm_shuttle_lobby_runtime* p)
{
	mm_zkwb_path_join(&p->zkwb_path);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_runtime_assign_unique_id(struct mm_shuttle_lobby_runtime* p,mm_uint32_t unique_id)
{
	p->runtime_state.unique_id = unique_id;
	mm_zkwb_path_assign_unique_id(&p->zkwb_path,unique_id);
}
void mm_shuttle_lobby_runtime_assign_rsa_key_password(struct mm_shuttle_lobby_runtime* p, const char* key_password)
{
	mm_openssl_rsa_lock(&p->openssl_rsa);
	mm_openssl_rsa_assign_key_password(&p->openssl_rsa, key_password);
	mm_openssl_rsa_unlock(&p->openssl_rsa);
}
void mm_shuttle_lobby_runtime_assign_rsa_secret_length(struct mm_shuttle_lobby_runtime* p, mm_uint32_t length)
{
	p->rsa_secret_length = length;
}
void mm_shuttle_lobby_runtime_assign_launch_info(struct mm_shuttle_lobby_runtime* p,struct mm_shuttle_lobby_launch* launch_info)
{
	p->launch_info = launch_info;
}
void mm_shuttle_lobby_runtime_assign_internal_mailbox(struct mm_shuttle_lobby_runtime* p,struct mm_mailbox* internal_mailbox)
{
	p->internal_mailbox = internal_mailbox;
}
void mm_shuttle_lobby_runtime_assign_external_mailbox(struct mm_shuttle_lobby_runtime* p,struct mm_mailbox* external_mailbox)
{
	p->external_mailbox = external_mailbox;
}
void mm_shuttle_lobby_runtime_assign_zkwb_path(struct mm_shuttle_lobby_runtime* p,const char* path)
{
	mm_zkwb_path_assign_path(&p->zkwb_path,path);
}
void mm_shuttle_lobby_runtime_assign_zkwb_host(struct mm_shuttle_lobby_runtime* p,const char* host)
{
	mm_zkwb_path_assign_host(&p->zkwb_path,host);
}
// update runtime value.
void mm_shuttle_lobby_runtime_update_runtime(struct mm_shuttle_lobby_runtime* p)
{
	char node[MM_NODE_NAME_LENGTH] = {0};
	mm_ushort_t port = 0;
	if ( NULL != p->internal_mailbox )
	{
		mm_sockaddr_node_port( &p->internal_mailbox->accepter.socket.ss_native, node, &port );
		p->runtime_state.node_i = node;
		p->runtime_state.bind_i = node;
		p->runtime_state.port_i = port;
		p->runtime_state.workers_i = mm_mailbox_get_length(p->internal_mailbox);
	}
	else
	{
		p->runtime_state.node_i = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.bind_i = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.port_i = MM_ADDR_DEFAULT_PORT;
		p->runtime_state.workers_i = 0;
	}
	if ( NULL != p->external_mailbox )
	{
		mm_sockaddr_node_port( &p->external_mailbox->accepter.socket.ss_native, node, &port );
		p->runtime_state.node_e = p->launch_info->node_address_ipvalue.s;
		p->runtime_state.bind_e = node;
		p->runtime_state.port_e = port;
		p->runtime_state.workers_e = mm_mailbox_get_length(p->external_mailbox);
	}
	else
	{
		p->runtime_state.node_e = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.bind_e = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.port_e = MM_ADDR_DEFAULT_PORT;
		p->runtime_state.workers_e = 0;
	}
	{
		// update the machine information.
		mm_runtime_stat_update(&p->runtime);
		mm_runtime_state_update(&p->runtime_state, &p->runtime);
	}
	{
		size_t mem_size = 0;
		// openssl public key.
		mm_openssl_rsa_lock(&p->openssl_rsa);

		mem_size = mm_openssl_rsa_pub_mem_size(&p->openssl_rsa);
		// rsa public key is a string.
		p->runtime_state.key.resize(mem_size + 1);
		mm_memset((void*)p->runtime_state.key.data(), 0, mem_size + 1);
		mm_openssl_rsa_pub_mem_get(&p->openssl_rsa, (mm_uint8_t*)p->runtime_state.key.data(), 0, mem_size);

		mm_openssl_rsa_unlock(&p->openssl_rsa);
	}
	{
		// update zookeeper.
		rapidjson::StringBuffer string_buffer;
		rapidjson::Document _jc(rapidjson::kObjectType);
		rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
		mm::mm_j_runtime_state j_runtime_state(p->runtime_state);
		j_runtime_state.encode(_jc,_jc);
		string_buffer.Clear();
		_writer.Reset(string_buffer);
		_jc.Accept(_writer);
		mm_string_assigns(&p->zkwb_path.value_buffer,string_buffer.GetString());
	}
}
// commit db.
void mm_shuttle_lobby_runtime_commit_db(struct mm_shuttle_lobby_runtime* p)
{
	// common runtime.
}
// commit zk.
void mm_shuttle_lobby_runtime_commit_zk(struct mm_shuttle_lobby_runtime* p)
{
	mm_zkwb_path_commit(&p->zkwb_path);
}
void mm_shuttle_lobby_runtime_update_openssl(struct mm_shuttle_lobby_runtime* p)
{
	mm_openssl_rsa_lock(&p->openssl_rsa);

	// openssl new rsa key.
	mm_openssl_rsa_generate(&p->openssl_rsa, p->rsa_secret_length, RSA_3);
	// cache key.
	mm_openssl_rsa_ctx_to_pri_mem(&p->openssl_rsa);
	mm_openssl_rsa_ctx_to_pub_mem(&p->openssl_rsa);

	mm_openssl_rsa_unlock(&p->openssl_rsa);
}
//////////////////////////////////////////////////////////////////////////
