#ifndef __mm_shuttle_lobby_cback_h__
#define __mm_shuttle_lobby_cback_h__

#include "core/mm_core.h"

#include "net/mm_packet.h"

#include "protobuf/mm_protobuff_cxx.h"

#include "core/mm_prefix.h"

struct mm_mailbox;
struct mm_tcp;
//////////////////////////////////////////////////////////////////////////
extern void hd_shuttle_lobby_cback_sid_message(struct mm_mailbox* mailbox, mm_uint64_t sid, mm_uint32_t mid, ::google::protobuf::Message* message, struct mm_packet* nt_pack);
extern void hd_shuttle_lobby_cback_target(struct mm_mailbox* mailbox,struct mm_tcp* t_tcp,struct mm_packet* rs_pack,struct mm_packet* pr_pack);
extern void hd_shuttle_lobby_cback(struct mm_shuttle_lobby* impl,struct mm_tcp* s_tcp,struct mm_packet* nt_pack);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_lobby_cback_h__