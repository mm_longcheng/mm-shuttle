#include "mm_shuttle_lobby.h"
#include "core/mm_logger.h"
#include "core/mm_time_cache.h"
#include "core/mm_runtime_stat.h"
#include "core/mm_byte.h"
#include "net/mm_streambuf_packet.h"
#include "net/mm_default_handle.h"

#include "protobuf/mm_protobuff_cxx.h"

#include "shuttle_common/mm_error_code_common.h"
#include "shuttle_common/mm_error_code_mysql.h"
#include "shuttle_common/mm_control_tcp_hd.h"
#include "shuttle_common/mm_runtime_calculate.h"

#include "zookeeper/mm_zookeeper_zkwm_path.h"

#include "openssl/mm_openssl_tcp_context.h"

#include "cxx/protodef/s_control.pb.h"
#include "cxx/protodef/c_shuttle_lobby.pb.h"

#include "mm_shuttle_lobby_external_mailbox_tcp_hd.h"
#include "mm_shuttle_lobby_internal_mailbox_tcp_hd.h"
#include "mm_shuttle_lobby_contact_tcp_hd.h"
#include "mm_shuttle_lobby_error_code.h"
#include "mm_shuttle_lobby_tcp_context.h"
#include "mm_shuttle_lobby_kicker.h"
#include "mm_shuttle_lobby_message_publish.h"

//////////////////////////////////////////////////////////////////////////
static void __static_mm_shuttle_lobby_msec_update_dt_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
static void __static_mm_shuttle_lobby_msec_launch_db_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
static void __static_mm_shuttle_lobby_msec_commit_db_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
static void __static_mm_shuttle_lobby_msec_commit_zk_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
static void __static_mm_shuttle_lobby_msec_update_sl_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
//
static void __static_mm_shuttle_lobby_msec_kicker_us_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);

static void __static_shuttle_lobby_event_mt_tcp_alloc( void* p, struct mm_mt_tcp* mt_tcp );
static void __static_shuttle_lobby_event_mt_tcp_relax( void* p, struct mm_mt_tcp* mt_tcp );
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_init(struct mm_shuttle_lobby* p)
{
	struct mm_accepter_callback accepter_callback;
	struct mm_mailbox_callback external_mailbox_callback;
	struct mm_mailbox_callback internal_mailbox_callback;
	struct mm_mt_contact_event_mt_tcp_alloc contact_callback;
	struct mm_pool_element_callback pool_element_callback;
	struct mm_mailbox_event_tcp_alloc mailbox_event_tcp_alloc;
	struct mm_crypto_callback mailbox_crypto_callback;

	mm_mailbox_init(&p->external_mailbox);
	mm_mailbox_init(&p->internal_mailbox);
	mm_timer_init(&p->timer_kicker);
	mm_timer_init(&p->timer);
	mm_shuttle_lobby_launch_init(&p->launch_info);
	mm_shuttle_lobby_runtime_init(&p->runtime_info);
	mm_sharder_holder_init(&p->sharder_holder);
	mm_shuttle_lobby_cache_init(&p->db_redis_cache_lobby);
	mm_mt_contact_init(&p->contact);
	mm_streambuf_array_init(&p->streambuf_array);
	mm_jwt_authority_array_init(&p->jwt_authority_array);
	mm_openssl_rsa_array_init(&p->openssl_rsa_array);
	mm_openssl_rc4_array_init(&p->openssl_rc4_array);
	mm_pool_element_init(&p->pool_element);
	mm_message_publish_init(&p->message_publish);
	mm_xoshiro256starstar_init(&p->rabbitmq_password_random);
	mm_error_desc_init(&p->error_desc);

	// the max thread mm_streambuf thread instance is 1;
	mm_streambuf_array_set_length(&p->streambuf_array, 1);

	p->msec_kicker_us = MM_SHUTTLE_LOBBY_MSEC_KICKER_US;
	p->msec_update_dt = MM_SHUTTLE_LOBBY_MSEC_UPDATE_DT;
	p->msec_launch_db = MM_SHUTTLE_LOBBY_MSEC_LAUNCH_DB;
	p->msec_commit_db = MM_SHUTTLE_LOBBY_MSEC_COMMIT_DB;
	p->msec_commit_zk = MM_SHUTTLE_LOBBY_MSEC_COMMIT_ZK;
	p->msec_update_sl = MM_SHUTTLE_LOBBY_MSEC_UPDATE_SL;

	accepter_callback.accept = &hd_shuttle_lobby_external_mailbox_accept;
	accepter_callback.obj = &p->external_mailbox;
	mm_accepter_assign_callback(&p->external_mailbox.accepter,&accepter_callback);

	external_mailbox_callback.handle = &hd_shuttle_lobby_external_mailbox_handle;
	external_mailbox_callback.broken = &hd_shuttle_lobby_external_mailbox_broken;
	external_mailbox_callback.nready = &hd_shuttle_lobby_external_mailbox_nready;
	external_mailbox_callback.finish = &hd_shuttle_lobby_external_mailbox_finish;
	external_mailbox_callback.obj = p;
	mm_mailbox_assign_mailbox_callback(&p->external_mailbox,&external_mailbox_callback);

	mailbox_event_tcp_alloc.event_tcp_alloc = &mm_shuttle_lobby_external_mailbox_event_tcp_alloc;
	mailbox_event_tcp_alloc.event_tcp_relax = &mm_shuttle_lobby_external_mailbox_event_tcp_relax;
	mailbox_event_tcp_alloc.obj = p;
	mm_mailbox_assign_event_tcp_alloc(&p->external_mailbox, &mailbox_event_tcp_alloc);

	mailbox_crypto_callback.encrypt = &mm_openssl_tcp_context_mailbox_crypto_encrypt;
	mailbox_crypto_callback.decrypt = &mm_openssl_tcp_context_mailbox_crypto_decrypt;
	mailbox_crypto_callback.obj = p;
	mm_mailbox_assign_crypto_callback(&p->external_mailbox, &mailbox_crypto_callback);

	internal_mailbox_callback.handle = &hd_shuttle_lobby_internal_mailbox_handle;
	internal_mailbox_callback.broken = &hd_shuttle_lobby_internal_mailbox_broken;
	internal_mailbox_callback.nready = &hd_shuttle_lobby_internal_mailbox_nready;
	internal_mailbox_callback.finish = &hd_shuttle_lobby_internal_mailbox_finish;
	internal_mailbox_callback.obj = p;
	mm_mailbox_assign_mailbox_callback(&p->internal_mailbox,&internal_mailbox_callback);

	contact_callback.event_mt_tcp_alloc = &__static_shuttle_lobby_event_mt_tcp_alloc;
	contact_callback.event_mt_tcp_relax = &__static_shuttle_lobby_event_mt_tcp_relax;
	contact_callback.obj = p;
	mm_mt_contact_assign_event_mt_tcp_alloc(&p->contact,&contact_callback);

	mm_mailbox_assign_callback(&p->external_mailbox, c_shuttle_lobby::exchange_key_rq_msg_id, &hd_shuttle_lobby_c_shuttle_lobby_exchange_key_rq);
	mm_mailbox_assign_callback(&p->external_mailbox, c_shuttle_lobby::token_verify_rq_msg_id, &hd_shuttle_lobby_c_shuttle_lobby_token_verify_rq);
	mm_mailbox_assign_callback(&p->external_mailbox, c_shuttle_lobby::heartbeat_rq_msg_id,    &hd_shuttle_lobby_c_shuttle_lobby_heartbeat_rq);

	mm_mailbox_assign_callback(&p->internal_mailbox, s_control::set_logger_rq_msg_id, &hd_s_control_set_logger_rq);
	mm_mailbox_assign_callback(&p->internal_mailbox, s_control::get_logger_rq_msg_id, &hd_s_control_get_logger_rq);

	mm_mailbox_assign_context(&p->internal_mailbox,p);
	mm_mailbox_assign_context(&p->external_mailbox,p);
	mm_mt_contact_assign_context(&p->contact,p);

	mm_error_desc_assign_core(&p->error_desc);
	mm_error_desc_assign_common(&p->error_desc);
	mm_error_desc_assign_mysql(&p->error_desc);
	mm_error_desc_assign_shuttle_lobby(&p->error_desc);
	//
	mm_shuttle_lobby_runtime_assign_launch_info(&p->runtime_info,&p->launch_info);
	mm_shuttle_lobby_runtime_assign_internal_mailbox(&p->runtime_info,&p->internal_mailbox);
	mm_shuttle_lobby_runtime_assign_external_mailbox(&p->runtime_info,&p->external_mailbox);
	// timer schedule.
	// note:
	// we must Manual call some of it at start function, because call order can not make determine at timer.
	mm_timer_schedule(&p->timer_kicker, p->msec_kicker_us, p->msec_kicker_us, &__static_mm_shuttle_lobby_msec_kicker_us_handle, p);
	//
	mm_timer_schedule(&p->timer, p->msec_update_dt, p->msec_update_dt, &__static_mm_shuttle_lobby_msec_update_dt_handle, p);
	mm_timer_schedule(&p->timer, p->msec_launch_db, p->msec_launch_db, &__static_mm_shuttle_lobby_msec_launch_db_handle, p);
	mm_timer_schedule(&p->timer, p->msec_commit_db, p->msec_commit_db, &__static_mm_shuttle_lobby_msec_commit_db_handle, p);
	mm_timer_schedule(&p->timer, p->msec_commit_zk, p->msec_commit_zk, &__static_mm_shuttle_lobby_msec_commit_zk_handle, p);
	mm_timer_schedule(&p->timer, p->msec_update_sl, p->msec_update_sl, &__static_mm_shuttle_lobby_msec_update_sl_handle, p);
	//
	pool_element_callback.alloc = &mm_openssl_tcp_context_pool_element_event_alloc;
	pool_element_callback.relax = &mm_openssl_tcp_context_pool_element_event_relax;
	pool_element_callback.obj = p;
	mm_pool_element_assign_element_size(&p->pool_element, sizeof(struct mm_openssl_tcp_context));
	mm_pool_element_assign_callback(&p->pool_element, &pool_element_callback);
	// secret length must same for account.
	mm_jwt_authority_array_assign_secret_length(&p->jwt_authority_array, MM_SHUTTLE_LOBBY_JWT_SECRET_LENGTH);
	//
	// srand the rc4 random.
	mm_openssl_rc4_array_srand(&p->openssl_rc4_array, (mm_uint64_t)time(NULL));
	//
	mm_protobuf_cxx_init();
}
void mm_shuttle_lobby_destroy(struct mm_shuttle_lobby* p)
{
	google::protobuf::ShutdownProtobufLibrary();
	mm_protobuf_cxx_destroy();
	//
	mm_mailbox_destroy(&p->external_mailbox);
	mm_mailbox_destroy(&p->internal_mailbox);
	mm_timer_destroy(&p->timer_kicker);
	mm_timer_destroy(&p->timer);
	mm_shuttle_lobby_launch_destroy(&p->launch_info);
	mm_shuttle_lobby_runtime_destroy(&p->runtime_info);
	mm_sharder_holder_destroy(&p->sharder_holder);
	mm_shuttle_lobby_cache_destroy(&p->db_redis_cache_lobby);
	mm_mt_contact_destroy(&p->contact);
	mm_streambuf_array_destroy(&p->streambuf_array);
	mm_jwt_authority_array_destroy(&p->jwt_authority_array);
	mm_openssl_rsa_array_destroy(&p->openssl_rsa_array);
	mm_openssl_rc4_array_destroy(&p->openssl_rc4_array);
	mm_pool_element_destroy(&p->pool_element);
	mm_message_publish_destroy(&p->message_publish);
	mm_xoshiro256starstar_destroy(&p->rabbitmq_password_random);
	mm_error_desc_destroy(&p->error_desc);

	p->msec_kicker_us = MM_SHUTTLE_LOBBY_MSEC_KICKER_US;
	p->msec_update_dt = MM_SHUTTLE_LOBBY_MSEC_UPDATE_DT;
	p->msec_launch_db = MM_SHUTTLE_LOBBY_MSEC_LAUNCH_DB;
	p->msec_commit_db = MM_SHUTTLE_LOBBY_MSEC_COMMIT_DB;
	p->msec_commit_zk = MM_SHUTTLE_LOBBY_MSEC_COMMIT_ZK;
	p->msec_update_sl = MM_SHUTTLE_LOBBY_MSEC_UPDATE_SL;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_assign_unique_id(struct mm_shuttle_lobby* p,mm_uint32_t unique_id)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	launch_info->unique_id = unique_id;
}
void mm_shuttle_lobby_assign_internal_mailbox_parameters(struct mm_shuttle_lobby* p,const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->internal_mailbox_parameters, parameters);
}
void mm_shuttle_lobby_assign_external_mailbox_parameters(struct mm_shuttle_lobby* p,const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->external_mailbox_parameters, parameters);
}
void mm_shuttle_lobby_assign_zookeeper_import_parameters(struct mm_shuttle_lobby* p,const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->zookeeper_import_parameters, parameters);
}
void mm_shuttle_lobby_assign_zookeeper_export_parameters(struct mm_shuttle_lobby* p,const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->zookeeper_export_parameters, parameters);
}
void mm_shuttle_lobby_assign_rdb_gateway_cache_parameters(struct mm_shuttle_lobby* p,const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->rdb_gateway_cache_parameters, parameters);
}
void mm_shuttle_lobby_assign_event_queue_rabbitmq_parameters(struct mm_shuttle_lobby* p, const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->event_queue_rabbitmq_parameters, parameters);
}
void mm_shuttle_lobby_assign_node_address_parameters(struct mm_shuttle_lobby* p, const char* parameters)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	mm_string_assigns(&launch_info->node_address_parameters, parameters);
}
void mm_shuttle_lobby_assign_token_seed(struct mm_shuttle_lobby* p, mm_uint32_t token_seed)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	launch_info->token_seed = token_seed;
}
void mm_shuttle_lobby_assign_rabbitmq_password_seed(struct mm_shuttle_lobby* p, mm_uint32_t rabbitmq_password_seed)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	launch_info->rabbitmq_password_seed = rabbitmq_password_seed;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_start(struct mm_shuttle_lobby* p)
{
	struct mm_shuttle_lobby_launch* launch_info = &p->launch_info;
	// pull launcher config.
	mm_shuttle_lobby_launch_pretreatment_config(&p->launch_info);
	mm_shuttle_lobby_launch_printf_information(&p->launch_info);
	//////////////////////////////////////////////////////////////////////////
	mm_shuttle_lobby_runtime_assign_unique_id(&p->runtime_info,launch_info->unique_id);
	mm_shuttle_lobby_cache_assign_rdb_gateway_cache_unique_id(&p->db_redis_cache_lobby,launch_info->unique_id);
	mm_mailbox_assign_parameters(&p->internal_mailbox, launch_info->internal_mailbox_parameters.s);
	mm_mailbox_assign_parameters(&p->external_mailbox, launch_info->external_mailbox_parameters.s);
	mm_shuttle_lobby_runtime_assign_zkwb_host(&p->runtime_info,launch_info->zookeeper_export_parameters.s);
	mm_sharder_holder_assign_zkrm_host(&p->sharder_holder,launch_info->zookeeper_import_parameters.s);
	mm_shuttle_lobby_cache_assign_rdb_gateway_cache_parameters(&p->db_redis_cache_lobby, launch_info->rdb_gateway_cache_parameters.s);
	//////////////////////////////////////////////////////////////////////////
	mm_shuttle_lobby_message_publish_config(p);
	//////////////////////////////////////////////////////////////////////////
	// assign callback
	mm_mailbox_assign_callback(&p->internal_mailbox,s_control::set_logger_rq_msg_id   ,&hd_s_control_set_logger_rq);
	mm_mailbox_assign_callback(&p->internal_mailbox,s_control::get_logger_rq_msg_id   ,&hd_s_control_get_logger_rq);
	//////////////////////////////////////////////////////////////////////////
	mm_shuttle_lobby_runtime_assign_zkwb_path(&p->runtime_info,MM_SHUTTLE_LOBBY_ZK_EXPORT);

	mm_sharder_holder_assign_zkrm_path(&p->sharder_holder,MM_SHUTTLE_LOBBY_ZK_IMPORT);
	//////////////////////////////////////////////////////////////////////////
	mm_jwt_authority_array_assign_token_seed(&p->jwt_authority_array, launch_info->token_seed);
	mm_jwt_authority_array_generate_secret(&p->jwt_authority_array);
	//////////////////////////////////////////////////////////////////////////
	// start mailbox.
	mm_mailbox_fopen_socket(&p->external_mailbox);
	mm_mailbox_bind(&p->external_mailbox);
	mm_mailbox_listen(&p->external_mailbox);
	mm_mailbox_start(&p->external_mailbox);

	mm_mailbox_fopen_socket(&p->internal_mailbox);
	mm_mailbox_bind(&p->internal_mailbox);
	mm_mailbox_listen(&p->internal_mailbox);
	mm_mailbox_start(&p->internal_mailbox);

	mm_timer_start(&p->timer_kicker);
	mm_timer_start(&p->timer);

	mm_mt_contact_start(&p->contact);

	mm_shuttle_lobby_cache_start(&p->db_redis_cache_lobby);
	//////////////////////////////////////////////////////////////////////////
	mm_shuttle_lobby_runtime_start(&p->runtime_info);
	mm_sharder_holder_start(&p->sharder_holder);
	//////////////////////////////////////////////////////////////////////////
	// timer schedule.
	// note:
	// we must Manual call some of it at start function, because call order can not make determine at timer.
	mm_shuttle_lobby_runtime_update_openssl(&p->runtime_info);
	//
	mm_shuttle_lobby_runtime_update_runtime(&p->runtime_info);
	mm_sharder_holder_update_watcher(&p->sharder_holder);
	mm_sharder_holder_update(&p->sharder_holder);
	//
	mm_shuttle_lobby_runtime_commit_db(&p->runtime_info);
	//
	mm_shuttle_lobby_runtime_commit_zk(&p->runtime_info);
	//
	mm_shuttle_lobby_external_mailbox_kicker_us(&p->external_mailbox);
	//////////////////////////////////////////////////////////////////////////
	hd_shuttle_lobby_external_mailbox_motion(p);
}
void mm_shuttle_lobby_interrupt(struct mm_shuttle_lobby* p)
{
	mm_mailbox_interrupt(&p->external_mailbox);
	mm_mailbox_interrupt(&p->internal_mailbox);
	mm_timer_interrupt(&p->timer_kicker);
	mm_timer_interrupt(&p->timer);
	mm_mt_contact_interrupt(&p->contact);
	mm_shuttle_lobby_cache_interrupt(&p->db_redis_cache_lobby);
	mm_shuttle_lobby_runtime_interrupt(&p->runtime_info);
	mm_sharder_holder_interrupt(&p->sharder_holder);
}
void mm_shuttle_lobby_shutdown(struct mm_shuttle_lobby* p)
{
	mm_mailbox_shutdown(&p->external_mailbox);
	mm_mailbox_shutdown(&p->internal_mailbox);
	mm_timer_shutdown(&p->timer_kicker);
	mm_timer_shutdown(&p->timer);
	mm_mt_contact_shutdown(&p->contact);
	mm_shuttle_lobby_cache_shutdown(&p->db_redis_cache_lobby);
	mm_shuttle_lobby_runtime_shutdown(&p->runtime_info);
	mm_sharder_holder_shutdown(&p->sharder_holder);
}
void mm_shuttle_lobby_join(struct mm_shuttle_lobby* p)
{
	mm_mailbox_join(&p->external_mailbox);
	mm_mailbox_join(&p->internal_mailbox);
	mm_timer_join(&p->timer_kicker);
	mm_timer_join(&p->timer);
	mm_mt_contact_join(&p->contact);
	mm_shuttle_lobby_cache_join(&p->db_redis_cache_lobby);
	mm_shuttle_lobby_runtime_join(&p->runtime_info);
	mm_sharder_holder_join(&p->sharder_holder);
	//////////////////////////////////////////////////////////////////////////
	hd_shuttle_lobby_external_mailbox_closed(p);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mm_shuttle_lobby_msec_update_dt_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(entry->callback.obj);
	mm_shuttle_lobby_runtime_update_runtime(&shuttle_lobby->runtime_info);
	mm_sharder_holder_update_watcher(&shuttle_lobby->sharder_holder);
	mm_sharder_holder_update(&shuttle_lobby->sharder_holder);
	mm_logger_log_T(g_logger,"%s %d",__FUNCTION__,__LINE__);
}
static void __static_mm_shuttle_lobby_msec_launch_db_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	mm_logger_log_T(g_logger,"%s %d",__FUNCTION__,__LINE__);
}
static void __static_mm_shuttle_lobby_msec_commit_db_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(entry->callback.obj);
	mm_shuttle_lobby_runtime_commit_db(&shuttle_lobby->runtime_info);
	mm_logger_log_T(g_logger,"%s %d",__FUNCTION__,__LINE__);
}
static void __static_mm_shuttle_lobby_msec_commit_zk_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(entry->callback.obj);
	mm_shuttle_lobby_runtime_commit_zk(&shuttle_lobby->runtime_info);
	mm_logger_log_T(g_logger,"%s %d",__FUNCTION__,__LINE__);
}
static void __static_mm_shuttle_lobby_msec_update_sl_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(entry->callback.obj);
	mm_shuttle_lobby_runtime_update_openssl(&shuttle_lobby->runtime_info);
	mm_logger_log_T(g_logger,"%s %d",__FUNCTION__,__LINE__);
}
static void __static_mm_shuttle_lobby_msec_kicker_us_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(entry->callback.obj);
	mm_shuttle_lobby_external_mailbox_kicker_us(&shuttle_lobby->external_mailbox);
	mm_logger_log_T(g_logger, "%s %d", __FUNCTION__, __LINE__);
}
//////////////////////////////////////////////////////////////////////////
static void __static_shuttle_lobby_event_mt_tcp_alloc( void* p, struct mm_mt_tcp* mt_tcp )
{
	struct mm_mt_tcp_callback mt_tcp_callback;
	mt_tcp_callback.handle = &hd_shuttle_lobby_contact_handle;
	mt_tcp_callback.broken = &hd_shuttle_lobby_contact_broken;
	mt_tcp_callback.nready = &hd_shuttle_lobby_contact_nready;
	mt_tcp_callback.finish = &hd_shuttle_lobby_contact_finish;
	mt_tcp_callback.obj = p;
	mm_mt_tcp_assign_default_callback(mt_tcp,&mt_tcp_callback);
}
static void __static_shuttle_lobby_event_mt_tcp_relax( void* p, struct mm_mt_tcp* mt_tcp )
{
	// nothing todo.
}
//////////////////////////////////////////////////////////////////////////