﻿#ifndef __mm_shuttle_lobby_error_code_h__
#define __mm_shuttle_lobby_error_code_h__

#include "core/mm_core.h"

#include "dish/mm_error_desc.h"

#include "core/mm_prefix.h"
//////////////////////////////////////////////////////////////////////////
enum mm_err_shuttle_lobby
{
	err_shuttle_lobby_token_verify_failure      = 400001001,//(400001001)大厅令牌验证失败
	err_shuttle_lobby_server_public_key_invalid = 400001002,//(400001002)大厅服务端秘钥公钥无效
	err_shuttle_lobby_client_public_key_invalid = 400001003,//(400001003)大厅客户端秘钥公钥无效
};
//////////////////////////////////////////////////////////////////////////
extern void mm_error_desc_assign_shuttle_lobby(struct mm_error_desc* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_lobby_error_code_h__