﻿#ifndef __mm_shuttle_lobby_launch_h__
#define __mm_shuttle_lobby_launch_h__

#include "core/mm_core.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
// 服务实例编名称    mm_shuttle_lobby_0
// 程序名            mm_shuttle_lobby
// 日志文件夹路径    ../log
// 日志等级          7
// 实例编号          1
// 内地址启动参数    127.0.0.1-10000[2]
// 外地址启动参数    127.0.0.1-20000[2]
// 读取监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
// 写入监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
// 网关缓存地址号    101.200.169.28-10200
// 事件队列地址号    101.200.169.28-5672
// 节点地址          net.e(101.200.169.28)
// JWT令牌种子       123456
// 事件队列密码种子  456789
struct mm_shuttle_lobby_launch
{
	mm_uint32_t unique_id;
	struct mm_string internal_mailbox_parameters;
	struct mm_string external_mailbox_parameters;
	struct mm_string zookeeper_import_parameters;
	struct mm_string zookeeper_export_parameters;
	struct mm_string rdb_gateway_cache_parameters;
	struct mm_string event_queue_rabbitmq_parameters;
	struct mm_string node_address_parameters;
	mm_uint32_t token_seed;
	mm_uint32_t rabbitmq_password_seed;

	struct mm_string node_address_ipvalue;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_launch_init(struct mm_shuttle_lobby_launch* p);
extern void mm_shuttle_lobby_launch_destroy(struct mm_shuttle_lobby_launch* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_launch_pretreatment_config(struct mm_shuttle_lobby_launch* p);
extern void mm_shuttle_lobby_launch_printf_information(struct mm_shuttle_lobby_launch* p);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_lobby_launch_h__