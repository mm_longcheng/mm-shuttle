#include "mm_shuttle_lobby_external_mailbox_tcp_hd.h"

#include "core/mm_logger.h"
#include "core/mm_byte.h"

#include "net/mm_packet.h"
#include "net/mm_streambuf_packet.h"
#include "net/mm_default_handle.h"
#include "redis/mm_redis_common.h"

#include "redis/mm_redis_hash.h"

#include "protobuf/mm_protobuff_cxx.h"
#include "protobuf/mm_protobuff_cxx_net.h"

#include "shuttle_common/mm_error_code_common.h"

#include "openssl/mm_openssl_tcp_context.h"
#include "openssl/mm_openssl_rsa.h"

#include "cxx/protodef/c_shuttle_lobby.pb.h"

#include "mm_shuttle_lobby.h"
#include "mm_shuttle_lobby_error_code.h"
#include "mm_shuttle_lobby_tcp_context.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void hd_shuttle_lobby_c_shuttle_lobby_exchange_key_rq(void* obj, void* u, struct mm_packet* rq_pack)
{
	c_shuttle_lobby::exchange_key_rq rq_msg;
	c_shuttle_lobby::exchange_key_rs rs_msg;
	struct mm_packet rs_pack;
	struct mm_string proto_desc;
	b_error::info* error_info = rs_msg.mutable_error();
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	struct mm_shuttle_lobby* impl = (struct mm_shuttle_lobby*)(u);
	struct mm_error_desc* error_desc = &impl->error_desc;
	////////////////////////////////
	struct mm_string rc4_key_l_encrypt;
	struct mm_string rc4_key_r_encrypt;
	char link_name[MM_LINK_NAME_LENGTH] = { 0 };
	////////////////////////////////
	error_info->set_code(0);
	error_info->set_desc("");
	rs_msg.set_encrypt_key_r("");
	////////////////////////////////
	mm_string_init(&proto_desc);

	mm_string_init(&rc4_key_l_encrypt);
	mm_string_init(&rc4_key_r_encrypt);

	struct mm_openssl_rc4* openssl_rc4 = mm_openssl_rc4_array_thread_instance(&impl->openssl_rc4_array);

	do
	{
		if (0 != mm_protobuf_cxx_decode_message(rq_pack, &rq_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		// logger rq.
		mm_protobuf_cxx_logger_append_packet_message(&proto_desc, rq_pack, &rq_msg);
		mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		{
			// rc4 key rc4_key_r generate.
			mm_openssl_rc4_lock(openssl_rc4);
			mm_openssl_rc4_random_buffer(openssl_rc4, &openssl_rc4->key_r, MM_SHUTTLE_LOBBY_RC4_SECRET_LENGTH);
			mm_openssl_rc4_unlock(openssl_rc4);
		}
		{
			// decode rc4_key_l.
			struct mm_shuttle_lobby* impl = (struct mm_shuttle_lobby*)(u);
			struct mm_shuttle_lobby_runtime* runtime_info = &impl->runtime_info;
			struct mm_openssl_rsa* openssl_rsa = &runtime_info->openssl_rsa;

			const std::string& encrypt_key_l = rq_msg.encrypt_key_l();

			int pri_decrypt_len = 0;
			// copy rc4_key_l.
			mm_string_resize(&rc4_key_l_encrypt, encrypt_key_l.size());
			mm_memcpy(rc4_key_l_encrypt.s, (const char*)encrypt_key_l.data(), encrypt_key_l.size());

			mm_openssl_rsa_lock(openssl_rsa);

			// private key decrypt rc4_key_l.
			mm_openssl_rc4_lock(openssl_rc4);
			pri_decrypt_len = mm_openssl_rsa_pri_decrypt(openssl_rsa, &rc4_key_l_encrypt, &openssl_rc4->key_l);
			mm_openssl_rc4_unlock(openssl_rc4);

			mm_openssl_rsa_unlock(openssl_rsa);

			// can not decrypt, server public key invalid.
			if (0 > pri_decrypt_len)
			{
				error_info->set_code(err_shuttle_lobby_server_public_key_invalid);
				error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
				break;
			}
		}
		{
			// rc4 key assembly.
			mm_openssl_rc4_lock(openssl_rc4);
			mm_openssl_rc4_assembly(openssl_rc4);
			mm_openssl_rc4_unlock(openssl_rc4);
		}
		{
			// encrypt rc4_key_r.
			const std::string& public_key = rq_msg.public_key();

			int pub_encrypt_len = 0;

			struct mm_openssl_rsa* openssl_rsa = mm_openssl_rsa_array_thread_instance(&impl->openssl_rsa_array);

			mm_openssl_rsa_lock(openssl_rsa);

			// set public key.
			mm_openssl_rsa_pub_mem_set(openssl_rsa, (mm_uint8_t*)public_key.data(), 0, (size_t)public_key.size());
			// public key to ctx.
			mm_openssl_rsa_pub_mem_to_ctx(openssl_rsa);
			// public encrypt rc4_key_r.
			mm_openssl_rc4_lock(openssl_rc4);
			pub_encrypt_len = mm_openssl_rsa_pub_encrypt(openssl_rsa, &openssl_rc4->key_r, &rc4_key_r_encrypt);
			mm_openssl_rc4_unlock(openssl_rc4);

			mm_openssl_rsa_unlock(openssl_rsa);

			// can not encrypt, client public key invalid.
			if (0 > pub_encrypt_len)
			{
				error_info->set_code(err_shuttle_lobby_client_public_key_invalid);
				error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
				break;
			}
		}
		{
			//////////////////////////////////////////////////////////////////////////
			// set rc4 encrypt_key_r to client.
			std::string* mutable_encrypt_key_r = rs_msg.mutable_encrypt_key_r();
			mutable_encrypt_key_r->resize(rc4_key_r_encrypt.l);
			mm_memcpy((void*)mutable_encrypt_key_r->data(), rc4_key_r_encrypt.s, rc4_key_r_encrypt.l);
		}
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t sid = mm_byte_uint64_a(impl->launch_info.unique_id, (mm_uint32_t)(tcp->socket.socket));
		mm_socket_string(&tcp->socket, link_name);
		//////////////////////////////////////////////////////////////////////////
		// logger.
		mm_logger_log_T(g_logger, "%s %d sid: %" PRIu64 " %s", __FUNCTION__, __LINE__,
			sid,
			link_name);
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	// rs
	mm_tcp_o_lock(tcp);
	mm_protobuf_cxx_n_tcp_append_rs(mailbox, tcp, c_shuttle_lobby::exchange_key_rs_msg_id, &rs_msg, rq_pack, &rs_pack);
	mm_protobuf_cxx_n_tcp_flush_send(tcp);
	mm_tcp_o_unlock(tcp);
	{
		//////////////////////////////////////////////////////////////////////////
		// set rc4 key to client ==> servet tcp context.
		struct mm_openssl_tcp_context* tcp_context = NULL;

		mm_tcp_s_lock(tcp);
		tcp_context = (struct mm_openssl_tcp_context*)mm_tcp_get_context(tcp);
		mm_tcp_s_unlock(tcp);

		mm_openssl_tcp_context_lock(tcp_context);

		// assign crypto.
		mm_openssl_rc4_lock(openssl_rc4);
		mm_openssl_tcp_context_assign_key(tcp_context, (const mm_uint8_t*)openssl_rc4->key.s, 0, (mm_uint32_t)openssl_rc4->key.l);
		mm_openssl_rc4_unlock(openssl_rc4);
		// activate tcp rc4 context.
		mm_openssl_tcp_context_assign_state(tcp_context, CRYPTO_CONTEXT_ACTIVATE);

		mm_openssl_tcp_context_unlock(tcp_context);
	}
	// logger rs.
	mm_string_clear(&proto_desc);
	mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &rs_pack, &rs_msg);
	mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
	mm_string_destroy(&proto_desc);
	//
	mm_string_destroy(&rc4_key_l_encrypt);
	mm_string_destroy(&rc4_key_r_encrypt);
}
