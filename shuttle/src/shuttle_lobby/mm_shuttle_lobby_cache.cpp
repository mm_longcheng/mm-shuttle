﻿#include "mm_shuttle_lobby_cache.h"

#include "core/mm_logger_manager.h"

#include "redis/mm_redis_common.h"
#include "redis/mm_redis_hash.h"

#include "shuttle_common/mm_rdb_gateway.h"

void mm_shuttle_lobby_cache_init(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_init(&p->rdb_gateway_cache);
	mm_rbtree_u64_u64_init(&p->rbtree_uid2sid);
	mm_rbtree_u64_u64_init(&p->rbtree_sid2uid);
	mm_spinlock_init(&p->rbtree_uid2sid_locker, NULL);
	mm_spinlock_init(&p->rbtree_sid2uid_locker, NULL);
	p->unique_id = 0;
	mm_memset(p->lobby_key,0,sizeof(char) * MM_LOBBY_CACHE_KEY_LENGTH);
}
void mm_shuttle_lobby_cache_destroy(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_destroy(&p->rdb_gateway_cache);
	mm_rbtree_u64_u64_destroy(&p->rbtree_uid2sid);
	mm_rbtree_u64_u64_destroy(&p->rbtree_sid2uid);
	mm_spinlock_destroy(&p->rbtree_uid2sid_locker);
	mm_spinlock_destroy(&p->rbtree_sid2uid_locker);
	p->unique_id = 0;
	mm_memset(p->lobby_key,0,sizeof(char) * MM_LOBBY_CACHE_KEY_LENGTH);
}
void mm_shuttle_lobby_cache_assign_rdb_gateway_cache_unique_id(struct mm_shuttle_lobby_cache* p,mm_uint32_t unique_id)
{
	p->unique_id = unique_id;
	mm_rdb_gateway_sid2add_key(p->lobby_key,p->unique_id);
}

void mm_shuttle_lobby_cache_assign_rdb_gateway_cache_parameters(struct mm_shuttle_lobby_cache* p,const char* parameters)
{
	mm_redis_sync_array_assign_parameters(&p->rdb_gateway_cache,parameters);
	mm_redis_sync_array_apply_broken(&p->rdb_gateway_cache);
}
void mm_shuttle_lobby_cache_assign_rdb_gateway_cache_auth(struct mm_shuttle_lobby_cache* p,const char* auth)
{
	mm_redis_sync_array_assign_auth(&p->rdb_gateway_cache,auth);
}

void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2add_hset(struct mm_shuttle_lobby_cache* p,mm_uint64_t sid,char addr_name[MM_ADDR_NAME_LENGTH])
{
	do 
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if ( NULL == redis )
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->rdb_gateway_cache.node.s,p->rdb_gateway_cache.port);
		}
		if ( 0 == sid )
		{
			// need do nothing.
			break;
		}
		{
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_bin_hset(redis,p->lobby_key,sid,addr_name,MM_ADDR_NAME_LENGTH);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2add_hdel(struct mm_shuttle_lobby_cache* p,mm_uint64_t sid)
{
	do 
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if ( NULL == redis )
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->rdb_gateway_cache.node.s,p->rdb_gateway_cache.port);
		}
		if ( 0 == sid )
		{
			// need do nothing.
			break;
		}
		{
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_hdel(redis,p->lobby_key,sid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2add_del(struct mm_shuttle_lobby_cache* p)
{
	do 
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if ( NULL == redis )
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->rdb_gateway_cache.node.s,p->rdb_gateway_cache.port);
		}
		{
			mm_redis_sync_lock(redis);
			mm_redis_str_del(redis,p->lobby_key);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
void mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hdel(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid)
{
	do
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if (NULL == redis)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->rdb_gateway_cache.node.s, p->rdb_gateway_cache.port);
		}
		if (0 == uid)
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = { 0 };
			mm_rdb_gateway_uid2sid_key_by_uid(key, uid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_hdel(redis, key, uid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2uid_hdel(struct mm_shuttle_lobby_cache* p, mm_uint64_t sid)
{
	do
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if (NULL == redis)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->rdb_gateway_cache.node.s, p->rdb_gateway_cache.port);
		}
		if (0 == sid)
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = { 0 };
			mm_rdb_gateway_sid2uid_key_by_sid(key, sid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_hdel(redis, key, sid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hset(struct mm_shuttle_lobby_cache* p,mm_uint64_t uid,mm_uint64_t sid)
{
	do 
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if ( NULL == redis )
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->rdb_gateway_cache.node.s,p->rdb_gateway_cache.port);
		}
		if ( 0 == uid || 0 == sid )
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = {0};
			mm_rdb_gateway_uid2sid_key_by_uid(key, uid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_u64_hset(redis,key,uid,sid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
void mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hget(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid, mm_uint64_t* sid)
{
	do
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if (NULL == redis)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->rdb_gateway_cache.node.s, p->rdb_gateway_cache.port);
		}
		if (0 == uid)
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = { 0 };
			mm_rdb_gateway_uid2sid_key_by_uid(key, uid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_u64_hget(redis, key, uid, sid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2uid_hset(struct mm_shuttle_lobby_cache* p,mm_uint64_t sid,mm_uint64_t uid)
{
	do 
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if ( NULL == redis )
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->rdb_gateway_cache.node.s,p->rdb_gateway_cache.port);
		}
		if ( 0 == uid || 0 == sid )
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = {0};
			mm_rdb_gateway_sid2uid_key_by_sid(key, sid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_u64_hset(redis,key,sid,uid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
extern void mm_shuttle_lobby_cache_rdb_gateway_cache_sid2uid_hget(struct mm_shuttle_lobby_cache* p, mm_uint64_t sid, mm_uint64_t* uid)
{
	do
	{
		struct mm_redis_sync* redis = mm_redis_sync_array_thread_instance(&p->rdb_gateway_cache);
		if (NULL == redis)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->rdb_gateway_cache.node.s, p->rdb_gateway_cache.port);
		}
		if (0 == sid)
		{
			// need do nothing.
			break;
		}
		{
			char key[32] = { 0 };
			mm_rdb_gateway_sid2uid_key_by_sid(key, sid);
			mm_redis_sync_lock(redis);
			mm_redis_hash_u64_u64_hget(redis, key, sid, uid);
			mm_redis_sync_unlock(redis);
		}
	} while (0);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_cache_rbtree_add(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid, mm_uint64_t sid)
{
	mm_spinlock_lock(&p->rbtree_uid2sid_locker);
	mm_rbtree_u64_u64_set(&p->rbtree_uid2sid, uid, sid);
	mm_spinlock_unlock(&p->rbtree_uid2sid_locker);

	mm_spinlock_lock(&p->rbtree_sid2uid_locker);
	mm_rbtree_u64_u64_set(&p->rbtree_sid2uid, sid, uid);
	mm_spinlock_unlock(&p->rbtree_sid2uid_locker);

	mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hset(p, uid, sid);
	mm_shuttle_lobby_cache_rdb_gateway_cache_sid2uid_hset(p, sid, uid);
}
void mm_shuttle_lobby_cache_rbtree_rmv(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid, mm_uint64_t sid)
{
	mm_spinlock_lock(&p->rbtree_uid2sid_locker);
	mm_rbtree_u64_u64_rmv(&p->rbtree_uid2sid, uid);
	mm_spinlock_unlock(&p->rbtree_uid2sid_locker);

	mm_spinlock_lock(&p->rbtree_sid2uid_locker);
	mm_rbtree_u64_u64_rmv(&p->rbtree_sid2uid, sid);
	mm_spinlock_unlock(&p->rbtree_sid2uid_locker);

	mm_shuttle_lobby_cache_rdb_gateway_cache_uid2sid_hdel(p, uid);
	mm_shuttle_lobby_cache_rdb_gateway_cache_sid2uid_hdel(p, sid);
}
void mm_shuttle_lobby_cache_rbtree_rmv_by_uid(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid)
{
	mm_uint64_t sid = 0;
	// uid -> sid.
	mm_shuttle_lobby_cache_rbtree_uid2sid(p, uid, &sid);

	mm_shuttle_lobby_cache_rbtree_rmv(p, uid, sid);
}
void mm_shuttle_lobby_cache_rbtree_rmv_by_sid(struct mm_shuttle_lobby_cache* p, mm_uint64_t sid)
{
	mm_uint64_t uid = 0;
	// sid -> uid.
	mm_shuttle_lobby_cache_rbtree_sid2uid(p, sid, &uid);

	mm_shuttle_lobby_cache_rbtree_rmv(p, uid, sid);
}
void mm_shuttle_lobby_cache_rbtree_uid2sid(struct mm_shuttle_lobby_cache* p, mm_uint64_t uid, mm_uint64_t* sid)
{
	struct mm_rbtree_u64_u64_iterator* it = NULL;
	mm_spinlock_lock(&p->rbtree_uid2sid_locker);
	it = mm_rbtree_u64_u64_get_iterator(&p->rbtree_uid2sid, uid);
	mm_spinlock_unlock(&p->rbtree_uid2sid_locker);
	*sid = NULL == it ? 0 : it->v;
}
void mm_shuttle_lobby_cache_rbtree_sid2uid(struct mm_shuttle_lobby_cache* p, mm_uint64_t sid, mm_uint64_t* uid)
{
	struct mm_rbtree_u64_u64_iterator* it = NULL;
	mm_spinlock_lock(&p->rbtree_sid2uid_locker);
	it = mm_rbtree_u64_u64_get_iterator(&p->rbtree_sid2uid, sid);
	mm_spinlock_unlock(&p->rbtree_sid2uid_locker);
	*uid = NULL == it ? 0 : it->v;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_cache_start(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_start(&p->rdb_gateway_cache);
}
void mm_shuttle_lobby_cache_interrupt(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_interrupt(&p->rdb_gateway_cache);
}
void mm_shuttle_lobby_cache_shutdown(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_shutdown(&p->rdb_gateway_cache);
}
void mm_shuttle_lobby_cache_join(struct mm_shuttle_lobby_cache* p)
{
	mm_redis_sync_array_join(&p->rdb_gateway_cache);
}