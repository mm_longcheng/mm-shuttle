﻿#include "mm_shuttle_lobby_tcp_context.h"
#include "core/mm_string.h"
#include "core/mm_time.h"

#include "net/mm_mailbox.h"

#include "openssl/mm_openssl_tcp_context.h"

#include "mm_shuttle_lobby.h"

//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_external_mailbox_event_tcp_alloc(void* p, struct mm_tcp* tcp)
{
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(p);
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(mailbox->event_tcp_alloc.obj);
	mm_uint64_t msec_current = (mm_uint64_t)(mm_time_current_usec() / MM_MSEC_PER_SEC);

	mm_uint32_t sec_current = (mm_uint32_t)(msec_current / MM_MSEC_PER_SEC);

	struct mm_openssl_tcp_context* tcp_context = NULL;

	mm_tcp_s_lock(tcp);
	tcp_context = (struct mm_openssl_tcp_context*)mm_tcp_get_context(tcp);
	mm_tcp_s_unlock(tcp);

	assert(NULL == tcp_context && "NULL == tcp_context is not invalid");

	// pool_element produce.
	mm_pool_element_lock(&shuttle_lobby->pool_element);
	tcp_context = (struct mm_openssl_tcp_context*)mm_pool_element_produce(&shuttle_lobby->pool_element);
	mm_pool_element_unlock(&shuttle_lobby->pool_element);
	// note: 
	// pool_element manager init and destroy.
	// when mm_pool_element_produce and mm_pool_element_recycle only need reset.
	mm_openssl_tcp_context_reset(tcp_context);

	mm_openssl_tcp_context_assign_timecode_create(tcp_context, sec_current);

	mm_tcp_s_lock(tcp);
	mm_tcp_set_context(tcp, tcp_context);
	mm_tcp_s_unlock(tcp);
}
void mm_shuttle_lobby_external_mailbox_event_tcp_relax(void* p, struct mm_tcp* tcp)
{
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(p);
	struct mm_shuttle_lobby* shuttle_lobby = (struct mm_shuttle_lobby*)(mailbox->event_tcp_alloc.obj);

	struct mm_openssl_tcp_context* tcp_context = NULL;

	mm_tcp_s_lock(tcp);
	tcp_context = (struct mm_openssl_tcp_context*)mm_tcp_get_context(tcp);
	mm_tcp_set_context(tcp, NULL);
	mm_tcp_s_unlock(tcp);

	assert(NULL != tcp_context && "NULL != tcp_context is not invalid");

	// note: 
	// pool_element manager init and destroy.
	// when mm_pool_element_produce and mm_pool_element_recycle only need reset.
	mm_openssl_tcp_context_reset(tcp_context);
	// pool_element recycle.
	mm_pool_element_lock(&shuttle_lobby->pool_element);
	mm_pool_element_recycle(&shuttle_lobby->pool_element, tcp_context);
	mm_pool_element_unlock(&shuttle_lobby->pool_element);
}
//////////////////////////////////////////////////////////////////////////
