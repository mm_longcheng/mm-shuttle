#ifndef __mm_shuttle_lobby_kicker_h__
#define __mm_shuttle_lobby_kicker_h__

#include "core/mm_core.h"

#include "net/mm_mailbox.h"

//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_external_mailbox_kicker_us(struct mm_mailbox* external_mailbox);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_lobby_kicker_h__