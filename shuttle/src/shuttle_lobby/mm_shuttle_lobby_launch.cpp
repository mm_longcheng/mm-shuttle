﻿#include "mm_shuttle_lobby_launch.h"
#include "mm_application.h"
#include "core/mm_logger_manager.h"
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_launch_init(struct mm_shuttle_lobby_launch* p)
{
	p->unique_id = 0;
	mm_string_init(&p->internal_mailbox_parameters);
	mm_string_init(&p->external_mailbox_parameters);
	mm_string_init(&p->zookeeper_import_parameters);
	mm_string_init(&p->zookeeper_export_parameters);
	mm_string_init(&p->rdb_gateway_cache_parameters);
	mm_string_init(&p->event_queue_rabbitmq_parameters);
	mm_string_init(&p->node_address_parameters);
	p->token_seed = 0;
	p->rabbitmq_password_seed = 0;
	mm_string_init(&p->node_address_ipvalue);
}
void mm_shuttle_lobby_launch_destroy(struct mm_shuttle_lobby_launch* p)
{
	p->unique_id = 0;
	mm_string_destroy(&p->internal_mailbox_parameters);
	mm_string_destroy(&p->external_mailbox_parameters);
	mm_string_destroy(&p->zookeeper_import_parameters);
	mm_string_destroy(&p->zookeeper_export_parameters);
	mm_string_destroy(&p->rdb_gateway_cache_parameters);
	mm_string_destroy(&p->event_queue_rabbitmq_parameters);
	mm_string_destroy(&p->node_address_parameters);
	p->token_seed = 0;
	p->rabbitmq_password_seed = 0;
	mm_string_destroy(&p->node_address_ipvalue);
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_lobby_launch_pretreatment_config(struct mm_shuttle_lobby_launch* p)
{
	char node[MM_NODE_NAME_LENGTH] = { 0 };
	mm_ushort_t port = 0;

	struct mm_sockaddr addr;
	mm_sockaddr_assign(&addr, p->node_address_parameters.s, 0);
	mm_sockaddr_node_port(&addr, node, &port);

	mm_string_assigns(&p->node_address_ipvalue, node);
}
void mm_shuttle_lobby_launch_printf_information(struct mm_shuttle_lobby_launch* p)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();

	// 服务实例编名称    mm_shuttle_lobby_0
	// 程序名            mm_shuttle_lobby
	// 日志文件夹路径    ../log
	// 日志等级          7
	// 实例编号          1
	// 内地址启动参数    127.0.0.1-10000[2]
	// 外地址启动参数    127.0.0.1-20000[2]
	// 读取监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
	// 写入监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
	// 网关缓存地址号    101.200.169.28-10200
	// 事件队列地址号    101.200.169.28-5672
	// 节点地址          net.e(101.200.169.28)
	// JWT令牌种子       123456
	// 事件队列密码种子  456789

	mm_logger_log_I(g_logger,"##############################################################################################");
	mm_logger_log_I(g_logger,"%s %d launch information:",__FUNCTION__,__LINE__);
	mm_logger_log_I(g_logger,"服务实例编名称     mm_shuttle_lobby_%d",p->unique_id);
	mm_logger_log_I(g_logger,"程序名             %s","mm_shuttle_lobby");
	mm_logger_log_I(g_logger,"日志文件夹路径     %s",g_logger_manager->logger_path.s);
	mm_logger_log_I(g_logger,"日志等级           %u",g_logger_manager->logger_level);
	mm_logger_log_I(g_logger,"实例编号           %u",p->unique_id);
	mm_logger_log_I(g_logger,"内地址启动参数     %s",p->internal_mailbox_parameters.s);
	mm_logger_log_I(g_logger,"外地址启动参数     %s",p->external_mailbox_parameters.s);
	mm_logger_log_I(g_logger,"读取监控集群号     %s",p->zookeeper_import_parameters.s);
	mm_logger_log_I(g_logger,"写入监控集群号     %s",p->zookeeper_export_parameters.s);
	mm_logger_log_I(g_logger,"网关缓存地址号     %s", p->rdb_gateway_cache_parameters.s);
	mm_logger_log_I(g_logger,"事件队列地址号     %s", p->event_queue_rabbitmq_parameters.s);
	mm_logger_log_I(g_logger,"节点地址           %s(%s)", p->node_address_parameters.s, p->node_address_ipvalue.s);
	mm_logger_log_I(g_logger,"JWT令牌种子        %u", p->token_seed);
	mm_logger_log_I(g_logger,"事件队列密码种子   %u", p->rabbitmq_password_seed);
	mm_logger_log_I(g_logger,"##############################################################################################");
}
