#include "mm_shuttle_lobby_external_mailbox_tcp_hd.h"

#include "core/mm_logger.h"
#include "core/mm_byte.h"
#include "core/mm_time_cache.h"

#include "net/mm_packet.h"
#include "net/mm_streambuf_packet.h"
#include "net/mm_default_handle.h"
#include "redis/mm_redis_common.h"

#include "redis/mm_redis_hash.h"

#include "protobuf/mm_protobuff_cxx.h"
#include "protobuf/mm_protobuff_cxx_net.h"

#include "openssl/mm_openssl_tcp_context.h"

#include "shuttle_common/mm_error_code_common.h"

#include "cxx/protodef/c_shuttle_lobby.pb.h"
#include "cxx/protodef/s_shuttle_lobby.pb.h"

#include "mm_shuttle_lobby.h"
#include "mm_shuttle_lobby_error_code.h"
//////////////////////////////////////////////////////////////////////////
void hd_shuttle_lobby_c_shuttle_lobby_heartbeat_rq(void* obj, void* u, struct mm_packet* rq_pack)
{
	c_shuttle_lobby::heartbeat_rq rq_msg;
	c_shuttle_lobby::heartbeat_rs rs_msg;
	struct mm_packet rs_pack;
	struct mm_string proto_desc;
	b_error::info* error_info = rs_msg.mutable_error();
	b_math::coord* coord_info = rs_msg.mutable_coord_info();
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	struct mm_shuttle_lobby* impl = (struct mm_shuttle_lobby*)(u);
	struct mm_error_desc* error_desc = &impl->error_desc;
	////////////////////////////////
	error_info->set_code(0);
	error_info->set_desc("");
	coord_info->set_j(0);
	coord_info->set_w(0);
	rs_msg.set_uid(0);
	rs_msg.set_timecode_native(0);
	////////////////////////////////
	mm_string_init(&proto_desc);
	do
	{
		if (0 != mm_protobuf_cxx_decode_message(rq_pack, &rq_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		// logger rq.
		mm_protobuf_cxx_logger_append_packet_message(&proto_desc, rq_pack, &rq_msg);
		mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t uid = rq_msg.uid();
		//////////////////////////////////////////////////////////////////////////
		const b_math::coord& rq_coord_info = rq_msg.coord_info();
		coord_info->set_j(rq_coord_info.j());
		coord_info->set_w(rq_coord_info.w());
		rs_msg.set_uid(uid);
		rs_msg.set_timecode_native(rq_msg.timecode_native());
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t sid = mm_byte_uint64_a(impl->launch_info.unique_id, (mm_uint32_t)(tcp->socket.socket));
		//////////////////////////////////////////////////////////////////////////
		{
			mm_uint64_t msec_current = (mm_uint64_t)(mm_time_current_usec() / MM_MSEC_PER_SEC);

			mm_uint32_t sec_current = (mm_uint32_t)(msec_current / MM_MSEC_PER_SEC);

			struct mm_openssl_tcp_context* tcp_context = NULL;

			mm_tcp_s_lock(tcp);
			tcp_context = (struct mm_openssl_tcp_context*)mm_tcp_get_context(tcp);
			mm_tcp_s_unlock(tcp);

			mm_openssl_tcp_context_lock(tcp_context);

			// activate tcp rc4 context.
			mm_openssl_tcp_context_assign_timecode_update(tcp_context, sec_current);

			mm_openssl_tcp_context_unlock(tcp_context);

		}
		{
			struct mm_time_cache* g_time_cache = mm_time_cache_instance();

			mm_uint64_t sid_now = mm_byte_uint64_a(impl->launch_info.unique_id, (mm_uint32_t)(tcp->socket.socket));

			struct mm_packet ev_pack;
			s_shuttle_lobby::heartbeat_ev ev_msg;
			b_math::coord* coord_info = ev_msg.mutable_coord_info();

			ev_msg.set_event_time(g_time_cache->msec_current);

			const b_math::coord& rq_coord_info = rq_msg.coord_info();
			coord_info->set_j(rq_coord_info.j());
			coord_info->set_w(rq_coord_info.w());

			ev_msg.set_uid(uid);
			rs_msg.set_timecode_native(rq_msg.timecode_native());

			// message_publish_publish api is lock free.
			mm_message_publish_publish(&impl->message_publish, s_shuttle_lobby::heartbeat_ev_msg_id, sid_now, uid, &ev_msg, &ev_pack);

			// logger ev.
			mm_string_clear(&proto_desc);
			mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &ev_pack, &ev_msg);
			mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		}
		// logger.
		mm_logger_log_T(g_logger, "%s %d sid: %" PRIu64 " uid: %" PRIu64 "(%lf, %lf) timecode_native: %" PRIu64 "", __FUNCTION__, __LINE__,
			sid,
			rq_msg.uid(),
			rq_coord_info.j(), rq_coord_info.w(), 
			rq_msg.timecode_native());
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	// rs
	mm_tcp_o_lock(tcp);
	mm_protobuf_cxx_n_tcp_append_rs(mailbox, tcp, c_shuttle_lobby::heartbeat_rs_msg_id, &rs_msg, rq_pack, &rs_pack);
	mm_protobuf_cxx_n_tcp_flush_send(tcp);
	mm_tcp_o_unlock(tcp);
	// logger rs.
	mm_string_clear(&proto_desc);
	mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &rs_pack, &rs_msg);
	mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
	mm_string_destroy(&proto_desc);
}
