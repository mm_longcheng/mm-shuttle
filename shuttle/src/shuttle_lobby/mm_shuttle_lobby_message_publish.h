#ifndef __mm_shuttle_lobby_message_publish_h__
#define __mm_shuttle_lobby_message_publish_h__

#include "core/mm_core.h"

struct mm_shuttle_lobby;
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_lobby_message_publish_config(struct mm_shuttle_lobby* impl);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_lobby_message_publish_h__