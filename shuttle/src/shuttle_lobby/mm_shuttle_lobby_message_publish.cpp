﻿#include "mm_shuttle_lobby_message_publish.h"

#include "net/mm_sockaddr.h"

#include "dish/mm_xoshiro_string.h"

#include "shuttle_common/mm_message_publish.h"

#include "mm_shuttle_lobby.h"
#include "mm_shuttle_lobby_launch.h"

void mm_shuttle_lobby_message_publish_config(struct mm_shuttle_lobby* impl)
{
	struct mm_message_publish* message_publish = &impl->message_publish;
	struct mm_shuttle_lobby_launch* lobby_launch = &impl->launch_info;

	struct mm_string rabbitmq_password;
	struct mm_amqp_exchange exchange;
	struct mm_amqp_publish publish;

	mm_string_init(&rabbitmq_password);
	mm_amqp_exchange_init(&exchange);
	mm_amqp_publish_init(&publish);

	mm_xoshiro256starstar_srand(&impl->rabbitmq_password_random, lobby_launch->rabbitmq_password_seed);
	mm_xoshiro256starstar_random_string(&impl->rabbitmq_password_random, &rabbitmq_password, MM_SHUTTLE_LOBBY_RABBITMQ_SECRET_LENGTH);

	mm_string_assigns(&exchange.exchange_name, "mm.exchange.shuttle_lobby");
	mm_string_assigns(&exchange.exchange_type, "topic");
	exchange.exchange_passive = 0;
	exchange.exchange_durable = 1;
	exchange.exchange_auto_delete = 0;
	exchange.exchange_internal = 0;

	mm_string_assigns(&publish.publish_routingkey, "mm.shuttle_lobby");
	publish.publish_mandatory = 0;
	publish.publish_immediate = 0;

	mm_message_publish_assign_index(message_publish, lobby_launch->unique_id);

	mm_message_publish_assign_exchange(message_publish, &exchange);
	mm_message_publish_assign_publish(message_publish, &publish);
	mm_message_publish_assign_publish_trytimes(message_publish, MM_AMQP_CONN_TRYTIMES);

	mm_message_publish_assign_parameters(message_publish, lobby_launch->event_queue_rabbitmq_parameters.s);
	mm_message_publish_assign_vhost(message_publish, "/mm.shuttle");
	mm_message_publish_assign_account(message_publish, "mm_shuttle_lobby", rabbitmq_password.s);
	mm_message_publish_assign_conn_attr(message_publish, 0, 131072, 0, 1);
	mm_message_publish_assign_conn_timeout(message_publish, MM_AMQP_CONN_CONN_TIMEOUT, MM_AMQP_CONN_TRYTIMES);

	mm_string_destroy(&rabbitmq_password);
	mm_amqp_exchange_destroy(&exchange);
	mm_amqp_publish_destroy(&publish);
}