#include "mm_proxy_curl_curl_unit_handle.h"
#include "mm_proxy_curl.h"
#include "core/mm_logger.h"

#include "mm_proxy_curl_tcp_hd.h"
#include "protobuf/mm_protobuff_cxx.h"

#include "cxx/protodef/s_proxy_curl.pb.h"

void mm_proxy_curl_curl_unit_handle( struct mm_curl_unit* obj )
{
	long curl_code = 0;
	CURLcode code;
	struct mm_logger* g_logger = mm_logger_instance();
	//
	code = curl_easy_getinfo(obj->curl, CURLINFO_RESPONSE_CODE , &curl_code);
	if (code != CURLE_OK)
	{
		mm_logger_log_E(g_logger,"%s %d errno:(%d) %s",__FUNCTION__,__LINE__,(int)code, curl_easy_strerror(code));
	}
	else
	{
		struct mm_packet rq_pack;
		mm_packet_reset(&rq_pack);
		mm_curl_packet_data_decode(obj, &rq_pack);
		mm_uint32_t pid = rq_pack.phead.pid;
		struct mm_proxy_curl* impl = (struct mm_proxy_curl*)(obj->u);
		struct mm_mailbox* mailbox = &impl->mailbox;
		struct mm_tcp* tcp = mm_mailbox_get(mailbox, pid);
		if (NULL != tcp)
		{
			struct mm_packet rs_pack;
			struct mm_string proto_desc;
			//////////////////////////////////////////////////////////////////////////
			mm_packet_reset(&rs_pack);
			mm_string_init(&proto_desc);

			size_t size = mm_streambuf_size(&obj->buff_recv);
			const char* buff = (const char*)(obj->buff_recv.buff + obj->buff_recv.gptr);

			s_proxy_curl::json_data_rs rs;
			b_error::info* error_info = rs.mutable_error();
			error_info->set_code(0);
			error_info->set_desc("");
			rs.set_curl_code(curl_code);
			rs.set_buffer(buff, size);

			mm_tcp_lock(tcp);
			mm_protobuf_cxx_n_tcp_append_rs( mailbox, tcp, s_proxy_curl::json_data_rs_msg_id, &rs, &rq_pack, &rs_pack );
			mm_protobuf_cxx_n_tcp_flush_send( tcp );
			mm_tcp_unlock(tcp);

			mm_protobuf_cxx_logger_append_packet_message( &proto_desc, &rs_pack, &rs );
			mm_logger_log_T(g_logger,"%s %d nt unit->id:%u nt:%s",__FUNCTION__,__LINE__,obj->id, proto_desc.s);
			mm_string_destroy(&proto_desc);
		}
		else
		{
			mm_logger_log_W(g_logger,"%s %d tcp socket:%u is disconnect.",__FUNCTION__,__LINE__,pid);
		}
	}
}
