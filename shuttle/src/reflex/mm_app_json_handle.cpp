#include "mm_app_json_handle.h"

namespace mm
{
	mm_script_json_data_buffer::mm_script_json_data_buffer()
	{
		this->data = Milk::SharedPtr<rapidjson::Value>(new rapidjson::Value);
	}
	mm_script_json_data_buffer::~mm_script_json_data_buffer()
	{

	}

	int mm_script_json_data_buffer::decode( rapidjson::Document& _jc,const rapidjson::Value& _jv )
	{
		data->CopyFrom(_jv, _jc.GetAllocator());
		return mm_json_coder::success;
	}

	int mm_script_json_data_buffer::encode( rapidjson::Document& _jc,rapidjson::Value& _jv ) const
	{
		_jv.CopyFrom(*data, _jc.GetAllocator());
		return mm_json_coder::success;
	}


	mm_script_json_data_result::mm_script_json_data_result() 
		: broadcast(sjt_return_to_target_user_at_channel)
		, target_uid(0)
	{
		data.data->SetObject();
		uids.data->SetArray();
	}

	int mm_script_json_data_result::decode( rapidjson::Document& _jc,const rapidjson::Value& _jv )
	{
		mm_json_coder::decode(_jc, _jv, "broadcast", broadcast);
		mm_json_coder::decode(_jc, _jv, "target_uid", target_uid);
		mm_json_coder::decode(_jc, _jv, "data", data);
		mm_json_coder::decode(_jc, _jv, "uids", uids);
		return mm_json_coder::success;
	}

	int mm_script_json_data_result::encode( rapidjson::Document& _jc,rapidjson::Value& _jv ) const
	{
		mm_json_coder::encode(_jc, _jv, "broadcast", broadcast);
		mm_json_coder::encode(_jc, _jv, "target_uid", target_uid);
		mm_json_coder::encode(_jc, _jv, "data", data);
		mm_json_coder::encode(_jc, _jv, "uids", uids);
		return mm_json_coder::success;
	}


	mm_script_json_data_server_data::mm_script_json_data_server_data() 
		: plugin_type(0)
		, uid(0)
		, cid(0)
		, sid(0)
		, usercount(0)
		, roler(0)
		, uid_onmic(0)
		, receiver("")
		, sender("")
	{

	}

	int mm_script_json_data_server_data::decode( rapidjson::Document& _jc,const rapidjson::Value& _jv )
	{
		mm_json_coder::decode(_jc, _jv, "plugin_type", plugin_type);
		mm_json_coder::decode(_jc, _jv, "uid", uid);
		mm_json_coder::decode(_jc, _jv, "cid", cid);
		mm_json_coder::decode(_jc, _jv, "sid", sid);
		mm_json_coder::decode(_jc, _jv, "usercount", usercount);
		mm_json_coder::decode(_jc, _jv, "roler", roler);
		mm_json_coder::decode(_jc, _jv, "uid_onmic", uid_onmic);
		mm_json_coder::decode(_jc, _jv, "receiver", receiver);
		mm_json_coder::decode(_jc, _jv, "sender", sender);
		return mm_json_coder::success;
	}

	int mm_script_json_data_server_data::encode( rapidjson::Document& _jc,rapidjson::Value& _jv ) const
	{
		mm_json_coder::encode(_jc, _jv, "plugin_type", plugin_type);
		mm_json_coder::encode(_jc, _jv, "uid", uid);
		mm_json_coder::encode(_jc, _jv, "cid", cid);
		mm_json_coder::encode(_jc, _jv, "sid", sid);
		mm_json_coder::encode(_jc, _jv, "usercount", usercount);
		mm_json_coder::encode(_jc, _jv, "roler", roler);
		mm_json_coder::encode(_jc, _jv, "uid_onmic", uid_onmic);
		mm_json_coder::encode(_jc, _jv, "receiver", receiver);
		mm_json_coder::encode(_jc, _jv, "sender", sender);
		return mm_json_coder::success;
	}


	mm_script_json_data_rs::mm_script_json_data_rs() 
		: success(false)
	{

	}

	int mm_script_json_data_rs::decode( rapidjson::Document& _jc,const rapidjson::Value& _jv )
	{
		mm_json_coder::decode(_jc, _jv, "success", success);
		mm_json_coder::decode(_jc, _jv, "result", result);
		mm_json_coder::decode(_jc, _jv, "server_data", server_data);
		return mm_json_coder::success;
	}

	int mm_script_json_data_rs::encode( rapidjson::Document& _jc,rapidjson::Value& _jv ) const
	{
		mm_json_coder::encode(_jc, _jv, "success", success);
		mm_json_coder::encode(_jc, _jv, "result", result);
		mm_json_coder::encode(_jc, _jv, "server_data", server_data);
		return mm_json_coder::success;
	}


	mm_script_json_data_rq::mm_script_json_data_rq()
	{

	}

	int mm_script_json_data_rq::decode( rapidjson::Document& _jc,const rapidjson::Value& _jv )
	{
		mm_json_coder::decode(_jc, _jv, "user_data", user_data);
		mm_json_coder::decode(_jc, _jv, "server_data", server_data);
		return mm_json_coder::success;
	}

	int mm_script_json_data_rq::encode( rapidjson::Document& _jc,rapidjson::Value& _jv ) const
	{
		mm_json_coder::encode(_jc, _jv, "user_data", user_data);
		mm_json_coder::encode(_jc, _jv, "server_data", server_data);
		return mm_json_coder::success;
	}

}
