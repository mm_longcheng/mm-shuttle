#ifndef __mm_shuttle_reflex_surface_h__
#define __mm_shuttle_reflex_surface_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_timer.h"

#include "net/mm_mailbox.h"

//////////////////////////////////////////////////////////////////////////
struct mm_shuttle_reflex_surface
{
	struct mm_mailbox mailbox;
	pthread_t notify_thread;
	mm_uint32_t unique_id;
	mm_uint64_t frame_number;
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_surface_init(struct mm_shuttle_reflex_surface* p);
void mm_shuttle_reflex_surface_destroy(struct mm_shuttle_reflex_surface* p);
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_surface_start(struct mm_shuttle_reflex_surface* p);
void mm_shuttle_reflex_surface_interrupt(struct mm_shuttle_reflex_surface* p);
void mm_shuttle_reflex_surface_shutdown(struct mm_shuttle_reflex_surface* p);
void mm_shuttle_reflex_surface_join(struct mm_shuttle_reflex_surface* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_reflex_surface_h__