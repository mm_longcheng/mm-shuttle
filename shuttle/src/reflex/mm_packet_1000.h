#ifndef __mm_packet_1000_h__
#define __mm_packet_1000_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_alloc.h"
#include "core/mm_byte.h"

#include "net/mm_export_net.h"
#include "net/mm_sockaddr.h"

// packet_head struct is little endian.
// must have the mm_uint32_t msg id.
// mm_uint32_t   mid;// msg     id.
void mm_packet_head_mid_decode(struct mm_byte_buffer* hbuff, mm_uint32_t* mid_h, mm_uint32_t* mid_n);
void mm_packet_head_mid_encode(struct mm_byte_buffer* hbuff, mm_uint32_t* mid_h, mm_uint32_t* mid_n);


// normal the head base is minimum demand.
struct mm_packet_head_base
{
	mm_uint32_t   mid;// msg     id.
};
// normal the head comm is enough to use.
struct mm_packet_head_comm
{
	// msg base head.include into head data.
	mm_uint32_t   mid;// msg     id.
	//
	mm_uint32_t   cfd;// client  fd.
	mm_uint32_t   lfd;// lobby   fd.
	mm_uint32_t   pid;// proxy   id.
	mm_uint64_t   uid;// unique  id.
};
// 
//struct mm_packet_head
//{
//	union 
//	{
//		mm_uint32_t   mid;// msg     id.
//		struct mm_packet_head_base head_base;
//		struct mm_packet_head_comm head_comm;
//	};
//};

// mm_uint32_t   uri;
// mm_uint16_t   sid;
// mm_uint16_t   res;
// mm_uint8_t    tag;
#define MM_MSG_COMM_HEAD_1000_SIZE 9

struct mm_packet_head_1000
{
	mm_uint32_t uri;
	mm_uint16_t sid;
	mm_uint16_t res;
	mm_uint8_t  tag;
};

struct mm_packet_head_11
{
	mm_uint16_t sid;
	mm_uint16_t res;
	mm_uint8_t  tag;
};
// hbuff is contain the base mm_uint32_t mid.here coder jump over mid.
void mm_packet_head_11_decode(struct mm_packet_head_11* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_11* phead);
void mm_packet_head_11_encode(struct mm_packet_head_11* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_11* phead);

struct mm_packet_head_22
{
	mm_uint32_t   sid;// socket  id.
	mm_uint32_t   lid;// lobby   id.
	mm_uint32_t   pid;// proxy   id.
	mm_uint64_t   uid;// unique  id.
};
void mm_packet_head_22_decode(struct mm_packet_head_22* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_22* phead);
void mm_packet_head_22_encode(struct mm_packet_head_22* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_22* phead);

struct mm_packet_head_33
{
	mm_uint64_t   uid;// unique  id.
};
void mm_packet_head_33_decode(struct mm_packet_head_33* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_33* phead);
void mm_packet_head_33_encode(struct mm_packet_head_33* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_33* phead);

typedef void (*packet_head_decode)(struct mm_byte_buffer* hbuff, void* h, void* n);
typedef void (*packet_head_encode)(struct mm_byte_buffer* hbuff, void* h, void* n);

struct mm_packet_coder
{
	mm_uint32_t             mid;// msg     id.
	struct mm_byte_buffer hbuff;// buffer for head.
	struct mm_byte_buffer bbuff;// buffer for body.
	packet_head_decode head_decode;// host => network.
	packet_head_encode head_encode;// network => host.
};
// mid hbuff bbuff.
typedef void (*packet_coder_handle_tcp)(void* obj, struct mm_packet_coder* pack);


//void __func_0(struct mm_packet_head_1000* p0, struct mm_packet* p1)
//{
//	// mm_uint32_t   uri;
//	// mm_uint16_t   sid;
//	// mm_uint16_t   res;
//	// mm_uint8_t    tag;
//
//	// mm_uint32_t   mid;// msg     id.
//	// mm_uint32_t   pid;// proxy   id.
//	// mm_uint64_t   sid;// section id.
//	// mm_uint64_t   uid;// unique  id.
//
//	mm_uint32_t server_id = 0;
//	mm_uint32_t socket_fd = 0;
//
//	p1->phead.mid = p0->uri;
//	p1->phead.pid = 0;
//	p1->phead.sid = mm_byte_uint64_a(server_id, socket_fd);
//	p1->phead.uid = mm_byte_uint32_a(p0->sid, p0->res);
//}
//void __func_1()
//{
//
//}
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_packet_1000_h__