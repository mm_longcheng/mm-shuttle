#ifndef __mm_proxy_curl_h__
#define __mm_proxy_curl_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_timer.h"

#include "net/mm_mailbox.h"
#include "net/mm_net_tcp.h"

#include "container/mm_rbtree_visible.h"
#include "curl/mm_curl_unit_manager.h"
//////////////////////////////////////////////////////////////////////////
struct mm_proxy_curl_config
{
	struct mm_string node;
	struct mm_string index_file;
	struct mm_string method_name;
	mm_short_t port;
};
void mm_proxy_curl_config_init(struct mm_proxy_curl_config* p);
void mm_proxy_curl_config_destroy(struct mm_proxy_curl_config* p);
void mm_proxy_curl_config_assign_config(struct mm_proxy_curl_config* p, const char* index, const char* method);
void mm_proxy_curl_config_assign_remote(struct mm_proxy_curl_config* p, const char* node, mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
struct mm_proxy_curl
{
	struct mm_curl_unit_manager curl_unit_manager;
	struct mm_mailbox mailbox;
	struct mm_curl_unit_callback curl_unit_callback;
	struct mm_proxy_curl_config proxy_curl_config;
};
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_init(struct mm_proxy_curl* p);
void mm_proxy_curl_destroy(struct mm_proxy_curl* p);
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_assign_native(struct mm_proxy_curl* p, const char* info);
void mm_proxy_curl_assign_remote(struct mm_proxy_curl* p, const char* info);
void mm_proxy_curl_assign_native_length(struct mm_proxy_curl* p, mm_uint32_t length);
void mm_proxy_curl_assign_remote_length(struct mm_proxy_curl* p, mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_start(struct mm_proxy_curl* p);
void mm_proxy_curl_interrupt(struct mm_proxy_curl* p);
void mm_proxy_curl_shutdown(struct mm_proxy_curl* p);
void mm_proxy_curl_join(struct mm_proxy_curl* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_proxy_curl_h__