﻿#ifndef __mm_application_context_h__
#define __mm_application_context_h__

#include "core/mm_core.h"

namespace mm
{
	// application context is use for lazy init mm core lib.
	class mm_application_context
	{
	public:
		mm_application_context();
		virtual ~mm_application_context();
	public:
		void launching();
		void terminate();
	public:
		void init();
		void destroy();
		void start();
		void interrupt();
		void shutdown();
		void join();
	};

}
#endif//__mm_application_context_h__