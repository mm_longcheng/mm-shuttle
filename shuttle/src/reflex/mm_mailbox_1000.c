#include "mm_mailbox_1000.h"
#include "core/mm_logger.h"
#include "core/mm_string.h"
#include "core/mm_byteswap.h"
#include "mm_packet_1000.h"
//////////////////////////////////////////////////////////////////////////
void mm_crypto_rc4_init(struct mm_crypto_rc4* p)
{
	mm_memset(&p->context_send, 0, sizeof(RC4_KEY));
	mm_memset(&p->context_recv, 0, sizeof(RC4_KEY));
	p->state = MM_CRYPTO_RC4_INACTIVE;
}
void mm_crypto_rc4_destroy(struct mm_crypto_rc4* p)
{
	mm_memset(&p->context_send, 0, sizeof(RC4_KEY));
	mm_memset(&p->context_recv, 0, sizeof(RC4_KEY));
	p->state = MM_CRYPTO_RC4_INACTIVE;
}
void mm_crypto_rc4_set_key(struct mm_crypto_rc4* p, const mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	RC4_set_key(&p->context_send, length, (const unsigned char*)(buffer + offset));
	RC4_set_key(&p->context_recv, length, (const unsigned char*)(buffer + offset));
}
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_1000_event_tcp_alloc( void* p, struct mm_tcp* tcp );
static void __static_mailbox_1000_event_tcp_relax( void* p, struct mm_tcp* tcp );

static void __static_mailbox_1000_crypto_rc4_alloc( struct mm_tcp* tcp );
static void __static_mailbox_1000_crypto_rc4_relax( struct mm_tcp* tcp );

static int __static_mailbox_1000_streambuf_packet_handle_tcp(struct mm_streambuf* p, packet_handle_tcp handle, void* obj);
static int __static_mailbox_1000_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
void mm_mailbox_1000_init( struct mm_mailbox_1000* p )
{
	struct mm_tcp_callback tcp_callback;
	struct mm_mailbox_event_tcp_alloc mailbox_event_tcp_alloc;
	mailbox_event_tcp_alloc.event_tcp_alloc = &__static_mailbox_1000_event_tcp_alloc;
	mailbox_event_tcp_alloc.event_tcp_relax = &__static_mailbox_1000_event_tcp_relax;
	mailbox_event_tcp_alloc.obj = p;

	mm_mailbox_init(&p->mailbox);
	mm_mailbox_assign_event_tcp_alloc(&p->mailbox, &mailbox_event_tcp_alloc);

	tcp_callback.handle = &__static_mailbox_1000_tcp_handle_callback;
	tcp_callback.broken = &mm_mailbox_tcp_broken_callback;
	tcp_callback.obj = p;
	mm_mailbox_assign_tcp_callback(&p->mailbox, &tcp_callback);
}

void mm_mailbox_1000_destroy( struct mm_mailbox_1000* p )
{
	mm_mailbox_destroy(&p->mailbox);
}

void mm_mailbox_1000_assign_native(struct mm_mailbox_1000* p, const char* node, mm_ushort_t port)
{
	mm_mailbox_assign_native(&p->mailbox, node, port);
}

void mm_mailbox_1000_start(struct mm_mailbox_1000* p)
{
	mm_mailbox_start(&p->mailbox);
}
void mm_mailbox_1000_interrupt(struct mm_mailbox_1000* p)
{
	mm_mailbox_interrupt(&p->mailbox);
}
void mm_mailbox_1000_shutdown(struct mm_mailbox_1000* p)
{
	mm_mailbox_shutdown(&p->mailbox);
}
void mm_mailbox_1000_join(struct mm_mailbox_1000* p)
{
	mm_mailbox_join(&p->mailbox);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_1000_event_tcp_alloc( void* p, struct mm_tcp* tcp )
{
	__static_mailbox_1000_crypto_rc4_alloc(tcp);
}
static void __static_mailbox_1000_event_tcp_relax( void* p, struct mm_tcp* tcp )
{
	__static_mailbox_1000_crypto_rc4_relax(tcp);
}

static void __static_mailbox_1000_crypto_rc4_alloc( struct mm_tcp* tcp )
{
	struct mm_crypto_rc4* rc4_context = NULL;
	mm_tcp_lock(tcp);
	rc4_context = (struct mm_crypto_rc4*)mm_tcp_get_context(tcp);
	if (NULL == rc4_context)
	{
		struct mm_crypto_rc4* rc4_context = (struct mm_crypto_rc4*)mm_malloc(sizeof(struct mm_crypto_rc4));
		mm_crypto_rc4_init(rc4_context);
		mm_tcp_set_context(tcp, &rc4_context);
	}
	mm_tcp_unlock(tcp);
}
static void __static_mailbox_1000_crypto_rc4_relax( struct mm_tcp* tcp )
{
	struct mm_crypto_rc4* rc4_context = NULL;
	mm_tcp_lock(tcp);
	rc4_context = (struct mm_crypto_rc4*)mm_tcp_get_context(tcp);
	if (NULL != rc4_context)
	{
		mm_tcp_set_context(tcp, NULL);
		mm_crypto_rc4_destroy(rc4_context);
		mm_free(rc4_context);
	}
	mm_tcp_unlock(tcp);
}

static int __static_mailbox_1000_streambuf_packet_handle_tcp(struct mm_streambuf* p, packet_handle_tcp handle, void* obj)
{
	int code = 0;
	struct mm_packet pack;
	mm_uint16_t msg_size = 0;
	size_t buff_size = 0;
	// use net byte order.
	mm_uint32_t message_size_ne = 0;
	mm_uint16_t hbuff_length_ne = 0;
	mm_uint32_t mid_n = 0;
	assert(handle&&"handle callback is invalid.");
	do
	{
		buff_size = mm_streambuf_size(p);
		mm_memcpy(&message_size_ne, (void*)(p->buff + p->gptr), sizeof(mm_uint32_t));
		msg_size = (mm_uint16_t)mm_ltoh32((mm_uint32_t)(message_size_ne));
		if (buff_size >= msg_size)
		{
			hbuff_length_ne = MM_MSG_COMM_HEAD_1000_SIZE;
			// here we just use the weak ref from streambuf.for data recv callback.
			pack.hbuff.length = mm_ltoh16((mm_uint16_t)(hbuff_length_ne));
			pack.hbuff.buffer = p->buff;
			pack.hbuff.offset = (mm_uint32_t)(p->gptr + sizeof(mm_uint32_t));
			//
			pack.bbuff.length = msg_size - sizeof(mm_uint32_t) - pack.hbuff.length;
			pack.bbuff.buffer = p->buff;
			pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
			// check buffer.
			if (msg_size - sizeof(mm_uint32_t) < pack.hbuff.length)
			{
				struct mm_logger* g_logger = mm_logger_instance();
				mm_logger_log_E(g_logger,"%s %d invalid tcp message buffer.",__FUNCTION__,__LINE__);
				code = -1;
				break;
			}
			else
			{
				mm_packet_head_mid_decode((struct mm_byte_buffer*)&pack.hbuff, &pack.phead.mid, &mid_n);
				//
				// fire the field buffer recv event.
				(*(handle))(obj,&pack);
				// no matter session_recv how much buffer reading,
				// we removeread fixed size to make sure the socket buffer sequence is correct.
				mm_streambuf_gbump(p,msg_size);
			}
		}
	} while ( buff_size > msg_size && msg_size >= MM_MSG_BASE_HEAD_SIZE );
	return code;
}
static int __static_mailbox_1000_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	mm_mailbox_crypto_decrypt(mailbox,tcp,buffer,offset,length);
	return __static_mailbox_1000_streambuf_packet_handle_tcp(&tcp->buff_recv, &mm_mailbox_tcp_handle_packet_callback, tcp);
}
