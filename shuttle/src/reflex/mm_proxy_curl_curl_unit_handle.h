#ifndef __mm_proxy_curl_curl_unit_handle_h__
#define __mm_proxy_curl_curl_unit_handle_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "net/mm_mailbox.h"

//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_curl_unit_handle( struct mm_curl_unit* obj );
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_proxy_curl_curl_unit_handle_h__