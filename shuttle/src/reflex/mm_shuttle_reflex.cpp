#include "mm_shuttle_reflex.h"
#include "core/mm_logger.h"
#include "core/mm_time_cache.h"
#include "core/mm_runtime_stat.h"
#include "core/mm_byte.h"
#include "core/mm_thread.h"
#include "core/mm_file_system.h"

#include "curl/curl.h"

#include <string>
#include <iostream>
#include "container/mm_list_vpt.h"
#include "protobuf/mm_protobuff_cxx.h"
#include "cxx/protodef/s_proxy_curl.pb.h"

#include "net/mm_mt_contact.h"
#include "mm_app_json_handle.h"

#include "core/mm_byteswap.h"
#include "dish/mm_archive_endian.h"
#include "mm_application_context.h"
#include "mm_packet_1000.h"
#include "logger_file.h"
#include "dish/mm_package.h"
#include "mm_shuttle_test_address.h"
#include "mm_shuttle_test_hmget.h"
#include "mm_shuttle_test_curl.h"


void mm_shuttle_reflex_init(struct mm_shuttle_reflex* p)
{
	{
		mm_shuttle_test_address_func();
		mm_shuttle_test_hmget_func();
		mm_shuttle_test_curl_func();
	}
	mm_net_performance_init(&p->net_performance);
	mm_redis_poper_array_init(&p->poper_array);
	mm_app_tcp_contact_init(&p->app_tcp_contact);
	mm_proxy_curl_init(&p->proxy_curl);
	mm_mailbox_init(&p->mailbox);
	mm_net_tcp_init(&p->net_tcp);
	p->state = ts_closed;
	mm_protobuf_cxx_init();
	//
	struct mm_app_tcp_contact_callback contact_callback;
	contact_callback.handle = &__static_shuttle_reflex_app_tcp_contact_handle;
	contact_callback.obj = p;
	mm_app_tcp_contact_assign_remote(&p->app_tcp_contact, "127.0.0.1", 56001);
	mm_app_tcp_contact_assign_context(&p->app_tcp_contact, p);
	mm_app_tcp_contact_assign_contact_callback(&p->app_tcp_contact, &contact_callback);
	//
	mm_proxy_curl_assign_native(&p->proxy_curl, "127.0.0.1-56001");
	mm_proxy_curl_assign_remote(&p->proxy_curl, "60.205.151.85-18080");
	mm_proxy_curl_assign_native_length(&p->proxy_curl, 1);
	mm_proxy_curl_assign_remote_length(&p->proxy_curl, 1);
	//
	mm_net_tcp_assign_remote(&p->net_tcp, "127.0.0.1", 56001);
	mm_net_tcp_assign_context(&p->net_tcp, p);
	//
	struct mm_redis_poper_callback poper_callback;
	poper_callback.poper = &__static_shuttle_reflex_redis_poper_callback;
	poper_callback.obj = p;
	mm_redis_poper_array_set_length(&p->poper_array, 1);
	mm_redis_poper_array_assign_callback(&p->poper_array, &poper_callback);
	mm_redis_poper_array_assign_remote(&p->poper_array, "101.200.169.28", 50500);
	//mm_redis_poper_array_assign_remote(&p->poper_array, "60.205.151.85", 18001);
	//mm_redis_poper_array_assign_auth(&p->poper_array, "xcRed.,0505");
	mm_redis_poper_array_assign_context(&p->poper_array, p);
	//mm_redis_poper_array_assign_cmd(&p->poper_array, "brpop");
	mm_redis_poper_array_assign_cmd(&p->poper_array, "rpop");
	mm_redis_poper_array_assign_key(&p->poper_array, "queue:list:link:notify");
	mm_redis_poper_array_assign_sleep_timeout(&p->poper_array, 2000, 1000, 3000);
}
void mm_shuttle_reflex_destroy(struct mm_shuttle_reflex* p)
{
	mm_net_performance_destroy(&p->net_performance);
	mm_redis_poper_array_destroy(&p->poper_array);
	mm_app_tcp_contact_destroy(&p->app_tcp_contact);
	mm_proxy_curl_destroy(&p->proxy_curl);
	mm_mailbox_destroy(&p->mailbox);
	mm_net_tcp_destroy(&p->net_tcp);
	p->state = ts_closed;
	mm_protobuf_cxx_destroy();
	// Optional:  Delete all global objects allocated by libprotobuf.
	google::protobuf::ShutdownProtobufLibrary();
}
//////////////////////////////////////////////////////////////////////////
static void* __static_reflex_task(void* arg)
{
	struct mm_shuttle_reflex* p = (struct mm_shuttle_reflex*)arg;
	struct mm_logger* g_logger = mm_logger_instance();
	while(ts_motion == p->state)
	{
		// real msleep time is 50 - cast time.
		// mm_msleep(1);
		//for (int i = 0;i<10000;i++)
		//{
		//	// notify op set.
		//	double a = 8454.0;
		//	double b = 554645.0;
		//	double c = a * b;
		//}
		//mm_logger_log_I(g_logger,"%s %d thread:%d.",__FUNCTION__,__LINE__,mm_current_thread_id());

		unsigned long int tid = mm_current_thread_id();
		char ch[32] = {0};
		mm_sprintf(ch, "%u", (mm_uint32_t)tid);
		std::string pack("package: ");
		pack = pack + ch;
		p->bq.push(NULL, &pack);
	}
	return NULL;
}
void mm_shuttle_reflex_assign_addr_dbmysql(struct mm_shuttle_reflex* p,const char* info)
{

}
void mm_shuttle_reflex_assign_unique_id(struct mm_shuttle_reflex* p,mm_uint32_t unique_id)
{

}
//////////////////////////////////////////////////////////////////////////
static void __static_packet_handle_tcp(void* obj, struct mm_packet* pack)
{

}
static void* __static_shuttle_reflex_surface_logic_task(void* arg)
{
	struct mm_shuttle_reflex* p = (struct mm_shuttle_reflex*)(arg);
	return NULL;
}
static void __static_mailbox_handle_event_1( void* obj, void* u, struct mm_packet* pack )
{

}
static void __static_net_tcp_handle(void* obj, void* u, struct mm_packet* rs_pack)
{
	char socket_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_proxy_curl* impl = (struct mm_proxy_curl*)u;
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_string proto_desc;
	mm_socket_string(&tcp->socket, socket_name);
	mm_string_init(&proto_desc);
	s_proxy_curl::json_data_rs rs;
	if (0 == mm_protobuf_cxx_decode_message(rs_pack, &rs))
	{
		mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rs_pack, &rs );
		mm_logger_log_T(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
		// logic.
		const std::string& buffer = rs.buffer();

		mm::mm_script_json_data_rs data_rs;
		rapidjson::Document runtime_doc(rapidjson::kObjectType);
		if (runtime_doc.Parse((const char*)(buffer.data()),buffer.size()).HasParseError())
		{
			mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc: %s.",__FUNCTION__,__LINE__,buffer.c_str());
			// break;
		}
		data_rs.decode(runtime_doc,runtime_doc);
		int a = 0;
	}
	else
	{
		mm_protobuf_cxx_logger_append_packet( &proto_desc, rs_pack );
		mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	}
	mm_string_destroy(&proto_desc);
}
static void __static_shuttle_reflex_handle_callback(void* obj, void* u, std::string* pack)
{
	struct mm_logger* g_logger = mm_logger_instance();
	mm_logger_log_I(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,pack->c_str());
}

void mm_shuttle_reflex_start(struct mm_shuttle_reflex* p)
{
	//{
	//	func_1();
	//}
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	//
	mm_net_performance_start(&p->net_performance);
	//
	mm_mailbox_assign_callback(&p->mailbox, 1,&__static_mailbox_handle_event_1);
	//
	mm_mailbox_assign_context(&p->mailbox, p);
	mm_mailbox_assign_native(&p->mailbox, "127.0.0.1", 56000);
	mm_mailbox_set_length(&p->mailbox, 1);
	mm_mailbox_fopen_socket(&p->mailbox);
	mm_mailbox_bind(&p->mailbox);
	mm_mailbox_listen(&p->mailbox);
	mm_mailbox_start(&p->mailbox);
	//
	mm_app_tcp_contact_start(&p->app_tcp_contact);
	mm_proxy_curl_start(&p->proxy_curl);
	//pthread_create(&p->thread_logic1, NULL, &__static_shuttle_reflex_surface_logic_task, p);
	for (int i = 0;i< thread_number;++i)
	{
		pthread_create(&p->accept_thread[i], NULL, &__static_reflex_task, p);
	}

	mm_net_tcp_assign_callback(&p->net_tcp, s_proxy_curl::json_data_rs_msg_id, &__static_net_tcp_handle);
	mm_net_tcp_fopen_socket(&p->net_tcp);
	mm_net_tcp_connect(&p->net_tcp);
	mm_net_tcp_start(&p->net_tcp);
	//
	//const char* params_json = "{\"server_data\":{},\"user_data\":{\"cmd\":\"get_saw_game_detail_rq\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}}";
	////
	//struct mm_packet rq_pack;
	//s_proxy_curl::json_data_rq rq;
	//rq.set_buffer(params_json);
	////
	//mm_net_tcp_lock(&p->net_tcp);
	//mm_protobuf_cxx_q_tcp_append_rq( &p->net_tcp, 90008000000005555, s_proxy_curl::json_data_rq_msg_id, &rq, &rq_pack );
	//mm_protobuf_cxx_n_net_tcp_flush_send( &p->net_tcp );
	//mm_net_tcp_unlock(&p->net_tcp);
	//
	mm_redis_poper_array_start(&p->poper_array);
	//
	mm::mm_buffer_queue::mm_buffer_queue_callback buffer_queue_callback;
	buffer_queue_callback.handle = &__static_shuttle_reflex_handle_callback;
	buffer_queue_callback.obj = p;
	p->bq.assign_queue_tcp_callback(&buffer_queue_callback);
}
void mm_shuttle_reflex_interrupt(struct mm_shuttle_reflex* p)
{
	p->state = ts_closed;
	mm_net_performance_interrupt(&p->net_performance);
	mm_app_tcp_contact_interrupt(&p->app_tcp_contact);
	mm_proxy_curl_interrupt(&p->proxy_curl);
	mm_mailbox_interrupt(&p->mailbox);
	mm_net_tcp_interrupt(&p->net_tcp);
	mm_redis_poper_array_interrupt(&p->poper_array);
	//
	p->bq.cond_not_full();
	p->bq.cond_not_null();
}
void mm_shuttle_reflex_shutdown(struct mm_shuttle_reflex* p)
{
	p->state = ts_finish;
	mm_net_performance_shutdown(&p->net_performance);
	mm_app_tcp_contact_shutdown(&p->app_tcp_contact);
	mm_proxy_curl_shutdown(&p->proxy_curl);
	mm_mailbox_shutdown(&p->mailbox);
	mm_net_tcp_shutdown(&p->net_tcp);
	mm_redis_poper_array_shutdown(&p->poper_array);
	//
	p->bq.cond_not_full();
	p->bq.cond_not_null();
}

void mm_shuttle_reflex_join(struct mm_shuttle_reflex* p)
{
	struct mm_logger* g_logger = mm_logger_instance();
	p->bq.assign_max_poper_number(-1);
	while(ts_motion == p->state)
	{
		//
		//rapidjson::StringBuffer string_buffer;
		//rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
		//const char* a = "{\"success\":true,\"result\":[{\"broadcast\":0,\"data\":{\"cmd\":\"RTextChat\",\"uid\":10003986,\"result\":0}},{\"broadcast\":1,\"data\":{\"cmd\":\"BTextChat\",\"uid\":10003986,\"sid\":102536,\"nickname\":\"\\u7528\\u623710003986\",\"isguard\":0,\"guardType\":0,\"guardName\":\"\",\"activeLevel\":1,\"activeLevelEffect\":0,\"context\":\"\\u5728\\u4e00\\u8d77\\u5427\\u2026\\u2026\",\"identity\":2,\"official\":0,\"new\":\"2\"}}],\"server_data\":{\"svid\":1,\"plugin_type\":6,\"uid\":10003986,\"cid\":1,\"sid\":102536,\"usercount\":7,\"roler\":25,\"uid_onmic\":10003986,\"receiver\":\"\\u7528\\u623710003986\",\"sender\":\"\\u7528\\u623710003986\"}}";
		//rapidjson::Document runtime_doc(rapidjson::kObjectType);
		//if (runtime_doc.Parse((const char*)(a), strlen(a)).HasParseError())
		//{
		//	mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
		//}
		//string_buffer.Clear();
		//_writer.Reset(string_buffer);
		//runtime_doc.Accept(_writer);
		//mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());

		//mm::mm_script_json_data_rs d;
		//d.decode(runtime_doc, runtime_doc);
		//
		p->bq.thread_handle();
		//std::string pack(string_buffer.GetString(), string_buffer.GetSize());
		//p->bq.push(NULL, &pack);
		// 
		//mm_app_tcp_contact_thread_handle(&p->app_tcp_contact);
		//////////////////////////////////////////////////////////////////////////////
		//const char* user_data = "{\"cmd\":\"get_saw_game_detail_rq\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}";
		//mm::mm_script_json_data_rq data_rq;

		//data_rq.server_data.plugin_type = 1;
		//data_rq.server_data.uid = 2;
		//data_rq.server_data.cid = 3;
		//data_rq.server_data.sid = 4;
		//data_rq.server_data.usercount = 5;
		//data_rq.server_data.roler = 6;
		//data_rq.server_data.uid_onmic = 7;
		//data_rq.server_data.receiver = "receiver";
		//data_rq.server_data.sender = "sender";

		//mm_app_tcp_contact_buffer_send(&p->app_tcp_contact, 10086, &data_rq, user_data, 0, mm_strlen(user_data));
		mm_msleep(10);
	}
	mm_net_performance_join(&p->net_performance);
	mm_app_tcp_contact_join(&p->app_tcp_contact);
	mm_proxy_curl_join(&p->proxy_curl);
	mm_mailbox_join(&p->mailbox);
	mm_net_tcp_join(&p->net_tcp);
	mm_redis_poper_array_join(&p->poper_array);
	//
	//pthread_join(p->thread_logic1, NULL);
	for (int i = 0;i< thread_number;++i)
	{
		pthread_join(p->accept_thread[i], NULL);
	}
}
//////////////////////////////////////////////////////////////////////////
