#ifndef __mm_app_tcp_contact_h__
#define __mm_app_tcp_contact_h__

#include "mm_app_json_handle.h"

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "net/mm_mt_tcp.h"
#include "net/mm_mq_tcp.h"

#include "protobuf/mm_protobuff_cxx.h"


#define MM_APP_TCP_CONTACT_MAX_POPER_NUMBER 8000

// obj is mm_app_tcp_contact.
typedef void (*app_tcp_contact_handle)( void* obj, void* u, rapidjson::Document* doc, mm::mm_script_json_data_rs* rs, mm::mm_script_json_data_result* r );
//
struct mm_app_tcp_contact_callback
{
	app_tcp_contact_handle handle;
	void* obj;// weak ref. user data for callback.
};
void mm_app_tcp_contact_callback_init(struct mm_app_tcp_contact_callback* p);
void mm_app_tcp_contact_callback_destroy(struct mm_app_tcp_contact_callback* p);

struct mm_app_tcp_contact
{
	struct mm_mt_tcp mt_tcp;
	struct mm_mq_tcp mq_tcp;
	struct mm_rbtree_u32_vpt rbtree;// rb tree for msg_id <--> callback.
	struct mm_app_tcp_contact_callback callback;// value ref. tcp callback.
	mm_atomic_t rbtree_locker;
	void* u;
};
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_init(struct mm_app_tcp_contact* p);
void mm_app_tcp_contact_destroy(struct mm_app_tcp_contact* p);
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_assign_native(struct mm_app_tcp_contact* p, const char* node, mm_ushort_t port);
void mm_app_tcp_contact_assign_remote(struct mm_app_tcp_contact* p, const char* node, mm_ushort_t port);
//
void mm_app_tcp_contact_assign_context(struct mm_app_tcp_contact* p, void* u);
//
void mm_app_tcp_contact_assign_callback(struct mm_app_tcp_contact* p, int id, app_tcp_contact_handle callback);
void mm_app_tcp_contact_assign_contact_callback(struct mm_app_tcp_contact* p, struct mm_app_tcp_contact_callback* contact_callback);
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_thread_handle(struct mm_app_tcp_contact* p);
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_buffer_send(struct mm_app_tcp_contact* p, mm_uint64_t uid, mm::mm_script_json_data_rq* rq, const char* buffer, mm_uint32_t offset, mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_start(struct mm_app_tcp_contact* p);
void mm_app_tcp_contact_interrupt(struct mm_app_tcp_contact* p);
void mm_app_tcp_contact_shutdown(struct mm_app_tcp_contact* p);
void mm_app_tcp_contact_join(struct mm_app_tcp_contact* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_app_tcp_contact_h__