#ifndef __mm_mailbox_1000_h__
#define __mm_mailbox_1000_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_string.h"

#include "net/mm_mailbox.h"

#include "openssl/rc4.h"

#define MM_CRYPTO_RC4_INACTIVE 0
#define MM_CRYPTO_RC4_ACTIVATE 1

struct mm_crypto_rc4
{
	RC4_KEY context_send;
	RC4_KEY context_recv;
	mm_uint8_t state;// MM_CRYPTO_RC4_INACTIVE have no crypto,MM_CRYPTO_RC4_ACTIVATE have crypto.default is MM_CRYPTO_RC4_INACTIVE.
};
void mm_crypto_rc4_init(struct mm_crypto_rc4* p);
void mm_crypto_rc4_destroy(struct mm_crypto_rc4* p);
void mm_crypto_rc4_set_key(struct mm_crypto_rc4* p, const char* node, mm_ushort_t port);

struct mm_mailbox_1000
{
	struct mm_mailbox mailbox;
};
//////////////////////////////////////////////////////////////////////////
void mm_mailbox_1000_init(struct mm_mailbox_1000* p);
void mm_mailbox_1000_destroy(struct mm_mailbox_1000* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
void mm_mailbox_1000_assign_native(struct mm_mailbox_1000* p, const char* node, mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
void mm_mailbox_1000_start(struct mm_mailbox_1000* p);
void mm_mailbox_1000_interrupt(struct mm_mailbox_1000* p);
void mm_mailbox_1000_shutdown(struct mm_mailbox_1000* p);
void mm_mailbox_1000_join(struct mm_mailbox_1000* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_mailbox_1000_h__