#include "mm_net_performance.h"
#include "core/mm_logger.h"
#include "cxx/protodef/s_proxy_curl.pb.h"
#include "protobuf/mm_protobuff_cxx.h"

static void __static_net_performance_mailbox_handle_10086( void* obj, void* u, struct mm_packet* rq_pack )
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_net_performance* impl = (struct mm_net_performance*)u;
	struct mm_logger* g_logger = mm_logger_instance();
	int send_size = 0;
	struct mm_packet rs_pack;

	rq_pack->phead.mid = s_proxy_curl::json_data_rs_msg_id;

	mm_tcp_lock(tcp);
	mm_protobuf_cxx_p_tcp_append_rs( &impl->mailbox, tcp, rq_pack, &rs_pack );
	send_size = mm_protobuf_cxx_n_tcp_flush_send( tcp );
	mm_tcp_unlock(tcp);
	if (0 == send_size)
	{
		mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,"mm_protobuf_cxx_n_net_tcp_flush_send error.");
	}

	//char socket_name[MM_LINK_NAME_LENGTH];
	//struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	//struct mm_net_performance* impl = (struct mm_net_performance*)u;
	//struct mm_logger* g_logger = mm_logger_instance();
	//struct mm_string proto_desc;
	//int send_size = 0;
	//mm_socket_string(&tcp->socket, socket_name);
	//mm_string_init(&proto_desc);
	//s_proxy_curl::json_data_rq rq;
	//if (0 == mm_protobuf_cxx_decode_message(rq_pack, &rq))
	//{
	//	rq_pack->phead.pid = tcp->socket.socket;
	//	//mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rq_pack, &rq );
	//	//// logic.
	//	//mm_logger_log_I(g_logger,"%s %d nt rq:%s",__FUNCTION__,__LINE__,proto_desc.s);
	//	//
	//	struct mm_packet rs_pack;
	//	//
	//	const char* params_json = "{rs}";
	//	//const char* params_json = "{\"server_data\":{},\"user_data\":{\"cmd\":\"get_saw_game_detail_rq\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}}";
	//	//const char* params_json = "{\"server_data\":{},\"user_data\":{\"cmd\":\"get_saw_game_detail_rqfddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeadfkljadfjlasjdfjasdfjasjfhasfhasdfjsfjjfjfjsdlfjlsjfkjfjdjfjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}}";

	//	s_proxy_curl::json_data_rs rs;
	//	b_error::info* error_info = rs.mutable_error();
	//	error_info->set_code(0);
	//	error_info->set_desc("");
	//	rs.set_curl_code(200);
	//	rs.set_buffer(params_json);

	//	mm_tcp_lock(tcp);
	//	mm_protobuf_cxx_n_tcp_append_rs( &impl->mailbox, tcp, s_proxy_curl::json_data_rs_msg_id, &rs, rq_pack, &rs_pack );
	//	send_size = mm_protobuf_cxx_n_tcp_flush_send( tcp );
	//	mm_tcp_unlock(tcp);
	//	if (0 == send_size)
	//	{
	//		mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,"mm_protobuf_cxx_n_net_tcp_flush_send error.");
	//	}
	//	//mm_string_clear(&proto_desc);
	//	//mm_protobuf_cxx_logger_append_packet_message( &proto_desc, &rs_pack, &rs );
	//	//mm_logger_log_T(g_logger,"%s %d nt nt:%s",__FUNCTION__,__LINE__, proto_desc.s);
	//}
	//else
	//{
	//	mm_protobuf_cxx_logger_append_packet( &proto_desc, rq_pack );
	//	mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	//}
	//mm_string_destroy(&proto_desc);
}

static void __static_net_performance_mailbox_handle_10087( void* obj, void* u, struct mm_packet* rs_pack )
{
	//char socket_name[MM_LINK_NAME_LENGTH];
	//struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	//struct mm_net_performance* impl = (struct mm_net_performance*)u;
	//struct mm_logger* g_logger = mm_logger_instance();
	//struct mm_string proto_desc;
	//mm_socket_string(&tcp->socket, socket_name);
	//mm_string_init(&proto_desc);
	//s_proxy_curl::json_data_rq rq;
	//if (0 == mm_protobuf_cxx_decode_message(rs_pack, &rq))
	//{
	//	//mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rs_pack, &rq );
	//	//// logic.
	//	//mm_logger_log_I(g_logger,"%s %d nt rq:%s",__FUNCTION__,__LINE__,proto_desc.s);
	//}
	//else
	//{
	//	mm_protobuf_cxx_logger_append_packet( &proto_desc, rs_pack );
	//	mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	//}
	//mm_string_destroy(&proto_desc);
}
static void* __static_net_performance_task(void* arg)
{
	const char* params_json = "{rq}";
	//const char* params_json = "{\"server_data\":{},\"user_data\":{\"cmd\":\"get_saw_game_detail_rq\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}}";
	struct mm_packet rq_pack;
	s_proxy_curl::json_data_rq rq;
	rq.set_buffer(params_json);
	//
	int rq_num = 0;
	int rs_num = 0;
	//
	int send_size = 0;
	//
	struct mm_net_performance* p = (struct mm_net_performance*)arg;
	struct mm_logger* g_logger = mm_logger_instance();
	mm_logger_log_I(g_logger,"%s %d motion.",__FUNCTION__,__LINE__);
	while(ts_motion == p->state)
	{
		// real msleep time is 50 - cast time.
		struct mm_mt_tcp* mt_tcp = mm_mt_contact_get_instance_quick(&p->mt_contact, 1000, "0.0.0.0", "127.0.0.1", 55521);
		struct mm_net_tcp* net_tcp = mm_mt_tcp_thread_instance(mt_tcp);
		//
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_check(net_tcp);
		mm_net_tcp_unlock(net_tcp);
		if (0 == mm_net_tcp_finally_state(net_tcp))
		{
			mm_net_tcp_lock(net_tcp);
			mm_protobuf_cxx_q_tcp_append_rq( net_tcp, rq_num ++, s_proxy_curl::json_data_rq_msg_id, &rq, &rq_pack );
			send_size = mm_protobuf_cxx_n_net_tcp_flush_send( net_tcp );
			mm_net_tcp_unlock(net_tcp);
		}
		if (rq_num >= 1)
		{
			break;
		}
		if (0 == send_size)
		{
			mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,"mm_protobuf_cxx_n_net_tcp_flush_send error.");
			break;
		}
	}
	mm_logger_log_I(g_logger,"%s %d finish.",__FUNCTION__,__LINE__);
	return NULL;
}

void mm_net_performance_init(struct mm_net_performance* p)
{
	mm_mailbox_init(&p->mailbox);
	mm_mt_contact_init(&p->mt_contact);
	p->state = ts_closed;

	mm_mailbox_assign_callback(&p->mailbox, s_proxy_curl::json_data_rq_msg_id, &__static_net_performance_mailbox_handle_10086);
	mm_mailbox_assign_native(&p->mailbox, "127.0.0.1", 55521);
	mm_mailbox_assign_context(&p->mailbox, p);
	mm_mailbox_set_length(&p->mailbox, MM_NET_PERFORMANCE_THREAD_NUMBER);

	struct mm_mt_tcp* mt_tcp = mm_mt_contact_add(&p->mt_contact, 1000, "0.0.0.0", "127.0.0.1", 55521);
	mm_mt_tcp_assign_callback(mt_tcp, s_proxy_curl::json_data_rs_msg_id, &__static_net_performance_mailbox_handle_10087);
	mm_mt_tcp_assign_context(mt_tcp, p);
}
void mm_net_performance_destroy(struct mm_net_performance* p)
{
	mm_mailbox_destroy(&p->mailbox);
	mm_mt_contact_destroy(&p->mt_contact);
	p->state = ts_closed;
}
void mm_net_performance_start(struct mm_net_performance* p)
{
	mm_mailbox_fopen_socket(&p->mailbox);
	mm_mailbox_bind(&p->mailbox);
	mm_mailbox_listen(&p->mailbox);
	mm_mailbox_start(&p->mailbox);
	mm_mt_contact_start(&p->mt_contact);

	p->state = ts_motion;
	for (int i = 0;i< MM_NET_PERFORMANCE_THREAD_NUMBER;++i)
	{
		pthread_create(&p->accept_thread[i], NULL, &__static_net_performance_task, p);
	}
}
void mm_net_performance_interrupt(struct mm_net_performance* p)
{
	mm_mailbox_interrupt(&p->mailbox);
	mm_mt_contact_interrupt(&p->mt_contact);
	p->state = ts_closed;
}
void mm_net_performance_shutdown(struct mm_net_performance* p)
{
	mm_mailbox_shutdown(&p->mailbox);
	mm_mt_contact_shutdown(&p->mt_contact);
	p->state = ts_finish;
}
void mm_net_performance_join(struct mm_net_performance* p)
{
	mm_mailbox_join(&p->mailbox);
	mm_mt_contact_join(&p->mt_contact);

	for (int i = 0;i< MM_NET_PERFORMANCE_THREAD_NUMBER;++i)
	{
		pthread_join(p->accept_thread[i], NULL);
	}
}
