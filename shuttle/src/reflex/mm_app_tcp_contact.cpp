#include "mm_app_tcp_contact.h"
#include "core/mm_logger.h"
#include "net/mm_default_handle.h"
#include <google/protobuf/message.h>

#include "cxx/protodef/s_proxy_curl.pb.h"

#include "mm_app_json_handle.h"

//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_mm_mt_tcp_handle(void* obj, void* u, struct mm_packet* pack);
static void __static_app_tcp_contact_mm_mt_tcp_broken(void* obj);
static void __static_app_tcp_contact_mm_mt_tcp_nready(void* obj);
static void __static_app_tcp_contact_mm_mt_tcp_finish(void* obj);
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_mq_tcp_default_handle(void* obj, void* u, struct mm_packet* pack);
static void __static_app_tcp_contact_mq_tcp_s_proxy_curl_json_data_rs(void* obj, void* u, struct mm_packet* pack);
//////////////////////////////////////////////////////////////////////////
static int __static_app_tcp_contact_mt_tcp_protobuf_send(struct mm_mt_tcp* mt_tcp, mm_uint64_t uid, mm_uint32_t mid, ::google::protobuf::Message* rq_msg);
static int __static_app_tcp_contact_net_tcp_protobuf_send(struct mm_net_tcp* net_tcp, mm_uint64_t uid, mm_uint32_t mid, ::google::protobuf::Message* rq_msg);
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_json_rs_handle(struct mm_app_tcp_contact* app_tcp_contact, rapidjson::Document* doc, mm::mm_script_json_data_rs* data_rs, mm::mm_script_json_data_result* data_result);
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_handle( void* obj, void* u, rapidjson::Document* doc, mm::mm_script_json_data_rs* rs, mm::mm_script_json_data_result* r )
{

}
void mm_app_tcp_contact_callback_init(struct mm_app_tcp_contact_callback* p)
{
	p->handle = &__static_app_tcp_contact_handle;
	p->obj = NULL;
}
void mm_app_tcp_contact_callback_destroy(struct mm_app_tcp_contact_callback* p)
{
	p->handle = &__static_app_tcp_contact_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_init(struct mm_app_tcp_contact* p)
{
	struct mm_mt_tcp_callback mt_tcp_callback;
	struct mm_mq_tcp_callback queue_tcp_callback;

	mm_mt_tcp_init(&p->mt_tcp);
	mm_mq_tcp_init(&p->mq_tcp);
	mm_rbtree_u32_vpt_init(&p->rbtree);
	mm_app_tcp_contact_callback_init(&p->callback);
	mm_spinlock_init(&p->rbtree_locker, NULL);
	p->u = NULL;

	mt_tcp_callback.handle = &__static_app_tcp_contact_mm_mt_tcp_handle;
	mt_tcp_callback.broken = &__static_app_tcp_contact_mm_mt_tcp_broken;
	mt_tcp_callback.nready = &__static_app_tcp_contact_mm_mt_tcp_nready;
	mt_tcp_callback.finish = &__static_app_tcp_contact_mm_mt_tcp_finish;
	mt_tcp_callback.obj = p;
	mm_mt_tcp_assign_default_callback(&p->mt_tcp, &mt_tcp_callback);
	mm_mt_tcp_assign_context(&p->mt_tcp, p);

	queue_tcp_callback.handle = &__static_app_tcp_contact_mq_tcp_default_handle;
	queue_tcp_callback.obj = p;
	mm_mq_tcp_assign_callback(&p->mq_tcp, s_proxy_curl::json_data_rs_msg_id, &__static_app_tcp_contact_mq_tcp_s_proxy_curl_json_data_rs);
	mm_mq_tcp_assign_queue_tcp_callback(&p->mq_tcp, &queue_tcp_callback);
	mm_mq_tcp_assign_max_poper_number(&p->mq_tcp, MM_APP_TCP_CONTACT_MAX_POPER_NUMBER);
	mm_mq_tcp_assign_context(&p->mq_tcp, p);
}
void mm_app_tcp_contact_destroy(struct mm_app_tcp_contact* p)
{
	mm_mt_tcp_destroy(&p->mt_tcp);
	mm_mq_tcp_destroy(&p->mq_tcp);
	mm_rbtree_u32_vpt_destroy(&p->rbtree);
	mm_app_tcp_contact_callback_destroy(&p->callback);
	mm_spinlock_destroy(&p->rbtree_locker);
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_assign_native(struct mm_app_tcp_contact* p, const char* node, mm_ushort_t port)
{
	mm_mt_tcp_assign_native(&p->mt_tcp, node, port);
}
void mm_app_tcp_contact_assign_remote(struct mm_app_tcp_contact* p, const char* node, mm_ushort_t port)
{
	mm_mt_tcp_assign_remote(&p->mt_tcp, node, port);
}
//
void mm_app_tcp_contact_assign_context(struct mm_app_tcp_contact* p, void* u)
{
	p->u = u;
}
//
void mm_app_tcp_contact_assign_callback(struct mm_app_tcp_contact* p, int id, app_tcp_contact_handle callback)
{
	mm_spinlock_lock(&p->rbtree_locker);
	mm_rbtree_u32_vpt_set(&p->rbtree, id, (void*)callback);
	mm_spinlock_unlock(&p->rbtree_locker);
}
void mm_app_tcp_contact_assign_contact_callback(struct mm_app_tcp_contact* p, struct mm_app_tcp_contact_callback* contact_callback)
{
	assert(NULL != contact_callback && "you can not assign null contact_callback.");
	p->callback = *contact_callback;
}
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_thread_handle(struct mm_app_tcp_contact* p)
{
	mm_mq_tcp_thread_handle(&p->mq_tcp);
}
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_buffer_send(struct mm_app_tcp_contact* p, mm_uint64_t uid, mm::mm_script_json_data_rq* rq, const char* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	struct mm_logger* g_logger = mm_logger_instance();
	rapidjson::Document runtime_doc(rapidjson::kObjectType);
	if (runtime_doc.Parse((const char*)(buffer + offset), length).HasParseError())
	{
		mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc: %s",__FUNCTION__,__LINE__, (const char*)(buffer + offset));
	}
	else
	{
		rq->user_data.decode(runtime_doc, runtime_doc);

		rapidjson::StringBuffer string_buffer;
		rapidjson::Document _jc(rapidjson::kObjectType, &runtime_doc.GetAllocator());
		rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);

		rq->encode(_jc, _jc);
		string_buffer.Clear();
		_writer.Reset(string_buffer);
		_jc.Accept(_writer);

		s_proxy_curl::json_data_rq rq_msg;
		rq_msg.set_buffer(string_buffer.GetString(), string_buffer.GetSize());
		__static_app_tcp_contact_mt_tcp_protobuf_send(&p->mt_tcp, uid, s_proxy_curl::json_data_rq_msg_id, &rq_msg);
	}
}
//////////////////////////////////////////////////////////////////////////
void mm_app_tcp_contact_start(struct mm_app_tcp_contact* p)
{
	mm_mt_tcp_start(&p->mt_tcp);
}
void mm_app_tcp_contact_interrupt(struct mm_app_tcp_contact* p)
{
	mm_mt_tcp_interrupt(&p->mt_tcp);
}
void mm_app_tcp_contact_shutdown(struct mm_app_tcp_contact* p)
{
	mm_mt_tcp_shutdown(&p->mt_tcp);
}
void mm_app_tcp_contact_join(struct mm_app_tcp_contact* p)
{
	mm_mt_tcp_join(&p->mt_tcp);
}
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_mm_mt_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{
	struct mm_app_tcp_contact* app_tcp_contact = (struct mm_app_tcp_contact*)(u);
	mm_mq_tcp_push(&app_tcp_contact->mq_tcp, obj, pack);
}
static void __static_app_tcp_contact_mm_mt_tcp_broken(void* obj)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_logger* g_logger = mm_logger_instance();
	mm_socket_string(&tcp->socket, link_name);
	mm_logger_log_W(g_logger, "%s %d tcp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
static void __static_app_tcp_contact_mm_mt_tcp_nready(void* obj)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_logger* g_logger = mm_logger_instance();
	mm_socket_string(&tcp->socket, link_name);
	mm_logger_log_W(g_logger, "%s %d tcp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
static void __static_app_tcp_contact_mm_mt_tcp_finish(void* obj)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_logger* g_logger = mm_logger_instance();
	mm_socket_string(&tcp->socket, link_name);
	mm_logger_log_I(g_logger, "%s %d tcp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_mq_tcp_default_handle(void* obj, void* u, struct mm_packet* pack)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_logger* g_logger = mm_logger_instance();
	mm_socket_string(&tcp->socket, link_name);
	mm_logger_log_W(g_logger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
static void __static_app_tcp_contact_mq_tcp_s_proxy_curl_json_data_rs(void* obj, void* u, struct mm_packet* rs_pack)
{
	char socket_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_app_tcp_contact* app_tcp_contact = (struct mm_app_tcp_contact*)(u);
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_string proto_desc;
	mm_socket_string(&tcp->socket, socket_name);
	mm_string_init(&proto_desc);
	s_proxy_curl::json_data_rs rs;
	if (0 == mm_protobuf_cxx_decode_message(rs_pack, &rs))
	{
		mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rs_pack, &rs );
		mm_logger_log_T(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
		// logic.
		do 
		{
			const std::string& buffer = rs.buffer();
			if (200 != rs.curl_code())
			{
				mm_logger_log_E(g_logger,"%s %d curl error(%d): %s.",__FUNCTION__,__LINE__,rs.curl_code(),buffer.c_str());
				break;
			}
			mm::mm_script_json_data_rs data_rs;
			rapidjson::Document runtime_doc(rapidjson::kObjectType);
			if (runtime_doc.Parse((const char*)(buffer.data()),buffer.size()).HasParseError())
			{
				mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc: %s.",__FUNCTION__,__LINE__,buffer.c_str());
				break;
			}
			data_rs.decode(runtime_doc,runtime_doc);
			//
			typedef mm::mm_script_json_data_rs::script_json_data_result_list_type script_json_data_result_list_type;
			script_json_data_result_list_type& results = data_rs.result;
			for (script_json_data_result_list_type::iterator it = results.begin();
				it != results.end();++it)
			{
				__static_app_tcp_contact_json_rs_handle(app_tcp_contact, &runtime_doc, &data_rs, &(*it));
			}
			//
		} while (0);
	}
	else
	{
		mm_protobuf_cxx_logger_append_packet( &proto_desc, rs_pack );
		mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	}
	mm_string_destroy(&proto_desc);
}
//////////////////////////////////////////////////////////////////////////
static int __static_app_tcp_contact_mt_tcp_protobuf_send(struct mm_mt_tcp* mt_tcp, mm_uint64_t uid, mm_uint32_t mid, ::google::protobuf::Message* rq_msg)
{
	int code = MM_UNKNOWN;
	do 
	{
		struct mm_net_tcp* net_tcp = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		mm_mt_tcp_lock(mt_tcp);
		net_tcp = mm_mt_tcp_thread_instance(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
		if (NULL == net_tcp)
		{
			// system not enough memory.
			mm_logger_log_E(g_logger, "%s %d mid:0x%08X uid:%" PRIu64 " system not enough memory.", __FUNCTION__, __LINE__, mid, uid);
			code = MM_ENOBUFS;
			break;
		}
		code = __static_app_tcp_contact_net_tcp_protobuf_send(net_tcp, uid, mid, rq_msg);
	} while (0);
	return code;
}
static int __static_app_tcp_contact_net_tcp_protobuf_send(struct mm_net_tcp* net_tcp, mm_uint64_t uid, mm_uint32_t mid, ::google::protobuf::Message* rq_msg)
{
	int code = MM_UNKNOWN;
	do 
	{
		struct mm_packet rq_pack;
		struct mm_logger* g_logger = mm_logger_instance();
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_check(net_tcp);
		mm_net_tcp_unlock(net_tcp);
		if (0 != mm_net_tcp_finally_state(net_tcp))
		{
			char link_name[MM_LINK_NAME_LENGTH];
			struct mm_tcp* tcp = (struct mm_tcp*)(&net_tcp->tcp);
			mm_socket_string(&tcp->socket, link_name);
			// net connect failure.
			mm_logger_log_E(g_logger, "%s %d mid:0x%08X uid:%" PRIu64 " net_tcp %s connect failure.", __FUNCTION__, __LINE__, mid, uid, link_name);
			code = MM_ENOTCONN;
			break;
		}
		mm_net_tcp_lock(net_tcp);
		mm_protobuf_cxx_q_tcp_append_rq( net_tcp, uid, mid, rq_msg, &rq_pack );
		mm_protobuf_cxx_n_net_tcp_flush_send( net_tcp );
		mm_net_tcp_unlock(net_tcp);
		// logger
		{
			struct mm_string proto_desc;
			mm_string_init(&proto_desc);

			mm_protobuf_cxx_logger_append_packet_message( &proto_desc, &rq_pack, rq_msg );
			mm_logger_log_T(g_logger, "%s %d nt rs:%s", __FUNCTION__, __LINE__, proto_desc.s);

			mm_string_destroy(&proto_desc);
		}
		//
		code = MM_SUCCESS;
	} while (0);
	return code;
}
//////////////////////////////////////////////////////////////////////////
static void __static_app_tcp_contact_json_rs_handle(struct mm_app_tcp_contact* app_tcp_contact, rapidjson::Document* doc, mm::mm_script_json_data_rs* data_rs, mm::mm_script_json_data_result* data_result)
{
	app_tcp_contact_handle handle = NULL;
	mm_spinlock_lock(&app_tcp_contact->rbtree_locker);
	handle = (app_tcp_contact_handle)mm_rbtree_u32_vpt_get(&app_tcp_contact->rbtree, data_result->broadcast);
	mm_spinlock_unlock(&app_tcp_contact->rbtree_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(app_tcp_contact, app_tcp_contact->u, doc, data_rs, data_result);
	}
	else
	{
		assert(app_tcp_contact->callback.handle&&"app_tcp_contact->handle is a null.");
		(*(app_tcp_contact->callback.handle))(app_tcp_contact, app_tcp_contact->u, doc, data_rs, data_result);
	}
}
//////////////////////////////////////////////////////////////////////////
