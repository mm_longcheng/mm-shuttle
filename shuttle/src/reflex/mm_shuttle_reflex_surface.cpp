#include "mm_shuttle_reflex_surface.h"
#include "core/mm_logger.h"
#include "core/mm_time_cache.h"
#include "core/mm_runtime_stat.h"
#include "core/mm_byte.h"
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_handle_event_1( void* obj, void* u, struct mm_packet* pack )
{
	// upload
}
static void __static_mailbox_handle_event_2( void* obj, void* u, struct mm_packet* pack )
{
	// enter.
}
static void __static_mailbox_handle_event_3( void* obj, void* u, struct mm_packet* pack )
{
	// leave.
}
static void __static_mailbox_handle_event_4( void* obj, void* u, struct mm_packet* pack )
{
	// finish.
}
//////////////////////////////////////////////////////////////////////////
static void* __static_shuttle_reflex_surface_notify_task(void* arg)
{
	struct mm_shuttle_reflex_surface* p = (struct mm_shuttle_reflex_surface*)(arg);
	struct mm_logger* g_logger = mm_logger_instance();
	while(ts_motion == p->state)
	{
		// real msleep time is 50 - cast time.
		mm_msleep(20);
		for (int i = 0;i<10000;i++)
		{
			// notify op set.
			double a = 8454.0;
			double b = 554645.0;
			double c = a * b;
		}
		mm_logger_log_I(g_logger,"%s %d thread:%d.",__FUNCTION__,__LINE__,mm_current_thread_id());
	}
	return NULL;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_surface_init(struct mm_shuttle_reflex_surface* p)
{
	mm_mailbox_init(&p->mailbox);
	p->unique_id = 0;
	p->frame_number = 0;
	p->state = ts_closed;
}
void mm_shuttle_reflex_surface_destroy(struct mm_shuttle_reflex_surface* p)
{
	mm_mailbox_destroy(&p->mailbox);
	p->unique_id = 0;
	p->frame_number = 0;
	p->state = ts_closed;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_surface_start(struct mm_shuttle_reflex_surface* p)
{
	//////////////////////////////////////////////////////////////////////////
	mm_mailbox_assign_callback(&p->mailbox, 1,&__static_mailbox_handle_event_1);
	mm_mailbox_assign_callback(&p->mailbox, 2,&__static_mailbox_handle_event_2);
	//////////////////////////////////////////////////////////////////////////
	mm_mailbox_assign_context(&p->mailbox, p);
	mm_mailbox_assign_native(&p->mailbox, "::", 0);
	mm_mailbox_set_length(&p->mailbox, 1);
	mm_mailbox_fopen_socket(&p->mailbox);
	mm_mailbox_bind(&p->mailbox);
	mm_mailbox_listen(&p->mailbox);
	mm_mailbox_start(&p->mailbox);
	//
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->notify_thread, NULL, &__static_shuttle_reflex_surface_notify_task, p);
}
void mm_shuttle_reflex_surface_interrupt(struct mm_shuttle_reflex_surface* p)
{
	p->state = ts_closed;
	mm_mailbox_interrupt(&p->mailbox);
}
void mm_shuttle_reflex_surface_shutdown(struct mm_shuttle_reflex_surface* p)
{
	p->state = ts_finish;
	mm_mailbox_shutdown(&p->mailbox);
}
void mm_shuttle_reflex_surface_join(struct mm_shuttle_reflex_surface* p)
{
	mm_mailbox_join(&p->mailbox);
	pthread_join(p->notify_thread, NULL);
}
//////////////////////////////////////////////////////////////////////////
