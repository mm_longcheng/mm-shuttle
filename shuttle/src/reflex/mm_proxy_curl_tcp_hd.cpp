#include "mm_proxy_curl_tcp_hd.h"
#include "curl/mm_curl_unit.h"
#include "net/mm_streambuf_packet.h"

#include "c/protodef/s_proxy_curl.pb-c.h"
#include "cxx/protodef/s_proxy_curl.pb.h"
//////////////////////////////////////////////////////////////////////////
void mm_curl_packet_data_encode(struct mm_curl_unit* unit, struct mm_packet* pack)
{
	struct mm_packet_head phead;
	struct mm_byte_buffer hbuff;
	struct mm_streambuf* streambuf = &unit->buff_send;
	mm_streambuf_aligned_memory(streambuf, pack->hbuff.length);
	hbuff.length = pack->hbuff.length;
	hbuff.buffer = streambuf->buff;
	hbuff.offset = streambuf->pptr;
	mm_packet_head_encode(pack, &hbuff, &phead);
	mm_streambuf_pbump(streambuf, hbuff.length);
}
void mm_curl_packet_data_decode(struct mm_curl_unit* unit, struct mm_packet* pack)
{
	struct mm_packet_head phead;
	struct mm_byte_buffer hbuff;
	struct mm_streambuf* streambuf = &unit->buff_send;
	pack->hbuff.length = mm_streambuf_size(streambuf);
	hbuff.length = pack->hbuff.length;
	hbuff.buffer = streambuf->buff;
	hbuff.offset = streambuf->gptr;
	mm_packet_head_decode(pack, &hbuff, &phead);
	mm_streambuf_gbump(streambuf, hbuff.length);
}
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_tcp_assign_handle( struct mm_mailbox* mailbox )
{
	mm_mailbox_assign_callback(mailbox, s_proxy_curl::json_data_rq_msg_id, &mm_proxy_curl_s_proxy_curl_json_data_rq);
}
