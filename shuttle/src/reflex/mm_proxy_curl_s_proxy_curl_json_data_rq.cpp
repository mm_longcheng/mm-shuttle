#include "mm_proxy_curl_tcp_hd.h"
#include "mm_proxy_curl.h"
#include "core/mm_logger.h"
#include "protobuf/mm_protobuff_cxx.h"

#include "cxx/protodef/s_proxy_curl.pb.h"

static void __static_curl_json_data(struct mm_proxy_curl* impl, struct mm_curl_unit* unit, const std::string& buffer)
{
	const char* params_json = buffer.c_str();
	CURL* curl = unit->curl;
	struct mm_proxy_curl_config* proxy_curl_config = &impl->proxy_curl_config;
	struct mm_logger* g_logger = mm_logger_instance();

	struct mm_string url;
	struct mm_string params;
	mm_string_init(&url);
	mm_string_init(&params);

	mm_string_sprintf(&url, "http://%s:%u/%s", proxy_curl_config->node.s, proxy_curl_config->port, proxy_curl_config->index_file.s);
	mm_string_sprintf(&params, "data=&method=%s&params=%s", proxy_curl_config->method_name.s, params_json);

	mm_logger_log_T(g_logger,"%s %d unit->id:%u params:%s",__FUNCTION__,__LINE__,unit->id, params.s);

	curl_easy_setopt(curl, CURLOPT_URL, url.s);
	curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, params.s);

	mm_string_destroy(&url);
	mm_string_destroy(&params);
}

void mm_proxy_curl_s_proxy_curl_json_data_rq( void* obj, void* u, struct mm_packet* rq_pack )
{
	char socket_name[MM_LINK_NAME_LENGTH];
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_proxy_curl* impl = (struct mm_proxy_curl*)u;
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_string proto_desc;
	mm_socket_string(&tcp->socket, socket_name);
	mm_string_init(&proto_desc);
	s_proxy_curl::json_data_rq rq;
	if (0 == mm_protobuf_cxx_decode_message(rq_pack, &rq))
	{
		rq_pack->phead.pid = tcp->socket.socket;
		mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rq_pack, &rq );
		// logic.
		struct mm_curl_unit* unit = mm_curl_unit_manager_produce(&impl->curl_unit_manager);
		//
		mm_logger_log_T(g_logger,"%s %d nt unit->id:%u rq:%s",__FUNCTION__,__LINE__,unit->id,proto_desc.s);
		//
		mm_curl_unit_lock(unit);
		mm_curl_unit_assign_callback(unit, &impl->curl_unit_callback);
		mm_curl_unit_assign_context(unit, impl);
		mm_curl_packet_data_encode(unit, rq_pack);
		__static_curl_json_data(impl, unit,rq.buffer());
		mm_curl_unit_unlock(unit);
		mm_curl_unit_manager_process_handle(&impl->curl_unit_manager, unit);
	}
	else
	{
		mm_protobuf_cxx_logger_append_packet( &proto_desc, rq_pack );
		mm_logger_log_W(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	}
	mm_string_destroy(&proto_desc);
}
