#ifndef __mm_shuttle_test_curl_h__
#define __mm_shuttle_test_curl_h__

#include "core/mm_core.h"

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"

#include "mm_app_json_handle.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
extern void __static_shuttle_reflex_app_tcp_contact_handle( void* obj, void* u, rapidjson::Document* doc, mm::mm_script_json_data_rs* rs, mm::mm_script_json_data_result* r );
extern void __static_shuttle_reflex_redis_poper_callback(struct mm_redis_poper* p,void* u,struct mm_string* data);

extern void mm_shuttle_test_curl_func();
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_test_curl_h__