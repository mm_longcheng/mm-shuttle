#include "mm_proxy_curl.h"
#include "core/mm_logger.h"
#include "core/mm_time_cache.h"
#include "core/mm_runtime_stat.h"
#include "core/mm_byte.h"
#include "core/mm_thread.h"

#include "container/mm_list_vpt.h"

#include "curl/curl.h"

#include "mm_proxy_curl_tcp_hd.h"
#include "mm_proxy_curl_curl_unit_handle.h"

//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_config_init(struct mm_proxy_curl_config* p)
{
	mm_string_init(&p->node);
	mm_string_init(&p->index_file);
	mm_string_init(&p->method_name);
	p->port = 0;
	//
	mm_string_assigns(&p->node, "");
	mm_string_assigns(&p->index_file, "");
	mm_string_assigns(&p->method_name, "");
}
void mm_proxy_curl_config_destroy(struct mm_proxy_curl_config* p)
{
	mm_string_destroy(&p->node);
	mm_string_destroy(&p->index_file);
	mm_string_destroy(&p->method_name);
	p->port = 0;
}
void mm_proxy_curl_config_assign_config(struct mm_proxy_curl_config* p, const char* index, const char* method)
{
	mm_string_assigns(&p->index_file, index);
	mm_string_assigns(&p->method_name, method);
}
void mm_proxy_curl_config_assign_remote(struct mm_proxy_curl_config* p, const char* node, mm_ushort_t port)
{
	mm_string_assigns(&p->node, node);
	p->port = port;
}
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_init(struct mm_proxy_curl* p)
{
	mm_curl_unit_manager_init(&p->curl_unit_manager);
	mm_mailbox_init(&p->mailbox);
	mm_curl_unit_callback_init(&p->curl_unit_callback);
	mm_proxy_curl_config_init(&p->proxy_curl_config);
	//
	p->curl_unit_callback.handle = &mm_proxy_curl_curl_unit_handle;
	p->curl_unit_callback.obj = p;
	//
	mm_mailbox_assign_context(&p->mailbox, p);
	//
	mm_proxy_curl_config_assign_config(&p->proxy_curl_config, "index.php", "rcec.index");
}
void mm_proxy_curl_destroy(struct mm_proxy_curl* p)
{
	mm_curl_unit_manager_destroy(&p->curl_unit_manager);
	mm_mailbox_destroy(&p->mailbox);
	mm_curl_unit_callback_destroy(&p->curl_unit_callback);
	mm_proxy_curl_config_destroy(&p->proxy_curl_config);
}
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_assign_native(struct mm_proxy_curl* p, const char* info)
{
	char addr_name[MM_ADDR_NAME_LENGTH] = {0};
	char node[MM_NODE_NAME_LENGTH] = {0};
	mm_ushort_t port = 0;
	mm_strcpy(addr_name, info);
	mm_sockaddr_format_decode_string( addr_name, node, &port );
	mm_mailbox_assign_native(&p->mailbox, node, port);
}
void mm_proxy_curl_assign_remote(struct mm_proxy_curl* p, const char* info)
{
	char addr_name[MM_ADDR_NAME_LENGTH] = {0};
	char node[MM_NODE_NAME_LENGTH] = {0};
	mm_ushort_t port = 0;
	mm_strcpy(addr_name, info);
	mm_sockaddr_format_decode_string( addr_name, node, &port );
	mm_proxy_curl_config_assign_remote(&p->proxy_curl_config, node, port);
}

void mm_proxy_curl_assign_native_length(struct mm_proxy_curl* p, mm_uint32_t length)
{
	mm_mailbox_set_length(&p->mailbox, length);
}
void mm_proxy_curl_assign_remote_length(struct mm_proxy_curl* p, mm_uint32_t length)
{
	mm_curl_unit_manager_set_length(&p->curl_unit_manager, length);
}
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_start(struct mm_proxy_curl* p)
{
	mm_proxy_curl_tcp_assign_handle(&p->mailbox);
	//
	mm_mailbox_fopen_socket(&p->mailbox);
	mm_mailbox_bind(&p->mailbox);
	mm_mailbox_listen(&p->mailbox);
	mm_mailbox_start(&p->mailbox);
	//
	mm_curl_unit_manager_start(&p->curl_unit_manager);
}
void mm_proxy_curl_interrupt(struct mm_proxy_curl* p)
{
	mm_curl_unit_manager_interrupt(&p->curl_unit_manager);
	mm_mailbox_interrupt(&p->mailbox);
}
void mm_proxy_curl_shutdown(struct mm_proxy_curl* p)
{
	mm_curl_unit_manager_shutdown(&p->curl_unit_manager);
	mm_mailbox_shutdown(&p->mailbox);
}
void mm_proxy_curl_join(struct mm_proxy_curl* p)
{
	mm_curl_unit_manager_join(&p->curl_unit_manager);
	mm_mailbox_join(&p->mailbox);
}
//////////////////////////////////////////////////////////////////////////
