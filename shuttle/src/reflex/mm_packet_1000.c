#include "mm_packet_1000.h"
#include "core/mm_byteswap.h"

void mm_packet_head_mid_decode(struct mm_byte_buffer* hbuff, mm_uint32_t* mid_h, mm_uint32_t* mid_n)
{
	mm_memcpy(mid_n, (void*)(hbuff->buffer + hbuff->offset), sizeof(mm_uint32_t));
	*mid_h = mm_ltoh32(*mid_n);
}
void mm_packet_head_mid_encode(struct mm_byte_buffer* hbuff, mm_uint32_t* mid_h, mm_uint32_t* mid_n)
{
	*mid_n = mm_htol32(*mid_h);
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset), mid_n, sizeof(mm_uint32_t));
}

void mm_packet_head_11_decode(struct mm_packet_head_11* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_11* phead)
{
	mm_memcpy(&phead->sid, (void*)(hbuff->buffer + hbuff->offset + 4), 2);
	mm_memcpy(&phead->res, (void*)(hbuff->buffer + hbuff->offset + 6), 2);
	mm_memcpy(&phead->tag, (void*)(hbuff->buffer + hbuff->offset + 8), 1);
	// 
	p->sid = mm_ltoh16(phead->sid);
	p->res = mm_ltoh16(phead->res);
	p->tag = mm_ltoh08(phead->tag);
}
void mm_packet_head_11_encode(struct mm_packet_head_11* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_11* phead)
{
	phead->sid = mm_htol16(p->sid);
	phead->res = mm_htol16(p->res);
	phead->tag = mm_htol08(p->tag);
	//
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset + 4), &phead->sid, 2);
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset + 6), &phead->res, 2);
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset + 8), &phead->tag, 1);
}


void mm_packet_head_22_decode(struct mm_packet_head_22* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_22* phead)
{
	mm_memcpy(phead, (void*)(hbuff->buffer + hbuff->offset), hbuff->length);
	p->sid = mm_ltoh32(phead->sid);
	p->lid = mm_ltoh32(phead->lid);
	p->pid = mm_ltoh32(phead->pid);
	p->uid = mm_ltoh64(phead->uid);
}
void mm_packet_head_22_encode(struct mm_packet_head_22* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_22* phead)
{
	phead->sid = mm_htol32(p->sid);
	phead->lid = mm_htol32(p->lid);
	phead->pid = mm_htol32(p->pid);
	phead->uid = mm_htol64(p->uid);
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset), phead, hbuff->length);
}

void mm_packet_head_33_decode(struct mm_packet_head_33* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_33* phead)
{
	mm_memcpy(&phead->uid, (void*)(hbuff->buffer + hbuff->offset + 4), 2);
	// 
	p->uid = mm_ltoh64(phead->uid);
}
void mm_packet_head_33_encode(struct mm_packet_head_33* p, struct mm_byte_buffer* hbuff, struct mm_packet_head_33* phead)
{
	phead->uid = mm_htol64(p->uid);
	//
	mm_memcpy((void*)(hbuff->buffer + hbuff->offset + 4), &phead->uid, 2);
}