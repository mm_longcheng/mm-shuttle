#ifndef __mm_shuttle_reflex_h__
#define __mm_shuttle_reflex_h__

#include "mm_app_tcp_contact.h"
#include "mm_buffer_queue.h"

#include <map>
#include <list>
#include <set>

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_timer.h"

#include "net/mm_mailbox.h"
#include "net/mm_net_tcp.h"

#include "container/mm_rbtree_visible.h"
#include "curl/mm_curl_unit_manager.h"
#include "mm_proxy_curl.h"

#include "redis/mm_redis_poper_array.h"
#include "mm_net_performance.h"
//////////////////////////////////////////////////////////////////////////

static const int thread_number = 1;
struct mm_shuttle_reflex
{
	struct mm_net_performance net_performance;
	mm::mm_buffer_queue bq;
	struct mm_redis_poper_array poper_array;

	struct mm_app_tcp_contact app_tcp_contact;
	struct mm_proxy_curl proxy_curl;
	struct mm_mailbox mailbox;
	struct mm_net_tcp net_tcp;
	pthread_t accept_thread[thread_number];
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_init(struct mm_shuttle_reflex* p);
void mm_shuttle_reflex_destroy(struct mm_shuttle_reflex* p);
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_assign_addr_dbmysql(struct mm_shuttle_reflex* p,const char* info);
void mm_shuttle_reflex_assign_unique_id(struct mm_shuttle_reflex* p,mm_uint32_t unique_id);
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_reflex_start(struct mm_shuttle_reflex* p);
void mm_shuttle_reflex_interrupt(struct mm_shuttle_reflex* p);
void mm_shuttle_reflex_shutdown(struct mm_shuttle_reflex* p);
void mm_shuttle_reflex_join(struct mm_shuttle_reflex* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_reflex_h__