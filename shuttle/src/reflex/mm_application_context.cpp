﻿#include "mm_application_context.h"
#include "core/mm_context.h"
#include "core/mm_time_cache.h"
#include "core/mm_logger_manager.h"
#include "core/mm_os_context.h"
//#include "core/sox/logger.h"

namespace mm
{
	static void __static_application_context_console_printf(const char* section, const char* time_stamp_string, int lvl, const char* message)
	{
		mm_os_context_logger_console(section, time_stamp_string, lvl, message);
		// [1992/01/26 09:13:14-520 8 V ]
		//const struct mm_level_mark* mark = mm_logger_level_mark(lvl);
		// mm_printf("%s %d %s %s\n",time_stamp_string,lvl,mark->m.s,message);
		// android logger time_stamp_string self.
		//switch (lvl)
		//{
		//case MM_LOG_UNKNOW:
		//	sox::log(Info, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_FATAL:
		//	sox::log(Godx, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_CRIT:
		//	sox::log(Godx, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_ERROR:
		//	sox::log(Error, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_ALERT:
		//	sox::log(Error, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_WARNING:
		//	sox::log(Warn, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_NOTICE:
		//	sox::log(Info, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_INFO:
		//	sox::log(Info, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_TRACE:
		//	sox::log(Debug, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_DEBUG:
		//	sox::log(Debug, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//case MM_LOG_VERBOSE:
		//	sox::log(Debug, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//default:
		//	sox::log(Info, section, " ", lvl, " ", mark->m.s, " ", message);
		//	break;
		//}
	}

	// application context is use for lazy init mm core lib.
	mm_application_context::mm_application_context()
	{

	}
	mm_application_context::~mm_application_context()
	{

	}
	void mm_application_context::launching()
	{
		this->init();
		this->start();
	}
	void mm_application_context::terminate()
	{
		this->shutdown();
		this->join();
		this->destroy();
	}
	void mm_application_context::init()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();

		mm_context_init(g_context);
		mm_time_cache_init(g_time_cache);
		//
		struct mm_logger_manager_callback logger_manager_callback;
		logger_manager_callback.console_printf = &__static_application_context_console_printf;
		logger_manager_callback.obj = g_logger_manager;
		mm_logger_manager_assign_callback(g_logger_manager, &logger_manager_callback);
		g_logger_manager->is_console = 1;
		g_logger_manager->is_immediately = 1;
		//
		mm_logger_log_I(g_logger, "%s %d.", __FUNCTION__, __LINE__);
	}
	void mm_application_context::destroy()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_logger_log_I(g_logger, "%s %d.", __FUNCTION__, __LINE__);
		//
		mm_time_cache_destroy(g_time_cache);
		mm_context_destroy(g_context);
	}
	void mm_application_context::start()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_logger_log_I(g_logger, "%s begin %d.", __FUNCTION__, __LINE__);
		//
		mm_context_start(g_context);
		mm_time_cache_start(g_time_cache);
		//
		mm_logger_log_I(g_logger, "%s end %d.", __FUNCTION__, __LINE__);
		mm_logger_log_I(g_logger, "%s %d.", __FUNCTION__, __LINE__);
	}
	void mm_application_context::interrupt()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_time_cache_interrupt(g_time_cache);
		mm_context_interrupt(g_context);
		mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
	}
	void mm_application_context::shutdown()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_time_cache_shutdown(g_time_cache);
		mm_context_shutdown(g_context);
		mm_logger_log_I(g_logger, "%s %d.", __FUNCTION__, __LINE__);
	}
	void mm_application_context::join()
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_context* g_context = mm_context_instance();
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_time_cache_join(g_time_cache);
		mm_context_join(g_context);
		mm_logger_log_I(g_logger, "%s %d.", __FUNCTION__, __LINE__);
	}

}