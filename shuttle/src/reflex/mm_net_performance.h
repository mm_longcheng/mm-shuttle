#ifndef __mm_net_performance_h__
#define __mm_net_performance_h__


#include "net/mm_mailbox.h"
#include "net/mm_mt_contact.h"

#include "core/mm_prefix.h"
//////////////////////////////////////////////////////////////////////////
#define MM_NET_PERFORMANCE_THREAD_NUMBER 1

struct mm_net_performance
{
	struct mm_mailbox mailbox;
	struct mm_mt_contact mt_contact;
	pthread_t accept_thread[MM_NET_PERFORMANCE_THREAD_NUMBER];
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
void mm_net_performance_init(struct mm_net_performance* p);
void mm_net_performance_destroy(struct mm_net_performance* p);
//////////////////////////////////////////////////////////////////////////
void mm_net_performance_start(struct mm_net_performance* p);
void mm_net_performance_interrupt(struct mm_net_performance* p);
void mm_net_performance_shutdown(struct mm_net_performance* p);
void mm_net_performance_join(struct mm_net_performance* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_net_performance_h__