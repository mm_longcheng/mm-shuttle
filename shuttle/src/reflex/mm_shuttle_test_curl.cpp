#include "mm_shuttle_test_curl.h"

#include "core/mm_string.h"
#include "core/mm_logger.h"
#include "core/mm_os_socket.h"
#include "net/mm_sockaddr.h"
#include <map>
#include "net/mm_mailbox.h"
#include "net/mm_mt_contact.h"
#include "mm_app_json_handle.h"

struct frame_data
{
	struct file_image* image;// weak ref.
	mm_uint64_t frame_number;
	size_t b;
	size_t e;
};
struct file_image
{
	typedef std::map<mm_uint64_t, frame_data> frame_data_map_type;
	FILE* stream;// strong ref.
	frame_data_map_type frame_data_map;
};
//////////////////////////////////////////////////////////////////////////
//static void __static_curl_baidu(struct mm_curl_unit* unit)
//{
//	CURL* curl = unit->curl;
//	curl_easy_setopt(curl, CURLOPT_URL, "http://www.baidu.com");
//	//curl_easy_setopt(curl, CURLOPT_HEADER, 1);
//
//	//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//	//curl_easy_setopt(curl, CURLOPT_CAPATH ,".");
//	//curl_easy_setopt(curl, CURLOPT_CAINFO ,"ov.pem");
//}
//static void __static_curl_datacenter(struct mm_curl_unit* unit)
//{
//	mm_short_t port = 8080;
//	const char* ip = "60.205.151.85";
//	const char* index_file = "index.php";
//	const char* method_name = "rcec.index";
//	const char* params_json = "{\"server_data\":{},\"user_data\":{\"cmd\":\"get_saw_game_detail_rq\",\"uid\":10002457,\"sid\":102492,\"singer_id\":\"10002494\",\"game_id\":100,\"game_type\":10001}}";
//	CURL* curl = unit->curl;
//
//	struct mm_string url;
//	struct mm_string params;
//	mm_string_init(&url);
//	mm_string_init(&params);
//
//	mm_string_sprintf(&url, "http://%s:%u/%s", ip, port, index_file);
//	mm_string_sprintf(&params, "data=&method=%s&params=%s", method_name, params_json);
//
//	curl_easy_setopt(curl, CURLOPT_URL, url.s);
//	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, params.s);
//
//	mm_string_destroy(&url);
//	mm_string_destroy(&params);
//}
//////////////////////////////////////////////////////////////////////////
static void __static_shuttle_reflex_rbtree_visible_handle( void* obj, void* u, mm_uint32_t k, void* v )
{
	struct mm_logger* g_logger = mm_logger_instance();
	mm_logger_log_I(g_logger,"%s %d %d",__FUNCTION__,__LINE__,k);
}

static void __static_mailbox_handle_event_10086( void* obj, void* u, struct mm_packet* pack )
{

}
void func()
{
	struct mm_mailbox mailbox;
	mm_mailbox_init(&mailbox);
	mm_mailbox_assign_native(&mailbox, "127.0.0.1", 56003);
	mm_mailbox_assign_callback(&mailbox, 10086, &__static_mailbox_handle_event_10086);
	mm_mailbox_start(&mailbox);
	mm_mailbox_join(&mailbox);
	mm_mailbox_destroy(&mailbox);
}

void func_0()
{
	struct mm_mt_contact mt_contact;
	mm_mt_contact_init(&mt_contact);
	mm_mt_contact_start(&mt_contact);
	mm_mt_contact_join(&mt_contact);
	mm_mt_contact_destroy(&mt_contact);

	mm_mt_contact_add(&mt_contact, 1000, "0.0.0.0", "127.0.0.1", 56003);

	struct mm_mt_tcp* mt_tcp = mm_mt_contact_get(&mt_contact, 1000);
	if (NULL != mt_tcp)
	{
		struct mm_net_tcp* net_tcp = NULL;
		mm_mt_tcp_lock(mt_tcp);
		net_tcp = mm_mt_tcp_thread_instance(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);

		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_check(net_tcp);
		if (0 == mm_net_tcp_finally_state(net_tcp))
		{

		}
		mm_net_tcp_unlock(net_tcp);
	}
}
void func_2()
{
	struct mm_logger* g_logger = mm_logger_instance();

	mm::mm_script_json_data_rq rq;

	rapidjson::StringBuffer string_buffer;
	rapidjson::Document _jc(rapidjson::kObjectType);
	rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);

	rq.encode(_jc,_jc);
	string_buffer.Clear();
	_writer.Reset(string_buffer);
	_jc.Accept(_writer);
	mm_logger_log_I(g_logger,"%s %d 1:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
}
void func_1()
{
	struct mm_logger* g_logger = mm_logger_instance();
	// {"success":true,"result":[{"broadcast":5,"data":{"uid":10003986,"target_type":8,"num":1,"extra_param":0}},{"broadcast":5,"data":{"uid":10000033,"target_type":20,"num":1,"extra_param":0}},{"broadcast":0,"data":{"cmd":"RTextChat","uid":10003986,"result":0}},{"broadcast":1,"data":{"cmd":"BTextChat","uid":10003986,"sid":100033,"nickname":"\u7528\u623710003986","isguard":0,"guardType":0,"guardName":"","activeLevel":1,"activeLevelEffect":0,"context":"t","identity":2,"official":0,"new":"2"}}],"server_data":{"cid":1,"receiver":"cantus","roler":25,"sender":"\u7528\u623710003986","sid":100033,"uid":10003986,"uid_onmic":10000033,"usercount":127}}
	std::string return_string = "{\"success\":true,\"result\":[{\"broadcast\":5,\"data\":{\"uid\":10003986,\"target_type\":8,\"num\":1,\"extra_param\":0}},{\"broadcast\":5,\"data\":{\"uid\":10000033,\"target_type\":20,\"num\":1,\"extra_param\":0}},{\"broadcast\":0,\"data\":{\"cmd\":\"RTextChat\",\"uid\":10003986,\"result\":0}},{\"broadcast\":1,\"data\":{\"cmd\":\"BTextChat\",\"uid\":10003986,\"sid\":100033,\"nickname\":\"\\u7528\\u623710003986\",\"isguard\":0,\"guardType\":0,\"guardName\":\"\",\"activeLevel\":1,\"activeLevelEffect\":0,\"context\":\"t\",\"identity\":2,\"official\":0,\"new\":\"2\"}}],\"server_data\":{\"cid\":1,\"receiver\":\"cantus\",\"roler\":25,\"sender\":\"\\u7528\\u623710003986\",\"sid\":100033,\"uid\":10003986,\"uid_onmic\":10000033,\"usercount\":127}}";
	mm::mm_script_json_data_rs jdata;
	rapidjson::Document runtime_doc(rapidjson::kObjectType);
	if (runtime_doc.Parse((const char*)(return_string.data()),return_string.size()).HasParseError())
	{
		mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc.",__FUNCTION__,__LINE__);
	}
	jdata.decode(runtime_doc,runtime_doc);

	rapidjson::StringBuffer string_buffer;
	rapidjson::Document _jc(rapidjson::kObjectType);
	rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);

	jdata.encode(_jc,_jc);
	string_buffer.Clear();
	_writer.Reset(string_buffer);
	_jc.Accept(_writer);
	mm_logger_log_I(g_logger,"%s %d 1:%s",__FUNCTION__,__LINE__,return_string.c_str());
	mm_logger_log_I(g_logger,"%s %d 2:%s",__FUNCTION__,__LINE__,string_buffer.GetString());

	func_2();
}
void __static_shuttle_reflex_app_tcp_contact_handle( void* obj, void* u, rapidjson::Document* doc, mm::mm_script_json_data_rs* rs, mm::mm_script_json_data_result* r )
{
	struct mm_logger* g_logger = mm_logger_instance();
	rapidjson::StringBuffer string_buffer;
	rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
	string_buffer.Clear();
	_writer.Reset(string_buffer);
	r->data.data->Accept(_writer);
	mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
	{
		rapidjson::Document _value_data_doc(rapidjson::kObjectType);
		r->data.encode(_value_data_doc, _value_data_doc);
		//
		string_buffer.Clear();
		_writer.Reset(string_buffer);
		_value_data_doc.Accept(_writer);
		mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
	}
	{
		rapidjson::Document _value_data_doc(rapidjson::kObjectType);
		r->data.encode(_value_data_doc, _value_data_doc);
		//
		string_buffer.Clear();
		_writer.Reset(string_buffer);
		_value_data_doc.Accept(_writer);
		mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
	}
	{
		struct mm_app_tcp_contact* app_tcp_contact = (struct mm_app_tcp_contact*)(obj);
		rapidjson::Document document(rapidjson::kObjectType, &doc->GetAllocator());
		rapidjson::StringBuffer string_buffer;
		rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);

		rapidjson::Value _value(rapidjson::kObjectType);
		_value.CopyFrom(*r->data.data, document.GetAllocator());
		document.AddMember(rapidjson::StringRef("data"), _value, document.GetAllocator());

		mm::mm_json_coder::encode( document, document, "type", (mm_uint32_t)0 );
		mm::mm_json_coder::encode( document, document, "all", true );

		string_buffer.Clear();
		_writer.Reset(string_buffer);
		document.Accept(_writer);

		//rapidjson::Value _value(rapidjson::kObjectType, document.GetAllocator());

		rapidjson::Value _v;
		_v.CopyFrom(*r->data.data, document.GetAllocator());

		{
			rapidjson::Document document(rapidjson::kObjectType, &doc->GetAllocator());
			mm::mm_script_json_data_result r0;
			mm::mm_script_json_data_result r1;
			r0.encode(document, document);
			r1.encode(document, document);
		}

		mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());

		{
			const char* a = "{\"success\":true,\"result\":[{\"broadcast\":0,\"data\":{\"cmd\":\"RCreateStream\",\"uid\":10003986,\"url\":\"\",\"audio_url\":\"\",\"token\":\"\",\"stream\":\"\",\"result\":0}},{\"broadcast\":9,\"data\":{\"cmd\":\"StreamStarted\"}}],\"server_data\":{\"plugin_type\":7,\"uid\":10003986,\"cid\":1,\"sid\":102536,\"usercount\":4,\"roler\":25,\"uid_onmic\":10003986,\"receiver\":\"\\u7528\\u623710003986\",\"sender\":\"\\u7528\\u623710003986\"}}";
			rapidjson::Document runtime_doc(rapidjson::kObjectType);
			if (runtime_doc.Parse((const char*)(a), strlen(a)).HasParseError())
			{
				mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());
			}
			string_buffer.Clear();
			_writer.Reset(string_buffer);
			runtime_doc.Accept(_writer);
			mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__,string_buffer.GetString());

			mm::mm_script_json_data_rs d;
			d.decode(runtime_doc, runtime_doc);
			{
				//rapidjson::Value vvv(123);
				//rapidjson::Value v23;
				//v23.Set(vvv);

				//{
				//	rapidjson::StringBuffer string_buffer;
				//	rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
				//	string_buffer.Clear();
				//	_writer.Reset(string_buffer);
				//	vvv.Accept(_writer);
				//	mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__, string_buffer.GetString());
				//}
				//{
				//	rapidjson::StringBuffer string_buffer;
				//	rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
				//	string_buffer.Clear();
				//	_writer.Reset(string_buffer);
				//	v23.Accept(_writer);
				//	mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__, string_buffer.GetString());
				//}
			}

			{
				rapidjson::StringBuffer string_buffer;
				rapidjson::Document _jc(rapidjson::kObjectType, &runtime_doc.GetAllocator());
				rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);

				d.encode(_jc,_jc);

				{
					rapidjson::StringBuffer string_buffer;
					rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
					string_buffer.Clear();
					_writer.Reset(string_buffer);
					_jc.Accept(_writer);
					mm_logger_log_I(g_logger,"%s %d buffer:%s",__FUNCTION__,__LINE__, string_buffer.GetString());
				}
			}


			typedef mm::mm_script_json_data_rs::script_json_data_result_list_type script_json_data_result_list_type;
			script_json_data_result_list_type& results = d.result;
			for (script_json_data_result_list_type::iterator it = results.begin();
				it != results.end();++it)
			{
				mm::mm_script_json_data_result& r = *it;

				rapidjson::StringBuffer string_buffer;
				rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
				string_buffer.Clear();
				_writer.Reset(string_buffer);
				r.data.data->Accept(_writer);


				mm_logger_log_I(g_logger,"%s %d %d buffer:%s",__FUNCTION__,__LINE__, r.broadcast, string_buffer.GetString());
			}

			// do not use resize.std vector resize(size_t n, value_type())
			// if value_type contain shared pointer will case bug.
			class BBB
			{
			public:
				BBB()
				{

				}
				~BBB()
				{

				}
			};
			int b =0;
			{
				std::list<BBB> a;
				a.resize(10);
				for (int i = 0;i<10;++i)
				{
					std::list<BBB>::iterator ib = a.insert(a.begin(),BBB());
					BBB& c = *ib;
				}

				std::list<BBB>::iterator ia = a.insert(a.begin(),BBB());

				std::vector<BBB> b;
				b.reserve(10);
				for (int i = 0;i<10;++i)
				{
					std::vector<BBB>::iterator ib = b.insert(b.begin(),BBB());
					BBB& c = *ib;
				}
			}
		}
	}
}
int __sockaddr_string_family( char* addr_name )
{
	int ss_family = MM_AF_UNSPE;
	size_t l = mm_strlen(addr_name);
	size_t i = 0;
	size_t a = 0;// a number
	size_t d = 0;// . number
	size_t m = 0;// : number
	size_t t = MM_ADDR_NAME_LENGTH < l ? MM_ADDR_NAME_LENGTH : l;
	//  l  a  d  m
	// 16 13  3  0      www.google.co.uk localhost
	// 15  0  3  0      192.168.111.123
	// 39  4  0  7      2001:0DB8:0000:0023:0008:0800:200C:417A
	do 
	{
		if ( MM_ADDR_NAME_LENGTH < l )
		{
			ss_family = MM_AF_UNSPE;
			break;
		}
		for ( i = 0; i < t; ++i )
		{
			if (0 != isalpha(addr_name[i]))
			{
				a ++;
			}
			if ( '.' == addr_name[i] )
			{
				d ++;
			}
			if ( ':' == addr_name[i] )
			{
				m ++;
			}
		}
		if ( 0 < a && 0 == m )
		{
			ss_family = MM_AF_UNSPE;
			break;
		}
		if ( 0 == a && 0 < d && 0 == m )
		{
			ss_family = MM_AF_INET4;
			break;
		}
		if ( 0 == d && 0 < m )
		{
			ss_family = MM_AF_INET6;
			break;
		}
	} while (0);
	return ss_family;
}

void __static_shuttle_reflex_redis_poper_callback(struct mm_redis_poper* p,void* u,struct mm_string* data)
{
	struct mm_logger* g_logger = mm_logger_instance();
	mm_logger_log_I(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,data->s);
}

void mm_shuttle_test_curl_func()
{

}
