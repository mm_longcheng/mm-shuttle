#ifndef __mm_app_json_handle_h__
#define __mm_app_json_handle_h__

#include "dish/mm_json.h"

#include "core/mm_core.h"
#include "milk/MilkSharedPtr.h"

#include "rapidjson/document.h"     // rapidjson's DOM-style API

#include "mm_app_json_type.h"

#include <list>
//////////////////////////////////////////////////////////////////////////
namespace mm
{
	class mm_script_json_data_buffer : public mm_i_json
	{
	public:
		Milk::SharedPtr<rapidjson::Value> data;
	public:
		mm_script_json_data_buffer();
		virtual ~mm_script_json_data_buffer();
	public:
		virtual int decode(rapidjson::Document& _jc,const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc,rapidjson::Value& _jv) const;
	};


	class mm_script_json_data_result : public mm_i_json
	{
	public:
		mm_uint32_t broadcast;
		mm_uint64_t target_uid;
		mm_script_json_data_buffer data;
		mm_script_json_data_buffer uids;
	public:
		mm_script_json_data_result();
	public:
		virtual int decode(rapidjson::Document& _jc,const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc,rapidjson::Value& _jv) const;
	};

	class mm_script_json_data_server_data : public mm_i_json
	{
	public:
		mm_uint32_t plugin_type;
		mm_uint64_t uid;
		mm_uint32_t cid;
		mm_uint32_t sid;
		mm_uint32_t usercount;
		mm_uint32_t roler;
		mm_uint64_t uid_onmic;
		std::string receiver;
		std::string sender;
	public:
		mm_script_json_data_server_data();
	public:
		virtual int decode(rapidjson::Document& _jc,const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc,rapidjson::Value& _jv) const;
	};

	class mm_script_json_data_rs : public mm_i_json
	{
	public:
		typedef std::list<mm_script_json_data_result> script_json_data_result_list_type;
	public:
		bool success;
		script_json_data_result_list_type result;
		mm_script_json_data_server_data server_data;
	public:
		mm_script_json_data_rs();
	public:
		virtual int decode(rapidjson::Document& _jc,const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc,rapidjson::Value& _jv) const;
	};

	class mm_script_json_data_rq : public mm_i_json
	{
	public:
		mm_script_json_data_buffer user_data;
		mm_script_json_data_server_data server_data;
	public:
		mm_script_json_data_rq();
	public:
		virtual int decode(rapidjson::Document& _jc,const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc,rapidjson::Value& _jv) const;
	};
}
//////////////////////////////////////////////////////////////////////////

#endif//__mm_app_json_handle_h__