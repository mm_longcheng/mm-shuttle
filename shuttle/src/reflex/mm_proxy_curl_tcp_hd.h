#ifndef __mm_proxy_curl_tcp_hd_h__
#define __mm_proxy_curl_tcp_hd_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "net/mm_mailbox.h"

//////////////////////////////////////////////////////////////////////////
void mm_curl_packet_data_encode(struct mm_curl_unit* unit, struct mm_packet* pack);
void mm_curl_packet_data_decode(struct mm_curl_unit* unit, struct mm_packet* pack);
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_tcp_assign_handle( struct mm_mailbox* mailbox );
//////////////////////////////////////////////////////////////////////////
void mm_proxy_curl_s_proxy_curl_json_data_rq( void* obj, void* u, struct mm_packet* rq_pack );
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_proxy_curl_tcp_hd_h__