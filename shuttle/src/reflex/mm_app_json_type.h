#ifndef __mm_app_json_type_h__
#define __mm_app_json_type_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

//////////////////////////////////////////////////////////////////////////
enum mm_app_script_json_type 
{
	sjt_return_to_target_user_at_channel = 0,// rs user.
	sjt_broadcast_at_channel             = 1,// broadcast at channel.
	sjt_broadcast_at_room                = 2,// broadcast at room,same as channel.
	sjt_broadcast_at_session             = 3,// broadcast at session(deprecated).
	sjt_broadcast_at_link                = 4,// broadcast at link all user.
	sjt_notify_to_task                   = 5,// notify to task.
	sjt_notify_to_target_user_at_channel = 6,// notify to target user at channel.
	sjt_notify_to_target_user_at_link    = 7,// notify to target user at link.
	sjt_notify_to_multiple_users_at_link = 8,// notify to multiple users at link.
	sjt_notify_to_session_only           = 9,// notify to session only.
	sjt_type_max,
};

//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_app_json_type_h__