#ifndef __mm_shuttle_test_h__
#define __mm_shuttle_test_h__

#include "core/mm_core.h"

#include "core/mm_prefix.h"

struct mm_shuttle_test
{
	mm_uint32_t unique_id;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_init(struct mm_shuttle_test* p);
extern void mm_shuttle_test_destroy(struct mm_shuttle_test* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_assign_unique_id(struct mm_shuttle_test* p,mm_uint32_t unique_id);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_start(struct mm_shuttle_test* p);
extern void mm_shuttle_test_interrupt(struct mm_shuttle_test* p);
extern void mm_shuttle_test_shutdown(struct mm_shuttle_test* p);
extern void mm_shuttle_test_join(struct mm_shuttle_test* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_test_h__