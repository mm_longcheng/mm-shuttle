#ifndef __mm_shuttle_test_address_h__
#define __mm_shuttle_test_address_h__

#include "core/mm_core.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_address_func();
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_test_address_h__