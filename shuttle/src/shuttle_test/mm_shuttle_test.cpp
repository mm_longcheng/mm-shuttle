#include "mm_shuttle_test.h"

#include "core/mm_logger.h"

#include "protobuf/mm_protobuff_cxx.h"

#include <google/protobuf/stubs/common.h>

#include "mm_shuttle_test_ini.h"
#include "mm_shuttle_test_message_lpusher.h"
#include "mm_shuttle_test_mulcast.h"
#include "mm_shuttle_test_openssl.h"
#include "mm_shuttle_test_zookeeper.h"
#include "mm_shuttle_test_parameters.h"
#include "mm_shuttle_test_mid.h"
#include "mm_shuttle_test_redis.h"
#include "mm_shuttle_test_loavger.h"

//////////////////////////////////////////////////////////////////////////
void mm_shuttle_test_init(struct mm_shuttle_test* p)
{
	mm_protobuf_cxx_init();
}
void mm_shuttle_test_destroy(struct mm_shuttle_test* p)
{
	google::protobuf::ShutdownProtobufLibrary();
	mm_protobuf_cxx_destroy();
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_test_assign_unique_id(struct mm_shuttle_test* p,mm_uint32_t unique_id)
{
	p->unique_id = unique_id;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_test_start(struct mm_shuttle_test* p)
{
	mm_shuttle_test_openssl_func();
	mm_shuttle_test_loavger_func();
	mm_shuttle_test_redis_func();
	mm_shuttle_test_parameters_func();
	mm_shuttle_test_mid_func();
	mm_shuttle_test_ini_func();
	mm_shuttle_test_message_lpusher_func();
	mm_shuttle_test_mulcast_func();
	mm_shuttle_test_zookeeper_func();
}
void mm_shuttle_test_interrupt(struct mm_shuttle_test* p)
{

}
void mm_shuttle_test_shutdown(struct mm_shuttle_test* p)
{

}
void mm_shuttle_test_join(struct mm_shuttle_test* p)
{

}
//////////////////////////////////////////////////////////////////////////