#include "mm_shuttle_test_redis.h"

#include "core/mm_time_cache.h"

#include "redis/mm_redis_common.h"
#include "redis/mm_redis_sync.h"
#include "redis/mm_redis_hash.h"

void mm_shuttle_test_redis_func()
{
	int i = 0;
	mm_uint64_t length = 0;
	mm_uint32_t redis_config_timeout = 0;
	int code = 0;
	struct mm_redis_sync redis;
	mm_redis_sync_init(&redis);
	//////////////////////////////////////////////////////////////////////////
	//mm_redis_sync_assign_remote(&redis, "101.200.169.28", 10200);
	mm_redis_sync_assign_remote(&redis, "101.200.169.28", 50500);

	mm_printf("#####################################\n");
	for ( i = 0; i < 50 ; ++i )
	{
		struct mm_time_cache* g_time_cache = mm_time_cache_instance();
		mm_time_cache_update(g_time_cache);

		code = mm_redis_config_timeout(&redis, &redis_config_timeout);
		code = mm_redis_hash_u64_hlen(&redis, "hash_key", &length);
		mm_printf("redis_config_timeout:%d code:%d length:%" PRIu64 "\n", redis_config_timeout, code, length);
		mm_msleep(11000);
	}
	mm_printf("#####################################\n");

	//////////////////////////////////////////////////////////////////////////
	mm_redis_sync_destroy(&redis);
}
