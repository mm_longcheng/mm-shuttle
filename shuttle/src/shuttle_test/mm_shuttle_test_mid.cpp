#include "mm_shuttle_test_mid.h"

#include "core/mm_string.h"

#include "shuttle_common/mm_sharder_holder.h"

static mm_uint32_t mm_sox_real_mid( mm_uint32_t mid)
{
	mm_uint32_t mid_l = mid >> 8;
	mm_uint32_t mid_r = mid & 0x000000FF;
	mm_uint32_t mid_n = mid_r << 24 | mid_l;
	return mid_n;
}
static mm_uint32_t mm_sox_svid_mid( mm_uint32_t mid)
{
	mm_uint32_t mid_l = mid >> 24;
	mm_uint32_t mid_r = mid & 0x00FFFFFF;
	mm_uint32_t mid_n = mid_r << 8 | mid_l;
	return mid_n;
}

static void check_mid( mm_uint32_t mid_0, mm_uint32_t mid_i, mm_uint32_t mid_1)
{
	assert(mid_0 <= mid_i && mid_i <= mid_1);
}

void mm_shuttle_test_mid_func()
{
	// |       8        |       8 8 8       |
	// | Module number  |   Message number  |

	// protocol::TASK_SVID
	mm_uint32_t mid_1 = 000 << 8 | 70;
	mm_uint32_t mid_2 = 400 << 8 | 70;

	mm_uint32_t mid_3 = 100 << 8 | 71;
	mm_uint32_t mid_4 = 400 << 8 | 71;

	mm_uint32_t mid_5 = 70 << 24 | 000;
	mm_uint32_t mid_6 = 70 << 24 | 400;

	mm_uint32_t mid_7 = 71 << 24 | 100;
	mm_uint32_t mid_8 = 71 << 24 | 400;

	mm_uint32_t mid_ttt_0 = 0x020001F6;
	mm_uint32_t mid_ttt_1 = mm_sox_svid_mid(mid_ttt_0);
	mm_uint32_t mid_ttt_2 = mm_sox_real_mid(mid_ttt_1);
	mm_printf("mid_ttt_0 %08" PRIX32 "\n",mid_ttt_0);
	mm_printf("mid_ttt_1 %08" PRIX32 "\n",mid_ttt_1);
	mm_printf("mid_ttt_2 %08" PRIX32 "\n",mid_ttt_2);

	mm_uint32_t mid_ttt_3 = mm_sox_svid_mid(0x460001F8);
	mid_ttt_3 = mm_sox_svid_mid(0x0B000001);
	{
		mm_uint32_t mid_1_n_1 = mm_sox_real_mid(000 << 8 | 70);
		mm_uint32_t mid_1_n_2 = mm_sox_real_mid(600 << 8 | 70);

		mm_printf("%08" PRIX32 " %08" PRIX32 "\n",mid_1_n_1,mid_1_n_2);
	}
	{
		mm_uint32_t mid_1_n_1 = mm_sox_real_mid((50 << 8 | 4));
		mm_uint32_t mid_1_n_2 = mm_sox_real_mid((57 << 8 | 4));

		mm_printf("%08" PRIX32 " %08" PRIX32 "\n",mid_1_n_1,mid_1_n_2);
	}
	{
		mm_uint32_t mid_1_n_1 = mm_sox_real_mid((401 << 8) | 70);
		mm_uint32_t mid_1_n_2 = mm_sox_real_mid((401 << 8) | 4 );

		mm_printf("%08" PRIX32 " %08" PRIX32 "\n",mid_1_n_1,mid_1_n_2);
	}
	{
		mm_uint32_t mid_1_n_1 = mm_sox_real_mid((401 << 8 | 70));
		mm_uint32_t mid_1_n_2 = mm_sox_real_mid((506 << 8 | 70));

		mm_printf("%08" PRIX32 " %08" PRIX32 "\n",mid_1_n_1,mid_1_n_2);
	}
	{
		mm_uint32_t mid_1_n_1 = mm_sox_real_mid((50 << 8 | 4));
		mm_uint32_t mid_1_n_2 = mm_sox_real_mid((57 << 8 | 4));

		mm_printf("%08" PRIX32 " %08" PRIX32 "\n",mid_1_n_1,mid_1_n_2);
	}
	assert( mid_5 <= mid_6 && mid_6 <= mid_7 && mid_7 <= mid_8);
	assert( mm_sox_real_mid(mid_1) <= mm_sox_real_mid(mid_2) && mm_sox_real_mid(mid_2) <= mm_sox_real_mid(mid_3) && mm_sox_real_mid(mid_3) <= mm_sox_real_mid(mid_4));
	{
		// 150 0x46000000 0x460001FA
		// 302 0x04000032 0x04000039
		mm::mm_m_runtime_state runtime_state;
		mm::mm_m_config_module config_module;
		struct mm_sharder_holder sh;
		mm_sharder_holder_init(&sh);
		mm_sharder_holder_get_instance(&sh, 0x46000191, 0x460001FA, NULL);
		mm_sharder_holder_get_instance(&sh, 0x04000032, 0x04000039, NULL);
		mm_sharder_holder_cycle(&sh,0x460001F8,&config_module,&runtime_state);
		mm_sharder_holder_cycle(&sh,0x04000039,&config_module,&runtime_state);
		mm_sharder_holder_destroy(&sh);
	}
	//for (mm_uint32_t i = 0;i < 599; ++i)
	//{
	//	check_mid(mid_1, ( 000 + i ) << 8 | 70, mid_1);
	//}
	printf("_____________\n");
}
