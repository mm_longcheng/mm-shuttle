﻿#include "mm_shuttle_test_openssl.h"

#include "net/mm_sockaddr.h"
#include "dish/mm_base64.h"

#include <string>

//  RSA 加密 ///

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#define OPENSSLKEY "test.key"
#define PUBLICKEY  "test_pub.key"
#define BUFFSIZE   1024

char *my_encrypt(char *str, char *path_key);    //加密
char *my_decrypt(char *str, char *path_key);        //解密

//加密
char *my_encrypt(char *str, char *path_key)
{
	char *p_en = NULL;
	RSA  *p_rsa = NULL;
	FILE *file = NULL;

	int  rsa_len = 0;    //flen为源文件长度， rsa_len为秘钥长度

	//1.打开秘钥文件
	if((file = fopen(path_key, "rb")) == NULL)
	{
		perror("fopen() error 111111111 ");
		goto End;
	}        

	//2.从公钥中获取 加密的秘钥
	if((p_rsa = PEM_read_RSA_PUBKEY(file, NULL,NULL,NULL )) == NULL)
	{
		ERR_print_errors_fp(stdout);
		goto End;
	}

	//3.获取秘钥的长度
	rsa_len = RSA_size(p_rsa);

	//4.为加密后的内容 申请空间（根据秘钥的长度+1）
	p_en = (char *)malloc(rsa_len + 1);
	if(!p_en)
	{
		perror("malloc() error 2222222222");
		goto End;
	}    
	memset(p_en, 0, rsa_len + 1);

	//5.对内容进行加密
	if(RSA_public_encrypt(rsa_len, (unsigned char*)str, (unsigned char*)p_en, p_rsa, RSA_NO_PADDING) < 0)
	{
		perror("RSA_public_encrypt() error 2222222222");
		goto End;
	}

End:

	//6.释放秘钥空间， 关闭文件
	if(p_rsa)    RSA_free(p_rsa);
	if(file)     fclose(file);

	return p_en;
}   

//解密
char *my_decrypt(char *str, char *path_key)
{
	char *p_de = NULL;
	RSA  *p_rsa = NULL;
	FILE *file = NULL;
	int   rsa_len = 0;


	//1.打开秘钥文件
	file = fopen(path_key, "rb");
	if(!file)
	{
		perror("fopen() error 22222222222");
		goto End;
	}        

	//2.从私钥中获取 解密的秘钥
	if((p_rsa = PEM_read_RSAPrivateKey(file, NULL,NULL,NULL )) == NULL)
	{
		ERR_print_errors_fp(stdout);
		goto End;
	}

	//3.获取秘钥的长度，
	rsa_len = RSA_size(p_rsa);

	//4.为加密后的内容 申请空间（根据秘钥的长度+1）
	p_de = (char *)malloc(rsa_len + 1);
	if(!p_de)
	{
		perror("malloc() error ");
		goto End;
	}    
	memset(p_de, 0, rsa_len + 1);

	//5.对内容进行加密
	if(RSA_private_decrypt(rsa_len, (unsigned char*)str, (unsigned char*)p_de, p_rsa, RSA_NO_PADDING) < 0)
	{
		perror("RSA_public_encrypt() error ");
		goto End;
	}

End:
	//6.释放秘钥空间， 关闭文件
	if(p_rsa)    RSA_free(p_rsa);
	if(file)     fclose(file);

	return p_de;
}

static void __static_bn2raw(const BIGNUM* bn, struct mm_string* raw)
{
	mm_string_clear(raw);
	mm_string_resize(raw, BN_num_bytes(bn));
	BN_bn2bin(bn, (unsigned char*)raw->s);
	if (raw->l % 2 == 1 && raw->s[0] == 0x00)
	{
		mm_string_substr(raw, raw, 1, raw->l - 1);
	}
}
static void __static_raw2bn(struct mm_string* raw, BIGNUM* bn)
{
	if ((mm_uint8_t)(raw->s[0]) >= 0x80)
	{
		mm_string_resize(raw, raw->l + 1);
		mm_memcpy(raw->s + 1, raw->s, raw->l - 1);
		raw->s[0] = 0x00;
	}
	BN_bin2bn((const unsigned char*)raw->s, (int)raw->l, bn);
}

static void __static_recv_rq_rsa(struct mm_string* key_plainbf, struct mm_string* key_encrypt, const std::string& n_string, const std::string& e_string)
{
	//////////////////////////////////////////////////////////////////////////
	int key_encrypt_len = 0;

	struct mm_string n_str;
	struct mm_string e_str;

	BIGNUM* n = BN_new();
	BIGNUM* e = BN_new();

	RSA* rsa = RSA_new();

	mm_string_init(&n_str);
	mm_string_init(&e_str);

	mm_string_resize(&n_str, n_string.size());
	mm_memcpy(n_str.s, n_string.data(), n_string.size());

	mm_string_resize(&e_str, e_string.size());
	mm_memcpy(e_str.s, e_string.data(), e_string.size());

	__static_raw2bn(&n_str, n);
	__static_raw2bn(&e_str, e);

	RSA_set0_key(rsa, n, e, NULL);

	mm_string_resize(key_encrypt, RSA_size(rsa));
	key_encrypt_len = RSA_public_encrypt((int)key_plainbf->l, (const unsigned char*)key_plainbf->s, (unsigned char*)key_encrypt->s, rsa, RSA_PKCS1_PADDING);
	mm_string_resize(key_encrypt, key_encrypt_len);

	RSA_free(rsa);

	mm_string_destroy(&n_str);
	mm_string_destroy(&e_str);

	// note: openssl api RSA_set0_key is strong reference swap.
	//BN_free(n);
	//BN_free(e);
}
static void __static_send_rq_rsa(std::string& n_string, std::string& e_string, std::string& d_string)
{
	BIGNUM* e = NULL;

	const BIGNUM* n = NULL;
	const BIGNUM* d = NULL;

	RSA* rsa = NULL;

	struct mm_string n_str;
	struct mm_string e_str;
	struct mm_string d_str;

	e = BN_new();
	rsa = RSA_new();

	mm_string_init(&n_str);
	mm_string_init(&e_str);
	mm_string_init(&d_str);

	// RSA_generate_key_ex api is a random generate.
	// do not random generate e.
	BN_set_word(e, RSA_3);

	RSA_generate_key_ex(rsa, 1024, e, NULL);
	// not need get e.
	RSA_get0_key(rsa, &n, NULL, &d);

	__static_bn2raw(n, &n_str);
	__static_bn2raw(e, &e_str);
	__static_bn2raw(d, &d_str);

	n_string.resize(n_str.l);
	mm_memcpy((void*)n_string.data(), n_str.s, n_str.l);

	e_string.resize(e_str.l);
	mm_memcpy((void*)e_string.data(), e_str.s, e_str.l);

	d_string.resize(d_str.l);
	mm_memcpy((void*)d_string.data(), d_str.s, d_str.l);

	mm_string_destroy(&n_str);
	mm_string_destroy(&e_str);
	mm_string_destroy(&d_str);

	RSA_free(rsa);
	BN_free(e);
}
static void __static_recv_rs_rsa(struct mm_string* key_decrypt, struct mm_string* key_encrypt, std::string& n_string, std::string& e_string, std::string& d_string)
{
	//////////////////////////////////////////////////////////////////////////
	int key_decrypt_len = 0;

	struct mm_string n_str;
	struct mm_string e_str;
	struct mm_string d_str;

	BIGNUM* n = BN_new();
	BIGNUM* e = BN_new();
	BIGNUM* d = BN_new();

	RSA* rsa = RSA_new();

	mm_string_init(&n_str);
	mm_string_init(&e_str);
	mm_string_init(&d_str);

	mm_string_resize(&n_str, n_string.size());
	mm_memcpy(n_str.s, n_string.data(), n_string.size());

	mm_string_resize(&e_str, e_string.size());
	mm_memcpy(e_str.s, e_string.data(), e_string.size());

	mm_string_resize(&d_str, d_string.size());
	mm_memcpy(d_str.s, d_string.data(), d_string.size());

	__static_raw2bn(&n_str, n);
	__static_raw2bn(&e_str, e);
	__static_raw2bn(&d_str, d);

	RSA_set0_key(rsa, n, e, d);

	mm_string_resize(key_decrypt, RSA_size(rsa));
	key_decrypt_len = RSA_private_decrypt((int)key_encrypt->l, (const unsigned char*)key_encrypt->s, (unsigned char*)key_decrypt->s, rsa, RSA_PKCS1_PADDING);
	mm_string_resize(key_decrypt, key_decrypt_len);

	RSA_free(rsa);

	mm_string_destroy(&e_str);
	mm_string_destroy(&d_str);

	// note: openssl api RSA_set0_key is strong reference swap.
	//BN_free(n);
	//BN_free(e);
	//BN_free(d);
}
static void __static_shuttle_test_openssl_rsa_func()
{
	std::string n_string;
	std::string e_string;
	std::string d_string;

	struct mm_string key_plainbf;
	struct mm_string key_encrypt;
	struct mm_string key_decrypt;

	struct mm_string key_encrypt_base64;

	int b64_encode_length = 0;

	mm_string_init(&key_plainbf);
	mm_string_init(&key_encrypt);
	mm_string_init(&key_decrypt);

	mm_string_init(&key_encrypt_base64);

	mm_string_assigns(&key_plainbf, "rsa test key");

	RAND_seed("a", 1);

	__static_send_rq_rsa(n_string, e_string, d_string);
	__static_recv_rq_rsa(&key_plainbf, &key_encrypt, n_string, e_string);
	__static_recv_rs_rsa(&key_decrypt, &key_encrypt, n_string, e_string, d_string);

	b64_encode_length = mm_base64_encode_length((int)key_encrypt.l);
	mm_string_resize(&key_encrypt_base64, b64_encode_length);
	mm_base64_encode(key_encrypt_base64.s, key_encrypt.s, (int)key_encrypt.l);

	mm_printf("key_plainbf        %s\n", key_plainbf.s);
	mm_printf("key_decrypt        %s\n", key_decrypt.s);

	mm_printf("key_encrypt_base64 %s\n", key_encrypt_base64.s);

	mm_string_destroy(&key_plainbf);
	mm_string_destroy(&key_encrypt);
	mm_string_destroy(&key_decrypt);

	mm_string_destroy(&key_encrypt_base64);
}
void mm_shuttle_test_openssl_func()
{
	__static_shuttle_test_openssl_rsa_func();
	{
		char addr_name[MM_ADDR_NAME_LENGTH] = {"::-12548"};
		char node[MM_NODE_NAME_LENGTH] = {0};
		mm_ushort_t port = 0;
		mm_sockaddr_format_decode_string( addr_name, node, &port );

	}
	const char *source = "i like dancing !!!";

	char *ptf_en, *ptf_de;

	printf("source is   :%s\n", source);

	//1.加密
	ptf_en = my_encrypt((char*)source, (char*)PUBLICKEY);
	printf("ptf_en is   :%s\n", ptf_en);

	//2.解密
	ptf_de = my_decrypt(ptf_en, (char*)OPENSSLKEY);
	printf("ptf_de is   :%s\n", ptf_de);

	if(ptf_en)            free(ptf_en);
	if(ptf_de)            free(ptf_de);
}
