#include "mm_shuttle_test_ini.h"

#include "core/mm_string.h"

#include "cpp/INIReader.h"

void mm_shuttle_test_ini_func()
{
	static const char* file_name = "../../../config/config_1";

	INIReader reader(file_name);

	std::string value = reader.Get("common", "zk_link", "127.0.0.1:10300,");
	std::string df = reader.Get("common", "dfs", "127.0.0.1:10300,");
	bool a = reader.Exists("common", "dfs");

	mm_printf("%s\n", value.c_str());
}
