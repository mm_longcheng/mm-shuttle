#include "mm_shuttle_test_hmget.h"

#include "core/mm_string.h"

#include "net/mm_sockaddr.h"

#include "container/mm_rbtree_u64.h"
#include "container/mm_rbtset_u64.h"

#include "redis/mm_redis_hash.h"
#include "redis/mm_redis_sync.h"
#include "core/mm_file_system.h"
#include "dish/mm_archive_endian.h"


void mm_shuttle_test_hmget_func()
{
	{
		struct mm_redis_sync rds;
		struct mm_string command;
		struct mm_rbtset_u64 rbtset_u64;
		struct mm_rbtree_u64_u64 rbtree;
		mm_redis_sync_init(&rds);
		mm_string_init(&command);
		mm_rbtset_u64_init(&rbtset_u64);
		mm_rbtree_u64_u64_init(&rbtree);
		//
		mm_rbtset_u64_add(&rbtset_u64, 1);
		mm_rbtset_u64_add(&rbtset_u64, 2);
		mm_rbtset_u64_add(&rbtset_u64, 3);
		mm_rbtset_u64_add(&rbtset_u64, 4);
		mm_rbtset_u64_add(&rbtset_u64, 5);
		mm_rbtset_u64_add(&rbtset_u64, 6);
		mm_rbtset_u64_add(&rbtset_u64, 7);
		mm_rbtset_u64_add(&rbtset_u64, 8);
		mm_rbtset_u64_add(&rbtset_u64, 9);
		//
		mm_redis_sync_assign_remote(&rds, "101.200.169.28", 50500);

		mm_redis_hash_u64_u64_hmget_pattern_page(&rds,"mkey:%03d",4,2,&rbtset_u64,&rbtree);
		//int err = mm_redis_hash_u32_u32_hmget_page(&rds,"mkey",&rbtset_u32,4,&rbtree);
		//int err = mm_redis_hash_u32_u32_hmget(&rds,"mkey",&rbtset_u32,&rbtree);
		{
			struct mm_rb_node* n = NULL;
			struct mm_rbtree_u64_u64_iterator* it = NULL;
			//
			n = mm_rb_last(&rbtree.rbt);
			while(NULL != n)
			{
				it = (struct mm_rbtree_u64_u64_iterator*)mm_rb_entry(n, struct mm_rbtree_u64_u64_iterator, n);
				n = mm_rb_prev(n);
				mm_printf("%" PRIu64 " %" PRIu64 "\n",it->k,it->v);
			}
		}
		mm_redis_sync_destroy(&rds);
		mm_string_destroy(&command);
		mm_rbtset_u64_destroy(&rbtset_u64);
		mm_rbtree_u64_u64_destroy(&rbtree);
	}
	{
		struct mm_redis_sync rds;
		struct mm_string command;
		struct mm_rbtset_u32 rbtset_u32;
		struct mm_rbtree_u32_u32 rbtree;
		mm_redis_sync_init(&rds);
		mm_string_init(&command);
		mm_rbtset_u32_init(&rbtset_u32);
		mm_rbtree_u32_u32_init(&rbtree);
		//
		mm_rbtset_u32_add(&rbtset_u32, 1);
		mm_rbtset_u32_add(&rbtset_u32, 2);
		mm_rbtset_u32_add(&rbtset_u32, 3);
		mm_rbtset_u32_add(&rbtset_u32, 4);
		mm_rbtset_u32_add(&rbtset_u32, 5);
		mm_rbtset_u32_add(&rbtset_u32, 6);
		mm_rbtset_u32_add(&rbtset_u32, 7);
		mm_rbtset_u32_add(&rbtset_u32, 8);
		mm_rbtset_u32_add(&rbtset_u32, 9);
		//
		mm_redis_sync_assign_remote(&rds, "101.200.169.28", 50500);

		mm_redis_hash_u32_u32_hmget_pattern_page(&rds,"mkey:%03d",4,2,&rbtset_u32,&rbtree);
		//int err = mm_redis_hash_u32_u32_hmget_page(&rds,"mkey",&rbtset_u32,4,&rbtree);
		//int err = mm_redis_hash_u32_u32_hmget(&rds,"mkey",&rbtset_u32,&rbtree);
		{
			struct mm_rb_node* n = NULL;
			struct mm_rbtree_u32_u32_iterator* it = NULL;
			//
			n = mm_rb_last(&rbtree.rbt);
			while(NULL != n)
			{
				it = (struct mm_rbtree_u32_u32_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_u32_iterator, n);
				n = mm_rb_prev(n);
				mm_printf("%" PRIu32 " %" PRIu32 "\n",it->k,it->v);
			}
		}
		mm_redis_sync_destroy(&rds);
		mm_string_destroy(&command);
		mm_rbtset_u32_destroy(&rbtset_u32);
		mm_rbtree_u32_u32_destroy(&rbtree);
	}
	//{
	//	int i = 0;  
	//	struct hostent *phostinfo = gethostbyname("localhost");    
	//	for(i = 0; NULL != phostinfo&& NULL != phostinfo->h_addr_list[i]; ++i)  
	//	{  
	//		char *pszAddr = inet_ntoa(*(struct in_addr *)phostinfo->h_addr_list[i]);  
	//		printf("%s\n", pszAddr);  
	//	}  
	//}
	//{
	//	int i = 0;  
	//	struct hostent *phostinfo = gethostbyname("");    
	//	for(i = 0; NULL != phostinfo&& NULL != phostinfo->h_addr_list[i]; ++i)  
	//	{  
	//		char *pszAddr = inet_ntoa(*(struct in_addr *)phostinfo->h_addr_list[i]);  
	//		printf("%s\n", pszAddr);  
	//	}  
	//}
	{
		int a = -1;
		a = mm_access("log", MM_ACCESS_F_OK);
		a = mm_access("ppt", MM_ACCESS_F_OK);
		a = mm_access("E:/mm/mm_shuttle/shuttle/proj/windows", MM_ACCESS_F_OK);
		a = mm_access("E:/mm/mm_shuttle/shuttle/proj/windowsdd", MM_ACCESS_F_OK);
		a = mm_mkdir("aaaaa",00700);
		a = mm_mkdir("aaaaa/bbbffc",00700);
		a = mm_mkdir("aaaaa/bbbffc/a",00700);

		struct _stat64 buf;
		_wstat64(L"log", &buf);
		_wstat64(L"compile.bat", &buf);
		struct _stat64i32 _Stat;
		_stat64i32("compile.bat",&_Stat);

		struct _stat64 _Stat64;
		a = _stat64("compile.bat",&_Stat64);

		struct _stat buf1;
		//struct _stat32i64 buf2;
		//struct _stat64i32 buf2;
		//struct _stat32 buf2;
		a = _stat( "compile.bat", (struct _stat*)&buf1 ); 

		mm_stat_t _Stat64_v;
		a = mm_stat("compile.bat",&_Stat64_v);

		int b = a;
	}
	//{
	//	logger_file_init();

	//	logger_file_assign_file_path("./aabbcc");
	//	logger_file_assign_level(6);
	//	logger_file_log(6, "afsdfasdfasdfasdfasdf");
	//	logger_file_log(7, "bfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
	//	logger_file_log(1, "cfffffffffffffffffff");

	//	logger_file_destroy();
	//}
	{
		struct mm_string a;
		mm_string_init(&a);
		mm_string_assigns(&a, "abc");
		int n0 = mm_string_compare_c_str(&a, "abcd");
		int n1 = mm_string_compare_c_str(&a, "abc");
		int n2 = mm_string_compare_c_str(&a, "acb");
		mm_string_destroy(&a);
	}
	//{
	//	//struct mm_packet_head_11 h11;
	//	size_t sz = sizeof(struct mm_packet_head_11);
	//	sz = sizeof(struct mm_packet_head_11);
	//}
	{
		mm_uint16_t a_0 = 12345;
		mm_uint32_t b_0 = 123;
		std::string c_0 = "123456";
		int d_0 = 12345;
		size_t e_0 = 12345;

		mm_uint16_t a_1 = 0;
		mm_uint32_t b_1 = 0;
		std::string c_1 = "";
		int d_1 = 12345;
		size_t e_1 = 12345;

		struct mm_streambuf streambuf;
		mm::mm_o_archive_little archive_o(streambuf);
		mm::mm_i_archive_little archive_i(streambuf);
		mm_streambuf_init(&streambuf);
		archive_o << a_0;
		archive_o << b_0;
		archive_o << c_0;
		archive_o << d_0;
		archive_o << e_0;

		archive_i >> a_1;
		archive_i >> b_1;
		archive_i >> c_1;
		archive_i >> d_1;
		archive_i >> e_1;
		mm_streambuf_destroy(&streambuf);
	}
	{
		mm_uint16_t a_0 = 12345;
		mm_uint32_t b_0 = 123;
		std::string c_0 = "123456";

		mm_uint16_t a_1 = 0;
		mm_uint32_t b_1 = 0;
		std::string c_1 = "";
		struct mm_streambuf streambuf;
		mm::mm_o_archive_bigger archive_o(streambuf);
		mm::mm_i_archive_bigger archive_i(streambuf);
		mm_streambuf_init(&streambuf);
		archive_o << a_0;
		archive_o << b_0;
		archive_o << c_0;
		archive_i >> a_1;
		archive_i >> b_1;
		archive_i >> c_1;
		mm_streambuf_destroy(&streambuf);
	}
	{
		mm_uint32_t lenght = 19;
		mm_uint32_t uri = (35 << 8 | 4);
		mm_uint16_t sid = 0;
		mm_uint16_t res_code = 200;
		mm_uint8_t tag = 1;

		mm_uint32_t pk_sid = 100011;
		mm_uint16_t pk_type = 1;

		struct mm_streambuf streambuf;
		mm::mm_o_archive_bigger archive_o(streambuf);
		mm::mm_i_archive_bigger archive_i(streambuf);
		mm_streambuf_init(&streambuf);
		//////////////////////////////////////////////////////////////////////////
		archive_o << lenght;
		archive_o << uri;
		archive_o << sid;
		archive_o << res_code;
		archive_o << tag;
		archive_o << pk_sid;
		archive_o << pk_type;
		//////////////////////////////////////////////////////////////////////////
		size_t sz = mm_streambuf_size(&streambuf);
		char buffer[32] = {0};
		mm_memcpy((void*)buffer, (void*)(streambuf.buff + streambuf.gptr), sz);
		//////////////////////////////////////////////////////////////////////////
		mm_streambuf_destroy(&streambuf);
	}
	{
		std::string a = "abc";
		size_t len = a.size();

		mm_uint16_t i16_0 = 0xAABB;
		mm_uint16_t i16_1 = mm_htob16(i16_0);

		mm_uint32_t i32_0 = 0xAABBCCDD;
		mm_uint32_t i32_1 = mm_htob32(i32_0);

		mm_uint64_t i64_0 = 0xAABBCCDDEEFF1122;
		mm_uint64_t i64_1 = mm_htob64(i64_0);

		printf("%04" PRIX16 " %04" PRIX16 "\n",i16_0,i16_1);
		printf("%08" PRIX32 " %08" PRIX32 "\n",i32_0,i32_1);
		printf("%16" PRIX64 " %16" PRIX64 "\n",i64_0,i64_1);

		len = a.size();
	}
	{
		char addr_name[MM_ADDR_NAME_LENGTH] = {0};
		struct mm_sockaddr sa;
		mm_sockaddr_assign(&sa, "www.google.co.uk", 10500);
		mm_sockaddr_string(&sa, addr_name );
		mm_printf("%s\n", addr_name);
		mm_sockaddr_assign(&sa, "www.baidu.com", 10500);
		mm_sockaddr_string(&sa, addr_name );
		mm_printf("%s\n", addr_name);
		mm_sockaddr_assign(&sa, "localhost", 10500);
		mm_sockaddr_string(&sa, addr_name );
		mm_printf("%s\n", addr_name);
		mm_sockaddr_assign(&sa, "192.168.111.123", 10500);
		mm_sockaddr_string(&sa, addr_name );
		mm_printf("%s\n", addr_name);
		mm_sockaddr_assign(&sa, "2001:0DB8:0000:0023:0008:0800:200C:417A", 10500);
		mm_sockaddr_string(&sa, addr_name );
		mm_printf("%s\n", addr_name);
	}
}
