#include "mm_shuttle_test_address.h"

#include "core/mm_string.h"

#include <winsock2.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>
#include "net/mm_headset.h"
#include <map>
#include <list>
#include <string>
#pragma comment(lib, "IPHLPAPI.lib")
#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))
int address_main()
{

	/* Declare and initialize variables */

	// It is possible for an adapter to have multiple
	// IPv4 addresses, gateways, and secondary WINS servers
	// assigned to the adapter. 
	//
	// Note that this sample code only prints out the 
	// first entry for the IP address/mask, and gateway, and
	// the primary and secondary WINS server for each adapter. 

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	UINT i;

	/* variables used to print DHCP time info */
	struct tm newtime;
	char buffer[32];
	errno_t error;

	ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO *) MALLOC(sizeof (IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		printf("Error allocating memory needed to call GetAdaptersinfo\n");
		return 1;
	}
	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		FREE(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *) MALLOC(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			printf("Error allocating memory needed to call GetAdaptersinfo\n");
			return 1;
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
		pAdapter = pAdapterInfo;
		while (pAdapter) {
			printf("\tComboIndex: \t%d\n", pAdapter->ComboIndex);
			printf("\tAdapter Name: \t%s\n", pAdapter->AdapterName);
			printf("\tAdapter Desc: \t%s\n", pAdapter->Description);
			printf("\tAdapter Addr: \t");
			for (i = 0; i < pAdapter->AddressLength; i++) {
				if (i == (pAdapter->AddressLength - 1))
					printf("%.2X\n", (int) pAdapter->Address[i]);
				else
					printf("%.2X-", (int) pAdapter->Address[i]);
			}
			printf("\tIndex: \t%d\n", pAdapter->Index);
			printf("\tType: \t");
			switch (pAdapter->Type) {
			case MIB_IF_TYPE_OTHER:
				printf("Other\n");
				break;
			case MIB_IF_TYPE_ETHERNET:
				printf("Ethernet\n");
				break;
			case MIB_IF_TYPE_TOKENRING:
				printf("Token Ring\n");
				break;
			case MIB_IF_TYPE_FDDI:
				printf("FDDI\n");
				break;
			case MIB_IF_TYPE_PPP:
				printf("PPP\n");
				break;
			case MIB_IF_TYPE_LOOPBACK:
				printf("Lookback\n");
				break;
			case MIB_IF_TYPE_SLIP:
				printf("Slip\n");
				break;
			default:
				printf("Unknown type %ld\n", pAdapter->Type);
				break;
			}

			printf("\tIP Address: \t%s\n",
				pAdapter->IpAddressList.IpAddress.String);
			printf("\tIP Mask: \t%s\n", pAdapter->IpAddressList.IpMask.String);

			printf("\tGateway: \t%s\n", pAdapter->GatewayList.IpAddress.String);
			printf("\t***\n");

			if (pAdapter->DhcpEnabled) {
				printf("\tDHCP Enabled: Yes\n");
				printf("\t  DHCP Server: \t%s\n",
					pAdapter->DhcpServer.IpAddress.String);

				printf("\t  Lease Obtained: ");
				/* Display local time */
				error = _localtime32_s(&newtime, (__time32_t*) &pAdapter->LeaseObtained);
				if (error)
					printf("Invalid Argument to _localtime32_s\n");
				else {
					// Convert to an ASCII representation 
					error = asctime_s(buffer, 32, &newtime);
					if (error)
						printf("Invalid Argument to asctime_s\n");
					else
						/* asctime_s returns the string terminated by \n\0 */
						printf("%s", buffer);
				}

				printf("\t  Lease Expires:  ");
				error = _localtime32_s(&newtime, (__time32_t*) &pAdapter->LeaseExpires);
				if (error)
					printf("Invalid Argument to _localtime32_s\n");
				else {
					// Convert to an ASCII representation 
					error = asctime_s(buffer, 32, &newtime);
					if (error)
						printf("Invalid Argument to asctime_s\n");
					else
						/* asctime_s returns the string terminated by \n\0 */
						printf("%s", buffer);
				}
			} else
				printf("\tDHCP Enabled: No\n");

			if (pAdapter->HaveWins) {
				printf("\tHave Wins: Yes\n");
				printf("\t  Primary Wins Server:    %s\n",
					pAdapter->PrimaryWinsServer.IpAddress.String);
				printf("\t  Secondary Wins Server:  %s\n",
					pAdapter->SecondaryWinsServer.IpAddress.String);
			} else
				printf("\tHave Wins: No\n");
			pAdapter = pAdapter->Next;
			printf("\n");
		}
	} else {
		printf("GetAdaptersInfo failed with error: %d\n", dwRetVal);

	}
	if (pAdapterInfo)
		FREE(pAdapterInfo);

	return 0;
}

#include <winsock2.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>

// Link with Iphlpapi.lib
#pragma comment(lib, "IPHLPAPI.lib")

#define WORKING_BUFFER_SIZE 15000
#define MAX_TRIES 3

//#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
//#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

/* Note: could also use malloc() and free() */

int dddd_main(int argc, char **argv)
{
	char buff[100];  
	DWORD bufflen=100; 
	/* Declare and initialize variables */

	DWORD dwSize = 0;
	DWORD dwRetVal = 0;

	unsigned int i = 0;

	// Set the flags to pass to GetAdaptersAddresses
	ULONG flags = GAA_FLAG_INCLUDE_PREFIX | GAA_FLAG_INCLUDE_GATEWAYS;

	// default to unspecified address family (both)
	ULONG family = AF_UNSPEC;

	LPVOID lpMsgBuf = NULL;

	PIP_ADAPTER_ADDRESSES pAddresses = NULL;
	ULONG outBufLen = 0;
	ULONG Iterations = 0;

	PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;
	PIP_ADAPTER_ANYCAST_ADDRESS pAnycast = NULL;
	PIP_ADAPTER_MULTICAST_ADDRESS pMulticast = NULL;
	IP_ADAPTER_DNS_SERVER_ADDRESS *pDnServer = NULL;
	IP_ADAPTER_GATEWAY_ADDRESS* pGateway = NULL;
	SOCKET_ADDRESS* pDhcpv4Server = NULL;
	SOCKET_ADDRESS* pDhcpv6Server = NULL;
	IP_ADAPTER_PREFIX *pPrefix = NULL;

	if (argc != 2) {
		printf(" Usage: getadapteraddresses family\n");
		printf("        getadapteraddresses 4 (for IPv4)\n");
		printf("        getadapteraddresses 6 (for IPv6)\n");
		printf("        getadapteraddresses A (for both IPv4 and IPv6)\n");
		exit(1);
	}

	if (atoi(argv[1]) == 4)
		family = AF_INET;
	else if (atoi(argv[1]) == 6)
		family = AF_INET6;

	printf("Calling GetAdaptersAddresses function with family = ");
	if (family == AF_INET)
		printf("AF_INET\n");
	if (family == AF_INET6)
		printf("AF_INET6\n");
	if (family == AF_UNSPEC)
		printf("AF_UNSPEC\n\n");

	// Allocate a 15 KB buffer to start with.
	outBufLen = WORKING_BUFFER_SIZE;

	do {

		pAddresses = (IP_ADAPTER_ADDRESSES *) MALLOC(outBufLen);
		if (pAddresses == NULL) {
			printf
				("Memory allocation failed for IP_ADAPTER_ADDRESSES struct\n");
			exit(1);
		}

		dwRetVal =
			GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen);

		if (dwRetVal == ERROR_BUFFER_OVERFLOW) {
			FREE(pAddresses);
			pAddresses = NULL;
		} else {
			break;
		}

		Iterations++;

	} while ((dwRetVal == ERROR_BUFFER_OVERFLOW) && (Iterations < MAX_TRIES));

	if (dwRetVal == NO_ERROR) {
		// If successful, output some information from the data we received
		pCurrAddresses = pAddresses;
		while (pCurrAddresses) {
			printf("\tLength of the IP_ADAPTER_ADDRESS struct: %ld\n",
				pCurrAddresses->Length);
			printf("\tIfIndex (IPv4 interface): %u\n", pCurrAddresses->IfIndex);
			printf("\tAdapter name: %s\n", pCurrAddresses->AdapterName);

			pUnicast = pCurrAddresses->FirstUnicastAddress;
			if (pUnicast != NULL) {
				for (i = 0; pUnicast != NULL; i++)
				{
					if (pUnicast->Address.lpSockaddr->sa_family == AF_INET)  
					{  
						sockaddr_in *sa_in = (sockaddr_in *)pUnicast->Address.lpSockaddr;  
						printf("\t\tIPV4 Unicast Address: %s/%d\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen),pUnicast->OnLinkPrefixLength);  
					}  
					else if (pUnicast->Address.lpSockaddr->sa_family == AF_INET6)  
					{  
						sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pUnicast->Address.lpSockaddr;  
						printf("\t\tIPV6 Unicast Address: %s/%d\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen),pUnicast->OnLinkPrefixLength);  
					}  
					else  
					{  
						printf("\t\tUNSPEC\n");  
					} 
					pUnicast = pUnicast->Next;
				}
				printf("\tNumber of Unicast Addresses: %d\n", i);
			} else
				printf("\tNo Unicast Addresses\n");

			pAnycast = pCurrAddresses->FirstAnycastAddress;
			if (pAnycast) {
				for (i = 0; pAnycast != NULL; i++)
				{
					if (pAnycast->Address.lpSockaddr->sa_family == AF_INET)  
					{  
						sockaddr_in *sa_in = (sockaddr_in *)pAnycast->Address.lpSockaddr;  
						printf("\t\tIPV4 Anycast Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
					}  
					else if (pAnycast->Address.lpSockaddr->sa_family == AF_INET6)  
					{  
						sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pAnycast->Address.lpSockaddr;  
						printf("\t\tIPV6 Anycast Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
					}  
					else  
					{  
						printf("\t\tUNSPEC\n");  
					} 
					pAnycast = pAnycast->Next;
				}
				printf("\tNumber of Anycast Addresses: %d\n", i);
			} else
				printf("\tNo Anycast Addresses\n");

			pMulticast = pCurrAddresses->FirstMulticastAddress;
			if (pMulticast) {
				for (i = 0; pMulticast != NULL; i++)
				{
					if (pMulticast->Address.lpSockaddr->sa_family == AF_INET)  
					{  
						sockaddr_in *sa_in = (sockaddr_in *)pMulticast->Address.lpSockaddr;  
						printf("\t\tIPV4 Multicast Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
					}  
					else if (pMulticast->Address.lpSockaddr->sa_family == AF_INET6)  
					{  
						sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pMulticast->Address.lpSockaddr;  
						printf("\t\tIPV6 Multicast Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
					}  
					else  
					{  
						printf("\t\tUNSPEC\n");  
					} 
					pMulticast = pMulticast->Next;
				}
				printf("\tNumber of Multicast Addresses: %d\n", i);
			} else
				printf("\tNo Multicast Addresses\n");

			pDnServer = pCurrAddresses->FirstDnsServerAddress;
			if (pDnServer) {
				for (i = 0; pDnServer != NULL; i++)
				{
					if (pDnServer->Address.lpSockaddr->sa_family == AF_INET)  
					{  
						sockaddr_in *sa_in = (sockaddr_in *)pDnServer->Address.lpSockaddr;  
						printf("\t\tIPV4 DnServer Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
					}  
					else if (pDnServer->Address.lpSockaddr->sa_family == AF_INET6)  
					{  
						sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pDnServer->Address.lpSockaddr;  
						printf("\t\tIPV6 DnServer Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
					}  
					else  
					{  
						printf("\t\tUNSPEC\n");  
					} 
					pDnServer = pDnServer->Next;
				}
				printf("\tNumber of DNS Server Addresses: %d\n", i);
			} else
				printf("\tNo DNS Server Addresses\n");
			pGateway = pCurrAddresses->FirstGatewayAddress;
			if (pGateway) {
				for (i = 0; pGateway != NULL; i++)
				{
					if (pGateway->Address.lpSockaddr->sa_family == AF_INET)  
					{  
						sockaddr_in *sa_in = (sockaddr_in *)pGateway->Address.lpSockaddr;  
						printf("\t\tIPV4 Gateway Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
					}  
					else if (pGateway->Address.lpSockaddr->sa_family == AF_INET6)  
					{  
						sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pGateway->Address.lpSockaddr;  
						printf("\t\tIPV6 Gateway Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
					}  
					else  
					{  
						printf("\t\tUNSPEC\n");  
					} 
					pGateway = pGateway->Next;
				}
				printf("\tNumber of Gateway Server Addresses: %d\n", i);
			} else
				printf("\tNo Gateway Server Addresses\n");
			pDhcpv4Server = &pCurrAddresses->Dhcpv4Server;
			if (pDhcpv4Server->lpSockaddr) {
				if (pDhcpv4Server->lpSockaddr->sa_family == AF_INET)  
				{  
					sockaddr_in *sa_in = (sockaddr_in *)pDhcpv4Server->lpSockaddr;  
					printf("\tIPV4 Dhcpv4Server Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
				}  
				else if (pDhcpv4Server->lpSockaddr->sa_family == AF_INET6)  
				{  
					sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pDhcpv4Server->lpSockaddr;  
					printf("\tIPV6 Dhcpv4Server Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
				}  
				else  
				{  
					printf("\tUNSPEC\n");  
				} 
			}
			pDhcpv6Server = &pCurrAddresses->Dhcpv6Server;
			if (pDhcpv6Server->lpSockaddr) {
				if (pDhcpv6Server->lpSockaddr->sa_family == AF_INET)  
				{  
					sockaddr_in *sa_in = (sockaddr_in *)pDhcpv6Server->lpSockaddr;  
					printf("\tIPV4 Dhcpv6Server Address: %s\n",inet_ntop(AF_INET,&(sa_in->sin_addr),buff,bufflen));  
				}  
				else if (pDhcpv6Server->lpSockaddr->sa_family == AF_INET6)  
				{  
					sockaddr_in6 *sa_in6 = (sockaddr_in6 *)pDhcpv6Server->lpSockaddr;  
					printf("\tIPV6 Dhcpv6Server Address: %s\n",inet_ntop(AF_INET6,&(sa_in6->sin6_addr),buff,bufflen));  
				}  
				else  
				{  
					printf("\tUNSPEC\n");  
				} 
			}

			printf("\tDNS Suffix: %wS\n", pCurrAddresses->DnsSuffix);
			printf("\tDescription: %wS\n", pCurrAddresses->Description);
			printf("\tFriendly name: %wS\n", pCurrAddresses->FriendlyName);

			if (pCurrAddresses->PhysicalAddressLength != 0) {
				printf("\tPhysical address: ");
				for (i = 0; i < (int) pCurrAddresses->PhysicalAddressLength;
					i++) {
						if (i == (pCurrAddresses->PhysicalAddressLength - 1))
							printf("%.2X\n",
							(int) pCurrAddresses->PhysicalAddress[i]);
						else
							printf("%.2X-",
							(int) pCurrAddresses->PhysicalAddress[i]);
				}
			}
			printf("\tFlags: %ld\n", pCurrAddresses->Flags);
			printf("\tMtu: %lu\n", pCurrAddresses->Mtu);
			printf("\tIfType: %ld\n", pCurrAddresses->IfType);
			printf("\tOperStatus: %ld\n", pCurrAddresses->OperStatus);
			printf("\tIpv6IfIndex (IPv6 interface): %u\n",
				pCurrAddresses->Ipv6IfIndex);
			printf("\tZoneIndices (hex): ");
			for (i = 0; i < 16; i++)
				printf("%lx ", pCurrAddresses->ZoneIndices[i]);
			printf("\n");

			printf("\tTransmit link speed: %I64u\n", pCurrAddresses->TransmitLinkSpeed);
			printf("\tReceive link speed: %I64u\n", pCurrAddresses->ReceiveLinkSpeed);

			pPrefix = pCurrAddresses->FirstPrefix;
			if (pPrefix) {
				for (i = 0; pPrefix != NULL; i++)
					pPrefix = pPrefix->Next;
				printf("\tNumber of IP Adapter Prefix entries: %d\n", i);
			} else
				printf("\tNumber of IP Adapter Prefix entries: 0\n");

			printf("\n");

			pCurrAddresses = pCurrAddresses->Next;
		}
	} else {
		printf("Call to GetAdaptersAddresses failed with error: %d\n",
			dwRetVal);
		if (dwRetVal == ERROR_NO_DATA)
			printf("\tNo addresses were found for the requested parameters\n");
		else {

			if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
				NULL, dwRetVal, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),   
				// Default language
				(LPTSTR) & lpMsgBuf, 0, NULL)) {
					printf("\tError: %s", (const char*)lpMsgBuf);
					LocalFree(lpMsgBuf);
					if (pAddresses)
						FREE(pAddresses);
					exit(1);
			}
		}
	}

	if (pAddresses) {
		FREE(pAddresses);
	}

	return 0;
}
struct ifaddrs  
{  
	struct ifaddrs  *ifa_next;    /* Next item in list */  
	char            *ifa_name;    /* Name of interface */  
	unsigned int     ifa_flags;   /* Flags from SIOCGIFFLAGS */  
	struct sockaddr *ifa_addr;    /* Address of interface */  
	struct sockaddr *ifa_netmask; /* Netmask of interface */  
	union  
	{  
		struct sockaddr *ifu_broadaddr; /* Broadcast address of interface */  
		struct sockaddr *ifu_dstaddr; /* Point-to-point destination address */  
	} ifa_ifu;  
#define              ifa_broadaddr ifa_ifu.ifu_broadaddr  
#define              ifa_dstaddr   ifa_ifu.ifu_dstaddr  
	void            *ifa_data;    /* Address-specific data */  
};
int getifaddrs(struct ifaddrs** ifa)
{
	return 0;
}
void freeifaddrs(struct ifaddrs* ifa)
{

}
uint8_t sockaddr_prefixlength(struct sockaddr* addr)
{
	uint8_t prefixlength = 0;
	if (addr->sa_family==AF_INET)  
	{
		uint32_t n = ((struct sockaddr_in *)addr)->sin_addr.s_addr;
		int i = 0;
		while (n > 0) 
		{
			if (n & 1) i++;
			n = n >> 1;
		}
		prefixlength = i;  
	}  
	else if (addr->sa_family==AF_INET6)  
	{
		unsigned char *c = ((struct sockaddr_in6 *)addr)->sin6_addr.s6_addr;
		int i = 0, j = 0;
		unsigned char n = 0;
		while (i < 16) 
		{
			n = c[i];
			while (n > 0) 
			{
				if (n & 1) j++;
				n = n/2;
			}
			i++;
		}
		prefixlength = j;  
	}   
	return prefixlength;
}

void printf_getifaddrs()
{
	struct ifaddrs * ifa=NULL;  
	struct ifaddrs * ifas=NULL;  
	void * tmpAddrPtr=NULL;  

	getifaddrs(&ifas);  
	ifa = ifas;
	while (ifa!=NULL)   
	{  
		printf("name:%s\n", ifa->ifa_name);
		if ( NULL != ifa->ifa_addr )
		{
			if (ifa->ifa_addr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("IPV4 Address %s\n", addressBuffer);   
			}  
			else if (ifa->ifa_addr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("IPV6 Address %s\n", addressBuffer);   
			}   
		}
		if ( NULL != ifa->ifa_broadaddr )
		{
			if (ifa->ifa_broadaddr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_broadaddr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("broadaddr IPV4 Address %s\n", addressBuffer);   
			}  
			else if (ifa->ifa_broadaddr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_broadaddr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("broadaddr IPV6 Address %s\n", addressBuffer);   
			}   
		}
		if ( NULL != ifa->ifa_dstaddr )
		{
			if (ifa->ifa_dstaddr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_dstaddr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("dstaddr IPV4 Address %s\n", addressBuffer);   
			}  
			else if (ifa->ifa_dstaddr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_dstaddr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("dstaddr IPV6 Address %s\n", addressBuffer);   
			}   
		}
		if ( NULL != ifa->ifa_netmask )
		{
			if (ifa->ifa_netmask->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				uint8_t prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
				printf("netmask IPV4 Address %s %d\n", addressBuffer,prefixlength);   
			}  
			else if (ifa->ifa_netmask->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				uint8_t prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
				printf("netmask IPV6 Address %s %d\n", addressBuffer,prefixlength);   
			}   
		}
		printf("\n");   
		ifa = ifa->ifa_next;  
	}  	

	freeifaddrs(ifas);
}
struct if_address_info 
{
	struct mm_sockaddr address;
	uint8_t prefixlength;
};
struct if_adapter_info
{
	std::string name;
	std::list<struct if_address_info> address_v4;
	std::list<struct if_address_info> address_v6;
	std::list<struct mm_sockaddr> broadaddr_v4;
	std::list<struct mm_sockaddr> broadaddr_v6;
};
struct if_adapters
{
	std::map<std::string, struct if_adapter_info> adapters;
};
void get_if_adapters(struct if_adapters* p)
{
	struct ifaddrs * ifa=NULL;  
	struct ifaddrs * ifas=NULL;  
	void * tmpAddrPtr=NULL;  

	getifaddrs(&ifas);  
	ifa = ifas;
	while (ifa!=NULL)   
	{  
		printf("name:%s\n", ifa->ifa_name);
		struct if_adapter_info& adapter_info = p->adapters[ifa->ifa_name];
		adapter_info.name = ifa->ifa_name;
		if ( NULL != ifa->ifa_addr )
		{
			if (ifa->ifa_addr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("IPV4 Address %s\n", addressBuffer);
				struct if_address_info info;
				memcpy(&info.address, ifa->ifa_addr, sizeof(struct sockaddr_in));
				if ( NULL != ifa->ifa_netmask )
				{
					info.prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
				}
				adapter_info.address_v4.push_back(info);
			}  
			else if (ifa->ifa_addr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("IPV6 Address %s\n", addressBuffer);   
				struct if_address_info info;
				memcpy(&info.address, ifa->ifa_addr, sizeof(struct sockaddr_in6));
				if ( NULL != ifa->ifa_netmask )
				{
					info.prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
				}
				adapter_info.address_v6.push_back(info);
			}   
		}
		if ( NULL != ifa->ifa_broadaddr )
		{
			if (ifa->ifa_broadaddr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_broadaddr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("broadaddr IPV4 Address %s\n", addressBuffer);   
				struct mm_sockaddr info;
				memcpy(&info, ifa->ifa_broadaddr, sizeof(struct sockaddr_in));
				adapter_info.broadaddr_v4.push_back(info);
			}  
			else if (ifa->ifa_broadaddr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_broadaddr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("broadaddr IPV6 Address %s\n", addressBuffer);   
				struct mm_sockaddr info;
				memcpy(&info, ifa->ifa_broadaddr, sizeof(struct sockaddr_in6));
				adapter_info.broadaddr_v6.push_back(info);
			}   
		}
		if ( NULL != ifa->ifa_dstaddr )
		{
			if (ifa->ifa_dstaddr->sa_family==AF_INET)  
			{   // check it is IP4  
				// is a valid IP4 Address  
				tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_dstaddr)->sin_addr;  
				char addressBuffer[INET_ADDRSTRLEN];  
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
				printf("dstaddr IPV4 Address %s\n", addressBuffer);   
			}  
			else if (ifa->ifa_dstaddr->sa_family==AF_INET6)  
			{   // check it is IP6  
				// is a valid IP6 Address  
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_dstaddr)->sin_addr;  
				char addressBuffer[INET6_ADDRSTRLEN];  
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
				printf("dstaddr IPV6 Address %s\n", addressBuffer);   
			}   
		}
		//if ( NULL != ifa->ifa_netmask )
		//{
		//	if (ifa->ifa_netmask->sa_family==AF_INET)  
		//	{   // check it is IP4  
		//		// is a valid IP4 Address  
		//		tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr;  
		//		char addressBuffer[INET_ADDRSTRLEN];  
		//		inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);  
		//		uint8_t prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
		//		printf("netmask IPV4 Address %s %d\n", addressBuffer,prefixlength);   
		//		adapter_info.address_v4.prefixlength = prefixlength;
		//	}  
		//	else if (ifa->ifa_netmask->sa_family==AF_INET6)  
		//	{   // check it is IP6  
		//		// is a valid IP6 Address  
		//		tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr;  
		//		char addressBuffer[INET6_ADDRSTRLEN];  
		//		inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);  
		//		uint8_t prefixlength = sockaddr_prefixlength(ifa->ifa_netmask);
		//		printf("netmask IPV6 Address %s %d\n", addressBuffer,prefixlength);   
		//		adapter_info.address_v6.prefixlength = prefixlength;
		//	}   
		//}
		printf("\n");   
		ifa = ifa->ifa_next;  
	}  	

	freeifaddrs(ifas);
}
void mm_shuttle_test_address_func()
{
	//{
	//	char node[MM_NODE_NAME_LENGTH] = {0};
	//	mm_ushort_t port = 0;

	//	struct if_adapters adapters;
	//	get_if_adapters(&adapters);
	//	for (std::map<std::string, struct if_adapter_info>::iterator it = adapters.adapters.begin();
	//		it!=adapters.adapters.end();++it)
	//	{
	//		struct if_adapter_info& adapter_info = it->second;

	//		printf("name: %s\n", adapter_info.name.c_str());   
	//		for (std::list<struct if_address_info>::iterator iit = adapter_info.address_v4.begin();
	//			iit!=adapter_info.address_v4.end();++iit)
	//		{
	//			struct if_address_info& ai = *iit;
	//			memset(node,0,MM_NODE_NAME_LENGTH);
	//			mm_sockaddr_node_port( &ai.address, node, &port );
	//			printf("  address_v4: %s/%d\n", node, ai.prefixlength);   
	//		}
	//		for (std::list<struct if_address_info>::iterator iit = adapter_info.address_v6.begin();
	//			iit!=adapter_info.address_v6.end();++iit)
	//		{
	//			struct if_address_info& ai = *iit;
	//			memset(node,0,MM_NODE_NAME_LENGTH);
	//			mm_sockaddr_node_port( &ai.address, node, &port );
	//			printf("  address_v6: %s/%d\n", node, ai.prefixlength);   
	//		}
	//		for (std::list<struct mm_sockaddr>::iterator iit = adapter_info.broadaddr_v4.begin();
	//			iit!=adapter_info.broadaddr_v4.end();++iit)
	//		{
	//			struct mm_sockaddr& ai = *iit;
	//			memset(node,0,MM_NODE_NAME_LENGTH);
	//			mm_sockaddr_node_port( &ai, node, &port );
	//			printf("  broadaddr_v4: %s\n", node);
	//		}
	//		for (std::list<struct mm_sockaddr>::iterator iit = adapter_info.broadaddr_v6.begin();
	//			iit!=adapter_info.broadaddr_v6.end();++iit)
	//		{
	//			struct mm_sockaddr& ai = *iit;
	//			memset(node,0,MM_NODE_NAME_LENGTH);
	//			mm_sockaddr_node_port( &ai, node, &port );
	//			printf("  broadaddr_v6: %s\n", node);
	//		}
	//		printf("\n");   
	//	}
	//	//struct sockaddr_in6 impl_ipv6;
	//}
	int a = MIB_IF_TYPE_ETHERNET;
	{
		//struct mm_timer timer;

		struct mm_headset headset;
		mm_headset_init(&headset);

		mm_headset_assign_native(&headset, "0.0.0.0", 0);
		mm_headset_set_length(&headset, 1);
		mm_headset_fopen_socket(&headset);
		mm_headset_bind(&headset);

		char link_name[MM_LINK_NAME_LENGTH] = {0};
		mm_socket_string(&headset.udp.socket, link_name);

		mm_headset_destroy(&headset);
	}
	if (0)
	{
		struct ip_mreq mreq;

		struct sockaddr_in peeraddr;

		int sockfd = 0;

		sockfd = (int)socket(AF_INET, SOCK_DGRAM, 0);
		if (sockfd < 0) 
		{  
			printf("socket creating err in udptalk\n");  
			exit(1);  
		} 
		if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char*)&mreq,sizeof(struct ip_mreq)) == -1) 
		{  
			perror("setsockopt");  
			exit(-1);
		} 
		if (bind(sockfd, (struct sockaddr *) &peeraddr, sizeof(struct sockaddr_in)) == -1) 
		{  
			printf("Bind error\n");  
			exit(0);  
		}  
		//struct ipv6_mreq mreq;
	}
	const char* vv[] = 
	{
		"getadapteraddresses",
		"A",
	};
	dddd_main(2,(char**)vv);
	address_main();
	//printf_getifaddrs();
}
