#include "mm_shuttle_test_loavger.h"

#include "net/mm_message_coder.h"

#include "shuttle_common/mm_sharder_holder.h"
#include "shuttle_common/mm_loavger_holder.h"

#include <map>

int number = 0;
static void __static_net_tcp_finish_default(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)tcp->callback.obj;

	number++;
	//if (number > 20)
	//{
	//	mm_net_tcp_shutdown(net_tcp);
	//	return;
	//}

	for (size_t i = 0; i < 1; i++)
	{
		const size_t max_length = 3 * MM_SOCKET_PAGE_SIZE / 4;
		char m[max_length] = { 0 };
		mm_sprintf(m, "tcp rq++ %d", (int)number);

		struct mm_byte_buffer message;

		struct mm_packet_head packet_head;
		packet_head.mid = 0;

		struct mm_packet pack;

		message.buffer = (mm_uint8_t*)m;
		message.offset = 0;
		message.length = (mm_uint32_t)mm_strlen(m);
		message.length = (mm_uint32_t)max_length - 8;

		mm_message_coder_encode_message_byte_buffer(&tcp->buff_send, &message, &packet_head, 4, &pack);

		mm_net_tcp_o_lock(net_tcp);
		mm_net_tcp_flush_send(net_tcp);
		mm_net_tcp_o_unlock(net_tcp);
		//mm_msleep(1000);
	}
}

void mm_shuttle_test_loavger_func()
{
	{
		mm::mm_m_runtime_state runtime_state;
		struct mm_loavger_holder loavger_holder;
		mm_loavger_holder_init(&loavger_holder);
		struct mm_loavger_holder_elem* e1 = mm_loavger_holder_get_instance(&loavger_holder, 1, NULL);
		e1->weights = 100;
		e1->runtime_state.unique_id = 1;
		e1->runtime_state.weights = e1->weights;
		struct mm_loavger_holder_elem* e2 = mm_loavger_holder_get_instance(&loavger_holder, 2, NULL);
		e2->weights = 100;
		e2->runtime_state.unique_id = 2;
		e2->runtime_state.weights = e2->weights;
		struct mm_loavger_holder_elem* e3 = mm_loavger_holder_get_instance(&loavger_holder, 3, NULL);
		e3->weights = 40;
		e3->runtime_state.unique_id = 3;
		e3->runtime_state.weights = e3->weights;
		struct mm_loavger_holder_elem* e4 = mm_loavger_holder_get_instance(&loavger_holder, 4, NULL);
		e4->weights = 40;
		e4->runtime_state.unique_id = 4;
		e4->runtime_state.weights = e4->weights;
		mm_loavger_holder_cycle_update(&loavger_holder);

		std::map<int,int> int_map;

		for (int i = 0;i < 100;++i)
		{
			int cycle_code = mm_loavger_holder_cycle(&loavger_holder,&runtime_state);
			int_map[runtime_state.unique_id] ++;
		}

		for (std::map<int,int>::iterator it = int_map.begin();
			it != int_map.end(); ++ it)
		{
			mm_printf("%d %d\n",it->first,it->second);
		}

		mm_loavger_holder_destroy(&loavger_holder);
	}
	{
		struct mm_mt_contact contact;
		mm_mt_contact_init(&contact);

		int finally_state = -1;
		mm::mm_m_runtime_state runtime_state;
		mm::mm_m_config_module config_module;
		mm_uint64_t node_unique_id = 0;
		struct mm_sharder_holder sharder_holder;
		mm_sharder_holder_init(&sharder_holder);
		mm_sharder_holder_assign_zkrm_host(&sharder_holder,"127.0.0.1:10300,");
		mm_sharder_holder_assign_zkrm_path(&sharder_holder,"/mm_proxy");
		mm_sharder_holder_start(&sharder_holder);
		mm_msleep(1000);
		mm_sharder_holder_update(&sharder_holder);
		std::map<int,int> int_map;

		node_unique_id = mm_byte_uint64_a(150, 1);
		mm_uint64_t ui_1 = node_unique_id;
		node_unique_id = mm_byte_uint64_a(150, 2);
		mm_uint64_t ui_2 = node_unique_id;

		for (int i = 0;i < 1;++i)
		{
			int cycle_code = mm_sharder_holder_cycle(&sharder_holder,0x46000191,&config_module,&runtime_state);
			node_unique_id = mm_byte_uint64_a(config_module.unique_id, runtime_state.unique_id);
			int_map[runtime_state.unique_id] ++;
			struct mm_mt_tcp* mt_tcp = mm_mt_contact_get_instance(&contact, node_unique_id, MM_ADDR_DEFAULT_NODE, "127.0.0.1", 30002);
			struct mm_net_tcp* net_tcp = mm_mt_tcp_thread_instance(mt_tcp);

			mm_net_tcp_s_lock(net_tcp);
			mm_net_tcp_check(net_tcp);
			finally_state = mm_net_tcp_finally_state(net_tcp);
			mm_net_tcp_s_unlock(net_tcp);

			mm_printf("%d %d %p %p\n", config_module.unique_id, runtime_state.unique_id, mt_tcp, net_tcp);
			__static_net_tcp_finish_default(net_tcp);
		}
		for (std::map<int,int>::iterator it = int_map.begin();
			it != int_map.end(); ++ it)
		{
			mm_printf("%d %d\n",it->first,it->second);
		}
		mm_sharder_holder_join(&sharder_holder);
		mm_sharder_holder_destroy(&sharder_holder);

		mm_mt_contact_destroy(&contact);
	}
}
