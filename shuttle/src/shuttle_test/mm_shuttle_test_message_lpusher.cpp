#include "mm_shuttle_test_message_lpusher.h"

#include "shuttle_common/mm_mulcast_c.h"
#include "shuttle_common/mm_message_lpusher.h"

void mm_shuttle_test_message_lpusher_func()
{
	static const char* buffer_value = "1234567890";

	struct mm_byte_buffer message;
	struct mm_packet_head packet_head;
	struct mm_rbtset_u64 arrays;
	struct mm_packet nt_pack;
	struct mm_message_lpusher message_lpusher;
	mm_byte_buffer_init(&message);
	mm_rbtset_u64_init(&arrays);
	mm_message_lpusher_init(&message_lpusher);

	mm_message_lpusher_assign_queue_remote(&message_lpusher, "101.200.169.28", 50500);
	mm_message_lpusher_assign_queue_passwd(&message_lpusher, "");
	mm_message_lpusher_assign_queue_nameky(&message_lpusher, "mm:cback:001");

	message.buffer = (mm_uint8_t*)buffer_value;
	message.offset = 0;
	message.length = (mm_uint32_t)mm_strlen(buffer_value);

	packet_head.mid = 10086;
	packet_head.sid = 10000;
	packet_head.uid = 0;

	mm_rbtset_u64_add(&arrays, 1);
	mm_rbtset_u64_add(&arrays, 2);
	mm_rbtset_u64_add(&arrays, 3);

	mm_message_lpusher_lpush_arrays_byte_buffer(&message_lpusher, &message, &packet_head, &arrays, &nt_pack);
	mm_message_lpusher_destroy(&message_lpusher);
	mm_rbtset_u64_destroy(&arrays);
	mm_byte_buffer_destroy(&message);
}
