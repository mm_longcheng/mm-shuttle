#ifndef __mm_test_loavger_h__
#define __mm_test_loavger_h__

#include "core/mm_core.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_loavger_func();
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_test_loavger_h__