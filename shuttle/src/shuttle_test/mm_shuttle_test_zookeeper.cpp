#include "mm_shuttle_test_zookeeper.h"

#include "zookeeper/mm_zookeeper_zkrp_path.h"
#include "zookeeper/mm_zookeeper_zkwp_path.h"


void mm_shuttle_test_zookeeper_func()
{
	struct mm_zkrp_path zkrp_path;
	struct mm_zkwp_path zkwp_path;

	mm_zkrp_path_init(&zkrp_path);
	mm_zkwp_path_init(&zkwp_path);
	//
	mm_zkrp_path_assign_path(&zkrp_path,"/test_zk");
	mm_zkrp_path_assign_host(&zkrp_path,"127.0.0.1:10300,");

	mm_zkwp_path_assign_path(&zkwp_path,"/test_zk");
	mm_zkwp_path_assign_host(&zkwp_path,"127.0.0.1:10300,");
	//
	mm_zkrp_path_watcher(&zkrp_path);
	mm_zkwp_path_watcher(&zkwp_path);
	//
	mm_zkwp_path_assign_unique_id(&zkwp_path,0);
	mm_zkwp_path_assign_slot(&zkwp_path,1,0);

	mm_zkwp_path_commit(&zkwp_path);

	mm_msleep(30000);
	//
	mm_zkwp_path_abandon(&zkwp_path);
	mm_msleep(30000);
	mm_zkrp_path_abandon(&zkrp_path);
	mm_msleep(30000);
	//
	mm_zkrp_path_destroy(&zkrp_path);
	mm_zkwp_path_destroy(&zkwp_path);
}
