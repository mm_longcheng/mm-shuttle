#include "mm_shuttle_test_parameters.h"

#include "core/mm_string.h"

#include "core/mm_parameters.h"

static const char* _argument_impl[] = 
{
	"./mm_shuttle_test",
	"--logdir=/data/raidcall/log/vnc_live_cback_1",
	"-p=10086",
	//"--logdir=./log",
	"--loglvl=7",
	"--unique_id=0",
	"--internal_mailbox=::-65535(2)",
	"--zookeeper_import=127.0.0.1:10300,",
	"--rdb_account_cache=192.168.111.123-65535",
	"--rdb_message_queue=192.168.111.123-65535(2)",
	"--queue_name=mm:cback:001",
	"--pop_mode=rpop",
	"--server_id=1",
};
const struct mm_argument _argument = { MM_ARRAY_SIZE(_argument_impl),(const char**)_argument_impl, };
//--logdir=/data/raidcall/log/vnc_live_cback_1
void mm_shuttle_test_parameters_func()
{
	struct mm_string p;
	mm_string_init(&p);
	mm_string_assigns(&p, "p");
	mm_string_destroy(&p);

	struct mm_string q;
	mm_string_make_weak(&q, "p");

	struct mm_parameters parameters;
	mm_parameters_init(&parameters);
	mm_parameters_assign_name_snap(&parameters, "port", "p");
	mm_parameters_assign_name_snap(&parameters, "logdir", "");
	mm_parameters_assign_argument(&parameters, (struct mm_argument*)&_argument);
	struct mm_parameters_option* pp = mm_parameters_get_instance_option_by_name(&parameters, "logdir");
	mm_parameters_destroy(&parameters);
}
