#ifndef __mm_openssl_test_h__
#define __mm_openssl_test_h__


#include "core/mm_core.h"
#include "core/mm_argument.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_test_openssl_func();
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_openssl_test_h__