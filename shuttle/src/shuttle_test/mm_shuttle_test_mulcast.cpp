#include "mm_shuttle_test_mulcast.h"
#include "shuttle_common/mm_mulcast_c.h"

void mm_shuttle_test_mulcast_func()
{
	static const char* buffer_value = "1234567890";

	struct mm_streambuf streambuf_0;
	struct mm_streambuf streambuf_1;
	struct mm_streambuf streambuf_2;
	struct mm_streambuf streambuf_3;

	struct mm_mulcast_vector o_mulcast_1;
	struct mm_mulcast_vector i_mulcast_1;

	struct mm_mulcast_rbtset o_mulcast_2;
	struct mm_mulcast_rbtset i_mulcast_2;

	mm_streambuf_init(&streambuf_0);
	mm_streambuf_init(&streambuf_1);
	mm_streambuf_init(&streambuf_2);
	mm_streambuf_init(&streambuf_3);

	mm_mulcast_vector_init(&o_mulcast_1);
	mm_mulcast_vector_init(&i_mulcast_1);

	mm_mulcast_rbtset_init(&o_mulcast_2);
	mm_mulcast_rbtset_init(&i_mulcast_2);
	//////////////////////////////////////////////////////////////////////////
	o_mulcast_1.buffer.buffer = (mm_uint8_t*)buffer_value;
	o_mulcast_1.buffer.offset = 0;
	o_mulcast_1.buffer.length = (mm_uint32_t)mm_strlen(buffer_value);
	mm_vector_u64_push_back(&o_mulcast_1.arrays, 9008000000009000);
	mm_vector_u64_push_back(&o_mulcast_1.arrays, 9008000000009001);
	mm_vector_u64_push_back(&o_mulcast_1.arrays, 9008000000009002);
	mm_vector_u64_push_back(&o_mulcast_1.arrays, 9008000000009003);
	//
	mm_mulcast_vector_encode(&o_mulcast_1, &streambuf_1);
	mm_memcpy((void*)(o_mulcast_1.buffer.buffer + o_mulcast_1.buffer.offset),buffer_value,10);
	mm_mulcast_vector_decode(&i_mulcast_1, &streambuf_1);
	//////////////////////////////////////////////////////////////////////////
	o_mulcast_2.buffer.buffer = (mm_uint8_t*)buffer_value;
	o_mulcast_2.buffer.offset = 0;
	o_mulcast_2.buffer.length = (mm_uint32_t)mm_strlen(buffer_value);
	mm_rbtset_u64_add(&o_mulcast_2.arrays, 9008000000009000);
	mm_rbtset_u64_add(&o_mulcast_2.arrays, 9008000000009001);
	mm_rbtset_u64_add(&o_mulcast_2.arrays, 9008000000009002);
	mm_rbtset_u64_add(&o_mulcast_2.arrays, 9008000000009003);
	//
	mm_mulcast_rbtset_encode(&o_mulcast_2, &streambuf_2);
	mm_memcpy((void*)(o_mulcast_2.buffer.buffer + o_mulcast_2.buffer.offset),buffer_value,10);
	mm_mulcast_rbtset_decode(&i_mulcast_2, &streambuf_2);
	//////////////////////////////////////////////////////////////////////////
	int len_0 = (mm_uint32_t)mm_streambuf_size(&streambuf_0);
	int len_1 = (mm_uint32_t)mm_streambuf_size(&streambuf_1);
	int len_2 = (mm_uint32_t)mm_streambuf_size(&streambuf_2);
	int len_3 = (mm_uint32_t)mm_streambuf_size(&streambuf_3);
	if (len_0 == len_1)
	{
		int a = mm_memcmp(streambuf_0.buff, streambuf_1.buff, streambuf_0.pptr);
		printf("memcmp 0 1 %d\n", a);
		{
			for (size_t i = 0;i < streambuf_0.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_0.buff + i)));
			}
			printf("\n");
		}
		{
			for (size_t i = 0;i < streambuf_1.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_1.buff + i)));
			}
			printf("\n");
		}
		{
			size_t index = 0;
			for (index = 0; index < i_mulcast_1.arrays.size; ++ index)
			{
				printf("vector_u64 %" PRIu64 "\n", mm_vector_u64_get_index(&i_mulcast_1.arrays, index));
			}
		}
	}
	if (len_0 == len_2)
	{
		int a = mm_memcmp(streambuf_0.buff, streambuf_2.buff, streambuf_0.pptr);
		printf("memcmp 0 2 %d\n", a);
		{
			for (size_t i = 0;i < streambuf_0.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_0.buff + i)));
			}
			printf("\n");
		}
		{
			for (size_t i = 0;i < streambuf_2.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_2.buff + i)));
			}
			printf("\n");
		}
		{
			struct mm_rb_node* n = NULL;
			struct mm_rbtset_u64_iterator* it = NULL;
			n = mm_rb_last(&i_mulcast_2.arrays.rbt);
			while(NULL != n)
			{
				it = (struct mm_rbtset_u64_iterator*)mm_rb_entry(n, struct mm_rbtset_u64_iterator, n);
				n = mm_rb_prev(n);
				printf("rbtset_u64 %" PRIu64 "\n", it->k);
			}
		}
	}
	if (len_0 == len_3)
	{
		int a = mm_memcmp(streambuf_0.buff, streambuf_3.buff, streambuf_0.pptr);
		printf("memcmp 0 3 %d\n", a);
		{
			for (size_t i = 0;i < streambuf_0.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_0.buff + i)));
			}
			printf("\n");
		}
		{
			for (size_t i = 0;i < streambuf_3.pptr; ++i)
			{
				printf("%d ",((int)*(char*)(streambuf_3.buff + i)));
			}
			printf("\n");
		}
	}
	//////////////////////////////////////////////////////////////////////////
	mm_streambuf_destroy(&streambuf_0);
	mm_streambuf_destroy(&streambuf_1);
	mm_streambuf_destroy(&streambuf_2);
	mm_streambuf_destroy(&streambuf_3);
	mm_mulcast_vector_destroy(&o_mulcast_1);
	mm_mulcast_vector_destroy(&i_mulcast_1);
	mm_mulcast_rbtset_destroy(&o_mulcast_2);
	mm_mulcast_rbtset_destroy(&i_mulcast_2);
}
