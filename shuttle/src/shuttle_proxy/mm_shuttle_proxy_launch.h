﻿#ifndef __mm_shuttle_proxy_launch_h__
#define __mm_shuttle_proxy_launch_h__

#include "core/mm_core.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
// 服务实例编名称    mm_shuttle_proxy_0
// 程序名            mm_shuttle_proxy
// 日志文件夹路径    ../log
// 日志等级          7
// 实例编号          1
// 内地址启动参数    127.0.0.1-10002[2]
// 外地址启动参数    127.0.0.1-20002[2]
// 读取监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
// 写入监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
// 模块号            100
// 包号区间左        0x01000100
// 包号区间右        0x010001FF
// 分片规模(0用负载) 2
struct mm_shuttle_proxy_launch
{
	mm_uint32_t unique_id;
	struct mm_string internal_mailbox_parameters;
	struct mm_string external_mailbox_parameters;
	struct mm_string zookeeper_import_parameters;
	struct mm_string zookeeper_export_parameters;
	mm_uint32_t module;
	mm_uint32_t mid_l;
	mm_uint32_t mid_r;
	mm_uint32_t shard;

	mm_uint64_t number_id;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_proxy_launch_init(struct mm_shuttle_proxy_launch* p);
extern void mm_shuttle_proxy_launch_destroy(struct mm_shuttle_proxy_launch* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_proxy_launch_load_config( struct mm_shuttle_proxy_launch* p, const char* config_file );
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_proxy_launch_printf_information(struct mm_shuttle_proxy_launch* p);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_proxy_launch_h__