//
//	Generated by lua
//	copyright longer 2019
//  mm_q_cobweb_common_proxy.cpp

#include "mm_q_cobweb_common_proxy.h"

namespace mm
{
	//
	//	struct definition for p_get_config_launch_proxy	//
	p_get_config_launch_proxy::~p_get_config_launch_proxy()
	{

	}
	p_get_config_launch_proxy::p_get_config_launch_proxy()
		:code(0)
		,info()
	{

	}
	void p_get_config_launch_proxy::encode(mm_o_archive& archive) const
	{
		archive << code;
		archive << info;
	}
	void p_get_config_launch_proxy::decode(const mm_i_archive& archive)
	{
		archive >> code;
		archive >> info;
	}
	int p_get_config_launch_proxy::query(struct mm_db_mysql* _sql, mm_uint32_t logger_level)
	{
		int error_code = db_mysql_state_unknown;
		struct mm_logger* g_logger = mm_logger_instance();
		assert(NULL != _sql && "p_get_config_launch_proxy input struct mm_db_mysql is a null.");
		mm_db_mysql_query db(_sql);
		mm_db_mysql_stream oss( mm_db_mysql_get_context( _sql ) );
		mm_db_mysql_lock(_sql);
		oss
			<< "call xcbb_cobweb.p_get_config_launch_proxy("
			<< " " << "@code"
			<< ",'" << info.unique_id << "'" << " );"
			<< "select "
			<< " " << "@code" << "  ;";
		const std::string& sql_str = oss.get_string();
		mm_logger_message(g_logger,logger_level,"dump sql:%s",sql_str.c_str());
		if ( 0 == mm_db_mysql_real_query( _sql, sql_str.c_str() ) )
		{
			// result 1.
			mm_db_mysql_store_next_result(_sql);
			{
				assert( 5 == mm_db_mysql_get_field_count(_sql) && "field count is not match.");
				if ( 0 != mm_db_mysql_get_has_row(_sql) )
				{
					db.get_field( 0, info.zkimport_address );
					db.get_field( 1, info.zkexport_address );
					db.get_field( 2, info.internal_address );
					db.get_field( 3, info.internal_workers );
					db.get_field( 4, info.module );
				}
			}
			mm_db_mysql_free_current_result(_sql);
			// result 2.
			mm_db_mysql_store_next_result(_sql);
			{
				assert( 1 == mm_db_mysql_get_field_count(_sql) && "field count is not match.");
				if ( 0 != mm_db_mysql_get_has_row(_sql) )
				{
					db.get_field( 0, code );
				}
			}
			mm_db_mysql_free_current_result(_sql);
			// free unused result.
			mm_db_mysql_free_unused_result(_sql);
			//
			error_code = db_mysql_state_success;
		}
		else
		{
			error_code = db_mysql_state_failure;
		}
		mm_db_mysql_unlock(_sql);
		return error_code;
	}
	//
	//	struct definition for p_set_config_launch_proxy	//
	p_set_config_launch_proxy::~p_set_config_launch_proxy()
	{

	}
	p_set_config_launch_proxy::p_set_config_launch_proxy()
		:code(0)
		,info()
	{

	}
	void p_set_config_launch_proxy::encode(mm_o_archive& archive) const
	{
		archive << code;
		archive << info;
	}
	void p_set_config_launch_proxy::decode(const mm_i_archive& archive)
	{
		archive >> code;
		archive >> info;
	}
	int p_set_config_launch_proxy::query(struct mm_db_mysql* _sql, mm_uint32_t logger_level)
	{
		int error_code = db_mysql_state_unknown;
		struct mm_logger* g_logger = mm_logger_instance();
		assert(NULL != _sql && "p_set_config_launch_proxy input struct mm_db_mysql is a null.");
		mm_db_mysql_query db(_sql);
		mm_db_mysql_stream oss( mm_db_mysql_get_context( _sql ) );
		mm_db_mysql_lock(_sql);
		oss
			<< "call xcbb_cobweb.p_set_config_launch_proxy("
			<< " " << "@code"
			<< ",'" << info.unique_id << "'"
			<< ",'" << info.zkimport_address << "'"
			<< ",'" << info.zkexport_address << "'"
			<< ",'" << info.internal_address << "'"
			<< ",'" << info.internal_workers << "'"
			<< ",'" << info.module << "'" << " );"
			<< "select "
			<< " " << "@code" << "  ;";
		const std::string& sql_str = oss.get_string();
		mm_logger_message(g_logger,logger_level,"dump sql:%s",sql_str.c_str());
		if ( 0 == mm_db_mysql_real_query( _sql, sql_str.c_str() ) )
		{
			// result 1.
			mm_db_mysql_store_next_result(_sql);
			{
				assert( 1 == mm_db_mysql_get_field_count(_sql) && "field count is not match.");
				if ( 0 != mm_db_mysql_get_has_row(_sql) )
				{
					db.get_field( 0, code );
				}
			}
			mm_db_mysql_free_current_result(_sql);
			// free unused result.
			mm_db_mysql_free_unused_result(_sql);
			//
			error_code = db_mysql_state_success;
		}
		else
		{
			error_code = db_mysql_state_failure;
		}
		mm_db_mysql_unlock(_sql);
		return error_code;
	}
	//
	//	struct definition for p_get_config_launch_module_proxy	//
	p_get_config_launch_module_proxy::~p_get_config_launch_module_proxy()
	{

	}
	p_get_config_launch_module_proxy::p_get_config_launch_module_proxy()
		:code(0)
		,info()
	{

	}
	void p_get_config_launch_module_proxy::encode(mm_o_archive& archive) const
	{
		archive << code;
		archive << info;
	}
	void p_get_config_launch_module_proxy::decode(const mm_i_archive& archive)
	{
		archive >> code;
		archive >> info;
	}
	int p_get_config_launch_module_proxy::query(struct mm_db_mysql* _sql, mm_uint32_t logger_level)
	{
		int error_code = db_mysql_state_unknown;
		struct mm_logger* g_logger = mm_logger_instance();
		assert(NULL != _sql && "p_get_config_launch_module_proxy input struct mm_db_mysql is a null.");
		mm_db_mysql_query db(_sql);
		mm_db_mysql_stream oss( mm_db_mysql_get_context( _sql ) );
		mm_db_mysql_lock(_sql);
		oss
			<< "call xcbb_cobweb.p_get_config_launch_module_proxy("
			<< " " << "@code"
			<< ",'" << info.unique_id << "'" << " );"
			<< "select "
			<< " " << "@code" << "  ;";
		const std::string& sql_str = oss.get_string();
		mm_logger_message(g_logger,logger_level,"dump sql:%s",sql_str.c_str());
		if ( 0 == mm_db_mysql_real_query( _sql, sql_str.c_str() ) )
		{
			// result 1.
			mm_db_mysql_store_next_result(_sql);
			{
				assert( 9 == mm_db_mysql_get_field_count(_sql) && "field count is not match.");
				if ( 0 != mm_db_mysql_get_has_row(_sql) )
				{
					db.get_field( 0, info.zkimport_address );
					db.get_field( 1, info.zkexport_address );
					db.get_field( 2, info.internal_address );
					db.get_field( 3, info.internal_workers );
					db.get_field( 4, info.module );
					db.get_field( 5, info.mid_l );
					db.get_field( 6, info.mid_r );
					db.get_field( 7, info.shard_size );
					db.get_field( 8, info.depth_size );
				}
			}
			mm_db_mysql_free_current_result(_sql);
			// result 2.
			mm_db_mysql_store_next_result(_sql);
			{
				assert( 1 == mm_db_mysql_get_field_count(_sql) && "field count is not match.");
				if ( 0 != mm_db_mysql_get_has_row(_sql) )
				{
					db.get_field( 0, code );
				}
			}
			mm_db_mysql_free_current_result(_sql);
			// free unused result.
			mm_db_mysql_free_unused_result(_sql);
			//
			error_code = db_mysql_state_success;
		}
		else
		{
			error_code = db_mysql_state_failure;
		}
		mm_db_mysql_unlock(_sql);
		return error_code;
	}
}