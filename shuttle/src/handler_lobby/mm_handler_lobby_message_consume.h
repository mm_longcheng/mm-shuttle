#ifndef __mm_handler_lobby_message_consume_h__
#define __mm_handler_lobby_message_consume_h__

#include "core/mm_core.h"

struct mm_handler_lobby;
//////////////////////////////////////////////////////////////////////////
extern void mm_handler_lobby_message_consume_config(struct mm_handler_lobby* impl);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_handler_lobby_message_consume_h__