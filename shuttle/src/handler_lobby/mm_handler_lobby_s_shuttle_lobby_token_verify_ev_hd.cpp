#include "mm_handler_lobby_hd.h"

#include "core/mm_logger.h"
#include "core/mm_time_cache.h"

#include "net/mm_packet.h"

#include "protobuf/mm_protobuff_cxx.h"
#include "protobuf/mm_protobuff_cxx_net.h"

#include "cxx/protodef/c_shuttle_lobby.pb.h"
#include "cxx/protodef/s_shuttle_lobby.pb.h"

#include "shuttle_common/mm_error_code_common.h"

#include "mm_handler_lobby.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void hd_s_shuttle_lobby_token_verify_ev(void* obj, void* u, struct mm_packet* ev_pack)
{
	s_shuttle_lobby::token_verify_ev ev_msg;
	b_error::info error_info_impl;
	b_error::info* error_info = &error_info_impl;
	struct mm_string proto_desc;
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_handler_lobby* impl = (struct mm_handler_lobby*)(u);
	struct mm_error_desc* error_desc = &impl->error_desc;
	////////////////////////////////
	error_info->set_code(0);
	error_info->set_desc("");
	////////////////////////////////
	mm_string_init(&proto_desc);
	do
	{
		if (0 != mm_protobuf_cxx_decode_message(ev_pack, &ev_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc(mm_error_desc_string(error_desc, error_info->code()));
			break;
		}
		// logger ev.
		mm_protobuf_cxx_logger_append_packet_message(&proto_desc, ev_pack, &ev_msg);
		mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		mm_uint64_t uid = ev_msg.uid();
		mm_uint64_t sid_now = ev_msg.sid_now();
		mm_uint64_t sid_old = ev_msg.sid_old();
		if (0 != sid_old && sid_now != sid_old)
		{
			struct mm_time_cache* g_time_cache = mm_time_cache_instance();
			struct mm_packet nt_pack;
			struct mm_packet_head packet_head;

			packet_head.mid = c_shuttle_lobby::token_verify_nt_msg_id;
			packet_head.pid = 0;
			packet_head.sid = sid_old;
			packet_head.uid = 0;

			c_shuttle_lobby::token_verify_nt nt_msg;
			nt_msg.set_uid(uid);
			nt_msg.set_timecode_remote(g_time_cache->msec_current);

			mm_message_lpusher_lpush_buffer_protobuf(&impl->message_lpusher, &nt_msg, &packet_head, &nt_pack);

			mm_logger_log_T(g_logger, "%s %d token_verify_ev handle" " sid_now: %" PRIu64 " sid_old: %" PRIu64 " uid: %" PRIu64 "", __FUNCTION__, __LINE__,
				sid_now,
				sid_old,
				uid);

			// logger nt.
			mm_string_clear(&proto_desc);
			mm_protobuf_cxx_logger_append_packet_message(&proto_desc, &nt_pack, &nt_msg);
			mm_logger_log_D(g_logger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
		}
		//////////////////////////////////////////////////////////////////////////
		// logger.
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	// logger error.
	if (0 != error_info->code())
	{
		mm_uint32_t code = error_info->code();
		const std::string& desc = error_info->desc();
		mm_logger_log_E(g_logger, "%s %d (%u)%s", __FUNCTION__, __LINE__, code, desc.c_str());
	}
}
