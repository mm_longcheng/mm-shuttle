#ifndef __mm_handler_lobby_h__
#define __mm_handler_lobby_h__

#include "core/mm_core.h"
#include "core/mm_timer.h"

#include "random/mm_xoshiro.h"

#include "dish/mm_error_desc.h"

#include "rabbitmq/mm_amqp_consume_array.h"

#include "shuttle_common/mm_message_lpusher.h"

#include "mm_handler_lobby_launch.h"

#include "core/mm_prefix.h"

#define MM_HANDLER_LOBBY_RABBITMQ_SECRET_LENGTH 16

struct mm_handler_lobby
{
	struct mm_handler_lobby_launch launch_info;
	struct mm_amqp_consume_array consume_array;
	struct mm_message_lpusher message_lpusher;
	struct mm_xoshiro256starstar rabbitmq_password_random;
	struct mm_error_desc error_desc;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_handler_lobby_init(struct mm_handler_lobby* p);
extern void mm_handler_lobby_destroy(struct mm_handler_lobby* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_handler_lobby_assign_unique_id(struct mm_handler_lobby* p, mm_uint32_t unique_id);
extern void mm_handler_lobby_assign_rdb_gateway_queue_parameters(struct mm_handler_lobby* p, const char* parameters);
extern void mm_handler_lobby_assign_event_queue_rabbitmq_parameters(struct mm_handler_lobby* p, const char* parameters);
extern void mm_handler_lobby_assign_queue_name(struct mm_handler_lobby* p, const char* queue_name);
extern void mm_handler_lobby_assign_rabbitmq_password_seed(struct mm_handler_lobby* p, mm_uint32_t rabbitmq_password_seed);
//////////////////////////////////////////////////////////////////////////
extern void mm_handler_lobby_start(struct mm_handler_lobby* p);
extern void mm_handler_lobby_interrupt(struct mm_handler_lobby* p);
extern void mm_handler_lobby_shutdown(struct mm_handler_lobby* p);
extern void mm_handler_lobby_join(struct mm_handler_lobby* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_handler_lobby_h__