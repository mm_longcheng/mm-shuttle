#ifndef __mm_shuttle_password_h__
#define __mm_shuttle_password_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "random/mm_xoshiro.h"

#define MM_SHUTTLE_PASSWORD_SECRET_LENGTH 16

struct mm_shuttle_password
{
	struct mm_xoshiro256starstar password_random;
	mm_uint32_t secret_length;
	mm_uint32_t seed;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_password_init(struct mm_shuttle_password* p);
extern void mm_shuttle_password_destroy(struct mm_shuttle_password* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_password_assign_secret_length(struct mm_shuttle_password* p, mm_uint32_t secret_length);
extern void mm_shuttle_password_assign_secret_seed(struct mm_shuttle_password* p, mm_uint32_t seed);
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_password_start(struct mm_shuttle_password* p);
extern void mm_shuttle_password_interrupt(struct mm_shuttle_password* p);
extern void mm_shuttle_password_shutdown(struct mm_shuttle_password* p);
extern void mm_shuttle_password_join(struct mm_shuttle_password* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_shuttle_password_h__