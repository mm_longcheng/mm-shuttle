#include "mm_shuttle_password.h"
#include "core/mm_logger.h"

#include "dish/mm_xoshiro_string.h"

#include "mm_application.h"

//////////////////////////////////////////////////////////////////////////
void mm_shuttle_password_init(struct mm_shuttle_password* p)
{
	mm_xoshiro256starstar_init(&p->password_random);
	p->secret_length = MM_SHUTTLE_PASSWORD_SECRET_LENGTH;
	p->seed = 0;
}
void mm_shuttle_password_destroy(struct mm_shuttle_password* p)
{
	mm_xoshiro256starstar_destroy(&p->password_random);
	p->secret_length = MM_SHUTTLE_PASSWORD_SECRET_LENGTH;
	p->seed = 0;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_password_assign_secret_length(struct mm_shuttle_password* p, mm_uint32_t secret_length)
{
	p->secret_length = secret_length;
}
void mm_shuttle_password_assign_secret_seed(struct mm_shuttle_password* p, mm_uint32_t seed)
{
	p->seed = seed;
}
//////////////////////////////////////////////////////////////////////////
void mm_shuttle_password_start(struct mm_shuttle_password* p)
{
	struct mm_application* g_application = mm_application_instance();

	struct mm_string password;

	mm_string_init(&password);

	mm_xoshiro256starstar_srand(&p->password_random, p->seed);
	mm_xoshiro256starstar_random_string(&p->password_random, &password, p->secret_length);

	mm_printf("secret_length: %u seed: %u password: %s\n", p->secret_length, p->seed, password.s);

	mm_string_destroy(&password);

	mm_application_shutdown(g_application);
}
void mm_shuttle_password_interrupt(struct mm_shuttle_password* p)
{

}
void mm_shuttle_password_shutdown(struct mm_shuttle_password* p)
{

}
void mm_shuttle_password_join(struct mm_shuttle_password* p)
{

}
//////////////////////////////////////////////////////////////////////////