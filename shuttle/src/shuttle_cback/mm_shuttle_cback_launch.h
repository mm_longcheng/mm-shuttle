﻿#ifndef __mm_shuttle_cback_launch_h__
#define __mm_shuttle_cback_launch_h__

#include "core/mm_core.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
// 服务实例编名称    mm_shuttle_cback_0
// 程序名            mm_shuttle_cback
// 日志文件夹路径    ../log
// 日志等级          7
// 实例编号          1
// 读取监控集群号    127.0.0.1:10300,127.0.0.1:10301,127.0.0.1:10302,
// 读取缓存地址号    192.168.111.123-65535
// 弹队列启动参数    192.168.111.123-65535[2]
// 弹出队列管道名    mm:queue:lobby:cback:000
// 弹出方式          brpop
//////////////////////////////////////////////////////////////////////////
struct mm_shuttle_cback_launch
{
	mm_uint32_t unique_id;
	struct mm_string zookeeper_import_parameters;
	struct mm_string rdb_account_cache_parameters;
	struct mm_string rdb_message_queue_parameters;
	struct mm_string queue_name;
	struct mm_string queue_pop_mode;
	//
	struct mm_string rdb_message_queue_auth;
	struct mm_string rdb_gateway_cache_auth;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_cback_launch_init( struct mm_shuttle_cback_launch* p );
extern void mm_shuttle_cback_launch_destroy( struct mm_shuttle_cback_launch* p );
//////////////////////////////////////////////////////////////////////////
extern void mm_shuttle_cback_launch_load_config(struct mm_shuttle_cback_launch* p, const char* config_file);
extern void mm_shuttle_cback_launch_printf_information(struct mm_shuttle_cback_launch* p);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_shuttle_cback_launch_h__