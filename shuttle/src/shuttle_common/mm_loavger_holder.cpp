#include "mm_loavger_holder.h"
#include "core/mm_logger.h"
#include "shuttle_common/mm_j_runtime_state.h"
//////////////////////////////////////////////////////////////////////////
static const char* const_loavger_holder_cycle_error[] = 
{
	"",
	"loavger_holder length not enough",
	"loavger_holder unit is null",
};
//////////////////////////////////////////////////////////////////////////
static void __static_loavger_holder_path_elem_value_updated( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e, const char* buffer, int offset, int length );
static void __static_loavger_holder_path_elem_event_created( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e );
static void __static_loavger_holder_path_elem_event_deleted( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e );
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_init(struct mm_loavger_holder_elem* p)
{
	mm_spinlock_init(&p->locker,NULL);
	p->unique_id = 0;
	p->weights = MM_LOAVGER_HOLDER_ELEM_DEFAULT_WEIGHTS;
	p->zkrb_path_elem = NULL;
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_destroy(struct mm_loavger_holder_elem* p)
{
	mm_spinlock_destroy(&p->locker);
	p->unique_id = 0;
	p->weights = MM_LOAVGER_HOLDER_ELEM_DEFAULT_WEIGHTS;
	p->zkrb_path_elem = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_lock(struct mm_loavger_holder_elem* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_unlock(struct mm_loavger_holder_elem* p)
{
	mm_spinlock_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_init(struct mm_loavger_holder* p)
{
	struct mm_rbtree_u32_vpt_alloc rbtree_u32_vpt_alloc;
	struct mm_zkrb_path_callback zkrb_path_callback;

	mm_zkrb_path_init(&p->zkrb_path);
	mm_rbtree_u32_vpt_init(&p->rbtree);
	mm_lavg_assign_init(&p->lavg_assign);
	pthread_rwlock_init(&p->rbtree_locker, NULL);
	mm_spinlock_init(&p->locker_holder_cycle,NULL);
	mm_spinlock_init(&p->instance_locker,NULL);

	rbtree_u32_vpt_alloc.alloc = &mm_rbtree_u32_vpt_weak_alloc;
	rbtree_u32_vpt_alloc.relax = &mm_rbtree_u32_vpt_weak_relax;
	rbtree_u32_vpt_alloc.obj = p;
	mm_rbtree_u32_vpt_assign_alloc(&p->rbtree,&rbtree_u32_vpt_alloc);

	mm_zkrb_path_callback_init(&zkrb_path_callback);
	zkrb_path_callback.path_elem_updated = &__static_loavger_holder_path_elem_value_updated;
	zkrb_path_callback.path_elem_created = &__static_loavger_holder_path_elem_event_created;
	zkrb_path_callback.path_elem_deleted = &__static_loavger_holder_path_elem_event_deleted;
	zkrb_path_callback.obj = p;
	mm_zkrb_path_assign_callback(&p->zkrb_path,&zkrb_path_callback);
	mm_zkrb_path_callback_destroy(&zkrb_path_callback);
	//
	mm_lavg_assign_set_length(&p->lavg_assign,MM_LAVG_ASSIGN_LENGTH);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_destroy(struct mm_loavger_holder* p)
{
	mm_loavger_holder_clear(p);
	//
	mm_zkrb_path_destroy(&p->zkrb_path);
	mm_rbtree_u32_vpt_destroy(&p->rbtree);
	mm_lavg_assign_destroy(&p->lavg_assign);
	pthread_rwlock_destroy(&p->rbtree_locker);
	mm_spinlock_destroy(&p->locker_holder_cycle);
	mm_spinlock_destroy(&p->instance_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_start(struct mm_loavger_holder* p)
{
	mm_zkrb_path_start(&p->zkrb_path);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_interrupt(struct mm_loavger_holder* p)
{
	mm_zkrb_path_interrupt(&p->zkrb_path);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_shutdown(struct mm_loavger_holder* p)
{
	mm_zkrb_path_shutdown(&p->zkrb_path);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_join(struct mm_loavger_holder* p)
{
	mm_zkrb_path_join(&p->zkrb_path);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_assign_zkrb_path(struct mm_loavger_holder* p,const char* path)
{
	mm_zkrb_path_assign_path(&p->zkrb_path,path);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_assign_zkrb_host(struct mm_loavger_holder* p,const char* host)
{
	mm_zkrb_path_assign_host(&p->zkrb_path,host);
}
//////////////////////////////////////////////////////////////////////////
// update loavg to lavg_assign.
MM_EXPORT_SHUTTLE void mm_loavger_holder_update(struct mm_loavger_holder* p)
{
	struct mm_rb_root* root = &p->rbtree.rbt;
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_loavger_holder_elem* u = NULL;
	size_t hsize = 0; size_t index = 0;
	mm_float32_t a_lavg = 0;
	mm_float32_t m_lavg = 1;
	mm_float32_t l_lavg = 0;
	size_t ii = 0;	size_t ai = 0;	size_t ci = 0;	size_t wi = 0;
	mm_float32_t ti = 0;
	struct mm_loavger_holder_value* vec = NULL;

	pthread_rwlock_rdlock(&p->rbtree_locker);
	//
	hsize = mm_rbtree_u32_vpt_size(&p->rbtree);
	// hsize = 4;
	vec = (struct mm_loavger_holder_value*)mm_malloc( sizeof(struct mm_loavger_holder_value) * hsize );
	//
	n = mm_rb_first(root);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		//
		u = (struct mm_loavger_holder_elem*)(it->v);
		mm_loavger_holder_elem_lock(u);
		vec[index].k = u->unique_id;
		vec[index].v = u->weights;
		mm_loavger_holder_elem_unlock(u);
		a_lavg += vec[index].v;
		m_lavg *= vec[index].v;
		// mm_printf("%d %f\n",vec[index].k,vec[index].v);
		index++;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
	// 1/a / ( 1/a + 1/b + 1/c ) = a*b*c/ ( a * (b*c+a*c+a*b) )  = b*c / (b*c+a*c+a*b)
	// l_lavg = (b*c+a*c+a*b)
	for (ii = 0; ii < hsize; ++ii)
	{
		l_lavg += m_lavg / vec[ii].v;
	}
	pthread_rwlock_wrlock(&p->lavg_assign.v_locker);
	//
	if (p->lavg_assign.m < hsize)
	{
		mm_lavg_assign_set_length(&p->lavg_assign, (mm_uint32_t)hsize);
	}
	mm_lavg_assign_cycle_empty(&p->lavg_assign);
	//
	for (ii = 0; ii < hsize; ++ii)
	{
		ti = ( m_lavg / vec[ii].v / l_lavg ) * p->lavg_assign.l + 0.5f;
		// when ti is nan wi will > p->lavg_assign.len.
		wi += (size_t)ti;
		wi = wi > p->lavg_assign.l ? p->lavg_assign.l : wi;
		// mm_printf("%d %f\n",vec[ii].k,ti);
		// mm_printf("ci:%d wi:%d l:%d\n",ci,wi,p->lavg_assign.l);
		for (ai = ci; ai < wi; ++ai)
		{
			// mm_printf("%d %d\n",ai,vec[ii].k);
			pthread_rwlock_rdlock(&p->rbtree_locker);
			p->lavg_assign.v[ai] = mm_rbtree_u32_vpt_get(&p->rbtree,vec[ii].k);
			// mm_printf("ii:%d k:%d v[ii]:%p\n",ai,vec[ii].k,p->lavg_assign.v[ai]);
			pthread_rwlock_unlock(&p->rbtree_locker);
		}
		ci += (size_t)ti;
	}
	pthread_rwlock_unlock(&p->lavg_assign.v_locker);
	// free
	mm_free(vec);
}
// update watcher.
MM_EXPORT_SHUTTLE void mm_loavger_holder_update_watcher(struct mm_loavger_holder* p)
{
	mm_zkrb_path_update(&p->zkrb_path);
}
// cycle ref.
MM_EXPORT_SHUTTLE int mm_loavger_holder_cycle(struct mm_loavger_holder* p,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_loavger_holder_elem* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		if (0 == mm_lavg_assign_get_length(&p->lavg_assign))
		{
			// the lavg length is zero.
			code = lhce_lavg_assign_length_not_enough;
			break;
		}
		e = (struct mm_loavger_holder_elem*)mm_lavg_assign_cycle(&p->lavg_assign);
		if (NULL == e)
		{
			// the elem is null.
			code = lhce_lavg_assign_unit_is_null;
			break;
		}
		mm_loavger_holder_elem_lock(e);
		*runtime_state = e->runtime_state;
		mm_loavger_holder_elem_unlock(e);
		code = MM_SUCCESS;
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
// update loavg to lavg_assign.will lock the locker_holder_cycle.can not lock out side.
MM_EXPORT_SHUTTLE void mm_loavger_holder_cycle_update(struct mm_loavger_holder* p)
{
	mm_spinlock_lock(&p->locker_holder_cycle);
	mm_loavger_holder_update(p);
	mm_spinlock_unlock(&p->locker_holder_cycle);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_add(struct mm_loavger_holder* p,mm_uint32_t unique_id,struct mm_zkrb_path_elem* zkrb_path_elem)
{
	struct mm_loavger_holder_elem* e = NULL;
	// only lock for instance create process.
	mm_spinlock_lock(&p->instance_locker);
	e = mm_loavger_holder_get(p, unique_id);
	if (NULL == e)
	{
		struct mm_logger* g_logger = mm_logger_instance();
		e = new struct mm_loavger_holder_elem;
		mm_loavger_holder_elem_init(e);
		e->unique_id = unique_id;
		e->zkrb_path_elem = zkrb_path_elem;
		//
		pthread_rwlock_wrlock(&p->rbtree_locker);
		mm_rbtree_u32_vpt_set(&p->rbtree,unique_id,e);
		pthread_rwlock_unlock(&p->rbtree_locker);
		mm_logger_log_I(g_logger,"%s %d unique_id:%u is add.",__FUNCTION__,__LINE__,e->unique_id);
	}
	mm_spinlock_unlock(&p->instance_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_get(struct mm_loavger_holder* p,mm_uint32_t unique_id)
{
	struct mm_loavger_holder_elem* e = NULL;
	//
	pthread_rwlock_rdlock(&p->rbtree_locker);
	e = (struct mm_loavger_holder_elem*)mm_rbtree_u32_vpt_get(&p->rbtree,unique_id);
	pthread_rwlock_unlock(&p->rbtree_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_get_instance(struct mm_loavger_holder* p,mm_uint32_t unique_id,struct mm_zkrb_path_elem* zkrb_path_elem)
{
	struct mm_loavger_holder_elem* e = mm_loavger_holder_get(p,unique_id);
	if (NULL == e)
	{
		e = mm_loavger_holder_add(p,unique_id,zkrb_path_elem);
	}
	return e;
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_rmv(struct mm_loavger_holder* p,mm_uint32_t unique_id)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	//
	pthread_rwlock_wrlock(&p->rbtree_locker);
	it = mm_rbtree_u32_vpt_get_iterator(&p->rbtree,unique_id);
	if (NULL != it)
	{
		struct mm_loavger_holder_elem* e = (struct mm_loavger_holder_elem*)(it->v);
		mm_logger_log_I(g_logger,"%s %d unique_id:%u is rmv.",__FUNCTION__,__LINE__,e->unique_id);
		mm_loavger_holder_elem_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_loavger_holder_elem_unlock(e);
		mm_loavger_holder_elem_destroy(e);
		delete e;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
MM_EXPORT_SHUTTLE void mm_loavger_holder_clear(struct mm_loavger_holder* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_loavger_holder_elem* e = NULL;
	pthread_rwlock_wrlock(&p->rbtree_locker);
	n = mm_rb_first(&p->rbtree.rbt);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		e = (struct mm_loavger_holder_elem*)(it->v);
		mm_loavger_holder_elem_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_loavger_holder_elem_unlock(e);
		mm_loavger_holder_elem_destroy(e);
		delete e;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
// not lock inside,lock e manual.
MM_EXPORT_SHUTTLE void mm_loavger_holder_traver(struct mm_loavger_holder* p,loavger_holder_traver func,void* u)
{
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_loavger_holder_elem* e = NULL;
	pthread_rwlock_rdlock(&p->rbtree_locker);
	n = mm_rb_first(&p->rbtree.rbt);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		e = (struct mm_loavger_holder_elem*)(it->v);
		(*(func))(p,e,u);
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
// state ref copy.
MM_EXPORT_SHUTTLE void mm_loavger_holder_get_state(struct mm_loavger_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state)
{
	struct mm_loavger_holder_elem* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		e = mm_loavger_holder_get(p,unique_id);
		if ( NULL == e )
		{
			// can not find.
			break;
		}
		mm_loavger_holder_elem_lock(e);
		*runtime_state = e->runtime_state;
		mm_loavger_holder_elem_unlock(e);
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
}
MM_EXPORT_SHUTTLE const char* mm_loavger_holder_cycle_error_message(int error)
{
	if (lhce_lavg_none < error && error < lhce_lavg_max)
	{
		return const_loavger_holder_cycle_error[error];
	}
	else
	{
		return "";
	}
}
//////////////////////////////////////////////////////////////////////////
static void __static_loavger_holder_path_elem_value_updated( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e, const char* buffer, int offset, int length )
{
	struct mm_loavger_holder* loavger_holder = (struct mm_loavger_holder*)(p->callback.obj);
	do 
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_loavger_holder_elem* elem = NULL;
		mm_logger_log_D(g_logger,"%s %d e->unique_id:%u.",__FUNCTION__,__LINE__,e->unique_id);
		//
		elem = mm_loavger_holder_get(loavger_holder,e->unique_id);
		if ( NULL == elem )
		{
			mm_spinlock_lock(&loavger_holder->locker_holder_cycle);
			elem = mm_loavger_holder_add(loavger_holder,e->unique_id,e);
			mm_loavger_holder_update(loavger_holder);
			mm_spinlock_unlock(&loavger_holder->locker_holder_cycle);
		}
		if ( NULL == elem )
		{
			// system memory error.
			mm_logger_log_E(g_logger,"%s %d system memory error..",__FUNCTION__,__LINE__);
			break;
		}
		mm_loavger_holder_elem_lock(elem);
		mm::mm_j_runtime_state j_runtime(elem->runtime_state);
		rapidjson::Document runtime_doc(rapidjson::kObjectType);
		if (runtime_doc.Parse((const char*)(buffer + offset),length).HasParseError())
		{
			mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc.",__FUNCTION__,__LINE__);
		}
		j_runtime.decode(runtime_doc,runtime_doc);
		elem->weights = elem->runtime_state.weights;
		mm_loavger_holder_elem_unlock(elem);
	} while (0);
}
static void __static_loavger_holder_path_elem_event_created( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e )
{
	struct mm_loavger_holder* loavger_holder = (struct mm_loavger_holder*)(p->callback.obj);
	mm_spinlock_lock(&loavger_holder->locker_holder_cycle);
	mm_loavger_holder_get_instance(loavger_holder,e->unique_id,e);
	mm_loavger_holder_update(loavger_holder);
	mm_spinlock_unlock(&loavger_holder->locker_holder_cycle);
}
static void __static_loavger_holder_path_elem_event_deleted( struct mm_zkrb_path* p, struct mm_zkrb_path_elem* e )
{
	struct mm_loavger_holder* loavger_holder = (struct mm_loavger_holder*)(p->callback.obj);
	mm_spinlock_lock(&loavger_holder->locker_holder_cycle);
	mm_loavger_holder_rmv(loavger_holder,e->unique_id);
	mm_loavger_holder_update(loavger_holder);
	mm_spinlock_unlock(&loavger_holder->locker_holder_cycle);
}