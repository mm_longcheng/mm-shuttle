#ifndef __mm_loavger_holder_h__
#define __mm_loavger_holder_h__

#include "core/mm_core.h"

#include "dish/mm_m_runtime_state.h"

#include "container/mm_rbtree_u32.h"

#include "zookeeper/mm_zookeeper_zkrb_path.h"

#include "mm_lavg_assign.h"

#include <pthread.h>

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

#define MM_LOAVGER_HOLDER_ELEM_DEFAULT_WEIGHTS 1

enum mm_loavger_holder_cycle_error_t
{
	lhce_lavg_none                     = 0,
	lhce_lavg_assign_length_not_enough = 1,
	lhce_lavg_assign_unit_is_null      = 2,
	lhce_lavg_max                         ,
};

struct mm_loavger_holder;
typedef void (*loavger_holder_traver)( struct mm_loavger_holder* p, struct mm_loavger_holder_elem* e, void* u );
//////////////////////////////////////////////////////////////////////////
struct mm_loavger_holder_value
{
	mm_uint32_t k;
	mm_float32_t v;
};
//////////////////////////////////////////////////////////////////////////
struct mm_loavger_holder_elem
{
	mm::mm_m_runtime_state runtime_state;// runtime state cache.
	mm_atomic_t locker;// locker for memory
	mm_uint32_t unique_id;
	mm_float32_t weights;// default is MM_LOAVGER_HOLDER_ELEM_DEFAULT_WEIGHTS.
	struct mm_zkrb_path_elem* zkrb_path_elem;// weak ref for mm_zknt_path.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_init(struct mm_loavger_holder_elem* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_destroy(struct mm_loavger_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_lock(struct mm_loavger_holder_elem* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_elem_unlock(struct mm_loavger_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
struct mm_loavger_holder
{
	struct mm_zkrb_path zkrb_path;
	struct mm_rbtree_u32_vpt rbtree;// unique_id<-->elem
	struct mm_lavg_assign lavg_assign;
	pthread_rwlock_t rbtree_locker;// locker for holder.
	mm_atomic_t locker_holder_cycle;// lock for lavg_assign and holder.
	mm_atomic_t instance_locker;// the locker for only get instance process thread safe.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_init(struct mm_loavger_holder* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_destroy(struct mm_loavger_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_start(struct mm_loavger_holder* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_interrupt(struct mm_loavger_holder* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_shutdown(struct mm_loavger_holder* p);
MM_EXPORT_SHUTTLE void mm_loavger_holder_join(struct mm_loavger_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_loavger_holder_assign_zkrb_path(struct mm_loavger_holder* p,const char* path);
MM_EXPORT_SHUTTLE void mm_loavger_holder_assign_zkrb_host(struct mm_loavger_holder* p,const char* host);
//////////////////////////////////////////////////////////////////////////
// update loavg to lavg_assign.
MM_EXPORT_SHUTTLE void mm_loavger_holder_update(struct mm_loavger_holder* p);
// update watcher.
MM_EXPORT_SHUTTLE void mm_loavger_holder_update_watcher(struct mm_loavger_holder* p);
// cycle ref copy.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_loavger_holder_cycle(struct mm_loavger_holder* p,mm::mm_m_runtime_state* runtime_state);
// update loavg to lavg_assign.will lock the locker_holder_cycle.can not lock out side.
MM_EXPORT_SHUTTLE void mm_loavger_holder_cycle_update(struct mm_loavger_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_add(struct mm_loavger_holder* p,mm_uint32_t unique_id,struct mm_zkrb_path_elem* zkrb_path_elem);
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_get(struct mm_loavger_holder* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE struct mm_loavger_holder_elem* mm_loavger_holder_get_instance(struct mm_loavger_holder* p,mm_uint32_t unique_id,struct mm_zkrb_path_elem* zkrb_path_elem);
MM_EXPORT_SHUTTLE void mm_loavger_holder_rmv(struct mm_loavger_holder* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE void mm_loavger_holder_clear(struct mm_loavger_holder* p);
//////////////////////////////////////////////////////////////////////////
// not lock inside,lock e manual.
MM_EXPORT_SHUTTLE void mm_loavger_holder_traver(struct mm_loavger_holder* p,loavger_holder_traver func,void* u);
// state ref copy.
MM_EXPORT_SHUTTLE void mm_loavger_holder_get_state(struct mm_loavger_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE const char* mm_loavger_holder_cycle_error_message(int error);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_loavger_holder_h__