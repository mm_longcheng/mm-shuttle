﻿#include "mm_error_code_mysql.h"

MM_EXPORT_SHUTTLE void mm_error_desc_assign_mysql(struct mm_error_desc* p)
{
	mm_error_core_assign_code_desc(p,err_mysql_data_insert_failure, "data insert failure.");
	mm_error_core_assign_code_desc(p,err_mysql_data_not_exist, "data not exist.");
	mm_error_core_assign_code_desc(p,err_mysql_data_update_failure, "data update failure.");
	mm_error_core_assign_code_desc(p,err_mysql_data_delete_failure, "data delete failure.");
	mm_error_core_assign_code_desc(p,err_mysql_data_parameter_invalid, "data parameter invalid.");
}