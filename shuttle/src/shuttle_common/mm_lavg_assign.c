#include "mm_lavg_assign.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_lavg_assign_init(struct mm_lavg_assign* p)
{
	p->l = 0;
	p->m = 0;
	p->v = NULL;
	p->n = 0;
	pthread_rwlock_init(&p->v_locker, NULL);
	mm_spinlock_init(&p->n_locker,NULL);
}
MM_EXPORT_SHUTTLE void mm_lavg_assign_destroy(struct mm_lavg_assign* p)
{
	p->l = 0;
	p->m = 0;
	mm_free(p->v);
	p->n = 0;
	pthread_rwlock_destroy(&p->v_locker);
	mm_spinlock_destroy(&p->n_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_lavg_assign_set_length(struct mm_lavg_assign* p, mm_uint32_t length)
{
	p->l = length;
	mm_lavg_assign_aligned_memory(p, p->l);
}
MM_EXPORT_SHUTTLE mm_uint32_t mm_lavg_assign_get_length(struct mm_lavg_assign* p)
{
	return p->l;
}
// must lock before set memory.
MM_EXPORT_SHUTTLE mm_uint32_t mm_lavg_assign_get_memory(struct mm_lavg_assign* p)
{
	return p->m;
}

MM_EXPORT_SHUTTLE void mm_lavg_assign_add_size(struct mm_lavg_assign* p, size_t size)
{
	void** v = (void**)mm_malloc(sizeof(void*) * (p->m + size));
	mm_memset(v, 0, p->m + size);
	mm_free(p->v);
	p->v = v;
	p->m += (mm_uint32_t)size;
}
MM_EXPORT_SHUTTLE void mm_lavg_assign_rmv_size(struct mm_lavg_assign* p, size_t size)
{
	void** v = (void**)mm_malloc(sizeof(void*) * (p->m + size));
	mm_memset(v, 0, p->m + size);
	mm_free(p->v);
	p->v = v;
	p->m -= (mm_uint32_t)size;
}

MM_EXPORT_SHUTTLE void mm_lavg_assign_try_decrease(struct mm_lavg_assign* p, size_t n)
{
	size_t fresz = p->m;
	size_t ssize = fresz - n;// the surplus size for decrease.
	if (fresz > n && MM_LAVG_ASSIGN_MEMORY < ssize)
	{
		size_t sz_1_4 = p->m / 4;
		size_t sz_3_4 = sz_1_4 * 3;
		size_t sz_sub = sz_1_4 / MM_LAVG_ASSIGN_MEMORY;
		if ( fresz > n && sz_sub > 0 && sz_3_4 < ssize )
		{
			size_t sz_rel = sz_sub * MM_LAVG_ASSIGN_MEMORY;
			// rmv surplus size.
			mm_lavg_assign_rmv_size(p, sz_rel);
		}
	}
}
// try increase.
MM_EXPORT_SHUTTLE void mm_lavg_assign_try_increase(struct mm_lavg_assign* p, size_t n)
{
	if ( n > p->m )
	{
		size_t dsz = n - p->m;
		size_t asz = dsz / MM_LAVG_ASSIGN_MEMORY;
		size_t bsz = dsz % MM_LAVG_ASSIGN_MEMORY;
		size_t nsz = ( asz + ( 0 == bsz ? 0 : 1 ) );
		mm_lavg_assign_add_size(p, nsz * MM_LAVG_ASSIGN_MEMORY);
	}
}
// must lock before set memory.
MM_EXPORT_SHUTTLE void mm_lavg_assign_aligned_memory(struct mm_lavg_assign* p, size_t n)
{
	// if current pptr + n not enough for target n.we make a aligned memory.
	if ( p->m >= n )
	{
		mm_lavg_assign_try_decrease(p, n);
	} 
	else
	{
		mm_lavg_assign_try_increase(p, n);
	}
}

MM_EXPORT_SHUTTLE void* mm_lavg_assign_cycle(struct mm_lavg_assign* p)
{
	mm_uint32_t x = 0;
	void* e = NULL;
	mm_spinlock_lock(&p->n_locker);
	x = p->n++ % p->l;
	mm_spinlock_unlock(&p->n_locker);

	pthread_rwlock_rdlock(&p->v_locker);
	e = p->v[x];
	pthread_rwlock_unlock(&p->v_locker);
	return e;
}
// all lavg elem is point to null.
MM_EXPORT_SHUTTLE void mm_lavg_assign_cycle_empty(struct mm_lavg_assign* p)
{
	mm_memset(p->v, 0, sizeof(void*) * p->l);
}