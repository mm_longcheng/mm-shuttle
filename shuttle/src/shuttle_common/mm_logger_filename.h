﻿#ifndef __mm_logger_filename_h__
#define __mm_logger_filename_h__

#include "core/mm_core.h"
#include "core/mm_logger_manager.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
// "application_name" + "_" + "unique_id_string"
MM_EXPORT_SHUTTLE void mm_logger_manager_assign_logger_filename(struct mm_logger_manager* logger_manager, const char* application_name, const char* unique_id_string);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_logger_filename_h__