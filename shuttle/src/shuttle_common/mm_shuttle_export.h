﻿#ifndef __mm_shuttle_export_h__
#define __mm_shuttle_export_h__

#include "core/mm_platform.h"

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

#  ifdef MM_STATIC_SHUTTLE
#    define MM_EXPORT_SHUTTLE
#    define MM_IMPORT_SHUTTLE
#  else
#    ifndef MM_EXPORT_SHUTTLE
#      ifdef MM_SHARED_SHUTTLE
/* We are building this library */
#        define MM_EXPORT_SHUTTLE __declspec(dllexport)
#      else
/* We are using this library */
#        define MM_EXPORT_SHUTTLE __declspec(dllimport)
#      endif
#    endif

#    ifndef MM_PRIVATE_SHUTTLE
#      define MM_PRIVATE_SHUTTLE 
#    endif
#  endif

#  ifndef MM_DEPRECATED_SHUTTLE
#    define MM_DEPRECATED_SHUTTLE __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_SHUTTLE
#    define MM_DEPRECATED_EXPORT_SHUTTLE MM_EXPORT_SHUTTLE MM_DEPRECATED_SHUTTLE
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_SHUTTLE
#    define MM_DEPRECATED_PRIVATE_SHUTTLE MM_PRIVATE_SHUTTLE MM_DEPRECATED_SHUTTLE
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).
#  ifdef MM_STATIC_SHUTTLE
#    define MM_EXPORT_SHUTTLE
#    define MM_IMPORT_SHUTTLE
#  else
#    ifndef MM_EXPORT_SHUTTLE
#      ifdef MM_SHARED_SHUTTLE
/* We are building this library */
#        define MM_EXPORT_SHUTTLE __attribute__ ((visibility("default")))
#      else
/* We are using this library */
#        define MM_EXPORT_SHUTTLE 
#      endif
#    endif

#    ifndef MM_PRIVATE_SHUTTLE
#      define MM_PRIVATE_SHUTTLE 
#    endif
#  endif

#  ifndef MM_DEPRECATED_SHUTTLE
#    define MM_DEPRECATED_SHUTTLE __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_SHUTTLE
#    define MM_DEPRECATED_EXPORT_SHUTTLE MM_EXPORT_SHUTTLE MM_DEPRECATED_SHUTTLE
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_SHUTTLE
#    define MM_DEPRECATED_PRIVATE_SHUTTLE MM_PRIVATE_SHUTTLE MM_DEPRECATED_SHUTTLE
#  endif

#endif

#endif//__mm_shuttle_export_h__