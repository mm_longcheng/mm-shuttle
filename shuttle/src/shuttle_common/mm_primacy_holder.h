#ifndef __mm_primacy_holder_h__
#define __mm_primacy_holder_h__

#include "dish/mm_m_runtime_state.h"

#include "core/mm_core.h"

#include "container/mm_rbtree_u32.h"

#include "shuttle_common/mm_lavg_assign.h"

#include "zookeeper/mm_zookeeper_zkrp_path.h"

#include <pthread.h>

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

#define MM_PRIMACY_HOLDER_UNIT_DEFAULT_WEIGHTS 1
#define MM_PRIMACY_HOLDER_ELEM_DEFAULT_WEIGHTS 1

enum mm_primacy_holder_cycle_error_t
{
	phce_lavg_none                          = 0,
	phce_lavg_assign_length_not_enough      = 1,
	phce_lavg_assign_unit_is_null           = 2,
	phce_lavg_assign_elem_not_find          = 3,
	phce_lavg_assign_elem_rbtree_size_empty = 4,
	phce_lavg_assign_elem_length_not_enough = 5,
	phce_lavg_assign_elem_unit_is_null      = 6,
	phce_lavg_max                         ,
};

struct mm_primacy_holder_value
{
	mm_uint32_t k;
	mm_float32_t v;
};
//////////////////////////////////////////////////////////////////////////
struct mm_primacy_holder_unit
{
	mm::mm_m_runtime_state runtime_state;// runtime state cache.
	mm_atomic_t locker;// locker for memory
	mm_uint32_t unique_id;
	mm_float32_t weights;// default is MM_primacy_holder_UNIT_DEFAULT_WEIGHTS.
	struct mm_zkrp_path_unit* zkrp_path_unit;// weak ref for mm_zknt_path.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_init(struct mm_primacy_holder_unit* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_destroy(struct mm_primacy_holder_unit* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_lock(struct mm_primacy_holder_unit* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_unlock(struct mm_primacy_holder_unit* p);
//////////////////////////////////////////////////////////////////////////
struct mm_primacy_holder_elem
{
	struct mm_rbtree_u32_vpt rbtree;// unique_id<-->elem
	struct mm_lavg_assign lavg_assign;
	pthread_rwlock_t rbtree_locker;// locker for holder.
	mm_atomic_t locker_holder_cycle;// lock for lavg_assign and holder.
	mm_atomic_t instance_locker;// the locker for only get instance process thread safe.
	mm_atomic_t locker;// locker for memory
	mm_uint32_t unique_id;
	mm_float32_t weights;// default is MM_LOAVGER_HOLDER_ELEM_DEFAULT_WEIGHTS.
	struct mm_zkrp_path_elem* zkrp_path_elem;// weak ref for mm_zknt_path.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_init(struct mm_primacy_holder_elem* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_destroy(struct mm_primacy_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_lock(struct mm_primacy_holder_elem* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_unlock(struct mm_primacy_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_update(struct mm_primacy_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_add(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id,struct mm_zkrp_path_unit* zkrp_path_unit);
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_get(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_get_instance(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id,struct mm_zkrp_path_unit* zkrp_path_unit);
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_rmv(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_clear(struct mm_primacy_holder_elem* p);
//////////////////////////////////////////////////////////////////////////
// cycle ref copy.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_elem_cycle(struct mm_primacy_holder_elem* p,mm::mm_m_runtime_state* runtime_state);
// cycle ref copy.return the unique_id for live node.the min unique_id for unit.
MM_EXPORT_SHUTTLE int mm_primacy_holder_elem_cycle_min(struct mm_primacy_holder_elem* p,mm::mm_m_runtime_state* runtime_state);
//////////////////////////////////////////////////////////////////////////
struct mm_primacy_holder
{
	struct mm_zkrp_path zkrp_path;
	struct mm_lavg_assign lavg_assign;
	struct mm_rbtree_u32_vpt rbtree;// unique_id<-->elem
	pthread_rwlock_t rbtree_locker;// locker for holder.
	mm_atomic_t locker_holder_cycle;// lock for lavg_assign and holder.
	mm_atomic_t instance_locker;// the locker for only get instance process thread safe.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_init(struct mm_primacy_holder* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_destroy(struct mm_primacy_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_start(struct mm_primacy_holder* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_interrupt(struct mm_primacy_holder* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_shutdown(struct mm_primacy_holder* p);
MM_EXPORT_SHUTTLE void mm_primacy_holder_join(struct mm_primacy_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_assign_zkrp_path(struct mm_primacy_holder* p,const char* path);
MM_EXPORT_SHUTTLE void mm_primacy_holder_assign_zkrp_host(struct mm_primacy_holder* p,const char* host);
//////////////////////////////////////////////////////////////////////////
// update loavg to lavg_assign.
MM_EXPORT_SHUTTLE void mm_primacy_holder_update(struct mm_primacy_holder* p);
// update watcher.
MM_EXPORT_SHUTTLE void mm_primacy_holder_update_watcher(struct mm_primacy_holder* p);
//////////////////////////////////////////////////////////////////////////
// cycle ref.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_cycle(struct mm_primacy_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state);
// cycle ref.return the unique_id for live node.the min unique_id for unit.
MM_EXPORT_SHUTTLE int mm_primacy_holder_cycle_min(struct mm_primacy_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state);
// cycle loavger ref copy.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_loavger_cycle(struct mm_primacy_holder* p,mm_uint32_t* unique_id, mm::mm_m_runtime_state* runtime_state);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_add(struct mm_primacy_holder* p,mm_uint32_t unique_id,struct mm_zkrp_path_elem* zkrp_path_elem);
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_get(struct mm_primacy_holder* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_get_instance(struct mm_primacy_holder* p,mm_uint32_t unique_id,struct mm_zkrp_path_elem* zkrp_path_elem);
MM_EXPORT_SHUTTLE void mm_primacy_holder_rmv(struct mm_primacy_holder* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE void mm_primacy_holder_clear(struct mm_primacy_holder* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE const char* mm_primacy_holder_cycle_error_message(int error);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_primacy_holder_h__