#include "mm_primacy_holder.h"
#include "core/mm_logger.h"
#include "shuttle_common/mm_j_runtime_state.h"
//////////////////////////////////////////////////////////////////////////
static const char* const_primacy_holder_cycle_error[] = 
{
	"",
	"primacy_holder length not enough",
	"primacy_holder unit is null",
	"primacy_holder elem not find",
	"primacy_holder elem rbtree size empty",
	"primacy_holder elem length not enough",
	"primacy_holder elem unit is null",
};
//////////////////////////////////////////////////////////////////////////
static void __static_primacy_holder_path_elem_unit_value_updated( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u, const char* buffer, int offset, int length );
static void __static_primacy_holder_path_elem_unit_event_created( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u );
static void __static_primacy_holder_path_elem_unit_event_deleted( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u );

static void __static_primacy_holder_path_elem_value_updated( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, const char* buffer, int offset, int length );
static void __static_primacy_holder_path_elem_event_created( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e );
static void __static_primacy_holder_path_elem_event_deleted( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e );
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_init(struct mm_primacy_holder_unit* p)
{
	mm_spinlock_init(&p->locker,NULL);
	p->unique_id = 0;
	p->weights = MM_PRIMACY_HOLDER_UNIT_DEFAULT_WEIGHTS;
	p->zkrp_path_unit = NULL;
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_destroy(struct mm_primacy_holder_unit* p)
{
	mm_spinlock_destroy(&p->locker);
	p->unique_id = 0;
	p->weights = MM_PRIMACY_HOLDER_UNIT_DEFAULT_WEIGHTS;
	p->zkrp_path_unit = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_lock(struct mm_primacy_holder_unit* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_unit_unlock(struct mm_primacy_holder_unit* p)
{
	mm_spinlock_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_init(struct mm_primacy_holder_elem* p)
{
	struct mm_rbtree_u32_vpt_alloc rbtree_u32_vpt_alloc;

	mm_rbtree_u32_vpt_init(&p->rbtree);
	mm_lavg_assign_init(&p->lavg_assign);
	pthread_rwlock_init(&p->rbtree_locker,NULL);
	mm_spinlock_init(&p->locker_holder_cycle,NULL);
	mm_spinlock_init(&p->instance_locker,NULL);
	mm_spinlock_init(&p->locker,NULL);
	p->unique_id = 0;
	p->weights = MM_PRIMACY_HOLDER_ELEM_DEFAULT_WEIGHTS;
	p->zkrp_path_elem = NULL;

	rbtree_u32_vpt_alloc.alloc = &mm_rbtree_u32_vpt_weak_alloc;
	rbtree_u32_vpt_alloc.relax = &mm_rbtree_u32_vpt_weak_relax;
	rbtree_u32_vpt_alloc.obj = p;
	mm_rbtree_u32_vpt_assign_alloc(&p->rbtree,&rbtree_u32_vpt_alloc);
	//
	mm_lavg_assign_set_length(&p->lavg_assign,MM_LAVG_ASSIGN_LENGTH);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_destroy(struct mm_primacy_holder_elem* p)
{
	mm_primacy_holder_elem_clear(p);
	//
	mm_rbtree_u32_vpt_destroy(&p->rbtree);
	mm_lavg_assign_destroy(&p->lavg_assign);
	pthread_rwlock_destroy(&p->rbtree_locker);
	mm_spinlock_destroy(&p->locker_holder_cycle);
	mm_spinlock_destroy(&p->instance_locker);
	mm_spinlock_destroy(&p->locker);
	p->unique_id = 0;
	p->weights = MM_PRIMACY_HOLDER_ELEM_DEFAULT_WEIGHTS;
	p->zkrp_path_elem = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_lock(struct mm_primacy_holder_elem* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_unlock(struct mm_primacy_holder_elem* p)
{
	mm_spinlock_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_update(struct mm_primacy_holder_elem* p)
{
	struct mm_rb_root* root = &p->rbtree.rbt;
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_primacy_holder_unit* u = NULL;
	size_t hsize = 0; size_t index = 0;
	mm_float32_t a_lavg = 0;
	mm_float32_t m_lavg = 1;
	mm_float32_t l_lavg = 0;
	size_t ii = 0;	size_t ai = 0;	size_t ci = 0;	size_t wi = 0;
	mm_float32_t ti = 0;
	struct mm_primacy_holder_value* vec = NULL;

	pthread_rwlock_rdlock(&p->rbtree_locker);
	//
	hsize = mm_rbtree_u32_vpt_size(&p->rbtree);
	// hsize = 4;
	vec = (struct mm_primacy_holder_value*)mm_malloc( sizeof(struct mm_primacy_holder_value) * hsize );
	//
	n = mm_rb_first(root);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		//
		u = (struct mm_primacy_holder_unit*)(it->v);
		mm_primacy_holder_unit_lock(u);
		vec[index].k = u->unique_id;
		vec[index].v = u->weights;
		mm_primacy_holder_unit_unlock(u);
		a_lavg += vec[index].v;
		m_lavg *= vec[index].v;
		// mm_printf("%d %f\n",vec[index].k,vec[index].v);
		index++;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
	// 1/a / ( 1/a + 1/b + 1/c ) = a*b*c/ ( a * (b*c+a*c+a*b) )  = b*c / (b*c+a*c+a*b)
	// l_lavg = (b*c+a*c+a*b)
	for (ii = 0; ii < hsize; ++ii)
	{
		l_lavg += m_lavg / vec[ii].v;
	}
	pthread_rwlock_wrlock(&p->lavg_assign.v_locker);
	//
	if (p->lavg_assign.m < hsize)
	{
		mm_lavg_assign_set_length(&p->lavg_assign, (mm_uint32_t)hsize);
	}
	mm_lavg_assign_cycle_empty(&p->lavg_assign);
	//
	for (ii = 0; ii < hsize; ++ii)
	{
		ti = ( m_lavg / vec[ii].v / l_lavg ) * p->lavg_assign.l + 0.5f;
		// when ti is nan wi will > p->lavg_assign.len.
		wi += (size_t)ti;
		wi = wi > p->lavg_assign.l ? p->lavg_assign.l : wi;
		// mm_printf("%d %f\n",vec[ii].k,ti);
		// mm_printf("ci:%d wi:%d l:%d\n",ci,wi,p->lavg_assign.l);
		for (ai = ci; ai < wi; ++ai)
		{
			// mm_printf("%d %d\n",ai,vec[ii].k);
			pthread_rwlock_rdlock(&p->rbtree_locker);
			p->lavg_assign.v[ai] = mm_rbtree_u32_vpt_get(&p->rbtree,vec[ii].k);
			// mm_printf("ii:%d k:%d v[ii]:%p\n",ai,vec[ii].k,p->lavg_assign.v[ai]);
			pthread_rwlock_unlock(&p->rbtree_locker);
		}
		ci += (size_t)ti;
	}
	pthread_rwlock_unlock(&p->lavg_assign.v_locker);
	// free
	mm_free(vec);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_add(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id,struct mm_zkrp_path_unit* zkrp_path_unit)
{
	struct mm_primacy_holder_unit* e = NULL;
	// only lock for instance create process.
	mm_spinlock_lock(&p->instance_locker);
	e = mm_primacy_holder_elem_get(p, unique_id);
	if (NULL == e)
	{
		struct mm_logger* g_logger = mm_logger_instance();
		e = new struct mm_primacy_holder_unit;
		mm_primacy_holder_unit_init(e);
		e->unique_id = unique_id;
		e->zkrp_path_unit = zkrp_path_unit;
		//
		pthread_rwlock_wrlock(&p->rbtree_locker);
		mm_rbtree_u32_vpt_set(&p->rbtree,unique_id,e);
		pthread_rwlock_unlock(&p->rbtree_locker);
		mm_logger_log_I(g_logger,"%s %d shard_index:%u depth_index:%u is add.",__FUNCTION__,__LINE__,p->unique_id,e->unique_id);
	}
	mm_spinlock_unlock(&p->instance_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_get(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id)
{
	struct mm_primacy_holder_unit* e = NULL;
	//
	pthread_rwlock_rdlock(&p->rbtree_locker);
	e = (struct mm_primacy_holder_unit*)mm_rbtree_u32_vpt_get(&p->rbtree,unique_id);
	pthread_rwlock_unlock(&p->rbtree_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_primacy_holder_unit* mm_primacy_holder_elem_get_instance(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id,struct mm_zkrp_path_unit* zkrp_path_unit)
{
	struct mm_primacy_holder_unit* e = mm_primacy_holder_elem_get(p,unique_id);
	if (NULL == e)
	{
		e = mm_primacy_holder_elem_add(p,unique_id,zkrp_path_unit);
	}
	return e;
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_rmv(struct mm_primacy_holder_elem* p,mm_uint32_t unique_id)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	//
	pthread_rwlock_wrlock(&p->rbtree_locker);
	it = mm_rbtree_u32_vpt_get_iterator(&p->rbtree,unique_id);
	if (NULL != it)
	{
		struct mm_primacy_holder_unit* e = (struct mm_primacy_holder_unit*)(it->v);
		mm_logger_log_I(g_logger,"%s %d shard_index:%u depth_index:%u is rmv.",__FUNCTION__,__LINE__,p->unique_id,e->unique_id);
		mm_primacy_holder_unit_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_primacy_holder_unit_unlock(e);
		mm_primacy_holder_unit_destroy(e);
		delete e;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_elem_clear(struct mm_primacy_holder_elem* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_primacy_holder_unit* e = NULL;
	pthread_rwlock_wrlock(&p->rbtree_locker);
	n = mm_rb_first(&p->rbtree.rbt);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		e = (struct mm_primacy_holder_unit*)(it->v);
		mm_primacy_holder_unit_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_primacy_holder_unit_unlock(e);
		mm_primacy_holder_unit_destroy(e);
		delete e;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
//////////////////////////////////////////////////////////////////////////
// cycle ref.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_elem_cycle(struct mm_primacy_holder_elem* p,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_primacy_holder_unit* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		if (0 == mm_lavg_assign_get_length(&p->lavg_assign))
		{
			// the lavg length is zero.
			code = phce_lavg_assign_elem_length_not_enough;
			break;
		}
		e = (struct mm_primacy_holder_unit*)mm_lavg_assign_cycle(&p->lavg_assign);
		if (NULL == e)
		{
			// the elem is null.
			code = phce_lavg_assign_elem_unit_is_null;
			break;
		}
		mm_primacy_holder_unit_lock(e);
		*runtime_state = e->runtime_state;
		mm_primacy_holder_unit_unlock(e);
		code = MM_SUCCESS;
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
// cycle ref copy.return the unique_id for live node.the min unique_id for unit.
MM_EXPORT_SHUTTLE int mm_primacy_holder_elem_cycle_min(struct mm_primacy_holder_elem* p,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_primacy_holder_unit* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		struct mm_rb_node* n = NULL;
		struct mm_rbtree_u32_vpt_iterator* it = NULL;
		// holder size is lock free.
		if (0 == p->rbtree.size)
		{
			// the holder length is zero.
			code = phce_lavg_assign_elem_rbtree_size_empty;
			break;
		}
		pthread_rwlock_rdlock(&p->rbtree_locker);
		// min key elem.
		n = mm_rb_last(&p->rbtree.rbt);
		if (NULL != n)
		{
			it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
			e = (struct mm_primacy_holder_unit*)(it->v);
			mm_primacy_holder_unit_lock(e);
			*runtime_state = e->runtime_state;
			mm_primacy_holder_unit_unlock(e);
		}
		pthread_rwlock_unlock(&p->rbtree_locker);
		code = MM_SUCCESS;
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_init(struct mm_primacy_holder* p)
{
	struct mm_zkrp_path_callback zkrp_path_callback;
	struct mm_rbtree_u32_vpt_alloc rbtree_u32_vpt_alloc;

	mm_zkrp_path_init(&p->zkrp_path);
	mm_lavg_assign_init(&p->lavg_assign);
	mm_rbtree_u32_vpt_init(&p->rbtree);
	pthread_rwlock_init(&p->rbtree_locker,NULL);
	mm_spinlock_init(&p->locker_holder_cycle,NULL);
	mm_spinlock_init(&p->instance_locker,NULL);

	mm_zkrp_path_callback_init(&zkrp_path_callback);
	zkrp_path_callback.path_elem_unit_updated = &__static_primacy_holder_path_elem_unit_value_updated;
	zkrp_path_callback.path_elem_unit_created = &__static_primacy_holder_path_elem_unit_event_created;
	zkrp_path_callback.path_elem_unit_deleted = &__static_primacy_holder_path_elem_unit_event_deleted;
	zkrp_path_callback.path_elem_updated = &__static_primacy_holder_path_elem_value_updated;
	zkrp_path_callback.path_elem_created = &__static_primacy_holder_path_elem_event_created;
	zkrp_path_callback.path_elem_deleted = &__static_primacy_holder_path_elem_event_deleted;
	zkrp_path_callback.obj = p;
	mm_zkrp_path_assign_callback(&p->zkrp_path,&zkrp_path_callback);
	mm_zkrp_path_callback_destroy(&zkrp_path_callback);

	rbtree_u32_vpt_alloc.alloc = &mm_rbtree_u32_vpt_weak_alloc;
	rbtree_u32_vpt_alloc.relax = &mm_rbtree_u32_vpt_weak_relax;
	rbtree_u32_vpt_alloc.obj = p;
	mm_rbtree_u32_vpt_assign_alloc(&p->rbtree,&rbtree_u32_vpt_alloc);
	//
	mm_lavg_assign_set_length(&p->lavg_assign,MM_LAVG_ASSIGN_LENGTH);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_destroy(struct mm_primacy_holder* p)
{
	mm_primacy_holder_clear(p);
	//
	mm_zkrp_path_destroy(&p->zkrp_path);
	mm_lavg_assign_destroy(&p->lavg_assign);
	mm_rbtree_u32_vpt_destroy(&p->rbtree);
	pthread_rwlock_destroy(&p->rbtree_locker);
	mm_spinlock_destroy(&p->locker_holder_cycle);
	mm_spinlock_destroy(&p->instance_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_start(struct mm_primacy_holder* p)
{
	mm_zkrp_path_start(&p->zkrp_path);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_interrupt(struct mm_primacy_holder* p)
{
	mm_zkrp_path_interrupt(&p->zkrp_path);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_shutdown(struct mm_primacy_holder* p)
{
	mm_zkrp_path_shutdown(&p->zkrp_path);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_join(struct mm_primacy_holder* p)
{
	mm_zkrp_path_join(&p->zkrp_path);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_primacy_holder_assign_zkrp_path(struct mm_primacy_holder* p,const char* path)
{
	mm_zkrp_path_assign_path(&p->zkrp_path,path);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_assign_zkrp_host(struct mm_primacy_holder* p,const char* host)
{
	mm_zkrp_path_assign_host(&p->zkrp_path,host);
}
//////////////////////////////////////////////////////////////////////////
// update loavg to lavg_assign.
MM_EXPORT_SHUTTLE void mm_primacy_holder_update(struct mm_primacy_holder* p)
{
	struct mm_rb_root* root = &p->rbtree.rbt;
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_primacy_holder_elem* u = NULL;
	size_t hsize = 0; size_t index = 0;
	mm_float32_t a_lavg = 0;
	mm_float32_t m_lavg = 1;
	mm_float32_t l_lavg = 0;
	size_t ii = 0;	size_t ai = 0;	size_t ci = 0;	size_t wi = 0;
	mm_float32_t ti = 0;
	struct mm_primacy_holder_value* vec = NULL;

	pthread_rwlock_rdlock(&p->rbtree_locker);
	//
	hsize = mm_rbtree_u32_vpt_size(&p->rbtree);
	// hsize = 4;
	vec = (struct mm_primacy_holder_value*)mm_malloc( sizeof(struct mm_primacy_holder_value) * hsize );
	//
	n = mm_rb_first(root);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		//
		u = (struct mm_primacy_holder_elem*)(it->v);
		mm_primacy_holder_elem_lock(u);
		vec[index].k = u->unique_id;
		vec[index].v = u->weights;
		mm_primacy_holder_elem_unlock(u);
		a_lavg += vec[index].v;
		m_lavg *= vec[index].v;
		// mm_printf("%d %f\n",vec[index].k,vec[index].v);
		index++;
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
	// 1/a / ( 1/a + 1/b + 1/c ) = a*b*c/ ( a * (b*c+a*c+a*b) )  = b*c / (b*c+a*c+a*b)
	// l_lavg = (b*c+a*c+a*b)
	for (ii = 0; ii < hsize; ++ii)
	{
		l_lavg += m_lavg / vec[ii].v;
	}
	pthread_rwlock_wrlock(&p->lavg_assign.v_locker);
	//
	if (p->lavg_assign.m < hsize)
	{
		mm_lavg_assign_set_length(&p->lavg_assign, (mm_uint32_t)hsize);
	}
	mm_lavg_assign_cycle_empty(&p->lavg_assign);
	//
	for (ii = 0; ii < hsize; ++ii)
	{
		ti = ( m_lavg / vec[ii].v / l_lavg ) * p->lavg_assign.l + 0.5f;
		// when ti is nan wi will > p->lavg_assign.len.
		wi += (size_t)ti;
		wi = wi > p->lavg_assign.l ? p->lavg_assign.l : wi;
		// mm_printf("%d %f\n",vec[ii].k,ti);
		// mm_printf("ci:%d wi:%d l:%d\n",ci,wi,p->lavg_assign.l);
		for (ai = ci; ai < wi; ++ai)
		{
			// mm_printf("%d %d\n",ai,vec[ii].k);
			pthread_rwlock_rdlock(&p->rbtree_locker);
			p->lavg_assign.v[ai] = mm_rbtree_u32_vpt_get(&p->rbtree,vec[ii].k);
			// mm_printf("ii:%d k:%d v[ii]:%p\n",ai,vec[ii].k,p->lavg_assign.v[ai]);
			pthread_rwlock_unlock(&p->rbtree_locker);
		}
		ci += (size_t)ti;
	}
	pthread_rwlock_unlock(&p->lavg_assign.v_locker);
	// free
	mm_free(vec);
}
// update watcher.
MM_EXPORT_SHUTTLE void mm_primacy_holder_update_watcher(struct mm_primacy_holder* p)
{
	mm_zkrp_path_update(&p->zkrp_path);
}
//////////////////////////////////////////////////////////////////////////
// cycle ref copy.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_cycle(struct mm_primacy_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_primacy_holder_elem* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		e = mm_primacy_holder_get(p,unique_id);
		if (NULL == e)
		{
			// not find sharder holder elem.
			code = phce_lavg_assign_elem_not_find;
			break;
		}
		mm_primacy_holder_elem_lock(e);
		code = mm_primacy_holder_elem_cycle(e,runtime_state);
		mm_primacy_holder_elem_unlock(e);
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
// cycle ref.return the unique_id for live node.the min unique_id for unit.
MM_EXPORT_SHUTTLE int mm_primacy_holder_cycle_min(struct mm_primacy_holder* p,mm_uint32_t unique_id,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_primacy_holder_elem* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		e = mm_primacy_holder_get(p,unique_id);
		if (NULL == e)
		{
			// not find sharder holder elem.
			code = phce_lavg_assign_elem_not_find;
			break;
		}
		mm_primacy_holder_elem_lock(e);
		code = mm_primacy_holder_elem_cycle_min(e,runtime_state);
		mm_primacy_holder_elem_unlock(e);
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
// cycle loavger ref.return the unique_id for live node.
MM_EXPORT_SHUTTLE int mm_primacy_holder_loavger_cycle(struct mm_primacy_holder* p,mm_uint32_t* unique_id,mm::mm_m_runtime_state* runtime_state)
{
	int code = MM_UNKNOWN;
	struct mm_primacy_holder_elem* e = NULL;
	assert(NULL != runtime_state && "runtime_state is a null.");
	mm_spinlock_lock(&p->locker_holder_cycle);
	do 
	{
		if (0 == mm_lavg_assign_get_length(&p->lavg_assign))
		{
			// the lavg length is zero.
			code = phce_lavg_assign_length_not_enough;
			break;
		}
		e = (struct mm_primacy_holder_elem*)mm_lavg_assign_cycle(&p->lavg_assign);
		if (NULL == e)
		{
			// the elem is null.
			code = phce_lavg_assign_unit_is_null;
			break;
		}
		mm_primacy_holder_elem_lock(e);
		*unique_id = e->unique_id;
		code = mm_primacy_holder_elem_cycle(e,runtime_state);
		mm_primacy_holder_elem_unlock(e);
	} while (0);
	mm_spinlock_unlock(&p->locker_holder_cycle);
	return code;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_add(struct mm_primacy_holder* p,mm_uint32_t unique_id,struct mm_zkrp_path_elem* zkrp_path_elem)
{
	struct mm_primacy_holder_elem* e = NULL;
	// only lock for instance create process.
	mm_spinlock_lock(&p->instance_locker);
	e = mm_primacy_holder_get(p, unique_id);
	if (NULL == e)
	{
		struct mm_logger* g_logger = mm_logger_instance();
		e = (struct mm_primacy_holder_elem*)mm_malloc(sizeof(struct mm_primacy_holder_elem));
		mm_primacy_holder_elem_init(e);
		e->unique_id = unique_id;
		e->zkrp_path_elem = zkrp_path_elem;
		//
		pthread_rwlock_wrlock(&p->rbtree_locker);
		mm_rbtree_u32_vpt_set(&p->rbtree,unique_id,e);
		pthread_rwlock_unlock(&p->rbtree_locker);
		//
		mm_logger_log_I(g_logger,"%s %d shard_index:%u is add.",__FUNCTION__,__LINE__,e->unique_id);
	}
	mm_spinlock_unlock(&p->instance_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_get(struct mm_primacy_holder* p,mm_uint32_t unique_id)
{
	struct mm_primacy_holder_elem* e = NULL;
	//
	pthread_rwlock_rdlock(&p->rbtree_locker);
	e = (struct mm_primacy_holder_elem*)mm_rbtree_u32_vpt_get(&p->rbtree,unique_id);
	pthread_rwlock_unlock(&p->rbtree_locker);
	return e;
}
MM_EXPORT_SHUTTLE struct mm_primacy_holder_elem* mm_primacy_holder_get_instance(struct mm_primacy_holder* p,mm_uint32_t unique_id,struct mm_zkrp_path_elem* zkrp_path_elem)
{
	struct mm_primacy_holder_elem* e = mm_primacy_holder_get(p,unique_id);
	if (NULL == e)
	{
		e = mm_primacy_holder_add(p,unique_id,zkrp_path_elem);
	}
	return e;
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_rmv(struct mm_primacy_holder* p,mm_uint32_t unique_id)
{
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	//
	pthread_rwlock_wrlock(&p->rbtree_locker);
	it = mm_rbtree_u32_vpt_get_iterator(&p->rbtree,unique_id);
	if (NULL != it)
	{
		struct mm_primacy_holder_elem* e = (struct mm_primacy_holder_elem*)(it->v);
		mm_logger_log_I(g_logger,"%s %d shard_index:%u is add.",__FUNCTION__,__LINE__,e->unique_id);
		mm_primacy_holder_elem_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_primacy_holder_elem_unlock(e);
		mm_primacy_holder_elem_destroy(e);
		mm_free(e);
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
MM_EXPORT_SHUTTLE void mm_primacy_holder_clear(struct mm_primacy_holder* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_rbtree_u32_vpt_iterator* it = NULL;
	struct mm_primacy_holder_elem* e = NULL;
	pthread_rwlock_wrlock(&p->rbtree_locker);
	n = mm_rb_first(&p->rbtree.rbt);
	while(NULL != n)
	{
		it = (struct mm_rbtree_u32_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		e = (struct mm_primacy_holder_elem*)(it->v);
		mm_primacy_holder_elem_lock(e);
		mm_rbtree_u32_vpt_erase(&p->rbtree,it);
		mm_primacy_holder_elem_unlock(e);
		mm_primacy_holder_elem_destroy(e);
		mm_free(e);
	}
	pthread_rwlock_unlock(&p->rbtree_locker);
}
MM_EXPORT_SHUTTLE const char* mm_primacy_holder_cycle_error_message(int error)
{
	if (phce_lavg_none < error && error < phce_lavg_max)
	{
		return const_primacy_holder_cycle_error[error];
	}
	else
	{
		return "";
	}
}
//////////////////////////////////////////////////////////////////////////
static void __static_primacy_holder_path_elem_unit_value_updated( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u, const char* buffer, int offset, int length )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	do 
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_primacy_holder_elem* elem = NULL;
		struct mm_primacy_holder_unit* unit = NULL;
		mm_logger_log_D(g_logger,"%s %d e->unique_id:%d u->unique_id:%u.",__FUNCTION__,__LINE__,e->unique_id,u->unique_id);
		//
		elem = mm_primacy_holder_get(primacy_holder,e->unique_id);
		if ( NULL == elem )
		{
			mm_spinlock_lock(&primacy_holder->locker_holder_cycle);
			elem = mm_primacy_holder_add(primacy_holder,e->unique_id,e);
			mm_primacy_holder_update(primacy_holder);
			mm_spinlock_unlock(&primacy_holder->locker_holder_cycle);
		}
		if ( NULL == elem )
		{
			// system memory error.
			mm_logger_log_E(g_logger,"%s %d system memory error..",__FUNCTION__,__LINE__);
			break;
		}
		unit = mm_primacy_holder_elem_get(elem,u->unique_id);
		if ( NULL == unit )
		{
			mm_spinlock_lock(&primacy_holder->locker_holder_cycle);
			unit = mm_primacy_holder_elem_add(elem,u->unique_id,u);
			mm_primacy_holder_elem_update(elem);
			mm_spinlock_unlock(&primacy_holder->locker_holder_cycle);
		}
		if ( NULL == unit )
		{
			// system memory error.
			mm_logger_log_E(g_logger,"%s %d system memory error..",__FUNCTION__,__LINE__);
			break;
		}
		mm_primacy_holder_unit_lock(unit);
		mm::mm_j_runtime_state j_runtime(unit->runtime_state);
		rapidjson::Document runtime_doc(rapidjson::kObjectType);
		if (runtime_doc.Parse((const char*)(buffer + offset),length).HasParseError())
		{
			mm_logger_log_E(g_logger,"%s %d failure to decode runtime_doc.",__FUNCTION__,__LINE__);
		}
		j_runtime.decode(runtime_doc,runtime_doc);
		mm_primacy_holder_unit_unlock(unit);
	} while (0);
}
static void __static_primacy_holder_path_elem_unit_event_created( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	struct mm_primacy_holder_elem* primacy_holder_elem = mm_primacy_holder_get_instance(primacy_holder,e->unique_id,e);
	mm_spinlock_lock(&primacy_holder_elem->locker_holder_cycle);
	mm_primacy_holder_elem_get_instance(primacy_holder_elem,u->unique_id,u);
	mm_primacy_holder_elem_update(primacy_holder_elem);
	mm_spinlock_unlock(&primacy_holder_elem->locker_holder_cycle);
}
static void __static_primacy_holder_path_elem_unit_event_deleted( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, struct mm_zkrp_path_unit* u )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	struct mm_primacy_holder_elem* primacy_holder_elem = mm_primacy_holder_get_instance(primacy_holder,e->unique_id,e);
	mm_spinlock_lock(&primacy_holder_elem->locker_holder_cycle);
	mm_primacy_holder_elem_rmv(primacy_holder_elem,u->unique_id);
	mm_primacy_holder_elem_update(primacy_holder_elem);
	mm_spinlock_unlock(&primacy_holder_elem->locker_holder_cycle);
}
static void __static_primacy_holder_path_elem_value_updated( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e, const char* buffer, int offset, int length )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	do 
	{
		struct mm_logger* g_logger = mm_logger_instance();
		struct mm_primacy_holder_elem* elem = NULL;
		mm_logger_log_D(g_logger,"%s %d e->unique_id:%u.",__FUNCTION__,__LINE__,e->unique_id);
		//
		elem = mm_primacy_holder_get(primacy_holder,e->unique_id);
		if ( NULL == elem )
		{
			mm_spinlock_lock(&primacy_holder->locker_holder_cycle);
			elem = mm_primacy_holder_add(primacy_holder,e->unique_id,e);
			mm_primacy_holder_update(primacy_holder);
			mm_spinlock_unlock(&primacy_holder->locker_holder_cycle);
		}
		if ( NULL == elem )
		{
			// system memory error.
			mm_logger_log_E(g_logger,"%s %d system memory error..",__FUNCTION__,__LINE__);
			break;
		}
		// here do nothing.
	} while (0);
}
static void __static_primacy_holder_path_elem_event_created( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	mm_spinlock_lock(&primacy_holder->locker_holder_cycle);
	mm_primacy_holder_get_instance(primacy_holder,e->unique_id,e);
	mm_primacy_holder_update(primacy_holder);
	mm_spinlock_unlock(&primacy_holder->locker_holder_cycle);
}
static void __static_primacy_holder_path_elem_event_deleted( struct mm_zkrp_path* p, struct mm_zkrp_path_elem* e )
{
	struct mm_primacy_holder* primacy_holder = (struct mm_primacy_holder*)(p->callback.obj);
	mm_spinlock_lock(&primacy_holder->locker_holder_cycle);
	mm_primacy_holder_rmv(primacy_holder,e->unique_id);
	mm_primacy_holder_update(primacy_holder);
	mm_spinlock_unlock(&primacy_holder->locker_holder_cycle);
}
