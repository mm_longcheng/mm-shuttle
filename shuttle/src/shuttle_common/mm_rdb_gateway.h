#ifndef __mm_rdb_gateway_h__
#define __mm_rdb_gateway_h__

#include "core/mm_core.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE extern const char* g_rdb_gateway_sid2add_pattern;
// "mm:gateway:sid2add:%04d"
MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2add_key( char key[32], int index );
//////////////////////////////////////////////////////////////////////////
#define MM_SID2UID_MOD_NUMBER 1024
#define MM_UID2SID_MOD_NUMBER 1024
MM_EXPORT_SHUTTLE extern const char* g_rdb_gateway_sid2uid_pattern;
MM_EXPORT_SHUTTLE extern const char* g_rdb_gateway_uid2sid_pattern;
// "mm:gateway:sid2uid:%04d" sid % MM_SID2UID_MOD_NUMBER {k(u64 sid),v(u64 uid)}
MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2uid_key( char key[32], int index );
// "mm:gateway:uid2sid:%04d" uid % MM_UID2SID_MOD_NUMBER {k(u64 uid),v(u64 sid)}
MM_EXPORT_SHUTTLE void mm_rdb_gateway_uid2sid_key( char key[32], int index );

MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2uid_key_by_sid( char key[32], mm_uint64_t sid );
MM_EXPORT_SHUTTLE void mm_rdb_gateway_uid2sid_key_by_uid( char key[32], mm_uint64_t uid );
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_rdb_gateway_h__