﻿#ifndef __mm_j_runtime_state_h__
#define __mm_j_runtime_state_h__

#include "core/mm_core.h"

#include "dish/mm_package.h"
#include "dish/mm_m_runtime_state.h"
#include "dish/mm_json.h"

#include "shuttle_common/mm_shuttle_export.h"

namespace mm
{
	// 
	struct MM_EXPORT_SHUTTLE mm_j_runtime_state : public mm_i_json
	{
		mm_m_runtime_state& _data;

		mm_j_runtime_state(mm_m_runtime_state& _rhs);
		virtual int decode(rapidjson::Document& _jc, const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc, rapidjson::Value& _jv) const;
	};
	// 
	struct MM_EXPORT_SHUTTLE mm_j_config_module : public mm_i_json
	{
		mm_m_config_module& _data;

		mm_j_config_module(mm_m_config_module& _rhs);
		virtual int decode(rapidjson::Document& _jc, const rapidjson::Value& _jv);
		virtual int encode(rapidjson::Document& _jc, rapidjson::Value& _jv) const;
	};
}
#endif//__mm_j_runtime_state_h__