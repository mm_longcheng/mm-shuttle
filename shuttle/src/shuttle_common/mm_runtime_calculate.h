#ifndef __mm_runtime_calculate_h__
#define __mm_runtime_calculate_h__

#include "core/mm_core.h"
#include "core/mm_runtime_stat.h"

#include "dish/mm_m_runtime_state.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"
//////////////////////////////////////////////////////////////////////////
// runtime weights.
MM_EXPORT_SHUTTLE mm_float32_t mm_runtime_loavger_calculate(struct mm_runtime_stat* runtime, mm_float32_t factor[3]);
// real worker number from config.
MM_EXPORT_SHUTTLE mm_uint32_t mm_runtime_real_worker_number(mm_uint32_t worker_number_config);
// update runtime info to runtime_state.
MM_EXPORT_SHUTTLE void mm_runtime_state_update(mm::mm_m_runtime_state* runtime_state, struct mm_runtime_stat* runtime);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_runtime_calculate_h__