#include "mm_message_lpusher.h"
#include "core/mm_logger.h"
#include "core/mm_streambuf.h"
#include "core/mm_parameters.h"

#include "net/mm_streambuf_packet.h"

#include "redis/mm_redis_list.h"

#include "protobuf/mm_protobuff_cxx.h"

#include "shuttle_common/mm_mulcast_c.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_init(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_init(&p->db_redis_queue);
	mm_streambuf_array_init(&p->streambuf_array);
	mm_string_init(&p->queue_key);
	mm_spinlock_init(&p->locker, NULL);
	// the max thread mm_streambuf thread instance is 2;
	mm_streambuf_array_set_length(&p->streambuf_array, 2);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_destroy(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_destroy(&p->db_redis_queue);
	mm_streambuf_array_destroy(&p->streambuf_array);
	mm_string_destroy(&p->queue_key);
	mm_spinlock_destroy(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_start(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_start(&p->db_redis_queue);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_interrupt(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_interrupt(&p->db_redis_queue);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_shutdown(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_shutdown(&p->db_redis_queue);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_join(struct mm_message_lpusher* p)
{
	mm_redis_sync_array_join(&p->db_redis_queue);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_lock(struct mm_message_lpusher* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_unlock(struct mm_message_lpusher* p)
{
	mm_spinlock_unlock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_parameters(struct mm_message_lpusher* p, const char* parameters)
{
	char node[MM_NODE_NAME_LENGTH] = { 0 };
	mm_ushort_t port = 0;
	//
	mm_parameters_client(parameters, node, &port);
	//
	mm_message_lpusher_assign_queue_remote(p, node, port);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_remote(struct mm_message_lpusher* p,const char* node,mm_ushort_t port)
{
	mm_redis_sync_array_assign_remote(&p->db_redis_queue,node,port);
	mm_redis_sync_array_apply_broken(&p->db_redis_queue);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_passwd(struct mm_message_lpusher* p,const char* passwd)
{
	mm_redis_sync_array_assign_auth(&p->db_redis_queue,passwd);
}
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_nameky(struct mm_message_lpusher* p,const char* nameky)
{
	mm_string_assigns(&p->queue_key,nameky);
}
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer(struct mm_message_lpusher* p, mm_uint8_t* buffer, size_t offset, size_t length)
{
	long long number = 0;
	do
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->db_redis_queue.node.s, p->db_redis_queue.port);
			break;
		}
		{
			//////////////////////////////////////////////////////////////////////////
			number = mm_redis_list_lpush(redis, p->queue_key.s, (void*)(buffer + offset), length);
		}
	} while (0);
	return number;
}
// lpush buffer data, but not lpush.
MM_EXPORT_SHUTTLE void mm_message_lpusher_buffer_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack)
{
	packet_head->pid = 0;
	do
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger, "%s %d dbredis %s:%d connect failure.", __FUNCTION__, __LINE__, p->db_redis_queue.node.s, p->db_redis_queue.port);
			break;
		}
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger, "%s %d system memory not enough.", __FUNCTION__, __LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			//////////////////////////////////////////////////////////////////////////
			mm_streambuf_unit_lock(streambuf_unit);
			streambuf_0 = streambuf_unit->arrays[0];
			mm_streambuf_reset(streambuf_0);
			//////////////////////////////////////////////////////////////////////////
			mm_protobuf_cxx_encode_message_protobuf(streambuf_0, message, packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
			// cback packet not need crypto.
			// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
			//////////////////////////////////////////////////////////////////////////
		}
	} while (0);
}
// lpush buffer data size.
MM_EXPORT_SHUTTLE size_t mm_message_lpusher_buffer_size(struct mm_message_lpusher* p)
{
	size_t sz = 0;
	do
	{
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger, "%s %d system memory not enough.", __FUNCTION__, __LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			streambuf_0 = streambuf_unit->arrays[0];
			sz = mm_streambuf_size(streambuf_0);
		}
	} while (0);
	return sz;
}
// lpush buffer data copy.
MM_EXPORT_SHUTTLE void mm_message_lpusher_buffer_copy(struct mm_message_lpusher* p, mm_uint8_t* buffer, size_t offset, size_t length)
{
	do
	{
		size_t sz = 0;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_streambuf* streambuf_0 = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger, "%s %d system memory not enough.", __FUNCTION__, __LINE__);
			break;
		}
		streambuf_0 = streambuf_unit->arrays[0];
		sz = mm_streambuf_size(streambuf_0);
		if (length < sz)
		{
			mm_logger_log_E(g_logger, "%s %d buffer memory not enough sz: %" PRIuPTR " length: %" PRIuPTR ".", __FUNCTION__, __LINE__, sz, length);
			break;
		}
		{
			mm_memcpy((buffer + offset), (void*)(streambuf_0->buff + streambuf_0->gptr), sz);
		}
	} while (0);
}
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack)
{
	long long number = 0;
	packet_head->pid = 0;
	do 
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->db_redis_queue.node.s,p->db_redis_queue.port);
			break;
		}
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger,"%s %d system memory not enough.",__FUNCTION__,__LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			size_t sz = 0;
			//////////////////////////////////////////////////////////////////////////
			mm_streambuf_unit_lock(streambuf_unit);
			streambuf_0 = streambuf_unit->arrays[0];
			mm_streambuf_reset(streambuf_0);
			//////////////////////////////////////////////////////////////////////////
			mm_protobuf_cxx_encode_message_protobuf(streambuf_0, message, packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
			// cback packet not need crypto.
			// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
			//////////////////////////////////////////////////////////////////////////
			sz = mm_streambuf_size(streambuf_0);
			number = mm_redis_list_lpush(redis, p->queue_key.s, (void*)(streambuf_0->buff + streambuf_0->gptr), sz);
			mm_streambuf_unit_unlock(streambuf_unit);
		}
	} while (0);
	return number;
}

MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer_byte_buffer(struct mm_message_lpusher* p, struct mm_byte_buffer* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack)
{
	long long number = 0;
	packet_head->pid = 0;
	do 
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->db_redis_queue.node.s,p->db_redis_queue.port);
			break;
		}
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger,"%s %d system memory not enough.",__FUNCTION__,__LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			size_t sz = 0;
			//////////////////////////////////////////////////////////////////////////
			mm_streambuf_unit_lock(streambuf_unit);
			streambuf_0 = streambuf_unit->arrays[0];
			mm_streambuf_reset(streambuf_0);
			//////////////////////////////////////////////////////////////////////////
			mm_message_coder_encode_message_byte_buffer(streambuf_0, message, packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
			// cback packet not need crypto.
			// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
			//////////////////////////////////////////////////////////////////////////
			sz = mm_streambuf_size(streambuf_0);
			number = mm_redis_list_lpush(redis, p->queue_key.s, (void*)(streambuf_0->buff + streambuf_0->gptr), sz);
			mm_streambuf_unit_unlock(streambuf_unit);
		}
	} while (0);
	return number;
}

MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_arrays_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_rbtset_u64* arrays, struct mm_packet* nt_pack)
{
	long long number = 0;
	packet_head->pid = mm_mulcast_rbtset_msg_id;
	do 
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->db_redis_queue.node.s,p->db_redis_queue.port);
			break;
		}
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger,"%s %d system memory not enough.",__FUNCTION__,__LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			struct mm_streambuf* streambuf_1 = NULL;
			size_t sz = 0;
			struct mm_byte_buffer unique_byte_buffer;
			//////////////////////////////////////////////////////////////////////////
			mm_byte_buffer_init(&unique_byte_buffer);
			//////////////////////////////////////////////////////////////////////////
			mm_streambuf_unit_lock(streambuf_unit);
			streambuf_0 = streambuf_unit->arrays[0];
			streambuf_1 = streambuf_unit->arrays[1];
			mm_streambuf_reset(streambuf_0);
			mm_streambuf_reset(streambuf_1);
			//////////////////////////////////////////////////////////////////////////
			unique_byte_buffer.length = (mm_uint32_t)message->ByteSizeLong();
			mm_mulcast_rbtset_encode_buffer_arrays(streambuf_1, &unique_byte_buffer, arrays);
			mm_protobuf_cxx_encode_message_byte_buffer(&unique_byte_buffer, message);
			//
			mm_message_coder_encode_message_streambuf(streambuf_0, streambuf_1, packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
			// cback packet not need crypto.
			// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
			//////////////////////////////////////////////////////////////////////////
			sz = mm_streambuf_size(streambuf_0);
			number = mm_redis_list_lpush(redis, p->queue_key.s, (void*)(streambuf_0->buff + streambuf_0->gptr), sz);
			mm_streambuf_unit_unlock(streambuf_unit);
			//////////////////////////////////////////////////////////////////////////
			mm_byte_buffer_destroy(&unique_byte_buffer);
		}
	} while (0);
	return number;
}
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_arrays_byte_buffer(struct mm_message_lpusher* p, struct mm_byte_buffer* message, struct mm_packet_head* packet_head, struct mm_rbtset_u64* arrays, struct mm_packet* nt_pack)
{
	long long number = 0;
	packet_head->pid = mm_mulcast_rbtset_msg_id;
	do 
	{
		struct mm_redis_sync* redis = NULL;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_logger* g_logger = mm_logger_instance();
		redis = mm_redis_sync_array_thread_instance(&p->db_redis_queue);
		if (NULL == redis)
		{
			mm_logger_log_E(g_logger,"%s %d dbredis %s:%d connect failure.",__FUNCTION__,__LINE__,p->db_redis_queue.node.s,p->db_redis_queue.port);
			break;
		}
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger,"%s %d system memory not enough.",__FUNCTION__,__LINE__);
			break;
		}
		{
			struct mm_streambuf* streambuf_0 = NULL;
			struct mm_streambuf* streambuf_1 = NULL;
			size_t sz = 0;
			struct mm_byte_buffer unique_byte_buffer;
			//////////////////////////////////////////////////////////////////////////
			mm_byte_buffer_init(&unique_byte_buffer);
			//////////////////////////////////////////////////////////////////////////
			mm_streambuf_unit_lock(streambuf_unit);
			streambuf_0 = streambuf_unit->arrays[0];
			streambuf_1 = streambuf_unit->arrays[1];
			mm_streambuf_reset(streambuf_0);
			mm_streambuf_reset(streambuf_1);
			//////////////////////////////////////////////////////////////////////////
			unique_byte_buffer.length = message->length;
			mm_mulcast_rbtset_encode_buffer_arrays(streambuf_1, &unique_byte_buffer, arrays);
			mm_memcpy((void*)(unique_byte_buffer.buffer + unique_byte_buffer.offset),(void*)(message->buffer + message->offset),unique_byte_buffer.length);
			//
			mm_message_coder_encode_message_streambuf(streambuf_0, streambuf_1, packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
			// cback packet not need crypto.
			// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
			//////////////////////////////////////////////////////////////////////////
			sz = mm_streambuf_size(streambuf_0);
			number = mm_redis_list_lpush(redis, p->queue_key.s, (void*)(streambuf_0->buff + streambuf_0->gptr), sz);
			mm_streambuf_unit_unlock(streambuf_unit);
			//////////////////////////////////////////////////////////////////////////
			mm_byte_buffer_destroy(&unique_byte_buffer);
		}
	} while (0);
	return number;
}
