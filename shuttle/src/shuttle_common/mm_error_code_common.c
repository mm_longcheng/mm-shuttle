﻿#include "mm_error_code_common.h"

MM_EXPORT_SHUTTLE void mm_error_desc_assign_common(struct mm_error_desc* p)
{
	mm_error_core_assign_code_desc(p,err_common_package_decode_failure, "package decode failure.");
	mm_error_core_assign_code_desc(p,err_common_package_encode_failure, "package encode failure.");

	mm_error_core_assign_code_desc(p,err_common_exec_procedure_failure, "exec procedure failure.");
	mm_error_core_assign_code_desc(p,err_common_exec_script_failure, "exec script failure.");
	mm_error_core_assign_code_desc(p,err_common_exec_command_failure, "exec command failure.");

	mm_error_core_assign_code_desc(p,err_common_not_support_module, "not support module.");
	mm_error_core_assign_code_desc(p,err_common_not_support_function, "not support function.");
	mm_error_core_assign_code_desc(p,err_common_not_support_interface, "not support interface.");
	mm_error_core_assign_code_desc(p,err_common_not_support_opcode, "not support opcode.");

	mm_error_core_assign_code_desc(p,err_common_invalid_parameter, "invalid parameter.");
	mm_error_core_assign_code_desc(p,err_common_invalid_account_id, "invalid account id.");

	mm_error_core_assign_code_desc(p,err_common_net_db_disconnect, "net db disconnect.");
	mm_error_core_assign_code_desc(p,err_common_net_server_disconnect, "net server disconnect.");

	mm_error_core_assign_code_desc(p,err_common_sys_not_enough_memory, "sys not enough memory.");

	mm_error_core_assign_code_desc(p,err_common_sys_io_fopen_failure, "sys io fopen failure.");
}