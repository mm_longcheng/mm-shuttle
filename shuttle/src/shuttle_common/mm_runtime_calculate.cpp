﻿#include "mm_runtime_calculate.h"
#include "core/mm_os_context.h"
#include "core/mm_logger_manager.h"

// runtime weights.
MM_EXPORT_SHUTTLE mm_float32_t mm_runtime_loavger_calculate(struct mm_runtime_stat* runtime, mm_float32_t factor[3])
{
	mm_float32_t la = runtime->loadavg_o.lavg_01 * 15 + runtime->loadavg_o.lavg_05 * 3 + runtime->loadavg_o.lavg_15 * 1;
	mm_float32_t v0 = la / runtime->cores;
	mm_float32_t v1 = runtime->cpu_pro / runtime->cores;
	mm_float32_t v2 = runtime->mem_pro;
	return factor[0] * v0 + factor[1] * v1 + factor[2] * v2 + 0.01f;
}
// real worker number from config.
MM_EXPORT_SHUTTLE mm_uint32_t mm_runtime_real_worker_number( mm_uint32_t worker_number_config )
{
	mm_uint32_t worker_number = worker_number_config;
	if (0 == worker_number_config)
	{
		struct mm_os_context* g_os_context = mm_os_context_instance();
		worker_number = g_os_context->cores;
	}
	else
	{
		worker_number = worker_number_config;
	}
	return worker_number;
}
// update runtime info to runtime_state.
MM_EXPORT_SHUTTLE void mm_runtime_state_update(mm::mm_m_runtime_state* runtime_state, struct mm_runtime_stat* runtime)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	//
	mm_float32_t factor[3] = {1,1,1};
	mm_runtime_stat_update(runtime);
	// update mysql.
	runtime_state->pid = runtime->pid;
	runtime_state->lavg_01 = runtime->loadavg_o.lavg_01;
	runtime_state->lavg_05 = runtime->loadavg_o.lavg_05;
	runtime_state->lavg_15 = runtime->loadavg_o.lavg_15;
	runtime_state->cpu_pro = runtime->cpu_pro;
	runtime_state->mem_pro = runtime->mem_pro;
	runtime_state->weights = mm_runtime_loavger_calculate(runtime, factor);
	runtime_state->logger_lvl = g_logger_manager->logger_level;
	runtime_state->logger_dir = g_logger_manager->logger_path.s;
	runtime_state->updatetime = (mm_uint32_t)(runtime->timecode_n / MM_MSEC_PER_SEC);
}