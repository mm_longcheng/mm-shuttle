#ifndef __mm_message_lpusher_h__
#define __mm_message_lpusher_h__

#include <google/protobuf/message.h>

#include "core/mm_core.h"
#include "core/mm_streambuf.h"
#include "core/mm_spinlock.h"
#include "core/mm_byte.h"

#include "container/mm_rbtset_u64.h"

#include "net/mm_packet.h"

#include "shuttle_common/mm_streambuf_array.h"

#include "redis/mm_redis_sync_array.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

struct mm_message_lpusher
{
	struct mm_redis_sync_array db_redis_queue;
	struct mm_streambuf_array streambuf_array;
	struct mm_string queue_key;
	mm_atomic_t locker;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_init(struct mm_message_lpusher* p);
MM_EXPORT_SHUTTLE void mm_message_lpusher_destroy(struct mm_message_lpusher* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_start(struct mm_message_lpusher* p);
MM_EXPORT_SHUTTLE void mm_message_lpusher_interrupt(struct mm_message_lpusher* p);
MM_EXPORT_SHUTTLE void mm_message_lpusher_shutdown(struct mm_message_lpusher* p);
MM_EXPORT_SHUTTLE void mm_message_lpusher_join(struct mm_message_lpusher* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_lpusher_lock(struct mm_message_lpusher* p);
MM_EXPORT_SHUTTLE void mm_message_lpusher_unlock(struct mm_message_lpusher* p);
//////////////////////////////////////////////////////////////////////////
// 192.168.111.123-65535
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_parameters(struct mm_message_lpusher* p, const char* parameters);
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_remote(struct mm_message_lpusher* p,const char* node,mm_ushort_t port);
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_passwd(struct mm_message_lpusher* p,const char* passwd);
MM_EXPORT_SHUTTLE void mm_message_lpusher_assign_queue_nameky(struct mm_message_lpusher* p,const char* nameky);
//////////////////////////////////////////////////////////////////////////
// lpush buffer to redis.
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer(struct mm_message_lpusher* p, mm_uint8_t* buffer, size_t offset, size_t length);
//////////////////////////////////////////////////////////////////////////
// lpush buffer data, but not lpush.
MM_EXPORT_SHUTTLE void mm_message_lpusher_buffer_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack);
// lpush buffer data size.
MM_EXPORT_SHUTTLE size_t mm_message_lpusher_buffer_size(struct mm_message_lpusher* p);
// lpush buffer data copy.
MM_EXPORT_SHUTTLE void mm_message_lpusher_buffer_copy(struct mm_message_lpusher* p, mm_uint8_t* buffer, size_t offset, size_t length);
//////////////////////////////////////////////////////////////////////////
// packet_head.pid must not mm_mulcast_rbtset_msg_id.
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack);
// packet_head.pid must not mm_mulcast_rbtset_msg_id.
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_buffer_byte_buffer(struct mm_message_lpusher* p, struct mm_byte_buffer* message, struct mm_packet_head* packet_head, struct mm_packet* nt_pack);
//////////////////////////////////////////////////////////////////////////
// packet_head.pid not need assignment, will auto assign mm_mulcast_rbtset_msg_id at internal.
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_arrays_protobuf(struct mm_message_lpusher* p, ::google::protobuf::Message* message, struct mm_packet_head* packet_head, struct mm_rbtset_u64* arrays, struct mm_packet* nt_pack);
// packet_head.pid not need assignment, will auto assign mm_mulcast_rbtset_msg_id at internal.
MM_EXPORT_SHUTTLE long long mm_message_lpusher_lpush_arrays_byte_buffer(struct mm_message_lpusher* p, struct mm_byte_buffer* message, struct mm_packet_head* packet_head, struct mm_rbtset_u64* arrays, struct mm_packet* nt_pack);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_message_lpusher_h__