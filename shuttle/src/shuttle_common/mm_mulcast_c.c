﻿#include "mm_mulcast_c.h"
#include "core/mm_alloc.h"
#include "core/mm_byteswap.h"
#include "core/mm_logger.h"

MM_EXPORT_SHUTTLE void mm_mulcast_vector_init(struct mm_mulcast_vector* p)
{
	mm_byte_buffer_init(&p->buffer);
	mm_vector_u64_init(&p->arrays);
}
MM_EXPORT_SHUTTLE void mm_mulcast_vector_destroy(struct mm_mulcast_vector* p)
{
	mm_byte_buffer_destroy(&p->buffer);
	mm_vector_u64_destroy(&p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_vector_reset(struct mm_mulcast_vector* p)
{
	mm_byte_buffer_reset(&p->buffer);
	mm_vector_u64_resize(&p->arrays, 0);
}

MM_EXPORT_SHUTTLE void mm_mulcast_vector_decode( struct mm_mulcast_vector* p, struct mm_streambuf* streambuf )
{
	mm_mulcast_vector_decode_buffer_arrays(streambuf, &p->buffer, &p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_vector_encode( struct mm_mulcast_vector* p, struct mm_streambuf* streambuf )
{
	mm_mulcast_vector_encode_buffer_arrays(streambuf, &p->buffer, &p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_vector_decode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_vector_u64* arrays)
{
	mm_uint32_t index = 0;
	mm_uint32_t arrays_sz = 0;
	mm_uint32_t size_ne = 0;
	mm_uint64_t u64_ne = 0;
	mm_uint8_t* ptr = NULL;
	size_t sz = 0;
	mm_uint32_t arrays_size_offset = 0;
	do 
	{
		size_t streambuf_size = mm_streambuf_size(streambuf);
		mm_memcpy(&size_ne, (void*)(streambuf->buff + streambuf->gptr), sizeof(mm_uint32_t));
		buffer->length = mm_ltoh32((mm_uint32_t)(size_ne));
		buffer->buffer = streambuf->buff;
		buffer->offset = (mm_uint32_t)(streambuf->gptr + sizeof(mm_uint32_t));
		arrays_size_offset = (mm_uint32_t)(streambuf->gptr + sizeof(mm_uint32_t) + buffer->length);
		if (arrays_size_offset + sizeof(mm_uint32_t) > streambuf->pptr)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d invalid rmq message buffer.",__FUNCTION__,__LINE__);
			break;
		}
		mm_memcpy(&size_ne, (void*)(streambuf->buff + arrays_size_offset), sizeof(mm_uint32_t));
		arrays_sz = mm_ltoh32((mm_uint32_t)(size_ne));
		mm_vector_u64_resize(arrays, arrays_sz);
		ptr = (mm_uint8_t*)(streambuf->buff + arrays_size_offset + sizeof(mm_uint32_t));
		sz = sizeof(mm_uint32_t) + buffer->length + sizeof(mm_uint32_t) + sizeof(mm_uint64_t) * arrays_sz;
		if (sz > streambuf_size)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d invalid rmq message buffer.",__FUNCTION__,__LINE__);
			break;
		}
		for (index = 0; index < arrays_sz; ++ index)
		{
			mm_memcpy(&u64_ne, (void*)(ptr + sizeof(mm_uint64_t) * index), sizeof(mm_uint64_t));
			mm_vector_u64_set_index(arrays, index, mm_ltoh64((mm_uint64_t)(u64_ne)));
		}
		mm_streambuf_gbump(streambuf, sz);
	} while (0);
}

MM_EXPORT_SHUTTLE void mm_mulcast_vector_encode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_vector_u64* arrays )
{
	mm_uint32_t index = 0;
	mm_uint64_t u64_ne = 0;
	mm_uint32_t size_ne = 0;
	mm_uint8_t* ptr = NULL;
	mm_uint32_t arrays_sz = (mm_uint32_t)(arrays->size);
	size_t sz = 0;
	mm_uint32_t arrays_size_offset = 0;
	sz = sizeof(mm_uint32_t) + buffer->length + sizeof(mm_uint32_t) + sizeof(mm_uint64_t) * arrays_sz;
	mm_streambuf_aligned_memory(streambuf, sz);
	size_ne = mm_htol32((mm_uint32_t)(buffer->length));
	buffer->buffer = streambuf->buff;
	buffer->offset = (mm_uint32_t)(streambuf->pptr + sizeof(mm_uint32_t));
	mm_memcpy((void*)(streambuf->buff + streambuf->pptr), &size_ne, sizeof(mm_uint32_t));
	size_ne = mm_htol32((mm_uint32_t)(arrays_sz));
	arrays_size_offset = (mm_uint32_t)(streambuf->pptr + sizeof(mm_uint32_t) + buffer->length);
	mm_memcpy((void*)(streambuf->buff + arrays_size_offset), &size_ne, sizeof(mm_uint32_t));
	ptr = (mm_uint8_t*)(streambuf->buff + arrays_size_offset + sizeof(mm_uint32_t));
	for (index = 0; index < arrays_sz; ++ index)
	{
		u64_ne = mm_htol64((mm_uint64_t)(mm_vector_u64_get_index(arrays, index)));
		mm_memcpy((void*)(ptr + sizeof(mm_uint64_t) * index), &u64_ne, sizeof(mm_uint64_t));
	}
	mm_streambuf_pbump(streambuf, sz);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_init( struct mm_mulcast_rbtset* p )
{
	mm_byte_buffer_init(&p->buffer);
	mm_rbtset_u64_init(&p->arrays);
}
MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_destroy( struct mm_mulcast_rbtset* p )
{
	mm_byte_buffer_destroy(&p->buffer);
	mm_rbtset_u64_destroy(&p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_reset( struct mm_mulcast_rbtset* p )
{
	mm_byte_buffer_reset(&p->buffer);
	mm_rbtset_u64_clear(&p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_decode( struct mm_mulcast_rbtset* p, struct mm_streambuf* streambuf )
{
	mm_mulcast_rbtset_decode_buffer_arrays(streambuf, &p->buffer, &p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_encode( struct mm_mulcast_rbtset* p, struct mm_streambuf* streambuf )
{
	mm_mulcast_rbtset_encode_buffer_arrays(streambuf, &p->buffer, &p->arrays);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_decode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_rbtset_u64* arrays)
{
	mm_uint32_t index = 0;
	mm_uint32_t arrays_sz = 0;
	mm_uint32_t size_ne = 0;
	mm_uint64_t u64_ne = 0;
	mm_uint8_t* ptr = NULL;
	size_t sz = 0;
	mm_uint32_t arrays_size_offset = 0;
	do 
	{
		size_t streambuf_size = mm_streambuf_size(streambuf);
		mm_memcpy(&size_ne, (void*)(streambuf->buff + streambuf->gptr), sizeof(mm_uint32_t));
		buffer->length = mm_ltoh32((mm_uint32_t)(size_ne));
		buffer->buffer = streambuf->buff;
		buffer->offset = (mm_uint32_t)(streambuf->gptr + sizeof(mm_uint32_t));
		arrays_size_offset = (mm_uint32_t)(streambuf->gptr + sizeof(mm_uint32_t) + buffer->length);
		if (arrays_size_offset + sizeof(mm_uint32_t) > streambuf->pptr)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d invalid rmq message buffer.",__FUNCTION__,__LINE__);
			break;
		}
		mm_memcpy(&size_ne, (void*)(streambuf->buff + arrays_size_offset), sizeof(mm_uint32_t));
		arrays_sz = mm_ltoh32((mm_uint32_t)(size_ne));
		ptr = (mm_uint8_t*)(streambuf->buff + arrays_size_offset + sizeof(mm_uint32_t));
		sz = sizeof(mm_uint32_t) + buffer->length + sizeof(mm_uint32_t) + sizeof(mm_uint64_t) * arrays_sz;
		if (sz > streambuf_size)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_E(g_logger,"%s %d invalid rmq message buffer.",__FUNCTION__,__LINE__);
			break;
		}
		for (index = 0; index < arrays_sz; ++ index)
		{
			mm_memcpy(&u64_ne, (void*)(ptr + sizeof(mm_uint64_t) * index), sizeof(mm_uint64_t));
			mm_rbtset_u64_add(arrays, mm_ltoh64((mm_uint64_t)(u64_ne)));
		}
		mm_streambuf_gbump(streambuf, sz);
	} while (0);
}

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_encode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_rbtset_u64* arrays)
{
	mm_uint32_t index = 0;
	mm_uint64_t u64_ne = 0;
	mm_uint32_t size_ne = 0;
	mm_uint8_t* ptr = NULL;
	mm_uint32_t arrays_sz = (mm_uint32_t)(arrays->size);
	size_t sz = 0;
	mm_uint32_t arrays_size_offset = 0;
	sz = sizeof(mm_uint32_t) + buffer->length + sizeof(mm_uint32_t) + sizeof(mm_uint64_t) * arrays_sz;
	mm_streambuf_aligned_memory(streambuf, sz);
	size_ne = mm_htol32((mm_uint32_t)(buffer->length));
	buffer->buffer = streambuf->buff;
	buffer->offset = (mm_uint32_t)(streambuf->pptr + sizeof(mm_uint32_t));
	mm_memcpy((void*)(streambuf->buff + streambuf->pptr), &size_ne, sizeof(mm_uint32_t));
	size_ne = mm_htol32((mm_uint32_t)(arrays_sz));
	arrays_size_offset = (mm_uint32_t)(streambuf->pptr + sizeof(mm_uint32_t) + buffer->length);
	mm_memcpy((void*)(streambuf->buff + arrays_size_offset), &size_ne, sizeof(mm_uint32_t));
	ptr = (mm_uint8_t*)(streambuf->buff + arrays_size_offset + sizeof(mm_uint32_t));
	{
		struct mm_rb_node* n = NULL;
		struct mm_rbtset_u64_iterator* it = NULL;
		n = mm_rb_last(&arrays->rbt);
		while(NULL != n)
		{
			it = (struct mm_rbtset_u64_iterator*)mm_rb_entry(n, struct mm_rbtset_u64_iterator, n);
			n = mm_rb_prev(n);
			u64_ne =  mm_htol64((mm_uint64_t)it->k);
			mm_memcpy((void*)(ptr + sizeof(mm_uint64_t) * index), &u64_ne, sizeof(mm_uint64_t));
			++ index;
		}
	}
	mm_streambuf_pbump(streambuf, sz);
}
