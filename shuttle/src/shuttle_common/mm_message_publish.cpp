#include "mm_message_publish.h"
#include "core/mm_logger.h"
#include "core/mm_streambuf.h"

#include "net/mm_streambuf_packet.h"

#include "redis/mm_redis_list.h"

#include "protobuf/mm_protobuff_cxx.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_init(struct mm_message_publish* p)
{
	mm_amqp_publish_array_init(&p->db_amqp_publish);
	mm_streambuf_array_init(&p->streambuf_array);
	mm_spinlock_init(&p->locker, NULL);
	p->index = 0;
	mm_memset(p->idx_key,0,sizeof(char) * MM_MESSAGE_IDX_KEY_LENGTH);

	mm_streambuf_array_set_length(&p->streambuf_array, 1);
}
MM_EXPORT_SHUTTLE void mm_message_publish_destroy(struct mm_message_publish* p)
{
	mm_amqp_publish_array_destroy(&p->db_amqp_publish);
	mm_streambuf_array_destroy(&p->streambuf_array);
	mm_spinlock_destroy(&p->locker);
	p->index = 0;
	mm_memset(p->idx_key,0,sizeof(char) * MM_MESSAGE_IDX_KEY_LENGTH);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_lock(struct mm_message_publish* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_message_publish_unlock(struct mm_message_publish* p)
{
	mm_spinlock_unlock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_parameters(struct mm_message_publish* p, const char* parameters)
{
	mm_amqp_publish_array_assign_parameters(&p->db_amqp_publish, parameters);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_index(struct mm_message_publish* p,mm_uint32_t index)
{
	p->index = index;
	mm_sprintf(p->idx_key,MM_MESSAGE_IDX_KEY_FORMAT,p->index);
	mm_amqp_publish_array_assign_index_key(&p->db_amqp_publish, p->idx_key);
}

MM_EXPORT_SHUTTLE void mm_message_publish_assign_exchange(struct mm_message_publish* p, struct mm_amqp_exchange* exchange)
{
	mm_amqp_publish_array_assign_exchange(&p->db_amqp_publish, exchange);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_publish(struct mm_message_publish* p, struct mm_amqp_publish* publish)
{
	mm_amqp_publish_array_assign_publish(&p->db_amqp_publish, publish);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_publish_trytimes(struct mm_message_publish* p, mm_uint32_t publish_trytimes)
{
	mm_amqp_publish_array_assign_publish_trytimes(&p->db_amqp_publish, publish_trytimes);
}
//
MM_EXPORT_SHUTTLE void mm_message_publish_assign_remote(struct mm_message_publish* p, const char* node, mm_ushort_t port)
{
	mm_amqp_publish_array_assign_remote(&p->db_amqp_publish, node, port);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_vhost(struct mm_message_publish* p, const char* vhost)
{
	mm_amqp_publish_array_assign_vhost(&p->db_amqp_publish, vhost);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_account(struct mm_message_publish* p, const char* username, const char* password)
{
	mm_amqp_publish_array_assign_account(&p->db_amqp_publish, username, password);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_conn_attr(struct mm_message_publish* p, int channel_max, int frame_max, int heartbeat, amqp_channel_t channel)
{
	mm_amqp_publish_array_assign_conn_attr(&p->db_amqp_publish, channel_max, frame_max, heartbeat, channel);
}
MM_EXPORT_SHUTTLE void mm_message_publish_assign_conn_timeout(struct mm_message_publish* p, mm_msec_t conn_timeout, mm_uint32_t trytimes)
{
	mm_amqp_publish_array_assign_conn_timeout(&p->db_amqp_publish, conn_timeout, trytimes);
}

MM_EXPORT_SHUTTLE int mm_message_publish_publish(struct mm_message_publish* p, mm_uint32_t mid, mm_uint64_t sid, mm_uint64_t uid, ::google::protobuf::Message* nt_msg, struct mm_packet* nt_pack)
{
	int rt = 0;
	do 
	{
		char mid_key[MM_MESSAGE_MID_KEY_LENGTH] = { 0 };
		struct mm_packet_head packet_head;
		struct mm_streambuf_unit* streambuf_unit = NULL;
		struct mm_streambuf* streambuf_0 = NULL;
		size_t sz = 0;
		struct mm_logger* g_logger = mm_logger_instance();
		streambuf_unit = mm_streambuf_array_thread_instance(&p->streambuf_array);
		if (NULL == streambuf_unit)
		{
			mm_logger_log_E(g_logger,"%s %d system memory not enough.",__FUNCTION__,__LINE__);
			break;
		}
		mm_sprintf(mid_key,MM_MESSAGE_MID_KEY_FORMAT,mid);
		//////////////////////////////////////////////////////////////////////////
		mm_streambuf_unit_lock(streambuf_unit);
		//////////////////////////////////////////////////////////////////////////
		streambuf_0 = streambuf_unit->arrays[0];
		mm_streambuf_reset(streambuf_0);
		//////////////////////////////////////////////////////////////////////////
		packet_head.mid = mid;
		packet_head.sid = sid;
		packet_head.uid = uid;
		mm_protobuf_cxx_encode_message_protobuf(streambuf_0, nt_msg, &packet_head, MM_MSG_COMM_HEAD_SIZE, nt_pack);
		// cback packet not need crypto.
		// mm_net_tcp_crypto_encrypt(net_tcp,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
		//////////////////////////////////////////////////////////////////////////
		sz = mm_streambuf_size(streambuf_0);
		rt = mm_amqp_publish_array_publish_message(&p->db_amqp_publish,mid_key, streambuf_0->buff, (mm_uint32_t)streambuf_0->gptr, (mm_uint32_t)sz);
		//////////////////////////////////////////////////////////////////////////
		mm_streambuf_unit_unlock(streambuf_unit);
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	return rt;
}