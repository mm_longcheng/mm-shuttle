#ifndef __mm_control_tcp_hd_h__
#define __mm_control_tcp_hd_h__

#include "core/mm_core.h"

#include "net/mm_packet.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void hd_s_control_set_logger_rq(void* obj,void* u,struct mm_packet* rq_pack);
MM_EXPORT_SHUTTLE void hd_s_control_get_logger_rq(void* obj,void* u,struct mm_packet* rq_pack);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_control_tcp_hd_h__