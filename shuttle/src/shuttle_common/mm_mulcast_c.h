﻿#ifndef __mm_mulcast_c_h__
#define __mm_mulcast_c_h__

#include "core/mm_core.h"
#include "core/mm_byte.h"
#include "core/mm_streambuf.h"

#include "container/mm_vector_u64.h"
#include "container/mm_rbtset_u64.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

// mulcast_nt have a complex struct.
// mulcast message struct
// 0xFFFFFFFF is a multicast special mid.we assign at mm_packet_head pid.
// message mulcast_nt
// {
//     enum msg{ id=0xFFFFFFFF;}
//     required bytes buffer              = 1               ; // message buffer.
//     repeated uint64 arrays             = 2               ; // multi arrays.
// }

// unicast_nt just buffer.mm_packet_head can routing correct.
// anycast_nt just buffer.mm_packet_head can routing correct.

// package buffer is little endian.
// multicast message struct serialization.
// |--mm_uint32_t--|-------char*------|--mm_uint32_t--|----mm_uint64_t[]----|
// |-buffer_length-|---buffer_array---|-arrays_length-|-----arrays_array----|
// |---------------buffer-------------|---------------arrays--------------|
// buffer_length = buffer_array size not include buffer_length(uint32 size) buffer_array.size()
// arrays_length = arrays_array size not include arrays_length(uint32 size) arrays_array.size()

// cback message struct.
// |--mm_packet_head--|--mulcast_nt--|
//     unicast pid != 0xFFFFFFFF
//     anycast pid != 0xFFFFFFFF
//     mulcast pid == 0xFFFFFFFF

enum { mm_mulcast_vector_msg_id = 0xFFFFFFFF, };

struct mm_mulcast_vector
{
	struct mm_byte_buffer buffer;
	struct mm_vector_u64 arrays;
};

MM_EXPORT_SHUTTLE void mm_mulcast_vector_init( struct mm_mulcast_vector* p );
MM_EXPORT_SHUTTLE void mm_mulcast_vector_destroy( struct mm_mulcast_vector* p );

MM_EXPORT_SHUTTLE void mm_mulcast_vector_reset( struct mm_mulcast_vector* p );

MM_EXPORT_SHUTTLE void mm_mulcast_vector_decode( struct mm_mulcast_vector* p, struct mm_streambuf* streambuf );
MM_EXPORT_SHUTTLE void mm_mulcast_vector_encode( struct mm_mulcast_vector* p, struct mm_streambuf* streambuf );

MM_EXPORT_SHUTTLE void mm_mulcast_vector_decode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_vector_u64* arrays );
MM_EXPORT_SHUTTLE void mm_mulcast_vector_encode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_vector_u64* arrays );

enum { mm_mulcast_rbtset_msg_id = 0xFFFFFFFF, };

struct mm_mulcast_rbtset
{
	struct mm_byte_buffer buffer;
	struct mm_rbtset_u64 arrays;
};

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_init( struct mm_mulcast_rbtset* p );
MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_destroy( struct mm_mulcast_rbtset* p );

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_reset( struct mm_mulcast_rbtset* p );

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_decode( struct mm_mulcast_rbtset* p, struct mm_streambuf* streambuf );
MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_encode( struct mm_mulcast_rbtset* p, struct mm_streambuf* streambuf );

MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_decode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_rbtset_u64* arrays );
MM_EXPORT_SHUTTLE void mm_mulcast_rbtset_encode_buffer_arrays( struct mm_streambuf* streambuf, struct mm_byte_buffer* buffer, struct mm_rbtset_u64* arrays );

#include "core/mm_suffix.h"

#endif//__mm_mulcast_c_h__