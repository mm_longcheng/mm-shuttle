#ifndef __mm_streambuf_array_h__
#define __mm_streambuf_array_h__

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_streambuf.h"

#include "container/mm_rbtree_u32.h"
#include "container/mm_list_vpt.h"

#include <pthread.h>

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

struct mm_streambuf_unit
{
	mm_atomic_t arrays_locker;
	mm_atomic_t locker;
	mm_uint32_t length;// length. default is 0.
	struct mm_streambuf** arrays;// array pointer.
};
MM_EXPORT_SHUTTLE void mm_streambuf_unit_init(struct mm_streambuf_unit* p);
MM_EXPORT_SHUTTLE void mm_streambuf_unit_destroy(struct mm_streambuf_unit* p);
//
MM_EXPORT_SHUTTLE void mm_streambuf_unit_lock(struct mm_streambuf_unit* p);
MM_EXPORT_SHUTTLE void mm_streambuf_unit_unlock(struct mm_streambuf_unit* p);
// set_length call by manager.
MM_EXPORT_SHUTTLE void mm_streambuf_unit_set_length(struct mm_streambuf_unit* p, mm_uint32_t length);
MM_EXPORT_SHUTTLE mm_uint32_t mm_streambuf_unit_get_length(struct mm_streambuf_unit* p);
//
MM_EXPORT_SHUTTLE void mm_streambuf_unit_clear(struct mm_streambuf_unit* p);

// mm_streambuf is a large object, if we need use some thread instance,temporary variables is not good idea.
// pre-allocated thread stack data is here.
struct mm_streambuf_array
{
	struct mm_list_head list;// strong ref.
	pthread_key_t thread_key;// thread key.
	mm_atomic_t list_locker;
	mm_uint32_t length;// length. default is 0.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_streambuf_array_init(struct mm_streambuf_array* p);
MM_EXPORT_SHUTTLE void mm_streambuf_array_destroy(struct mm_streambuf_array* p);
//////////////////////////////////////////////////////////////////////////
// recommend set length before thread instance.
MM_EXPORT_SHUTTLE struct mm_streambuf_unit* mm_streambuf_array_thread_instance(struct mm_streambuf_array* p);
MM_EXPORT_SHUTTLE void mm_streambuf_array_clear(struct mm_streambuf_array* p);
// recommend call this before thread instance.
MM_EXPORT_SHUTTLE void mm_streambuf_array_set_length(struct mm_streambuf_array* p, mm_uint32_t length);
MM_EXPORT_SHUTTLE mm_uint32_t mm_streambuf_array_get_length(struct mm_streambuf_array* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_streambuf_array_h__