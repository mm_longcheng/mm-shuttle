#ifndef __mm_message_publish_h__
#define __mm_message_publish_h__

#include <google/protobuf/message.h>

#include "core/mm_core.h"
#include "core/mm_streambuf.h"
#include "core/mm_spinlock.h"

#include "net/mm_packet.h"

#include "rabbitmq/mm_amqp_conn.h"
#include "rabbitmq/mm_amqp_publish_array.h"

#include "shuttle_common/mm_streambuf_array.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

// publish event for publish_routingkey_prefix.[%08X,mid].[%04u,index]
struct mm_message_publish
{
	struct mm_amqp_publish_array db_amqp_publish;
	struct mm_streambuf_array streambuf_array;
	mm_atomic_t locker;
	mm_uint32_t index;
	char idx_key[MM_MESSAGE_IDX_KEY_LENGTH];
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_init(struct mm_message_publish* p);
MM_EXPORT_SHUTTLE void mm_message_publish_destroy(struct mm_message_publish* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_lock(struct mm_message_publish* p);
MM_EXPORT_SHUTTLE void mm_message_publish_unlock(struct mm_message_publish* p);
//////////////////////////////////////////////////////////////////////////
// 192.168.111.123-65535
MM_EXPORT_SHUTTLE void mm_message_publish_assign_parameters(struct mm_message_publish* p, const char* parameters);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_assign_index(struct mm_message_publish* p,mm_uint32_t index);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_message_publish_assign_exchange(struct mm_message_publish* p, struct mm_amqp_exchange* exchange);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_publish(struct mm_message_publish* p, struct mm_amqp_publish* publish);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_publish_trytimes(struct mm_message_publish* p, mm_uint32_t publish_trytimes);
//
MM_EXPORT_SHUTTLE void mm_message_publish_assign_remote(struct mm_message_publish* p, const char* node, mm_ushort_t port);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_vhost(struct mm_message_publish* p, const char* vhost);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_account(struct mm_message_publish* p, const char* username, const char* password);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_conn_attr(struct mm_message_publish* p, int channel_max, int frame_max, int heartbeat, amqp_channel_t channel);
MM_EXPORT_SHUTTLE void mm_message_publish_assign_conn_timeout(struct mm_message_publish* p, mm_msec_t conn_timeout, mm_uint32_t trytimes);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE int mm_message_publish_publish(struct mm_message_publish* p, mm_uint32_t mid, mm_uint64_t sid, mm_uint64_t uid, ::google::protobuf::Message* nt_msg, struct mm_packet* nt_pack);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_message_publish_h__