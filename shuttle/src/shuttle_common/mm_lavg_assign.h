#ifndef __mm_lavg_assign_h__
#define __mm_lavg_assign_h__

#include "core/mm_core.h"
#include "core/mm_spinlock.h"

#include "container/mm_rbtree_u32.h"

#include <pthread.h>

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

// here we alloc memory 100 for simulation percent.
#define MM_LAVG_ASSIGN_MEMORY 128
// here we alloc length 100 for simulation percent.
#define MM_LAVG_ASSIGN_LENGTH 100

struct mm_lavg_assign
{
	mm_uint32_t m;// length. default is 0.
	mm_uint32_t l;// length. default is 0.
	void** v;// array pointer.weak ref for elem.
	pthread_rwlock_t v_locker;
	mm_uint32_t n;
	mm_atomic_t n_locker;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_lavg_assign_init(struct mm_lavg_assign* p);
MM_EXPORT_SHUTTLE void mm_lavg_assign_destroy(struct mm_lavg_assign* p);
//////////////////////////////////////////////////////////////////////////
// must lock before set length.this api will auto mm_lavg_assign_aligned_memory.
MM_EXPORT_SHUTTLE void mm_lavg_assign_set_length(struct mm_lavg_assign* p, mm_uint32_t length);
MM_EXPORT_SHUTTLE mm_uint32_t mm_lavg_assign_get_length(struct mm_lavg_assign* p);
// must lock before set memory.
MM_EXPORT_SHUTTLE void mm_lavg_assign_add_size(struct mm_lavg_assign* p, size_t size);
MM_EXPORT_SHUTTLE void mm_lavg_assign_rmv_size(struct mm_lavg_assign* p, size_t size);

MM_EXPORT_SHUTTLE void mm_lavg_assign_try_decrease(struct mm_lavg_assign* p, size_t n);
// try increase.
MM_EXPORT_SHUTTLE void mm_lavg_assign_try_increase(struct mm_lavg_assign* p, size_t n);
// must lock before set memory.
MM_EXPORT_SHUTTLE void mm_lavg_assign_aligned_memory(struct mm_lavg_assign* p, size_t n);
//
MM_EXPORT_SHUTTLE mm_uint32_t mm_lavg_assign_get_memory(struct mm_lavg_assign* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void* mm_lavg_assign_cycle(struct mm_lavg_assign* p);
// [0,l] lavg elem is point to null.lock it at out side.
MM_EXPORT_SHUTTLE void mm_lavg_assign_cycle_empty(struct mm_lavg_assign* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_lavg_assign_h__