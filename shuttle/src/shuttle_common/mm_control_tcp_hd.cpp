#include "mm_control_tcp_hd.h"
#include "core/mm_logger_manager.h"
#include "core/mm_string.h"
#include "net/mm_packet.h"
#include "net/mm_streambuf_packet.h"
#include "net/mm_tcp.h"
#include "protodef/cxx/protodef/s_control.pb.h"
#include "protobuf/mm_protobuff_cxx.h"
#include "protobuf/mm_protobuff_cxx_net.h"
#include "shuttle_common/mm_error_code_common.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void hd_s_control_set_logger_rq(void* obj,void* u,struct mm_packet* rq_pack)
{
	s_control::set_logger_rq rq_msg;
	s_control::set_logger_rs rs_msg;
	struct mm_packet rs_pack;
	struct mm_string proto_desc;
	b_error::info* error_info = rs_msg.mutable_error();
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	////////////////////////////////
	mm_string_init(&proto_desc);
	////////////////////////////////
	error_info->set_code(err_core_success);
	error_info->set_desc("");
	rs_msg.set_level(rq_msg.level());
	////////////////////////////////
	do 
	{
		if (0 != mm_protobuf_cxx_decode_message(rq_pack, &rq_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc("package decode failure.");
			break;
		}
		// logger rq.
		mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rq_pack, &rq_msg );
		mm_logger_log_I(g_logger,"%s %d nt rq:%s",__FUNCTION__,__LINE__,proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		g_logger_manager->logger_level = rq_msg.level();
		mm_printf("%s %d logger level:%2d\n",__FUNCTION__,__LINE__,g_logger_manager->logger_level);
		//////////////////////////////////////////////////////////////////////////
		rs_msg.set_level(rq_msg.level());
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	// rs
	mm_tcp_o_lock(tcp);
	mm_protobuf_cxx_n_tcp_append_rs( mailbox, tcp, s_control::set_logger_rs_msg_id, &rs_msg, rq_pack, &rs_pack );
	mm_protobuf_cxx_n_tcp_flush_send(tcp);
	mm_tcp_o_unlock(tcp);
	// logger rs.
	mm_string_clear(&proto_desc);
	mm_protobuf_cxx_logger_append_packet_message( &proto_desc, &rs_pack, &rs_msg );
	mm_logger_log_I(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	mm_string_destroy(&proto_desc);
}
MM_EXPORT_SHUTTLE void hd_s_control_get_logger_rq(void* obj,void* u,struct mm_packet* rq_pack)
{
	s_control::get_logger_rq rq_msg;
	s_control::get_logger_rs rs_msg;
	struct mm_packet rs_pack;
	struct mm_string proto_desc;
	b_error::info* error_info = rs_msg.mutable_error();
	//
	struct mm_logger* g_logger = mm_logger_instance();
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	////////////////////////////////
	mm_string_init(&proto_desc);
	////////////////////////////////
	error_info->set_code(err_core_success);
	error_info->set_desc("");
	rs_msg.set_level(0);
	////////////////////////////////
	do 
	{
		if (0 != mm_protobuf_cxx_decode_message(rq_pack, &rq_msg))
		{
			error_info->set_code(err_common_package_decode_failure);
			error_info->set_desc("package decode failure.");
			break;
		}
		// logger rq.
		mm_protobuf_cxx_logger_append_packet_message( &proto_desc, rq_pack, &rq_msg );
		mm_logger_log_I(g_logger,"%s %d nt rq:%s",__FUNCTION__,__LINE__,proto_desc.s);
		//////////////////////////////////////////////////////////////////////////
		rs_msg.set_level(g_logger_manager->logger_level);
		mm_printf("%s %d logger level:%2d\n",__FUNCTION__,__LINE__,g_logger_manager->logger_level);
		//////////////////////////////////////////////////////////////////////////
	} while (0);
	// rs
	mm_tcp_o_lock(tcp);
	mm_protobuf_cxx_n_tcp_append_rs( mailbox, tcp, s_control::get_logger_rs_msg_id, &rs_msg, rq_pack, &rs_pack );
	mm_protobuf_cxx_n_tcp_flush_send(tcp);
	mm_tcp_o_unlock(tcp);
	// logger rs.
	mm_string_clear(&proto_desc);
	mm_protobuf_cxx_logger_append_packet_message( &proto_desc, &rs_pack, &rs_msg );
	mm_logger_log_I(g_logger,"%s %d nt rs:%s",__FUNCTION__,__LINE__,proto_desc.s);
	mm_string_destroy(&proto_desc);
}