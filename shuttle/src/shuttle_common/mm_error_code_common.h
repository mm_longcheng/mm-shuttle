﻿#ifndef __mm_error_code_common_h__
#define __mm_error_code_common_h__


#include "core/mm_core.h"

#include "dish/mm_error_desc.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
enum mm_err_common
{
	err_common_package_decode_failure = 100000001,//(100000001)解包失败
	err_common_package_encode_failure = 100000002,//(100000002)打包失败

	err_common_exec_procedure_failure = 100000101,//(100000101)执行存储过程失败
	err_common_exec_script_failure    = 100000102,//(100000102)执行脚本失败
	err_common_exec_command_failure   = 100000103,//(100000103)执行命令失败

	err_common_not_support_module     = 100000201,//(100000201)不支持的模块
	err_common_not_support_function   = 100000202,//(100000202)不支持的功能
	err_common_not_support_interface  = 100000203,//(100000203)不支持的接口
	err_common_not_support_opcode     = 100000204,//(100000204)不支持的操作码

	err_common_invalid_parameter      = 100000301,//(100000301)无效的参数
	err_common_invalid_account_id     = 100000302,//(100000302)无效的用户号

	err_common_net_db_disconnect      = 100000701,//(100000701)网络数据库断开连接
	err_common_net_server_disconnect  = 100000702,//(100000702)网络服务断开连接

	err_common_sys_not_enough_memory  = 100000801,//(100000801)系统内存不足

	err_common_sys_io_fopen_failure   = 100000901,//(100000901)系统输入输出打开文件失败
};
MM_EXPORT_SHUTTLE void mm_error_desc_assign_common(struct mm_error_desc* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_error_code_common_h__