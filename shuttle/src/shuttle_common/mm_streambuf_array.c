#include "mm_streambuf_array.h"
#include "core/mm_logger.h"

MM_EXPORT_SHUTTLE void mm_streambuf_unit_init(struct mm_streambuf_unit* p)
{
	mm_spinlock_init(&p->arrays_locker, NULL);
	mm_spinlock_init(&p->locker, NULL);
	p->length = 0;
	p->arrays = NULL;
}
MM_EXPORT_SHUTTLE void mm_streambuf_unit_destroy(struct mm_streambuf_unit* p)
{
	mm_streambuf_unit_clear(p);
	//
	mm_spinlock_destroy(&p->arrays_locker);
	mm_spinlock_destroy(&p->locker);
	p->length = 0;
	p->arrays = NULL;
}
MM_EXPORT_SHUTTLE void mm_streambuf_unit_lock(struct mm_streambuf_unit* p)
{
	mm_spinlock_lock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_streambuf_unit_unlock(struct mm_streambuf_unit* p)
{
	mm_spinlock_unlock(&p->locker);
}
MM_EXPORT_SHUTTLE void mm_streambuf_unit_set_length(struct mm_streambuf_unit* p, mm_uint32_t length)
{
	mm_spinlock_lock(&p->arrays_locker);
	if (length < p->length)
	{
		mm_uint32_t i = 0;
		struct mm_streambuf* e = NULL;
		struct mm_streambuf** v = NULL;
		for ( i = length; i < p->length; ++i)
		{
			e = p->arrays[i];
			// mm_streambuf_lock(e);
			p->arrays[i] = NULL;
			// mm_streambuf_unlock(e);
			mm_streambuf_destroy(e);
			mm_free(e);
		}
		v = (struct mm_streambuf**)mm_malloc(sizeof(struct mm_streambuf*) * length);
		mm_memcpy(v, p->arrays, sizeof(struct mm_streambuf*) * length);
		mm_free(p->arrays);
		p->arrays = v;
		p->length = length;
	}
	else if (length > p->length)
	{
		mm_uint32_t i = 0;
		struct mm_streambuf* e = NULL;
		struct mm_streambuf** v = NULL;
		v = (struct mm_streambuf**)mm_malloc(sizeof(struct mm_streambuf*) * length);
		mm_memcpy(v, p->arrays, sizeof(struct mm_streambuf*) * p->length);
		mm_free(p->arrays);
		p->arrays = v;
		for ( i = p->length; i < length; ++i)
		{
			e = (struct mm_streambuf*)mm_malloc(sizeof(struct mm_streambuf));
			mm_streambuf_init(e);
			p->arrays[i] = e;
		}
		p->length = length;
	}
	mm_spinlock_unlock(&p->arrays_locker);
}
MM_EXPORT_SHUTTLE mm_uint32_t mm_streambuf_unit_get_length(struct mm_streambuf_unit* p)
{
	return p->length;
}
MM_EXPORT_SHUTTLE void mm_streambuf_unit_clear(struct mm_streambuf_unit* p)
{
	mm_uint32_t i = 0;
	struct mm_streambuf* e = NULL;
	mm_spinlock_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		e = p->arrays[i];
		//mm_streambuf_lock(e);
		p->arrays[i] = NULL;
		//mm_streambuf_unlock(e);
		mm_streambuf_destroy(e);
		mm_free(e);
	}
	mm_free(p->arrays);
	p->arrays = NULL;
	p->length = 0;
	mm_spinlock_unlock(&p->arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_streambuf_array_init(struct mm_streambuf_array* p)
{
	MM_LIST_INIT_HEAD(&p->list);
	pthread_key_create(&p->thread_key, NULL);
	mm_spinlock_init(&p->list_locker,NULL);
	p->length = 0;
}
MM_EXPORT_SHUTTLE void mm_streambuf_array_destroy(struct mm_streambuf_array* p)
{
	mm_streambuf_array_clear(p);
	//
	MM_LIST_INIT_HEAD(&p->list);
	pthread_key_delete(p->thread_key);
	mm_spinlock_destroy(&p->list_locker);
	p->length = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE struct mm_streambuf_unit* mm_streambuf_array_thread_instance(struct mm_streambuf_array* p)
{
	struct mm_streambuf_unit* e = NULL;
	struct mm_list_vpt_iterator* lvp  = NULL;
	lvp = (struct mm_list_vpt_iterator*)pthread_getspecific(p->thread_key);
	if (!lvp)
	{
		lvp = (struct mm_list_vpt_iterator*)mm_malloc(sizeof(struct mm_list_vpt_iterator));
		mm_list_vpt_iterator_init(lvp);
		pthread_setspecific(p->thread_key, lvp);
		e = (struct mm_streambuf_unit*)mm_malloc(sizeof(struct mm_streambuf_unit));
		mm_streambuf_unit_init(e);
		mm_streambuf_unit_set_length(e, p->length);
		lvp->v = e;
		mm_spinlock_lock(&p->list_locker);
		mm_list_add(&lvp->n, &p->list);
		mm_spinlock_unlock(&p->list_locker);
	}
	else
	{
		e = (struct mm_streambuf_unit*)(lvp->v);
	}
	return e;
}
MM_EXPORT_SHUTTLE void mm_streambuf_array_clear(struct mm_streambuf_array* p)
{
    struct mm_list_head* pos = NULL;
	struct mm_list_vpt_iterator* lvp =NULL;
	struct mm_streambuf_unit* e = NULL;
	mm_spinlock_lock(&p->list_locker);
    pos = p->list.next;
    while(pos != &p->list)
    {
        struct mm_list_head* curr = pos;
        pos = pos->next;
		lvp =(struct mm_list_vpt_iterator*)mm_list_entry(curr, struct mm_list_vpt_iterator, n);
        mm_list_del(curr);
		e = (struct mm_streambuf_unit*)(lvp->v);
		mm_streambuf_unit_lock(e);
		mm_streambuf_unit_unlock(e);
		mm_streambuf_unit_destroy(e);
		mm_free(e);
		mm_list_vpt_iterator_destroy(lvp);
		mm_free(lvp);
    }
	mm_spinlock_unlock(&p->list_locker);
}
// set_length.
MM_EXPORT_SHUTTLE void mm_streambuf_array_set_length(struct mm_streambuf_array* p, mm_uint32_t length)
{
	struct mm_list_head* pos = NULL;
	struct mm_list_vpt_iterator* lvp =NULL;
	struct mm_streambuf_unit* e = NULL;
	mm_spinlock_lock(&p->list_locker);
	p->length = length;
	pos = p->list.next;
	while(pos != &p->list)
	{
		struct mm_list_head* curr = pos;
		pos = pos->next;
		lvp =(struct mm_list_vpt_iterator*)mm_list_entry(curr, struct mm_list_vpt_iterator, n);
		e = (struct mm_streambuf_unit*)(lvp->v);
		mm_streambuf_unit_lock(e);
		mm_streambuf_unit_set_length(e, p->length);
		mm_streambuf_unit_unlock(e);
	}
	mm_spinlock_unlock(&p->list_locker);
}
MM_EXPORT_SHUTTLE mm_uint32_t mm_streambuf_array_get_length(struct mm_streambuf_array* p)
{
	return p->length;
}
