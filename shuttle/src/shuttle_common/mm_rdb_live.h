#ifndef __mm_rdb_live_h__
#define __mm_rdb_live_h__

#include "core/mm_core.h"

#include "container/mm_rbtset_u64.h"
#include "container/mm_rbtree_u64.h"
#include "container/mm_rbtree_u32.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"


struct mm_redis_sync;


/*****************************************
### 根据用户号获取其所在房间服务号
```
type        :hash
key         :h_ref:uid-mid:$uid % 1024
field       :$uid
value       :$mid
```

### 根据用户号获取其所在房间号
```
type        :hash
key         :h_ref:uid-sid:$uid % 1024
field       :$uid
value       :sid
```

### 根据房间号获取其所在房间服务号
```
type        :hash
key         :h_ref:sid-mid:$sid
field       :$mid
value       :$timestamp
```

### 根据房间服务号获取其房间号集合
```
type        :hash
key         :h_ref:mid-sid:$mid
field       :$sid
value       :$timestamp
```
******************************************/
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_sid_mid(uint64_t sid, char *key, size_t len);
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_uid_mid(uint64_t uid, char *key, size_t len);
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_uid_sid(uint64_t uid, char *key, size_t len);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hget_uid_mid(struct mm_redis_sync *ctx, uint64_t uid, uint32_t *mid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hmget_uid_mid(struct mm_redis_sync *ctx, mm_rbtset_u64 *uids, mm_rbtree_u64_u32 *smids);
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_uid_mid(struct mm_redis_sync *ctx, uint64_t uid, uint32_t mid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_uid_mid(struct mm_redis_sync *ctx, uint64_t uid);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hget_uid_sid(struct mm_redis_sync *ctx, uint64_t uid, uint64_t *sid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hmget_uid_sid(struct mm_redis_sync *ctx, mm_rbtset_u64 *uids, mm_rbtree_u64_u64 *usids);
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_uid_sid(struct mm_redis_sync *ctx, uint64_t uid, uint64_t sid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_uid_sid(struct mm_redis_sync *ctx, uint64_t uid);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hgetall_sid_mids(struct mm_redis_sync *ctx, uint64_t sid, mm_rbtree_u32_u32 *mids);
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_sid_mid(struct mm_redis_sync *ctx, uint64_t sid, uint32_t mid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_sid_mid(struct mm_redis_sync *ctx, uint64_t sid, uint32_t mid);
MM_EXPORT_SHUTTLE void mm_rdb_live_del_sid_mids(struct mm_redis_sync *ctx, uint64_t sid);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hgetall_mid_sdis(struct mm_redis_sync *ctx, uint32_t mid, mm_rbtree_u32_u32 *sids);
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_mid_sid(struct mm_redis_sync *ctx, uint32_t mid, uint32_t sid);
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_mid_sid(struct mm_redis_sync *ctx, uint32_t mid, uint32_t sid);
MM_EXPORT_SHUTTLE void mm_rdb_live_del_mid_sids(struct mm_redis_sync *ctx, uint32_t mid);
#include "core/mm_suffix.h"

#endif // !__mm_rdb_live_h__
