﻿#ifndef __mm_error_code_mysql_h__
#define __mm_error_code_mysql_h__

#include "core/mm_core.h"

#include "dish/mm_error_desc.h"

#include "shuttle_common/mm_shuttle_export.h"

#include "core/mm_prefix.h"
//////////////////////////////////////////////////////////////////////////
enum mm_err_mysql
{
	err_mysql_data_insert_failure    = 200000001,//(200000001)插入数据失败
	err_mysql_data_not_exist         = 200000002,//(200000002)获取数据失败
	err_mysql_data_update_failure    = 200000003,//(200000003)数据更新失败
	err_mysql_data_delete_failure    = 200000004,//(200000004)删除数据失败
	err_mysql_data_parameter_invalid = 200000005,//(200000005)传入参数错误
};
MM_EXPORT_SHUTTLE void mm_error_desc_assign_mysql(struct mm_error_desc* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_error_code_mysql_h__