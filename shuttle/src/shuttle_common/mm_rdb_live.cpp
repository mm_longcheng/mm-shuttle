#include "mm_rdb_live.h"
#include "core/mm_string.h"
#include "core/mm_atoi.h"
#include "redis/mm_redis_sync.h"
#include "redis/mm_redis_hash.h"
#include "redis/mm_redis_common.h"

// reflect uid module id.
const char *key_format_ref_uid_mid = "h_ref:uid-mid:%d";

// reflect uid sid.
const char *key_format_ref_uid_sid = "h_ref:uid-sid:%d";

// reflect sid module id.
const char *key_format_ref_sid_mid = "h_ref:sid-mid:%d";

// reflect mid sid.
const char *key_format_ref_mid_sid = "h_ref:mid-sid:%d";

MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_uid_mid(uint64_t uid, char *key, size_t len)
{
	memset(key, 0, len);
	mm_snprintf(key, len, key_format_ref_uid_mid, uid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_uid_sid(uint64_t uid, char *key, size_t len)
{
	memset(key, 0, len);
	mm_snprintf(key, len, key_format_ref_uid_sid, uid % 1024);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_sid_mid(uint64_t sid, char *key, size_t len)
{
	memset(key, 0, len);
	mm_snprintf(key, len, key_format_ref_sid_mid, sid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_key_ref_mid_sid(uint32_t mid, char *key, size_t len)
{
	memset(key, 0, len);
	mm_snprintf(key, len, key_format_ref_mid_sid, mid);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hget_uid_mid(struct mm_redis_sync *ctx, uint64_t uid, uint32_t *mid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_mid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_u32_hget(ctx, key, uid, mid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hmget_uid_mid(struct mm_redis_sync *ctx, mm_rbtset_u64 *uids, mm_rbtree_u64_u32 *smids)
{
	mm_redis_hash_u64_u32_hmget_pattern_page(ctx, key_format_ref_uid_mid, 1024, 32, uids, smids);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_uid_mid(struct mm_redis_sync *ctx, uint64_t uid, uint32_t mid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_mid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_u32_hset(ctx, key, uid, mid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_uid_mid(struct mm_redis_sync *ctx, uint64_t uid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_mid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_hdel(ctx, key, uid);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hget_uid_sid(struct mm_redis_sync *ctx, uint64_t uid, uint64_t *sid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_sid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_u64_hget(ctx, key, uid, sid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hmget_uid_sid(struct mm_redis_sync *ctx, mm_rbtset_u64 *uids, mm_rbtree_u64_u64 *usids)
{
	mm_redis_hash_u64_u64_hmget_pattern_page(ctx, key_format_ref_uid_sid, 1024, 32, uids, usids);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_uid_sid(struct mm_redis_sync *ctx, uint64_t uid, uint64_t sid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_sid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_u64_hset(ctx, key, uid, sid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_uid_sid(struct mm_redis_sync *ctx, uint64_t uid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_uid_sid(uid % 1024, key, sizeof(key));
	mm_redis_hash_u64_hdel(ctx, key, uid);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hgetall_sid_mids(struct mm_redis_sync *ctx, uint64_t sid, mm_rbtree_u32_u32 *mids)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_sid_mid(sid, key, sizeof(key));
	mm_redis_hash_u32_u32_hgetall(ctx, key, mids);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_sid_mid(struct mm_redis_sync *ctx, uint64_t sid, uint32_t mid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_sid_mid(sid, key, sizeof(key));
	mm_redis_hash_u32_u32_hset(ctx, key, mid, (uint32_t)time(NULL));
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_sid_mid(struct mm_redis_sync *ctx, uint64_t sid, uint32_t mid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_sid_mid(sid, key, sizeof(key));
	mm_redis_hash_u32_hdel(ctx, key, mid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_del_sid_mids(struct mm_redis_sync *ctx, uint64_t sid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_sid_mid(sid, key, sizeof(key));
	mm_redis_str_del(ctx, key);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_rdb_live_hgetall_mid_sdis(struct mm_redis_sync *ctx, uint32_t mid, mm_rbtree_u32_u32 *sids)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_mid_sid(mid, key, sizeof(key));
	mm_redis_hash_u32_u32_hgetall(ctx, key, sids);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hset_mid_sid(struct mm_redis_sync *ctx, uint32_t mid, uint32_t sid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_mid_sid(mid, key, sizeof(key));
	mm_redis_hash_u32_u32_hset(ctx, key, sid, (uint32_t)time(NULL));
}
MM_EXPORT_SHUTTLE void mm_rdb_live_hdel_mid_sid(struct mm_redis_sync *ctx, uint32_t mid, uint32_t sid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_mid_sid(mid, key, sizeof(key));
	mm_redis_hash_u32_hdel(ctx, key, sid);
}
MM_EXPORT_SHUTTLE void mm_rdb_live_del_mid_sids(struct mm_redis_sync *ctx, uint32_t mid)
{
	char key[32] = {0};
	mm_rdb_live_key_ref_mid_sid(mid, key, sizeof(key));
	mm_redis_str_del(ctx, key);
}
