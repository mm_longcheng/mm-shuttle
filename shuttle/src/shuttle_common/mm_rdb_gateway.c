#include "mm_rdb_gateway.h"
#include "core/mm_logger.h"
#include "core/mm_atoi.h"

#include "redis/mm_redis_sync.h"
#include "redis/mm_redis_hash.h"

//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE const char* g_rdb_gateway_sid2add_pattern = "mm:gateway:sid2add:%04d";
// "mm:gateway:sid2add:%04d"
MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2add_key( char key[32], int index )
{
	mm_sprintf(key, g_rdb_gateway_sid2add_pattern, index);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE const char* g_rdb_gateway_sid2uid_pattern = "mm:gateway:sid2uid:%04d";
MM_EXPORT_SHUTTLE const char* g_rdb_gateway_uid2sid_pattern = "mm:gateway:uid2sid:%04d";
// "mm:gateway:sid2uid:%04d" sid % MM_SID2UID_MOD_NUMBER {k(u64 sid),v(u64 uid)}
MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2uid_key( char key[32], int index )
{
	mm_sprintf(key, g_rdb_gateway_sid2uid_pattern, index);
}
// "mm:gateway:uid2sid:%04d" uid % MM_UID2SID_MOD_NUMBER {k(u64 uid),v(u64 sid)}
MM_EXPORT_SHUTTLE void mm_rdb_gateway_uid2sid_key( char key[32], int index )
{
	mm_sprintf(key, g_rdb_gateway_uid2sid_pattern, index);
}

MM_EXPORT_SHUTTLE void mm_rdb_gateway_sid2uid_key_by_sid( char key[32], mm_uint64_t sid )
{
	mm_rdb_gateway_sid2uid_key(key, sid % MM_SID2UID_MOD_NUMBER);
}
MM_EXPORT_SHUTTLE void mm_rdb_gateway_uid2sid_key_by_uid( char key[32], mm_uint64_t uid )
{
	mm_rdb_gateway_uid2sid_key(key, uid % MM_UID2SID_MOD_NUMBER);
}
//////////////////////////////////////////////////////////////////////////