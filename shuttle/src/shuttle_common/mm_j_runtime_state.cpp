﻿#include "mm_j_runtime_state.h"

namespace mm
{
	// 
	mm_j_runtime_state::mm_j_runtime_state(mm_m_runtime_state& _rhs)
		: _data(_rhs)
	{

	}

	int mm_j_runtime_state::encode(rapidjson::Document& _jc, rapidjson::Value& _jv) const
	{
		mm_json_coder::encode(_jc, _jv, "unique_id", _data.unique_id);
		mm_json_coder::encode(_jc, _jv, "pid", _data.pid);
		mm_json_coder::encode(_jc, _jv, "node_i", _data.node_i);
		mm_json_coder::encode(_jc, _jv, "bind_i", _data.bind_i);
		mm_json_coder::encode(_jc, _jv, "port_i", _data.port_i);
		mm_json_coder::encode(_jc, _jv, "workers_i", _data.workers_i);
		mm_json_coder::encode(_jc, _jv, "node_e", _data.node_e);
		mm_json_coder::encode(_jc, _jv, "bind_e", _data.bind_e);
		mm_json_coder::encode(_jc, _jv, "port_e", _data.port_e);
		mm_json_coder::encode(_jc, _jv, "workers_e", _data.workers_e);
		mm_json_coder::encode(_jc, _jv, "lavg_01", _data.lavg_01);
		mm_json_coder::encode(_jc, _jv, "lavg_05", _data.lavg_05);
		mm_json_coder::encode(_jc, _jv, "lavg_15", _data.lavg_15);
		mm_json_coder::encode(_jc, _jv, "cpu_pro", _data.cpu_pro);
		mm_json_coder::encode(_jc, _jv, "mem_pro", _data.mem_pro);
		mm_json_coder::encode(_jc, _jv, "weights", _data.weights);
		mm_json_coder::encode(_jc, _jv, "logger_lvl", _data.logger_lvl);
		mm_json_coder::encode(_jc, _jv, "logger_dir", _data.logger_dir);
		mm_json_coder::encode(_jc, _jv, "updatetime", _data.updatetime);
		mm_json_coder::encode(_jc, _jv, "key", _data.key);
		return mm_json_coder::success;
	}

	int mm_j_runtime_state::decode(rapidjson::Document& _jc, const rapidjson::Value& _jv)
	{
		mm_json_coder::decode(_jc, _jv, "unique_id", _data.unique_id);
		mm_json_coder::decode(_jc, _jv, "pid", _data.pid);
		mm_json_coder::decode(_jc, _jv, "node_i", _data.node_i);
		mm_json_coder::decode(_jc, _jv, "bind_i", _data.bind_i);
		mm_json_coder::decode(_jc, _jv, "port_i", _data.port_i);
		mm_json_coder::decode(_jc, _jv, "workers_i", _data.workers_i);
		mm_json_coder::decode(_jc, _jv, "node_e", _data.node_e);
		mm_json_coder::decode(_jc, _jv, "bind_e", _data.bind_e);
		mm_json_coder::decode(_jc, _jv, "port_e", _data.port_e);
		mm_json_coder::decode(_jc, _jv, "workers_e", _data.workers_e);
		mm_json_coder::decode(_jc, _jv, "lavg_01", _data.lavg_01);
		mm_json_coder::decode(_jc, _jv, "lavg_05", _data.lavg_05);
		mm_json_coder::decode(_jc, _jv, "lavg_15", _data.lavg_15);
		mm_json_coder::decode(_jc, _jv, "cpu_pro", _data.cpu_pro);
		mm_json_coder::decode(_jc, _jv, "mem_pro", _data.mem_pro);
		mm_json_coder::decode(_jc, _jv, "weights", _data.weights);
		mm_json_coder::decode(_jc, _jv, "logger_lvl", _data.logger_lvl);
		mm_json_coder::decode(_jc, _jv, "logger_dir", _data.logger_dir);
		mm_json_coder::decode(_jc, _jv, "updatetime", _data.updatetime);
		mm_json_coder::decode(_jc, _jv, "key", _data.key);
		return mm_json_coder::success;
	}
	// 
	mm_j_config_module::mm_j_config_module(mm_m_config_module& _rhs)
		: _data(_rhs)
	{

	}

	int mm_j_config_module::encode(rapidjson::Document& _jc, rapidjson::Value& _jv) const
	{
		mm_json_coder::encode(_jc, _jv, "unique_id", _data.unique_id);
		mm_json_coder::encode(_jc, _jv, "mid_l", _data.mid_l);
		mm_json_coder::encode(_jc, _jv, "mid_r", _data.mid_r);
		mm_json_coder::encode(_jc, _jv, "shard_size", _data.shard_size);
		mm_json_coder::encode(_jc, _jv, "depth_size", _data.depth_size);
		mm_json_coder::encode(_jc, _jv, "desc", _data.desc);
		return mm_json_coder::success;
	}

	int mm_j_config_module::decode(rapidjson::Document& _jc, const rapidjson::Value& _jv)
	{
		mm_json_coder::decode(_jc, _jv, "unique_id", _data.unique_id);
		mm_json_coder::decode(_jc, _jv, "mid_l", _data.mid_l);
		mm_json_coder::decode(_jc, _jv, "mid_r", _data.mid_r);
		mm_json_coder::decode(_jc, _jv, "shard_size", _data.shard_size);
		mm_json_coder::decode(_jc, _jv, "depth_size", _data.depth_size);
		mm_json_coder::decode(_jc, _jv, "desc", _data.desc);
		return mm_json_coder::success;
	}
}