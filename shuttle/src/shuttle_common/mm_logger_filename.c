﻿#include "mm_logger_filename.h"
#include "core/mm_string.h"
#include "dish/mm_file_path.h"

MM_EXPORT_SHUTTLE void mm_logger_manager_assign_logger_filename(struct mm_logger_manager* logger_manager, const char* application_name, const char* unique_id_string)
{
	struct mm_string logger_filename;
	struct mm_string basename;
	struct mm_string pathname;

	mm_string_init(&logger_filename);
	mm_string_init(&basename);
	mm_string_init(&pathname);

	mm_string_assigns(&logger_filename, application_name);
	mm_path_split_filename(&logger_filename, &basename, &pathname);
	mm_string_assigns(&logger_filename, basename.s);
	mm_path_split_suffixname(&logger_filename, &basename, &pathname);
	mm_string_assigns(&logger_filename, basename.s);

	mm_string_append(&logger_filename, "_");
	mm_string_append(&logger_filename, unique_id_string);
	mm_logger_manager_assign_file_name(logger_manager, logger_filename.s);

	mm_string_destroy(&logger_filename);
	mm_string_destroy(&basename);
	mm_string_destroy(&pathname);
}