﻿#include "mm_modules_runtime.h"

#include "core/mm_logger_manager.h"
#include "core/mm_os_context.h"

#include "net/mm_sockaddr.h"

#include "shuttle_common/mm_runtime_calculate.h"
#include "shuttle_common/mm_j_runtime_state.h"

MM_EXPORT_SHUTTLE void mm_modules_runtime_init(struct mm_modules_runtime* p)
{
	mm_runtime_stat_init(&p->runtime);
	mm_zkwp_path_init(&p->zkwp_path);
	p->internal_mailbox = NULL;
	p->external_mailbox = NULL;
	// first update.
	mm_runtime_stat_update(&p->runtime);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_destroy(struct mm_modules_runtime* p)
{
	mm_runtime_stat_destroy(&p->runtime);
	mm_zkwp_path_destroy(&p->zkwp_path);
	p->internal_mailbox = NULL;
	p->external_mailbox = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_start(struct mm_modules_runtime* p)
{
	mm_zkwp_path_start(&p->zkwp_path);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_interrupt(struct mm_modules_runtime* p)
{
	mm_zkwp_path_interrupt(&p->zkwp_path);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_shutdown(struct mm_modules_runtime* p)
{
	mm_zkwp_path_shutdown(&p->zkwp_path);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_join(struct mm_modules_runtime* p)
{
	mm_zkwp_path_join(&p->zkwp_path);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_unique_id(struct mm_modules_runtime* p,mm_uint32_t unique_id)
{
	mm_zkwp_path_assign_unique_id(&p->zkwp_path,unique_id);
	p->runtime_state.unique_id = unique_id;
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_internal_mailbox(struct mm_modules_runtime* p,struct mm_mailbox* internal_mailbox)
{
	p->internal_mailbox = internal_mailbox;
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_external_mailbox(struct mm_modules_runtime* p,struct mm_mailbox* external_mailbox)
{
	p->external_mailbox = external_mailbox;
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_path(struct mm_modules_runtime* p,const char* path)
{
	mm_zkwp_path_assign_path(&p->zkwp_path,path);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_host(struct mm_modules_runtime* p,const char* host)
{
	mm_zkwp_path_assign_host(&p->zkwp_path,host);
}
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_slot(struct mm_modules_runtime* p,mm_uint32_t shard,mm_uint32_t depth)
{
	mm_zkwp_path_assign_slot(&p->zkwp_path,shard,depth);
}
// update runtime value.
MM_EXPORT_SHUTTLE void mm_modules_runtime_update_runtime(struct mm_modules_runtime* p)
{
	char node[MM_NODE_NAME_LENGTH] = {0};
	mm_ushort_t port = 0;
	if ( NULL != p->internal_mailbox )
	{
		mm_sockaddr_node_port( &p->internal_mailbox->accepter.socket.ss_native, node, &port );
		p->runtime_state.node_i = node;
		p->runtime_state.bind_i = node;
		p->runtime_state.port_i = port;
		p->runtime_state.workers_i = mm_mailbox_get_length(p->internal_mailbox);
	}
	else
	{
		p->runtime_state.node_i = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.bind_i = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.port_i = MM_ADDR_DEFAULT_PORT;
		p->runtime_state.workers_i = 0;
	}
	if ( NULL != p->external_mailbox )
	{
		mm_sockaddr_node_port( &p->external_mailbox->accepter.socket.ss_native, node, &port );
		p->runtime_state.node_e = node;
		p->runtime_state.bind_e = node;
		p->runtime_state.port_e = port;
		p->runtime_state.workers_e = mm_mailbox_get_length(p->external_mailbox);
	}
	else
	{
		p->runtime_state.node_e = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.bind_e = MM_ADDR_DEFAULT_NODE;
		p->runtime_state.port_e = MM_ADDR_DEFAULT_PORT;
		p->runtime_state.workers_e = 0;
	}
	{
		// update the machine information.
		mm_runtime_stat_update(&p->runtime);
		mm_runtime_state_update(&p->runtime_state, &p->runtime);
	}
	{
		// update zookeeper.
		rapidjson::StringBuffer string_buffer;
		rapidjson::Document _jc(rapidjson::kObjectType);
		rapidjson::Writer<rapidjson::StringBuffer> _writer(string_buffer);
		mm::mm_j_runtime_state j_runtime_state(p->runtime_state);
		j_runtime_state.encode(_jc,_jc);
		string_buffer.Clear();
		_writer.Reset(string_buffer);
		_jc.Accept(_writer);
		mm_string_assigns(&p->zkwp_path.value_buffer,string_buffer.GetString());
	}
}
// commit db.
MM_EXPORT_SHUTTLE void mm_modules_runtime_commit_db(struct mm_modules_runtime* p)
{
	// common runtime.
}
// commit zk.
MM_EXPORT_SHUTTLE void mm_modules_runtime_commit_zk(struct mm_modules_runtime* p)
{
	mm_zkwp_path_commit(&p->zkwp_path);
}

MM_EXPORT_SHUTTLE void mm_modules_runtime_module_path(mm_uint32_t module, char path[64])
{
	mm_sprintf(path,"/mm_logic_%03u",module);
}
