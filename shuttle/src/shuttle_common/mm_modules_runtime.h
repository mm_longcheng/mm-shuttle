#ifndef __mm_modules_runtime_h__
#define __mm_modules_runtime_h__

#include "core/mm_core.h"
#include "core/mm_runtime_stat.h"

#include "net/mm_mailbox.h"

#include "dish/mm_m_runtime_state.h"

#include "zookeeper/mm_zookeeper_zkwp_path.h"

#include "shuttle_common/mm_shuttle_export.h"

//////////////////////////////////////////////////////////////////////////
struct mm_modules_runtime
{
	struct mm_runtime_stat runtime;

	mm::mm_m_runtime_state runtime_state;

	struct mm_zkwp_path zkwp_path;

	struct mm_mailbox* internal_mailbox;// weak ref.
	struct mm_mailbox* external_mailbox;// weak ref.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_init(struct mm_modules_runtime* p);
MM_EXPORT_SHUTTLE void mm_modules_runtime_destroy(struct mm_modules_runtime* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_start(struct mm_modules_runtime* p);
MM_EXPORT_SHUTTLE void mm_modules_runtime_interrupt(struct mm_modules_runtime* p);
MM_EXPORT_SHUTTLE void mm_modules_runtime_shutdown(struct mm_modules_runtime* p);
MM_EXPORT_SHUTTLE void mm_modules_runtime_join(struct mm_modules_runtime* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_unique_id(struct mm_modules_runtime* p,mm_uint32_t unique_id);
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_internal_mailbox(struct mm_modules_runtime* p,struct mm_mailbox* internal_mailbox);
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_external_mailbox(struct mm_modules_runtime* p,struct mm_mailbox* external_mailbox);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_path(struct mm_modules_runtime* p,const char* path);
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_host(struct mm_modules_runtime* p,const char* host);
MM_EXPORT_SHUTTLE void mm_modules_runtime_assign_zkwp_slot(struct mm_modules_runtime* p,mm_uint32_t shard,mm_uint32_t depth);
//////////////////////////////////////////////////////////////////////////
// update runtime value.
MM_EXPORT_SHUTTLE void mm_modules_runtime_update_runtime(struct mm_modules_runtime* p);
// commit db.
MM_EXPORT_SHUTTLE void mm_modules_runtime_commit_db(struct mm_modules_runtime* p);
// commit zk.
MM_EXPORT_SHUTTLE void mm_modules_runtime_commit_zk(struct mm_modules_runtime* p);
//////////////////////////////////////////////////////////////////////////
// /mm_logic_%03u
MM_EXPORT_SHUTTLE void mm_modules_runtime_module_path(mm_uint32_t module, char path[64]);
//////////////////////////////////////////////////////////////////////////

#endif//__mm_modules_runtime_h__