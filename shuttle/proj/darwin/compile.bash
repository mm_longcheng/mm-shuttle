make -f makefile_shuttle_protodef_static $@;
make -f makefile_shuttle_protodef_shared $@;
make -f makefile_shuttle_common_static $@;
make -f makefile_shuttle_common_shared $@;
make -f makefile_shuttle_cback $@;
make -f makefile_shuttle_entry $@;
make -f makefile_shuttle_lobby $@;
make -f makefile_shuttle_proxy $@;

make -f makefile_shuttle_control $@;
make -f makefile_shuttle_password $@;

make -f makefile_handler_lobby $@;
