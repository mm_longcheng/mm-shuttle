@echo off 

set VSVARS="%VS141COMNTOOLS%vcvars64.bat"
set VC_VER=141

call %VSVARS%

devenv %~dp0/mm_shuttle.sln /build "Debug|x64"
devenv %~dp0/mm_shuttle.sln /build "Release|x64"

pause