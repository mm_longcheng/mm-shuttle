﻿#include "mm_application.h"

#include "core/mm_time.h"

#include "AL/al.h"
#include "AL/alc.h"

static const char* g_argument_impl[] = 
{
	"./mm_audio",
	"./log",
	"7",
	"0",
};
const struct mm_argument g_argument = { MM_ARRAY_SIZE(g_argument_impl),(const char**)g_argument_impl, };
/////////////////////////////////////////////////////////////////
static struct mm_application g_application;
struct mm_application* mm_application_instance()
{
	return &g_application;
}
/////////////////////////////////////////////////////////////////
#define MAX_WIDTH  80
static void printList(const char *list, char separator)
{
	size_t col = MAX_WIDTH, len;
	const char *indent = "    ";
	const char *next;

	if(!list || *list == '\0')
	{
		fprintf(stdout, "\n%s!!! none !!!\n", indent);
		return;
	}

	do {
		next = strchr(list, separator);
		if(next)
		{
			len = next-list;
			do {
				next++;
			} while(*next == separator);
		}
		else
			len = strlen(list);

		if(len + col + 2 >= MAX_WIDTH)
		{
			fprintf(stdout, "\n%s", indent);
			col = strlen(indent);
		}
		else
		{
			fputc(' ', stdout);
			col++;
		}

		len = fwrite(list, 1, len, stdout);
		col += len;

		if(!next || *next == '\0')
			break;
		fputc(',', stdout);
		col++;

		list = next;
	} while(1);
	fputc('\n', stdout);
}

static void printDeviceList(const char *list)
{
	if(!list || *list == '\0')
		printf("    !!! none !!!\n");
	else do {
		printf("    %s\n", list);
		list += strlen(list) + 1;
	} while(*list != '\0');
}


static ALenum checkALErrors(int linenum)
{
	ALenum err = alGetError();
	if(err != AL_NO_ERROR)
		printf("OpenAL Error: %s (0x%x), @ %d\n", alGetString(err), err, linenum);
	return err;
}
#define checkALErrors() checkALErrors(__LINE__)

static ALCenum checkALCErrors(ALCdevice *device, int linenum)
{
	ALCenum err = alcGetError(device);
	if(err != ALC_NO_ERROR)
		printf("ALC Error: %s (0x%x), @ %d\n", alcGetString(device, err), err, linenum);
	return err;
}
#define checkALCErrors(x) checkALCErrors((x),__LINE__)

static void printALCInfo(ALCdevice *device)
{
	ALCint major, minor;

	if(device)
	{
		const ALCchar *devname = NULL;
		printf("\n");
		if(alcIsExtensionPresent(device, "ALC_ENUMERATE_ALL_EXT") != AL_FALSE)
			devname = alcGetString(device, ALC_ALL_DEVICES_SPECIFIER);
		if(checkALCErrors(device) != ALC_NO_ERROR || !devname)
			devname = alcGetString(device, ALC_DEVICE_SPECIFIER);
		printf("** Info for device \"%s\" **\n", devname);
	}
	alcGetIntegerv(device, ALC_MAJOR_VERSION, 1, &major);
	alcGetIntegerv(device, ALC_MINOR_VERSION, 1, &minor);
	if(checkALCErrors(device) == ALC_NO_ERROR)
		printf("ALC version: %d.%d\n", major, minor);
	if(device)
	{
		printf("ALC extensions:");
		printList(alcGetString(device, ALC_EXTENSIONS), ' ');
		checkALCErrors(device);
	}
}

void mm_application_init(struct mm_application* p)
{

}
void mm_application_destroy(struct mm_application* p)
{

}
void mm_application_initialize(struct mm_application* p,int argc,const char **argv)
{

}
#define NUM_BUFFERS 4

void mm_application_start(struct mm_application* p)
{
	const ALCchar* v = "";
	v = alcGetString(NULL,ALC_DEVICE_SPECIFIER);
	mm_printf("ALC_DEVICE_SPECIFIER\n");
	printDeviceList(v);
	v = alcGetString(NULL,ALC_DEFAULT_DEVICE_SPECIFIER);
	mm_printf("ALC_DEFAULT_DEVICE_SPECIFIER\n");
	printDeviceList(v);
	v = alcGetString(NULL,ALC_CAPTURE_DEVICE_SPECIFIER);
	mm_printf("ALC_CAPTURE_DEVICE_SPECIFIER\n");
	printDeviceList(v);
	v = alcGetString(NULL,ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER);
	mm_printf("ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER\n");
	printf("    %s\n", v);
	v = alcGetString(NULL,ALC_ALL_DEVICES_SPECIFIER);
	mm_printf("ALC_ALL_DEVICES_SPECIFIER\n");
	printDeviceList(v);

	ALboolean enumeration = AL_FALSE;

	enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
	if (enumeration == AL_FALSE) 
	{
		mm_printf("dosn't support ALC_ENUMERATION_EXT\n");
		return;
	}
	ALCdevice* device_encode = NULL;
	ALCdevice* device_decode = NULL;

	ALCcontext* device_decode_context = NULL;

	ALuint buffers;
	ALuint source;

	// 8000,AL_FORMAT_MONO16,960
	ALCuint frequency = 8000;
	ALCenum format = AL_FORMAT_MONO16;
	ALCsizei buffersize = 960;

	unsigned char audio_data[1920] = {0};

	{
		v = alcGetString(NULL,ALC_DEVICE_SPECIFIER);
		device_decode = alcOpenDevice(v);

		device_decode_context = alcCreateContext(device_decode, NULL);
		if ( NULL == device_decode_context ) 
		{
			mm_printf("can not create context\n");
			return;
		}
		if( AL_FALSE == alcMakeContextCurrent(device_decode_context) ) 
		{
			mm_printf("can not make context current\n");
			return;
		}
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}

		alGenBuffers(1, &buffers); // only one buffer
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}
		alGenSources(1, &source);
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}

		//
		//源声音的位置.  
		static ALfloat SourcePos[] = {0.0f, 0.0f, 0.0f};  
		//源声音的速度.  
		static ALfloat SourceVel[] = {0.0f, 0.0f, 0.1f};  
		//听者的位置.  
		static ALfloat ListenerPos[] = {0.0f, 0.0f, 0.0f};  
		//听者的速度  
		static ALfloat ListenerVel[] = {0.0f, 0.0f, 0.0f};  
		//听者的方向 (first 3 elements are "at", second 3 are "up")  
		static ALfloat ListenerOri[] = {0.0f, 0.0f, -1.0f,   0.0f, 1.0f, 0.0f};

		alSourcef(source, AL_PITCH, 1.0f);  
		alSourcef(source, AL_GAIN, 1.0f);  
		alSourcefv(source, AL_POSITION, SourcePos);  
		alSourcefv(source, AL_VELOCITY, SourceVel);  
		alSourcei(source, AL_LOOPING, AL_FALSE);  
		alListenerfv(AL_POSITION, ListenerPos);   
		alListenerfv(AL_VELOCITY, ListenerVel);   
		alListenerfv(AL_ORIENTATION, ListenerOri);  

		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}
		//
		alBufferData(buffers, AL_FORMAT_MONO16, audio_data, 1920, frequency);
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}
		alSourcei(source, AL_BUFFER, buffers);
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}
		alSourcePlay(source);
		if ( AL_NO_ERROR != checkALErrors() ) 
		{
			return;
		}
	}
	//{
	//	v = alcGetString(NULL,ALC_DEVICE_SPECIFIER);
	//	device_encode = alcOpenDevice(v);
	//	if(!device_encode)
	//	{
	//		printf("\n!!! Failed to open %s !!!\n", v);
	//	}
	//	else
	//	{
	//		printALCInfo(device_encode);
	//		alcCloseDevice(device_encode);
	//	}
	//}
	{
		v = alcGetString(NULL,ALC_CAPTURE_DEVICE_SPECIFIER);
		//***首先这里是用来初始化捕捉设备用的，参数NULL表示的是获取当前设备中默认的可用的捕捉工具，SAMPLING_RATE就是之前提到的音频采样率，
		//	这里我设置的是8000， 该参数与后一个一参数AL_FORMAT_MONO16一同用来决定你是否能够成功采集到样本。
		//	一般采样率在我的另一篇文章openAL-音频播放中讲到，这里不再重复提，一般常用采样率有8000，22050和44100(HZ)三种。
		//	AL_FORMAT_MONO16，表示单声道16位，在openAL中提供了 四种声道选择 AL_FORMAT_MONO16,AL_FORMAT_MONO8,AL_FORAMT_STEREO16 和AL_FORMAT_STEREO8,
		//	分别为 单声道16bit，单声道8bit和双声道16bit与双声道8bit。 关于采样率与声道选择与程序员使用的设备有关系也与当前pcm数据的采集频率有关，
		//	所以有些人在网上复制粘贴别人代码的时候，同样的代码自己却捕捉不到音频样本的原因之一就在于此。 最后一个参数是我自己定义的设备每次采集320个样本。
		//	采集音频数据是在一个while 死循环中执行，为了避免线程阻塞，最好放在子线程中处理。
		device_encode = alcCaptureOpenDevice(v, frequency, format, buffersize);
		if(!device_encode)
		{
			printf("\n!!! Failed to open %s !!!\n", v);
		}
		else
		{
			printALCInfo(device_encode);

			ALint samples = 0;
			// start
			alcCaptureStart(device_encode);  
			alDistanceModel(AL_NONE);
			//
			do 
			{
				{
					ALint processed, state;

					/* Get relevant source info */
					alGetSourcei(source, AL_SOURCE_STATE, &state);
					alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);
					if(alGetError() != AL_NO_ERROR)
					{
						mm_printf("Error checking source state\n");
					}
					/* Unqueue and handle each processed buffer */
					while(processed > 0)
					{
						ALuint bufid;
						size_t got;

						alSourceUnqueueBuffers(source, 1, &bufid);
						processed--;

						/* Read the next chunk of data, refill the buffer, and queue it
						 * back on the source */
						got = 1;// readAVAudioData(player->stream, player->data, player->datasize);
						if(got > 0)
						{
							alBufferData(buffers, AL_FORMAT_MONO16, audio_data, 1920, frequency);
							//alBufferSamplesSOFT(bufid, player->rate, player->format,
							//					BytesToFrames(got, player->channels, player->type),
							//					player->channels, player->type, player->data);
							alSourceQueueBuffers(source, 1, &bufid);
						}
						if(alGetError() != AL_NO_ERROR)
						{
							mm_printf("Error buffering data\n");
						}
					}

					/* Make sure the source hasn't underrun */
					if(state != AL_PLAYING && state != AL_PAUSED)
					{
						ALint queued;

						/* If no buffers are queued, playback is finished */
						alGetSourcei(source, AL_BUFFERS_QUEUED, &queued);
						if(queued == 0)
						{
							mm_printf("queued is 0\n");
						}

						alSourcePlay(source);
						if(alGetError() != AL_NO_ERROR)
						{
							mm_printf("Error restarting playback\n");
							fprintf(stdout, "Error buffering data\n");
						}
					}
				}
				//
				//***通过函数alcGetIntegerv可以得到当前设备采集到的样本数量，存放在sample中，在这里因为是个while的死循环，
				//	所以，sample的值会越来越多，会无止境的增加吗？ 不会！还记得之前初始化捕捉设备时候的最后一个参数么，
				//	没错，那个参数就决定了你sample的最大值。当sample等于设备每次采集的样本数量的最大值时，这些样本数据将会被设备丢弃掉，
				//	而我们所要做的就是要将每一个样本数据收集之后进行处理。然而，在用一个数组来存储这些样本的时候就会遇到另一个问题。
				//	我在网上看到不少例子都是在这里创建了一个与sample 值相等长度的数组来装这些数据，不知道他们这样做了之后，能正常的听到声音么？
				//	我看不能吧。pcm数据的长度在这里实际上为sample大小的两倍，那么只用sample大小的数组装的话程序就不会直接报错么？反正我这里是报错了。
				//	pcm源数据在未进行编码的时候，长度为实际长度的两倍，经过编码后，长度才会缩到原来的一半，这也是为什么pcm数据在经过解码后，
				//	需要用一个长度为源pcm数据两倍的数组来装，两个过程正好是相反的。所以，在这里，就应该使用长度为两倍sample的数组才可以。
				alcGetIntegerv(device_encode, ALC_CAPTURE_SAMPLES, (ALCsizei)sizeof(ALshort), &samples);
				if( samples >= buffersize  )
				{ 
					alcCaptureSamples(device_encode, (ALCvoid *)audio_data, samples);
					// handler buffer.
					mm_printf("handler buffer samples:%u\n", samples);
				}
				else
				{
					mm_msleep(100);
				}
			} while (1);
		}
	}
	if (0)
	{
		alBufferData(buffers, AL_FORMAT_MONO16, audio_data, 1920, frequency);

		alSourceQueueBuffers(source, 1, &buffers);
		alSourceUnqueueBuffers(source, 1, &buffers);
	}

	alSourceStop(source);
	if ( AL_NO_ERROR != checkALErrors() ) 
	{
		return;
	}

	//
	alcCaptureStop(device_encode);
	//
	alcCaptureCloseDevice(device_encode);
	//
	alDeleteSources(1, &source);
	alDeleteBuffers(1, &buffers);
	if ( AL_NO_ERROR != checkALErrors() ) 
	{
		return;
	}
	//
	alcMakeContextCurrent(NULL);
	alcDestroyContext(device_decode_context);
	alcCloseDevice(device_decode);
}
void mm_application_interrupt(struct mm_application* p)
{

}
void mm_application_shutdown(struct mm_application* p)
{

}
void mm_application_join(struct mm_application* p)
{

}